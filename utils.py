import os
import pickle
from constants import *
import seaborn as sns
import numpy as np

def load_decoding_data(settings_name, experiment_name,
                       area_spikes='V1', dataset='modid'):

    # dataset = 'ChangeDetectionConflict'
    # experiment_name = 'audiofreq_in_big_change'
    # area_spikes = 'V1'

    data_path = os.path.join(DATA_PATH, 'ephys_data', dataset)
    file_name = 'decoding_data_{}_{}_{}.pkl'.format(
        settings_name,
        experiment_name,
        area_spikes)

    data = pickle.load(open(os.path.join(data_path, file_name), 'rb'))
    return data


def log2_likelihood(true, predictions) :
    y_true = true
    y_pred = predictions[:, 1]
    eps = np.finfo(y_pred.dtype).eps
    y_pred = np.clip(y_pred, eps, 1 - eps)
    terms = y_true * np.log2(y_pred) + (1 - y_true) * np.log2(1 - y_pred)
    ll = np.sum(terms) / len(y_true)
    return ll

dataset_palette = {'ChangeDetectionConflict'  : sns.xkcd_rgb['darkish purple'],
                   'ChangeDetectionConflictDecor' : sns.xkcd_rgb['emerald'],
                   'VisOnlyTwolevels' : sns.xkcd_rgb['bright orange']}



