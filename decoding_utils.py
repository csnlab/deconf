import os
import pickle
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
from constants import *
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
try:
    from mlconfound.stats import full_confound_test, partial_confound_test
except ModuleNotFoundError:
    print('Spisak tests not installed')

try:
    from confound_prediction.deconfounding import confound_isolating_cv
except ModuleNotFoundError:
    print('Confound isolating cv not installed')

from sklearn.model_selection import cross_val_predict
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from sklearn.linear_model import LinearRegression, RidgeCV
from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from sklearn.linear_model import RidgeClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis, QuadraticDiscriminantAnalysis
import random
from sklearn.preprocessing import minmax_scale
from sklearn.preprocessing import scale
from utils import log2_likelihood
from sklearn.svm import SVC
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import balanced_accuracy_score, make_scorer
from sklearn.multioutput import MultiOutputRegressor
from sklearn.svm import SVR



class NotEnoughTrialsError(ValueError): pass


def posthoc_counterbalancing(X, y, C, distribution_match_binsize=None,
                             n_bins=None,
                             already_binned=False,
                             return_full_confound=True, seed=None):

    if seed is not None:
        np.random.seed(seed)

    if C.shape[1] > 1:
        # if more than one confound, we only look at first
        # could look at the sum as well
        print('Confound has more than 1 dimension, matching '
              'only the first dimension')
        C_full = C.copy()
        C = C[:, 0].reshape(-1, 1)

    if distribution_match_binsize is None:
        if n_bins is None:
            mean_diff = np.diff(np.sort(C, axis=0), axis=0).mean().round()
            distribution_match_binsize = mean_diff * 3
        else:
            print('Binning confound in {} bins'.format(n_bins))
            distribution_match_binsize = (C.max() - C.min()) / n_bins

    if already_binned:

        bins = np.unique(C)
        indx = np.arange(y.shape[0])

        indx_remove_all = []
        for bin in bins:

            indx_bin = np.where(C==bin)[0]
            indx_bin_0 = [ix for ix in indx_bin if y[ix] == 0]
            indx_bin_1 = [ix for ix in indx_bin if y[ix] == 1]

            y_bin = y[indx_bin]
            n0 = len(y_bin[y_bin == 0])
            n1 = len(y_bin[y_bin == 1])

            if n0 > n1 :
                n_remove = n0 - n1
                indx_remove = np.random.choice(indx_bin_0, n_remove,
                                               replace=False)
                indx_remove_all.append(indx_remove)

            elif n1 > n0 :
                n_remove = n1 - n0
                indx_remove = np.random.choice(indx_bin_1, n_remove,
                                               replace=False)
                indx_remove_all.append(indx_remove)

    else:
        bins = np.arange(C.min(), C.max() + 0.1, distribution_match_binsize)
        print('N bins: {}'.format(len(bins)-1))
        #print(bins)
        indx = np.arange(y.shape[0])

        indx_remove_all = []
        for j in range(bins.shape[0] - 1) :

            indx_bin = np.where(np.logical_and(C.flatten() >= bins[j],
                                               C.flatten() < bins[j + 1]))[0]
            indx_bin_0 = [ix for ix in indx_bin if y[ix] == 0]
            indx_bin_1 = [ix for ix in indx_bin if y[ix] == 1]

            y_bin = y[indx_bin]
            n0 = len(y_bin[y_bin == 0])
            n1 = len(y_bin[y_bin == 1])

            if n0 > n1 :
                n_remove = n0 - n1
                indx_remove = np.random.choice(indx_bin_0, n_remove, replace=False)
                indx_remove_all.append(indx_remove)

            elif n1 > n0 :
                n_remove = n1 - n0
                indx_remove = np.random.choice(indx_bin_1, n_remove, replace=False)
                indx_remove_all.append(indx_remove)
        #print(np.hstack(indx_remove_all))
    if len(indx_remove_all) > 0 :
        indx_remove_all = np.hstack(indx_remove_all)
        indx_keep = [ix for ix in indx if ix not in indx_remove_all]

        X_bal = X[indx_keep].copy()
        y_bal = y[indx_keep].copy()
        C_bal = C[indx_keep].copy()
        if False :
            # check the distribution to make sure you have successfully
            # balanced the 0 and 1 label within each confound bin
            f, ax = plt.subplots(1, 2)
            ax[0].hist(C[y == 0], color='green', alpha=0.5, bins=bins)
            ax[0].hist(C[y == 1], color='red', alpha=0.5, bins=bins)

            ax[1].hist(C_bal[y_bal == 0], color='green', alpha=0.5,
                       bins=bins)
            ax[1].hist(C_bal[y_bal == 1], color='red', alpha=0.5,
                       bins=bins)

        if return_full_confound:
            C_bal = C_full[indx_keep, :].copy()

        #print('Rebalancing')
        X = X_bal
        y = y_bal
        C = C_bal
    else:
        print('No samples to rebalance')
        raise ValueError

    return X, y, C




# TODO CURRENT VERSION
def decode_alt(X, C, y,
               deconf_method,
               decoder,
               distribution_match_binsize=None,
               n_bins=None,
               already_binned=False,
               n_kfold_splits=3, min_trials=20,
               run_spisak_partial_test=False,
               run_spisak_full_test=False,
               num_perms_spisak=1000,
               standardize_features=True,
               standardize_confound=True,
               linear_regression_regularization=False,
               tune_hyperpars_decoder=False,
               lr_penalty='l1', lr_solver='saga', C_reg=0.2,
               tune_regularization_predictability=False,
               Cs=None, max_iter=7000, tune_C_nfolds=3,
               n_estimators=50, max_depth=3,
               min_samples_split=2, min_samples_leaf=7,
               max_features='sqrt'):
    # print(np.diff(y.index).max())

    methods = ['raw',
               'decode_from_confound',
               'predictability_lin',
               'predictability_nonlin',
               'predictability_nonlin_ensemble',
               'predictability_lin_nodeconf',
               'predictability_nonlin_nodeconf',
               'predictability_nonlin_ensemble_nodeconf'
               'distribution_match',
               'confound_isolating_cv',
               'linear_confound_regression',
               'nonlinear_confound_regression']

    assert np.isin(deconf_method, methods)

    decoders = ['logistic_regression', 'random_forest',
                'linear_discriminant_analysis', 'quadratic_discriminant_analysis',
                'ridge_regression', 'SVC', 'none']
    assert np.isin(decoder, decoders)


    total_n_trials = X.shape[0]
    if total_n_trials < 20 :
        raise NotEnoughTrialsError('Not enough trials to decode')


    # methods that influence the data distribution
    if deconf_method == 'distribution_match' :

        X, y, C = posthoc_counterbalancing(X, y, C, distribution_match_binsize,
                                           n_bins=n_bins,
                                           already_binned=already_binned)
        total_n_trials = X.shape[0]
        print('N trials after counterbalancing: {}'.format(X.shape[0]))

        kfold = StratifiedKFold(n_splits=n_kfold_splits, shuffle=True,
                                random_state=92)
        iterable = kfold.split(X, y)


    elif deconf_method == 'confound_isolating_cv' :

        if C.shape[1] > 1 :
            # if more than one confound, we only look at first
            # could look at the sum as well
            print('Confound has more than 1 dimension, matching '
                  'only the first dimension')
            C = C[:, 0].reshape(-1, 1)


        _, _, _, _, ids_test, ids_train = confound_isolating_cv(X, y, C[:, 0],
                                                                    random_seed=92,
                                                                    min_sample_size=None,
                                                                    cv_folds=n_kfold_splits,
                                                                    n_remove=None)

        total_n_trials = np.unique(np.hstack([ids_test, ids_train])).shape[0]

        iterable = zip(ids_train, ids_test)

    else:
        # normal data
        total_n_trials = X.shape[0]

        kfold = StratifiedKFold(n_splits=n_kfold_splits, shuffle=True,
                                random_state=92)
        iterable = kfold.split(X, y)


    if total_n_trials < 20:
        raise NotEnoughTrialsError('Not enough trials to decode')

    if np.isin(deconf_method, ['raw', 'distribution_match', 'confound_isolating_cv',
                               'linear_confound_regression',
                               'nonlinear_confound_regression',
                               'decode_from_confound']):
        # run the decoding
        kfold_scores = []
        y_test_all, y_pred_all, c_all = [], [], []

        for fold, (training_ind, testing_ind) in enumerate(iterable):

            # decoder = SGDClassifier(random_state=92)
            X_train = X[training_ind, :]
            X_test = X[testing_ind, :]
            y_train = y[training_ind]
            y_test = y[testing_ind]
            C_train = C[training_ind, :]
            C_test = C[testing_ind, :]

            # TODO scale before or after conf regression?
            if standardize_features:
                ss = StandardScaler()
                X_train = ss.fit_transform(X_train)
                X_test = ss.transform(X_test)

            if standardize_confound:
                ss = StandardScaler()
                C_train = ss.fit_transform(C_train)
                C_test = ss.transform(C_test)

            # Apply confound regression (linear regression)
            if deconf_method == 'linear_confound_regression':
                # for high-dimensional confounds, avoid overfitting on the
                # train set
                if linear_regression_regularization:
                    reg = RidgeCV(alphas=[0.001, 0.1, 1, 10], cv=None, scoring=None, store_cv_values=False)
                    reg.fit(C_train, X_train)
                else:
                    reg = LinearRegression().fit(C_train, X_train)
                X_train = X_train  - reg.predict(C_train)
                X_test = X_test - reg.predict(C_test)

            # Apply confound regression (nonlinear variation)
            elif deconf_method == 'nonlinear_confound_regression' :
                if X.shape[1] == 1:
                    #reg = RandomForestRegressor(n_estimators=100).fit(C_train, X_train.flatten())
                    reg = RandomForestRegressor(n_estimators=n_estimators, random_state=fold,
                                                max_depth=max_depth,
                                                min_samples_split=min_samples_split,
                                                min_samples_leaf=min_samples_leaf,
                                                max_features=max_features)
                    reg.fit(C_train, X_train.flatten())
                    #reg = SVC().fit(C_train, X_train.flatten())
                    X_train = X_train - reg.predict(C_train).reshape(-1, 1)
                    X_test = X_test - reg.predict(C_test).reshape(-1, 1)
                elif X.shape[1] > 1:
                    #reg = RandomForestRegressor(n_estimators=100).fit(C_train, X_train)
                    reg = RandomForestRegressor(n_estimators=n_estimators, random_state=fold,
                                                max_depth=max_depth,
                                                min_samples_split=min_samples_split,
                                                min_samples_leaf=min_samples_leaf,
                                                max_features=max_features)
                    reg.fit(C_train, X_train)
                    #reg = SVC().fit(C_train, X_train)
                    X_train = X_train - reg.predict(C_train)
                    X_test = X_test - reg.predict(C_test)

            # DECODE
            if decoder == 'random_forest':
                if tune_hyperpars_decoder:
                    raise NotImplementedError
                model = RandomForestClassifier(n_estimators=200, random_state=92)

            elif decoder == 'logistic_regression':
                if tune_hyperpars_decoder:
                    parameters = {'C': [0.01, 0.1, 1, 10]}
                    lr = LogisticRegression(penalty='l2')
                    inner_cv = StratifiedKFold(n_splits=3, shuffle=True, random_state=1)
                    model = GridSearchCV(lr, parameters, refit=True, scoring=make_scorer(balanced_accuracy_score),cv=inner_cv)
                else:
                    model = LogisticRegression()

            elif decoder == 'linear_discriminant_analysis':
                if tune_hyperpars_decoder:
                    raise NotImplementedError
                model = LinearDiscriminantAnalysis()
            elif decoder == 'quadratic_discriminant_analysis':
                if tune_hyperpars_decoder:
                    raise NotImplementedError
                model = QuadraticDiscriminantAnalysis()
            elif decoder == 'ridge_regression':
                if tune_hyperpars_decoder:
                    raise NotImplementedError
                model = RidgeClassifier()
            elif decoder == 'SVC':
                if tune_hyperpars_decoder:
                    parameters = {'kernel': ('linear', 'rbf'), 'C': [0.001, 0.01, 0.1, 1]}
                    svc = SVC(probability=True)
                    inner_cv = StratifiedKFold(n_splits=3, shuffle=True, random_state=1)
                    model = GridSearchCV(svc, parameters, refit=True,
                                         scoring=make_scorer(balanced_accuracy_score), cv=inner_cv)
                else:
                    model = SVC()
            else:
                raise ValueError

            if deconf_method == 'decode_from_confound':
                model.fit(C_train, y_train)
                y_pred = model.predict(C_test)

            else:
                model.fit(X_train, y_train)
                y_pred = model.predict(X_test)

            if tune_hyperpars_decoder:
                try:
                    best_C = model.best_params_['C']
                    print('BEST C: {}'.format(best_C))
                except KeyError:
                    pass
            score = balanced_accuracy_score(y_test, y_pred)
            kfold_scores.append(score)
            y_test_all.append(y_test)
            y_pred_all.append(y_pred)
            c_all.append(C_test[:, 0])

        target = np.hstack(y_test_all)
        predictions = np.hstack(y_pred_all)
        confound = np.hstack(c_all)
        mean_score = np.mean(kfold_scores)

    # TODO this could be done in the same loop as above but it would get a bit dense
    elif np.isin(deconf_method, ['predictability_lin',
                                 'predictability_nonlin',
                                 'predictability_nonlin_ensemble',
                                 'predictability_lin_nodeconf',
                                 'predictability_nonlin_nodeconf',
                                 'predictability_nonlin_ensemble_nodeconf']):

        predictabilities = []
        for repeat in range(10):
            total_n_trials = X.shape[0]

            kfold = StratifiedKFold(n_splits=n_kfold_splits, shuffle=True,
                                    random_state=repeat)
            iterable = kfold.split(X, y)

            kfold_accuracy_full, kfold_accuracy_naive = [], []
            llratios = []

            for fold, (training_ind, testing_ind) in enumerate(iterable):

                X_train = X[training_ind, :]
                X_test = X[testing_ind, :]
                y_train = y[training_ind]
                y_test = y[testing_ind]
                C_train = C[training_ind, :]
                C_test = C[testing_ind, :]

                if standardize_features:
                    ss = StandardScaler()
                    X_train = ss.fit_transform(X_train)
                    X_test = ss.transform(X_test)

                if standardize_confound:
                    ss = StandardScaler()
                    C_train = ss.fit_transform(C_train)
                    C_test = ss.transform(C_test)


                if np.isin(deconf_method, ['predictability_lin',
                                           'predictability_nonlin',
                                           'predictability_nonlin_ensemble',]):
                    features_full_train = np.hstack([X_train, C_train])
                    features_full_test = np.hstack([X_test, C_test])
                    features_naive_train = C_train
                    features_naive_test = C_test

                if np.isin(deconf_method, ['predictability_lin_nodeconf',
                                           'predictability_nonlin_nodeconf',
                                           'predictability_nonlin_ensemble_nodeconf',]):
                    features_full_train = X_train
                    features_full_test = X_test
                    features_naive_train = np.ones_like(C_train)
                    features_naive_test = np.ones_like(C_test)

                if np.isin(deconf_method, ['predictability_lin',
                                           'predictability_lin_nodeconf']):
                    if tune_regularization_predictability:
                        full_model = LogisticRegressionCV(penalty=lr_penalty,
                                                          solver=lr_solver,
                                                          Cs=Cs,
                                                          # l1_ratios=l1_ratios,
                                                          max_iter=max_iter,
                                                          cv=tune_C_nfolds,
                                                          random_state=fold)
                        naive_model = LogisticRegressionCV(penalty=lr_penalty,
                                                           solver=lr_solver,
                                                           Cs=Cs,
                                                           # l1_ratios=l1_ratios,
                                                           max_iter=max_iter,
                                                           cv=tune_C_nfolds,
                                                           random_state=fold)
                        # TODO naive model not regularized!
                        # naive_model = LogisticRegression(penalty=None,
                        #                                  solver=lr_solver,
                        #                                   random_state=fold)

                    else:
                        full_model = LogisticRegression(penalty=lr_penalty,
                                                        solver=lr_solver,
                                                        C=C_reg,
                                                          random_state=fold)
                        # naive_model = LogisticRegression(penalty=lr_penalty,
                        #                                  solver=lr_solver,
                        #                                  C=C_reg)
                        # TODO naive model not regularized!
                        naive_model = LogisticRegression(penalty=None,
                                                         solver=lr_solver,
                                                          random_state=fold)

                elif np.isin(deconf_method, ['predictability_nonlin',
                                             'predictability_nonlin_nodeconf']):
                    # full_model = QuadraticDiscriminantAnalysis()
                    # naive_model = QuadraticDiscriminantAnalysis()
                    # full_model = RandomForestClassifier(n_estimators=200)
                    # naive_model = RandomForestClassifier(n_estimators=200)

                    # TODO old version used for simulation figures
                    # if tune_regularization_predictability:
                    #     parameters_1 = {'kernel': ('linear', 'rbf'), 'C': [0.01, 0.1, 1, 10]}
                    #     svc_1 = SVC(probability=True)
                    #     inner_cv_1 = StratifiedKFold(n_splits=3, shuffle=True, random_state=1)
                    #     full_model = GridSearchCV(svc_1, parameters_1, refit=True, scoring=make_scorer(balanced_accuracy_score),
                    #                               cv=inner_cv_1)
                    #
                    #     parameters = {'kernel': ('linear', 'rbf')}
                    #     svc_2 = SVC(probability=True)
                    #     inner_cv_2 = StratifiedKFold(n_splits=3, shuffle=True, random_state=1)
                    #     naive_model = GridSearchCV(svc_2, parameters, refit=True, scoring=make_scorer(balanced_accuracy_score),
                    #                                cv=inner_cv_2)

                    if tune_regularization_predictability:
                        parameters_1 = {'C': [0.0001, 0.001, 0.01, 0.1, 1, 10]}
                                        #'gamma' : ['scale', 0.0001, 0.001, 0.01, 0.1, 1]}
                        svc_1 = SVC(probability=True, gamma='scale', random_state=fold)
                        inner_cv_1 = StratifiedKFold(n_splits=3, shuffle=True, random_state=1)
                        full_model = GridSearchCV(svc_1, parameters_1, refit=True,
                                                  scoring=None,
                                                  cv=inner_cv_1)

                        parameters_2 = {'C': [0.0001, 0.001, 0.01, 0.1, 1, 10]}
                                        #'gamma': ['scale', 0.0001, 0.001, 0.01, 0.1, 1]}
                        svc_2 = SVC(probability=True, gamma='scale', random_state=fold)
                        inner_cv_2 = StratifiedKFold(n_splits=3, shuffle=True, random_state=1)
                        naive_model = GridSearchCV(svc_2, parameters_2, refit=True, scoring=None,
                                                   cv=inner_cv_2)

                        # naive_model = SVC(probability=True, gamma='scale',
                        #                   kernel='rbf', C=10)


                        # svc_2 = SVC(probability=True, C=10, kernel='rbf')
                        # naive_model = svc_2

                    else:
                        full_model = SVC(probability=True, kernel='rbf', C=10)
                        naive_model = SVC(probability=True, kernel='rbf', C=10)

                elif np.isin(deconf_method, ['predictability_nonlin_ensemble',
                                             'predictability_nonlin_ensemble_nodeconf']):
                    full_model = RandomForestClassifier(n_estimators=n_estimators, random_state=fold,
                                                        max_depth=max_depth,
                                                        min_samples_split=min_samples_split,
                                                        min_samples_leaf=min_samples_leaf,
                                                        max_features=max_features,
                                                        bootstrap=True)
                    naive_model = RandomForestClassifier(n_estimators=n_estimators, random_state=fold,
                                                         max_depth=max_depth,
                                                         min_samples_split=min_samples_split,
                                                         min_samples_leaf=min_samples_leaf,
                                                         max_features=max_features,
                                                         bootstrap=True)

                full_model.fit(features_full_train, y_train)
                naive_model.fit(features_naive_train, y_train)

                # try:
                #     print('Full model best C: {}'.format(full_model.best_params_['C']))
                #     print('Full model kernel: {}'.format(full_model.best_params_['kernel']))
                #     print('Naive model best C: {}'.format(naive_model.best_params_['C']))
                #     print('Naive model best kernel: {}'.format(naive_model.best_params_['kernel']))
                #
                # except:
                #     pass

                y_pred_proba_full = full_model.predict_proba(features_full_test)
                y_pred_proba_naive = naive_model.predict_proba(features_naive_test)

                fold_accuracy_full = balanced_accuracy_score(y_test, full_model.predict(features_full_test))
                fold_accuracy_naive = balanced_accuracy_score(y_test, naive_model.predict(features_naive_test))

                kfold_accuracy_full.append(fold_accuracy_full)
                kfold_accuracy_naive.append(fold_accuracy_naive)

                log_likelihood_full = log2_likelihood(y_test, y_pred_proba_full)
                log_likelihood_naive = log2_likelihood(y_test, y_pred_proba_naive)
                # print(fold_accuracy_full)
                # print(log_likelihood_full)
                # print(log_likelihood_naive)

                llratio = log_likelihood_full - log_likelihood_naive
                llratios.append(llratio)

            accuracy_full = np.mean(kfold_accuracy_full)
            accuracy_naive = np.mean(kfold_accuracy_naive)
            predictability = np.mean(llratios)
            # print(accuracy_full)
            # print(accuracy_naive)
            # print(predictability)
            predictabilities.append(predictability)


        mean_score = np.mean(predictabilities)
        kfold_scores = predictabilities


    if run_spisak_full_test:

        if np.unique(confound).shape[0] == 1 :
            confound += np.random.normal(0, 0.01, confound.shape[0])

        full_test = full_confound_test(target, predictions, confound,
                                       num_perms=num_perms_spisak,
                                       cat_y=True,
                                       cat_yhat=True,
                                       cat_c=False,
                                       mcmc_steps=50,
                                       cond_dist_method='gam',
                                       return_null_dist=False,
                                       random_state=92, progress=True,
                                       n_jobs=1)
        full_p = full_test.p
        full_sig = full_p < 0.05

    else :
        full_p = np.nan
        full_sig = False

    if run_spisak_partial_test:

        partial_test = partial_confound_test(target, predictions, confound,
                                             num_perms=num_perms_spisak,
                                             cat_y=True,
                                             cat_yhat=True,
                                             cat_c=False,
                                             mcmc_steps=50,
                                             cond_dist_method='gam',
                                             return_null_dist=False,
                                             random_state=92, progress=True,
                                             n_jobs=1)
        partial_p = partial_test.p
        partial_sig = partial_p < 0.05

    else :
        partial_p = np.nan
        partial_sig = False


    output = {'full_p' : full_p,
              'full_sig' : full_sig,
              'partial_p' : partial_p,
              'partial_sig' : partial_sig,
              'mean_score' : mean_score,
              'kfold_scores' : kfold_scores,
              'method' : deconf_method}

    return output


def decode_alt_norepeats(X, C, y,
               deconf_method,
               decoder,
               distribution_match_binsize=None,
               n_bins=None,
               already_binned=False,
               n_kfold_splits=3, min_trials=20,
               run_spisak_partial_test=False,
               run_spisak_full_test=False,
               num_perms_spisak=1000,
               standardize_features=True,
               standardize_confound=True,
               linear_regression_regularization=False,
               tune_hyperpars_decoder=False,
               lr_penalty='l1', lr_solver='saga', C_reg=0.2,
               tune_regularization_predictability=False,
               Cs=None, max_iter=7000, tune_C_nfolds=3,
               n_estimators=50, max_depth=3,
               min_samples_split=2, min_samples_leaf=7,
               max_features='sqrt',
               fill_C_nans=False):
    # print(np.diff(y.index).max())

    methods = ['raw',
               'decode_from_confound',
               'predictability_lin',
               'predictability_nonlin',
               'predictability_nonlin_ensemble',
               'predictability_lin_nodeconf',
               'predictability_nonlin_nodeconf',
               'predictability_nonlin_ensemble_nodeconf'
               'distribution_match',
               'confound_isolating_cv',
               'linear_confound_regression',
               'nonlinear_confound_regression']

    assert np.isin(deconf_method, methods)

    decoders = ['logistic_regression', 'random_forest',
                'linear_discriminant_analysis', 'quadratic_discriminant_analysis',
                'ridge_regression', 'SVC', 'none']
    assert np.isin(decoder, decoders)


    total_n_trials = X.shape[0]
    if total_n_trials < 20 :
        raise NotEnoughTrialsError('Not enough trials to decode')


    # methods that influence the data distribution
    if deconf_method == 'distribution_match' :

        X, y, C = posthoc_counterbalancing(X, y, C, distribution_match_binsize,
                                           n_bins=n_bins,
                                           already_binned=already_binned)
        total_n_trials = X.shape[0]
        print('N trials after counterbalancing: {}'.format(X.shape[0]))

        kfold = StratifiedKFold(n_splits=n_kfold_splits, shuffle=True,
                                random_state=92)
        iterable = kfold.split(X, y)


    elif deconf_method == 'confound_isolating_cv' :

        if C.shape[1] > 1 :
            # if more than one confound, we only look at first
            # could look at the sum as well
            print('Confound has more than 1 dimension, matching '
                  'only the first dimension')
            C = C[:, 0].reshape(-1, 1)


        _, _, _, _, ids_test, ids_train = confound_isolating_cv(X, y, C[:, 0],
                                                                    random_seed=92,
                                                                    min_sample_size=None,
                                                                    cv_folds=n_kfold_splits,
                                                                    n_remove=None)

        total_n_trials = np.unique(np.hstack([ids_test, ids_train])).shape[0]

        iterable = zip(ids_train, ids_test)

    else:
        # normal data
        total_n_trials = X.shape[0]

        kfold = StratifiedKFold(n_splits=n_kfold_splits, shuffle=True,
                                random_state=92)
        iterable = kfold.split(X, y)


    if total_n_trials < 20:
        raise NotEnoughTrialsError('Not enough trials to decode')

    if np.isin(deconf_method, ['raw', 'distribution_match', 'confound_isolating_cv',
                               'linear_confound_regression',
                               'nonlinear_confound_regression',
                               'decode_from_confound']):
        # run the decoding
        kfold_scores = []
        y_test_all, y_pred_all, c_all = [], [], []

        for fold, (training_ind, testing_ind) in enumerate(iterable):

            # decoder = SGDClassifier(random_state=92)
            X_train = X[training_ind, :]
            X_test = X[testing_ind, :]
            y_train = y[training_ind]
            y_test = y[testing_ind]
            C_train = C[training_ind, :]
            C_test = C[testing_ind, :]

            if fill_C_nans:
                C_train = np.where(np.isnan(C_train), np.nanmean(C_train, axis=0), C_train)
                C_test = np.where(np.isnan(C_test), np.nanmean(C_train, axis=0), C_test)

            # TODO scale before or after conf regression?
            if standardize_features:
                ss = StandardScaler()
                X_train = ss.fit_transform(X_train)
                X_test = ss.transform(X_test)

            if standardize_confound:
                ss = StandardScaler()
                C_train = ss.fit_transform(C_train)
                C_test = ss.transform(C_test)

            # Apply confound regression (linear regression)
            if deconf_method == 'linear_confound_regression':
                # for high-dimensional confounds, avoid overfitting on the
                # train set
                if linear_regression_regularization:
                    reg = RidgeCV(alphas=[0.001, 0.1, 1, 10], cv=None, scoring=None, store_cv_values=False)
                    reg.fit(C_train, X_train)
                else:
                    reg = LinearRegression().fit(C_train, X_train)
                X_train = X_train  - reg.predict(C_train)
                X_test = X_test - reg.predict(C_test)

            # Apply confound regression (nonlinear variation)
            elif deconf_method == 'nonlinear_confound_regression' :
                if X.shape[1] == 1:
                    #reg = RandomForestRegressor(n_estimators=100).fit(C_train, X_train.flatten())
                    reg = RandomForestRegressor(n_estimators=n_estimators, random_state=fold,
                                                max_depth=None)
                    reg.fit(C_train, X_train.flatten())
                    #reg = SVC().fit(C_train, X_train.flatten())
                    X_train = X_train - reg.predict(C_train).reshape(-1, 1)
                    X_test = X_test - reg.predict(C_test).reshape(-1, 1)
                elif X.shape[1] > 1:
                    #reg = RandomForestRegressor(n_estimators=100).fit(C_train, X_train)
                    reg = RandomForestRegressor(n_estimators=n_estimators, random_state=fold,
                                                max_depth=None)
                    reg.fit(C_train, X_train)
                    #reg = SVC().fit(C_train, X_train)
                    X_train = X_train - reg.predict(C_train)
                    X_test = X_test - reg.predict(C_test)

            # DECODE
            if decoder == 'random_forest':
                if tune_hyperpars_decoder:
                    raise NotImplementedError
                model = RandomForestClassifier(n_estimators=200, random_state=92)

            elif decoder == 'logistic_regression':
                if tune_hyperpars_decoder:
                    parameters = {'C': [0.01, 0.1, 1, 10]}
                    lr = LogisticRegression(penalty='l2')
                    inner_cv = StratifiedKFold(n_splits=3, shuffle=True, random_state=1)
                    model = GridSearchCV(lr, parameters, refit=True, scoring=make_scorer(balanced_accuracy_score),cv=inner_cv)
                else:
                    model = LogisticRegression()

            elif decoder == 'linear_discriminant_analysis':
                if tune_hyperpars_decoder:
                    raise NotImplementedError
                model = LinearDiscriminantAnalysis()
            elif decoder == 'quadratic_discriminant_analysis':
                if tune_hyperpars_decoder:
                    raise NotImplementedError
                model = QuadraticDiscriminantAnalysis()
            elif decoder == 'ridge_regression':
                if tune_hyperpars_decoder:
                    raise NotImplementedError
                model = RidgeClassifier()
            elif decoder == 'SVC':
                if tune_hyperpars_decoder:
                    parameters = {'kernel': ('linear', 'rbf'), 'C': [0.001, 0.01, 0.1, 1]}
                    svc = SVC(probability=True)
                    inner_cv = StratifiedKFold(n_splits=3, shuffle=True, random_state=1)
                    model = GridSearchCV(svc, parameters, refit=True,
                                         scoring=make_scorer(balanced_accuracy_score), cv=inner_cv)
                else:
                    model = SVC()
            else:
                raise ValueError

            if deconf_method == 'decode_from_confound':
                model.fit(C_train, y_train)
                y_pred = model.predict(C_test)

            else:
                model.fit(X_train, y_train)
                y_pred = model.predict(X_test)

            if tune_hyperpars_decoder:
                try:
                    best_C = model.best_params_['C']
                    print('BEST C: {}'.format(best_C))
                except KeyError:
                    pass
            score = balanced_accuracy_score(y_test, y_pred)
            kfold_scores.append(score)
            y_test_all.append(y_test)
            y_pred_all.append(y_pred)
            c_all.append(C_test[:, 0])

        target = np.hstack(y_test_all)
        predictions = np.hstack(y_pred_all)
        confound = np.hstack(c_all)
        mean_score = np.mean(kfold_scores)

    # TODO this could be done in the same loop as above but it would get a bit dense
    elif np.isin(deconf_method, ['predictability_lin',
                                 'predictability_nonlin',
                                 'predictability_nonlin_ensemble',
                                 'predictability_lin_nodeconf',
                                 'predictability_nonlin_nodeconf',
                                 'predictability_nonlin_ensemble_nodeconf']):

        total_n_trials = X.shape[0]
        kfold = StratifiedKFold(n_splits=n_kfold_splits, shuffle=True,
                                random_state=10)
        iterable = kfold.split(X, y)

        kfold_accuracy_full, kfold_accuracy_naive = [], []
        llratios = []

        for fold, (training_ind, testing_ind) in enumerate(iterable):

            X_train = X[training_ind, :]
            X_test = X[testing_ind, :]
            y_train = y[training_ind]
            y_test = y[testing_ind]
            C_train = C[training_ind, :]
            C_test = C[testing_ind, :]

            if fill_C_nans:
                C_train = np.where(np.isnan(C_train), np.nanmean(C_train, axis=0), C_train)
                C_test = np.where(np.isnan(C_test), np.nanmean(C_train, axis=0), C_test)

            if standardize_features:
                ss = StandardScaler()
                X_train = ss.fit_transform(X_train)
                X_test = ss.transform(X_test)

            if standardize_confound:
                ss = StandardScaler()
                C_train = ss.fit_transform(C_train)
                C_test = ss.transform(C_test)


            if np.isin(deconf_method, ['predictability_lin',
                                       'predictability_nonlin',
                                       'predictability_nonlin_ensemble',]):
                features_full_train = np.hstack([X_train, C_train])
                features_full_test = np.hstack([X_test, C_test])
                features_naive_train = C_train
                features_naive_test = C_test

            if np.isin(deconf_method, ['predictability_lin_nodeconf',
                                       'predictability_nonlin_nodeconf',
                                       'predictability_nonlin_ensemble_nodeconf',]):
                features_full_train = X_train
                features_full_test = X_test
                features_naive_train = np.ones_like(C_train)
                features_naive_test = np.ones_like(C_test)

            if np.isin(deconf_method, ['predictability_lin',
                                       'predictability_lin_nodeconf']):
                if tune_regularization_predictability:
                    full_model = LogisticRegressionCV(penalty=lr_penalty,
                                                      solver=lr_solver,
                                                      Cs=Cs,
                                                      # l1_ratios=l1_ratios,
                                                      max_iter=max_iter,
                                                      cv=tune_C_nfolds,
                                                      random_state=fold)
                    naive_model = LogisticRegressionCV(penalty=lr_penalty,
                                                       solver=lr_solver,
                                                       Cs=Cs,
                                                       # l1_ratios=l1_ratios,
                                                       max_iter=max_iter,
                                                       cv=tune_C_nfolds,
                                                       random_state=fold)
                    # TODO naive model not regularized!
                    # naive_model = LogisticRegression(penalty=None,
                    #                                  solver=lr_solver,
                    #                                   random_state=fold)

                else:
                    full_model = LogisticRegression(penalty=lr_penalty,
                                                    solver=lr_solver,
                                                    C=C_reg,
                                                      random_state=fold)
                    # naive_model = LogisticRegression(penalty=lr_penalty,
                    #                                  solver=lr_solver,
                    #                                  C=C_reg)
                    # TODO naive model not regularized!
                    naive_model = LogisticRegression(penalty=None,
                                                     solver=lr_solver,
                                                      random_state=fold)

            elif np.isin(deconf_method, ['predictability_nonlin',
                                         'predictability_nonlin_nodeconf']):
                # full_model = QuadraticDiscriminantAnalysis()
                # naive_model = QuadraticDiscriminantAnalysis()
                # full_model = RandomForestClassifier(n_estimators=200)
                # naive_model = RandomForestClassifier(n_estimators=200)

                # TODO old version used for simulation figures
                # if tune_regularization_predictability:
                #     parameters_1 = {'kernel': ('linear', 'rbf'), 'C': [0.01, 0.1, 1, 10]}
                #     svc_1 = SVC(probability=True)
                #     inner_cv_1 = StratifiedKFold(n_splits=3, shuffle=True, random_state=1)
                #     full_model = GridSearchCV(svc_1, parameters_1, refit=True, scoring=make_scorer(balanced_accuracy_score),
                #                               cv=inner_cv_1)
                #
                #     parameters = {'kernel': ('linear', 'rbf')}
                #     svc_2 = SVC(probability=True)
                #     inner_cv_2 = StratifiedKFold(n_splits=3, shuffle=True, random_state=1)
                #     naive_model = GridSearchCV(svc_2, parameters, refit=True, scoring=make_scorer(balanced_accuracy_score),
                #                                cv=inner_cv_2)

                if tune_regularization_predictability:
                    parameters_1 = {'C': [0.0001, 0.001, 0.01, 0.1, 1, 10]}
                                    #'gamma' : ['scale', 0.0001, 0.001, 0.01, 0.1, 1]}
                    svc_1 = SVC(probability=True, gamma='scale', random_state=fold)
                    inner_cv_1 = StratifiedKFold(n_splits=3, shuffle=True, random_state=1)
                    full_model = GridSearchCV(svc_1, parameters_1, refit=True,
                                              scoring=None,
                                              cv=inner_cv_1)

                    parameters_2 = {'C': [0.0001, 0.001, 0.01, 0.1, 1, 10]}
                                    #'gamma': ['scale', 0.0001, 0.001, 0.01, 0.1, 1]}
                    svc_2 = SVC(probability=True, gamma='scale', random_state=fold)
                    inner_cv_2 = StratifiedKFold(n_splits=3, shuffle=True, random_state=1)
                    naive_model = GridSearchCV(svc_2, parameters_2, refit=True, scoring=None,
                                               cv=inner_cv_2)

                    # naive_model = SVC(probability=True, gamma='scale',
                    #                   kernel='rbf', C=10)


                    # svc_2 = SVC(probability=True, C=10, kernel='rbf')
                    # naive_model = svc_2

                else:
                    full_model = SVC(probability=True, kernel='rbf', C=10)
                    naive_model = SVC(probability=True, kernel='rbf', C=10)

            elif np.isin(deconf_method, ['predictability_nonlin_ensemble',
                                         'predictability_nonlin_ensemble_nodeconf']):
                full_model = RandomForestClassifier(n_estimators=n_estimators, random_state=fold,
                                                    max_depth=max_depth,
                                                    min_samples_split=min_samples_split,
                                                    min_samples_leaf=min_samples_leaf,
                                                    max_features=max_features,
                                                    bootstrap=True)
                naive_model = RandomForestClassifier(n_estimators=n_estimators, random_state=fold,
                                                     max_depth=max_depth,
                                                     min_samples_split=min_samples_split,
                                                     min_samples_leaf=min_samples_leaf,
                                                     max_features=max_features,
                                                     bootstrap=True)

            full_model.fit(features_full_train, y_train)
            naive_model.fit(features_naive_train, y_train)

            # try:
            #     print('Full model best C: {}'.format(full_model.best_params_['C']))
            #     print('Full model kernel: {}'.format(full_model.best_params_['kernel']))
            #     print('Naive model best C: {}'.format(naive_model.best_params_['C']))
            #     print('Naive model best kernel: {}'.format(naive_model.best_params_['kernel']))
            #
            # except:
            #     pass

            y_pred_proba_full = full_model.predict_proba(features_full_test)
            y_pred_proba_naive = naive_model.predict_proba(features_naive_test)

            fold_accuracy_full = balanced_accuracy_score(y_test, full_model.predict(features_full_test))
            fold_accuracy_naive = balanced_accuracy_score(y_test, naive_model.predict(features_naive_test))

            kfold_accuracy_full.append(fold_accuracy_full)
            kfold_accuracy_naive.append(fold_accuracy_naive)

            log_likelihood_full = log2_likelihood(y_test, y_pred_proba_full)
            log_likelihood_naive = log2_likelihood(y_test, y_pred_proba_naive)
            # print(fold_accuracy_full)
            # print(log_likelihood_full)
            # print(log_likelihood_naive)

            llratio = log_likelihood_full - log_likelihood_naive
            llratios.append(llratio)

        accuracy_full = np.mean(kfold_accuracy_full)
        accuracy_naive = np.mean(kfold_accuracy_naive)
        predictability = np.mean(llratios)
        # print(accuracy_full)
        # print(accuracy_naive)
        # print(predictability)
        #predictabilities.append(predictability)

        mean_score = predictability
        kfold_scores = llratios


    if run_spisak_full_test:

        if np.unique(confound).shape[0] == 1 :
            confound += np.random.normal(0, 0.01, confound.shape[0])

        full_test = full_confound_test(target, predictions, confound,
                                       num_perms=num_perms_spisak,
                                       cat_y=True,
                                       cat_yhat=True,
                                       cat_c=False,
                                       mcmc_steps=50,
                                       cond_dist_method='gam',
                                       return_null_dist=False,
                                       random_state=92, progress=True,
                                       n_jobs=1)
        full_p = full_test.p
        full_sig = full_p < 0.05

    else :
        full_p = np.nan
        full_sig = False

    if run_spisak_partial_test:

        partial_test = partial_confound_test(target, predictions, confound,
                                             num_perms=num_perms_spisak,
                                             cat_y=True,
                                             cat_yhat=True,
                                             cat_c=False,
                                             mcmc_steps=50,
                                             cond_dist_method='gam',
                                             return_null_dist=False,
                                             random_state=92, progress=True,
                                             n_jobs=1)
        partial_p = partial_test.p
        partial_sig = partial_p < 0.05

    else :
        partial_p = np.nan
        partial_sig = False


    output = {'full_p' : full_p,
              'full_sig' : full_sig,
              'partial_p' : partial_p,
              'partial_sig' : partial_sig,
              'mean_score' : mean_score,
              'kfold_scores' : kfold_scores,
              'method' : deconf_method}

    return output



def decode_alt_repeats(X, C, y,
               deconf_method,
               decoder,
               distribution_match_binsize=None,
               n_bins=None,
               already_binned=False,
               n_kfold_splits=3, min_trials=20,
               run_spisak_partial_test=False,
               run_spisak_full_test=False,
               num_perms_spisak=1000,
               standardize_features=True,
               standardize_confound=True,
               linear_regression_regularization=False,
               tune_hyperpars_decoder=False,
               lr_penalty='l1', lr_solver='saga', C_reg=0.2,
               tune_regularization_predictability=False,
               Cs=None, max_iter=7000, tune_C_nfolds=3,
               n_estimators=50, max_depth=3,
               min_samples_split=2, min_samples_leaf=7,
               max_features='sqrt',
               fill_C_nans=False,
               n_repeats=10):
    # print(np.diff(y.index).max())

    methods = ['raw',
               'decode_from_confound',
               'predictability_lin',
               'predictability_nonlin',
               'predictability_nonlin_ensemble',
               'predictability_lin_nodeconf',
               'predictability_nonlin_nodeconf',
               'predictability_nonlin_ensemble_nodeconf'
               'distribution_match',
               'confound_isolating_cv',
               'linear_confound_regression',
               'nonlinear_confound_regression']

    assert np.isin(deconf_method, methods)

    decoders = ['logistic_regression', 'random_forest',
                'linear_discriminant_analysis', 'quadratic_discriminant_analysis',
                'ridge_regression', 'SVC', 'none']
    assert np.isin(decoder, decoders)


    total_n_trials = X.shape[0]
    if total_n_trials < 20 :
        raise NotEnoughTrialsError('Not enough trials to decode')






    if total_n_trials < 20:
        raise NotEnoughTrialsError('Not enough trials to decode')

    if np.isin(deconf_method, ['raw', 'distribution_match', 'confound_isolating_cv',
                               'linear_confound_regression',
                               'nonlinear_confound_regression',
                               'decode_from_confound']):


        mean_scores = []
        for n_rep in range(n_repeats):
            total_n_trials = X.shape[0]

            kfold = StratifiedKFold(n_splits=n_kfold_splits, shuffle=True,
                                    random_state=n_rep)
            iterable = kfold.split(X, y)

            # run the decoding
            kfold_scores = []
            y_test_all, y_pred_all, c_all = [], [], []

            for fold, (training_ind, testing_ind) in enumerate(iterable):

                # decoder = SGDClassifier(random_state=92)
                X_train = X[training_ind, :]
                X_test = X[testing_ind, :]
                y_train = y[training_ind]
                y_test = y[testing_ind]
                C_train = C[training_ind, :]
                C_test = C[testing_ind, :]

                if fill_C_nans:
                    C_train = np.where(np.isnan(C_train), np.nanmean(C_train, axis=0), C_train)
                    C_test = np.where(np.isnan(C_test), np.nanmean(C_train, axis=0), C_test)

                # TODO scale before or after conf regression?
                if standardize_features:
                    ss = StandardScaler()
                    X_train = ss.fit_transform(X_train)
                    X_test = ss.transform(X_test)

                if standardize_confound:
                    ss = StandardScaler()
                    C_train = ss.fit_transform(C_train)
                    C_test = ss.transform(C_test)

                # Apply confound regression (linear regression)
                if deconf_method == 'linear_confound_regression':
                    # for high-dimensional confounds, avoid overfitting on the
                    # train set
                    if linear_regression_regularization:
                        reg = RidgeCV(alphas=[0.001, 0.1, 1, 10], cv=None, scoring=None, store_cv_values=False)
                        reg.fit(C_train, X_train)
                    else:
                        reg = LinearRegression().fit(C_train, X_train)
                    X_train = X_train  - reg.predict(C_train)
                    X_test = X_test - reg.predict(C_test)

                # Apply confound regression (nonlinear variation)
                elif deconf_method == 'nonlinear_confound_regression' :
                    if X.shape[1] == 1:
                        #reg = RandomForestRegressor(n_estimators=100).fit(C_train, X_train.flatten())
                        reg = RandomForestRegressor(n_estimators=n_estimators, random_state=fold,
                                                    max_depth=None)
                        reg.fit(C_train, X_train.flatten())
                        #reg = SVC().fit(C_train, X_train.flatten())
                        X_train = X_train - reg.predict(C_train).reshape(-1, 1)
                        X_test = X_test - reg.predict(C_test).reshape(-1, 1)
                    elif X.shape[1] > 1:
                        #reg = RandomForestRegressor(n_estimators=100).fit(C_train, X_train)
                        reg = RandomForestRegressor(n_estimators=n_estimators, random_state=fold,
                                                    max_depth=None)
                        reg.fit(C_train, X_train)
                        #reg = SVC().fit(C_train, X_train)
                        X_train = X_train - reg.predict(C_train)
                        X_test = X_test - reg.predict(C_test)

                # DECODE
                if decoder == 'random_forest':
                    if tune_hyperpars_decoder:
                        raise NotImplementedError
                    model = RandomForestClassifier(n_estimators=200, random_state=92)

                elif decoder == 'logistic_regression':
                    if tune_hyperpars_decoder:
                        parameters = {'C': Cs}
                        lr = LogisticRegression(penalty=lr_penalty)
                        inner_cv = StratifiedKFold(n_splits=3, shuffle=True, random_state=fold)
                        model = GridSearchCV(lr, parameters, refit=True, scoring=None,
                                             # make_scorer(balanced_accuracy_score)
                                             cv=inner_cv)
                    else:
                        model = LogisticRegression()

                elif decoder == 'linear_discriminant_analysis':
                    if tune_hyperpars_decoder:
                        raise NotImplementedError
                    model = LinearDiscriminantAnalysis()
                elif decoder == 'quadratic_discriminant_analysis':
                    if tune_hyperpars_decoder:
                        raise NotImplementedError
                    model = QuadraticDiscriminantAnalysis()
                elif decoder == 'ridge_regression':
                    if tune_hyperpars_decoder:
                        raise NotImplementedError
                    model = RidgeClassifier()
                elif decoder == 'SVC':
                    if tune_hyperpars_decoder:
                        # parameters = {'kernel': ('linear', 'rbf'), 'C': [0.001, 0.01, 0.1, 1]}
                        # svc = SVC(probability=True)
                        # inner_cv = StratifiedKFold(n_splits=3, shuffle=True, random_state=1)
                        # model = GridSearchCV(svc, parameters, refit=True,
                        #                      scoring=make_scorer(balanced_accuracy_score), cv=inner_cv)
                        parameters = {'kernel': ['linear', 'rbf'],
                                      'C': [0.0001, 0.001, 0.01, 0.1, 1, 10],
                                      'gamma': ['scale', 'auto']}
                        svc = SVC(probability=True)
                        inner_cv = StratifiedKFold(n_splits=3, shuffle=True, random_state=fold)
                        model = GridSearchCV(svc, parameters, refit=True,
                                             scoring=None, cv=inner_cv)
                    else:
                        model = SVC()
                else:
                    raise ValueError

                if deconf_method == 'decode_from_confound':
                    model.fit(C_train, y_train)
                    y_pred = model.predict(C_test)

                else:
                    model.fit(X_train, y_train)
                    y_pred = model.predict(X_test)

                if tune_hyperpars_decoder:
                    try:
                        best_C = model.best_params_['C']
                        print('BEST C: {}'.format(best_C))
                    except KeyError:
                        pass
                score = balanced_accuracy_score(y_test, y_pred)
                kfold_scores.append(score)
                y_test_all.append(y_test)
                y_pred_all.append(y_pred)
                c_all.append(C_test[:, 0])

            target = np.hstack(y_test_all)
            predictions = np.hstack(y_pred_all)
            confound = np.hstack(c_all)
            mean_score = np.mean(kfold_scores)
            mean_scores.append(mean_score)
        mean_score = np.mean(mean_scores)

    # TODO this could be done in the same loop as above but it would get a bit dense
    elif np.isin(deconf_method, ['predictability_lin',
                                 'predictability_nonlin',
                                 'predictability_nonlin_ensemble',
                                 'predictability_lin_nodeconf',
                                 'predictability_nonlin_nodeconf',
                                 'predictability_nonlin_ensemble_nodeconf']):

        total_n_trials = X.shape[0]
        mean_scores = []
        for n_rep in range(n_repeats):
            kfold = StratifiedKFold(n_splits=n_kfold_splits, shuffle=True,
                                    random_state=10)
            iterable = kfold.split(X, y)

            kfold_accuracy_full, kfold_accuracy_naive = [], []
            llratios = []

            for fold, (training_ind, testing_ind) in enumerate(iterable):

                X_train = X[training_ind, :]
                X_test = X[testing_ind, :]
                y_train = y[training_ind]
                y_test = y[testing_ind]
                C_train = C[training_ind, :]
                C_test = C[testing_ind, :]

                if fill_C_nans:
                    C_train = np.where(np.isnan(C_train), np.nanmean(C_train, axis=0), C_train)
                    C_test = np.where(np.isnan(C_test), np.nanmean(C_train, axis=0), C_test)

                if standardize_features:
                    ss = StandardScaler()
                    X_train = ss.fit_transform(X_train)
                    X_test = ss.transform(X_test)

                if standardize_confound:
                    ss = StandardScaler()
                    C_train = ss.fit_transform(C_train)
                    C_test = ss.transform(C_test)


                if np.isin(deconf_method, ['predictability_lin',
                                           'predictability_nonlin',
                                           'predictability_nonlin_ensemble',]):
                    features_full_train = np.hstack([X_train, C_train])
                    features_full_test = np.hstack([X_test, C_test])
                    features_naive_train = C_train
                    features_naive_test = C_test

                if np.isin(deconf_method, ['predictability_lin_nodeconf',
                                           'predictability_nonlin_nodeconf',
                                           'predictability_nonlin_ensemble_nodeconf',]):
                    features_full_train = X_train
                    features_full_test = X_test
                    features_naive_train = np.ones_like(C_train)
                    features_naive_test = np.ones_like(C_test)

                if np.isin(deconf_method, ['predictability_lin',
                                           'predictability_lin_nodeconf']):
                    if tune_regularization_predictability:
                        full_model = LogisticRegressionCV(penalty=lr_penalty,
                                                          solver=lr_solver,
                                                          Cs=Cs,
                                                          # l1_ratios=l1_ratios,
                                                          max_iter=max_iter,
                                                          cv=tune_C_nfolds,
                                                          random_state=fold)
                        naive_model = LogisticRegressionCV(penalty=lr_penalty,
                                                           solver=lr_solver,
                                                           Cs=Cs,
                                                           # l1_ratios=l1_ratios,
                                                           max_iter=max_iter,
                                                           cv=tune_C_nfolds,
                                                           random_state=fold)
                        # TODO naive model not regularized!
                        # naive_model = LogisticRegression(penalty=None,
                        #                                  solver=lr_solver,
                        #                                   random_state=fold)

                    else:
                        full_model = LogisticRegression(penalty=lr_penalty,
                                                        solver=lr_solver,
                                                        C=C_reg,
                                                          random_state=fold)
                        # naive_model = LogisticRegression(penalty=lr_penalty,
                        #                                  solver=lr_solver,
                        #                                  C=C_reg)
                        # TODO naive model not regularized!
                        naive_model = LogisticRegression(penalty=None,
                                                         solver=lr_solver,
                                                          random_state=fold)

                elif np.isin(deconf_method, ['predictability_nonlin',
                                             'predictability_nonlin_nodeconf']):
                    # full_model = QuadraticDiscriminantAnalysis()
                    # naive_model = QuadraticDiscriminantAnalysis()
                    # full_model = RandomForestClassifier(n_estimators=200)
                    # naive_model = RandomForestClassifier(n_estimators=200)

                    # TODO old version used for simulation figures
                    # if tune_regularization_predictability:
                    #     parameters_1 = {'kernel': ('linear', 'rbf'), 'C': [0.01, 0.1, 1, 10]}
                    #     svc_1 = SVC(probability=True)
                    #     inner_cv_1 = StratifiedKFold(n_splits=3, shuffle=True, random_state=1)
                    #     full_model = GridSearchCV(svc_1, parameters_1, refit=True, scoring=make_scorer(balanced_accuracy_score),
                    #                               cv=inner_cv_1)
                    #
                    #     parameters = {'kernel': ('linear', 'rbf')}
                    #     svc_2 = SVC(probability=True)
                    #     inner_cv_2 = StratifiedKFold(n_splits=3, shuffle=True, random_state=1)
                    #     naive_model = GridSearchCV(svc_2, parameters, refit=True, scoring=make_scorer(balanced_accuracy_score),
                    #                                cv=inner_cv_2)

                    if tune_regularization_predictability:
                        parameters_1 = {'C': [0.0001, 0.001, 0.01, 0.1, 1, 10]}
                                        #'gamma' : ['scale', 0.0001, 0.001, 0.01, 0.1, 1]}
                        svc_1 = SVC(probability=True, gamma='scale', random_state=fold)
                        inner_cv_1 = StratifiedKFold(n_splits=3, shuffle=True, random_state=1)
                        full_model = GridSearchCV(svc_1, parameters_1, refit=True,
                                                  scoring=None,
                                                  cv=inner_cv_1)

                        parameters_2 = {'C': [0.0001, 0.001, 0.01, 0.1, 1, 10]}
                                        #'gamma': ['scale', 0.0001, 0.001, 0.01, 0.1, 1]}
                        svc_2 = SVC(probability=True, gamma='scale', random_state=fold)
                        inner_cv_2 = StratifiedKFold(n_splits=3, shuffle=True, random_state=1)
                        naive_model = GridSearchCV(svc_2, parameters_2, refit=True, scoring=None,
                                                   cv=inner_cv_2)

                        # naive_model = SVC(probability=True, gamma='scale',
                        #                   kernel='rbf', C=10)


                        # svc_2 = SVC(probability=True, C=10, kernel='rbf')
                        # naive_model = svc_2

                    else:
                        full_model = SVC(probability=True, kernel='rbf', C=10)
                        naive_model = SVC(probability=True, kernel='rbf', C=10)

                elif np.isin(deconf_method, ['predictability_nonlin_ensemble',
                                             'predictability_nonlin_ensemble_nodeconf']):
                    full_model = RandomForestClassifier(n_estimators=n_estimators, random_state=fold,
                                                        max_depth=max_depth,
                                                        min_samples_split=min_samples_split,
                                                        min_samples_leaf=min_samples_leaf,
                                                        max_features=max_features,
                                                        bootstrap=True)
                    naive_model = RandomForestClassifier(n_estimators=n_estimators, random_state=fold,
                                                         max_depth=max_depth,
                                                         min_samples_split=min_samples_split,
                                                         min_samples_leaf=min_samples_leaf,
                                                         max_features=max_features,
                                                         bootstrap=True)

                full_model.fit(features_full_train, y_train)
                naive_model.fit(features_naive_train, y_train)

                # try:
                #     print('Full model best C: {}'.format(full_model.best_params_['C']))
                #     print('Full model kernel: {}'.format(full_model.best_params_['kernel']))
                #     print('Naive model best C: {}'.format(naive_model.best_params_['C']))
                #     print('Naive model best kernel: {}'.format(naive_model.best_params_['kernel']))
                #
                # except:
                #     pass

                y_pred_proba_full = full_model.predict_proba(features_full_test)
                y_pred_proba_naive = naive_model.predict_proba(features_naive_test)

                fold_accuracy_full = balanced_accuracy_score(y_test, full_model.predict(features_full_test))
                fold_accuracy_naive = balanced_accuracy_score(y_test, naive_model.predict(features_naive_test))

                kfold_accuracy_full.append(fold_accuracy_full)
                kfold_accuracy_naive.append(fold_accuracy_naive)

                log_likelihood_full = log2_likelihood(y_test, y_pred_proba_full)
                log_likelihood_naive = log2_likelihood(y_test, y_pred_proba_naive)
                # print(fold_accuracy_full)
                # print(log_likelihood_full)
                # print(log_likelihood_naive)

                llratio = log_likelihood_full - log_likelihood_naive
                llratios.append(llratio)

            accuracy_full = np.mean(kfold_accuracy_full)
            accuracy_naive = np.mean(kfold_accuracy_naive)
            predictability = np.mean(llratios)
            # print(accuracy_full)
            # print(accuracy_naive)
            # print(predictability)
            #predictabilities.append(predictability)

            mean_score = predictability
            mean_scores.append(mean_score)

        mean_score = np.mean(mean_scores)

    if run_spisak_full_test:

        if np.unique(confound).shape[0] == 1 :
            confound += np.random.normal(0, 0.01, confound.shape[0])

        full_test = full_confound_test(target, predictions, confound,
                                       num_perms=num_perms_spisak,
                                       cat_y=True,
                                       cat_yhat=True,
                                       cat_c=False,
                                       mcmc_steps=50,
                                       cond_dist_method='gam',
                                       return_null_dist=False,
                                       random_state=92, progress=True,
                                       n_jobs=1)
        full_p = full_test.p
        full_sig = full_p < 0.05

    else :
        full_p = np.nan
        full_sig = False

    if run_spisak_partial_test:

        partial_test = partial_confound_test(target, predictions, confound,
                                             num_perms=num_perms_spisak,
                                             cat_y=True,
                                             cat_yhat=True,
                                             cat_c=False,
                                             mcmc_steps=50,
                                             cond_dist_method='gam',
                                             return_null_dist=False,
                                             random_state=92, progress=True,
                                             n_jobs=1)
        partial_p = partial_test.p
        partial_sig = partial_p < 0.05

    else :
        partial_p = np.nan
        partial_sig = False


    output = {'full_p' : full_p,
              'full_sig' : full_sig,
              'partial_p' : partial_p,
              'partial_sig' : partial_sig,
              'mean_score' : mean_score,
              'repeat_scores' : mean_scores,
              'method' : deconf_method}

    return output





# TODO LEGACY VERSION 1 (SIMULATIONS)
def decode(X, C, y,
           deconf_method,
           decoder,
           posthoc_counterbalancing_binsize=None,
           posthoc_counterbalancing_n_bins=None,
           posthoc_counterbalancing_already_binned=False,
           n_kfold_splits=3, min_trials=20,
           run_spisak_partial_test=False,
           run_spisak_full_test=False,
           num_perms_spisak=1000,
           standardize_features=True,
           standardize_confound=True,
           linear_regression_variation='ridge',
           tune_hyperpars_decoder=False,
           LR_predictability_penalty='l1',
           LR_predictability_solver='saga',
           LR_predictability_C=0.2,
           predictability_tune_regularization=False,
           LR_predictability_C_list=None, SVC_predictability_C_list=None,
           LR_decode_C_list=None, SVC_decode_C_list=None,
           LR_predictability_max_iter=7000, tune_C_predictability_nfolds=3,
           LR_decode_C=None, SVC_decode_C=None,
           SVC_predictability_C=None, confcv_min_sample_size=50, confcv_n_remove=1):
    # print(np.diff(y.index).max())

    methods = ['raw',
               'decode_from_confound',
               'predictability_lin',
               'predictability_nonlin',
               'predictability_lin_nodeconf',
               'predictability_nonlin_nodeconf',
               'distribution_match',
               'confound_isolating_cv',
               'linear_confound_regression',
               'nonlinear_confound_regression']

    assert np.isin(deconf_method, methods)

    decoders = ['logistic_regression', 'random_forest',
                'linear_discriminant_analysis', 'quadratic_discriminant_analysis',
                'ridge_regression', 'SVC', 'none']
    assert np.isin(decoder, decoders)


    total_n_trials = X.shape[0]
    if total_n_trials < 20 :
        raise NotEnoughTrialsError('Not enough trials to decode')


    # methods that influence the data distribution
    if deconf_method == 'distribution_match' :

        X, y, C = posthoc_counterbalancing(X, y, C, posthoc_counterbalancing_binsize,
                                           n_bins=posthoc_counterbalancing_n_bins,
                                           already_binned=posthoc_counterbalancing_already_binned)
        total_n_trials = X.shape[0]
        print('N trials after counterbalancing: {}'.format(X.shape[0]))

        kfold = StratifiedKFold(n_splits=n_kfold_splits, shuffle=True,
                                random_state=92)
        iterable = kfold.split(X, y)


    elif deconf_method == 'confound_isolating_cv' :

        if C.shape[1] > 1 :
            # if more than one confound, we only look at first
            # could look at the sum as well
            print('Confound has more than 1 dimension, matching '
                  'only the first dimension')
            C = C[:, 0].reshape(-1, 1)


        _, _, _, _, ids_test, ids_train = confound_isolating_cv(X, y, C[:, 0],
                                                                    random_seed=None,
                                                                    min_sample_size=confcv_min_sample_size,
                                                                    cv_folds=n_kfold_splits,
                                                                    n_remove=confcv_n_remove)

        total_n_trials = np.unique(np.hstack([ids_test, ids_train])).shape[0]

        iterable = zip(ids_train, ids_test)

    else:
        # normal data
        total_n_trials = X.shape[0]

        kfold = StratifiedKFold(n_splits=n_kfold_splits, shuffle=True,
                                random_state=92)
        iterable = kfold.split(X, y)


    if total_n_trials < 20:
        raise NotEnoughTrialsError('Not enough trials to decode')

    if np.isin(deconf_method, ['raw', 'distribution_match', 'confound_isolating_cv',
                               'linear_confound_regression',
                               'nonlinear_confound_regression',
                               'decode_from_confound']):
        # run the decoding
        kfold_scores = []
        y_test_all, y_pred_all, c_all = [], [], []

        for fold, (training_ind, testing_ind) in enumerate(iterable):

            # decoder = SGDClassifier(random_state=92)
            X_train = X[training_ind, :]
            X_test = X[testing_ind, :]
            y_train = y[training_ind]
            y_test = y[testing_ind]
            C_train = C[training_ind, :]
            C_test = C[testing_ind, :]

            # TODO scale before or after conf regression?
            if standardize_features:
                ss = StandardScaler()
                X_train = ss.fit_transform(X_train)
                X_test = ss.transform(X_test)

            if standardize_confound:
                ss = StandardScaler()
                C_train = ss.fit_transform(C_train)
                C_test = ss.transform(C_test)

            # Apply confound regression (linear regression)
            if deconf_method == 'linear_confound_regression':
                # for high-dimensional confounds, avoid overfitting on the
                # train set
                if linear_regression_variation == 'ridge':
                    reg = RidgeCV(alphas=[0.001, 0.1, 1, 10], cv=None, scoring=None, store_cv_values=False)
                    reg.fit(C_train, X_train)
                else:
                    reg = LinearRegression().fit(C_train, X_train)
                X_train = X_train  - reg.predict(C_train)
                X_test = X_test - reg.predict(C_test)

            # Apply confound regression (nonlinear variation)
            elif deconf_method == 'nonlinear_confound_regression' :
                reg = MultiOutputRegressor(SVR(C=1, kernel='rbf')).fit(C_train, X_train)
                # reg_nonlin = RandomForestRegressor(n_estimators=50).fit(C_train, X_train)
                if X.shape[1] == 1:
                    X_train = X_train - reg.predict(C_train).reshape(-1, 1)
                    X_test = X_test - reg.predict(C_test).reshape(-1, 1)
                elif X.shape[1] > 1:
                    X_train = X_train - reg.predict(C_train)
                    X_test = X_test - reg.predict(C_test)

            # DECODE
            if decoder == 'random_forest':
                if tune_hyperpars_decoder:
                    raise NotImplementedError
                model = RandomForestClassifier(n_estimators=200, random_state=92)

            elif decoder == 'logistic_regression':
                if tune_hyperpars_decoder:
                    parameters = {'C': LR_decode_C_list}
                    lr = LogisticRegression(penalty='l2')
                    inner_cv = StratifiedKFold(n_splits=3, shuffle=True, random_state=1)
                    model = GridSearchCV(lr, parameters, refit=True, scoring=make_scorer(balanced_accuracy_score),cv=inner_cv)
                else:
                    model = LogisticRegression(penalty='l2', C=LR_decode_C, random_state=1)

            elif decoder == 'linear_discriminant_analysis':
                if tune_hyperpars_decoder:
                    raise NotImplementedError
                model = LinearDiscriminantAnalysis()
            elif decoder == 'quadratic_discriminant_analysis':
                if tune_hyperpars_decoder:
                    raise NotImplementedError
                model = QuadraticDiscriminantAnalysis()
            elif decoder == 'ridge_regression':
                if tune_hyperpars_decoder:
                    raise NotImplementedError
                model = RidgeClassifier()
            elif decoder == 'SVC':
                if tune_hyperpars_decoder:
                    parameters = {'C': SVC_decode_C_list}
                    svc = SVC(probability=True, kernel='rbf', random_state=1)
                    inner_cv = StratifiedKFold(n_splits=3, shuffle=True, random_state=1)
                    model = GridSearchCV(svc, parameters, refit=True,
                                         scoring=make_scorer(balanced_accuracy_score), cv=inner_cv)
                else:
                    model = SVC(C=SVC_decode_C, kernel='rbf', random_state=1)
            else:
                raise ValueError

            if deconf_method == 'decode_from_confound':
                model.fit(C_train, y_train)
                y_pred = model.predict(C_test)

            else:
                model.fit(X_train, y_train)
                y_pred = model.predict(X_test)

            if tune_hyperpars_decoder:
                try:
                    best_C = model.best_params_['C']
                    #print('BEST C: {}'.format(best_C))
                except KeyError:
                    pass
            score = balanced_accuracy_score(y_test, y_pred)
            kfold_scores.append(score)
            y_test_all.append(y_test)
            y_pred_all.append(y_pred)
            c_all.append(C_test[:, 0])

        target = np.hstack(y_test_all)
        predictions = np.hstack(y_pred_all)
        confound = np.hstack(c_all)
        mean_score = np.mean(kfold_scores)

    # TODO this could be done in the same loop as above but it would get a bit dense
    elif np.isin(deconf_method, ['predictability_lin',
                                 'predictability_nonlin',
                                 'predictability_lin_nodeconf']):


        kfold_accuracy_full, kfold_accuracy_naive = [], []
        llratios = []

        for fold, (training_ind, testing_ind) in enumerate(iterable):

            X_train = X[training_ind, :]
            X_test = X[testing_ind, :]
            y_train = y[training_ind]
            y_test = y[testing_ind]
            C_train = C[training_ind, :]
            C_test = C[testing_ind, :]

            if standardize_features:
                ss = StandardScaler()
                X_train = ss.fit_transform(X_train)
                X_test = ss.transform(X_test)

            if standardize_confound:
                ss = StandardScaler()
                C_train = ss.fit_transform(C_train)
                C_test = ss.transform(C_test)


            if np.isin(deconf_method, ['predictability_lin',
                                       'predictability_nonlin']):
                features_full_train = np.hstack([X_train, C_train])
                features_full_test = np.hstack([X_test, C_test])
                features_naive_train = C_train
                features_naive_test = C_test

            if np.isin(deconf_method, ['predictability_lin_nodeconf',
                                       'predictability_nonlin_nodeconf']):
                features_full_train = X_train
                features_full_test = X_test
                features_naive_train = np.ones_like(C_train)
                features_naive_test = np.ones_like(C_test)

            if np.isin(deconf_method, ['predictability_lin',
                                       'predictability_lin_nodeconf']):
                if predictability_tune_regularization:
                    full_model = LogisticRegressionCV(penalty=LR_predictability_penalty,
                                                      solver=LR_predictability_solver,
                                                      Cs=LR_predictability_C_list,
                                                      max_iter=LR_predictability_max_iter,
                                                      cv=tune_C_predictability_nfolds,
                                                      random_state=1)
                    # naive_model = LogisticRegressionCV(penalty=lr_penalty,
                    #                                    solver=lr_solver,
                    #                                    Cs=Cs,
                    #                                    max_iter=max_iter,
                    #                                    cv=tune_C_nfolds)
                    # TODO naive model not regularized!
                    naive_model = LogisticRegression(penalty=None,
                                                     solver=LR_predictability_solver,
                                                     random_state=1, max_iter=LR_predictability_max_iter)

                else:
                    full_model = LogisticRegression(penalty=LR_predictability_penalty,
                                                    solver=LR_predictability_solver,
                                                    C=LR_predictability_C,
                                                    random_state=1,
                                                    max_iter=LR_predictability_max_iter)
                    # naive_model = LogisticRegression(penalty=lr_penalty,
                    #                                  solver=lr_solver,
                    #                                  C=C_reg)
                    # TODO naive model not regularized!
                    naive_model = LogisticRegression(penalty=None,
                                                     solver=LR_predictability_solver,
                                                     random_state=1,
                                                     max_iter=LR_predictability_max_iter)

            elif np.isin(deconf_method, ['predictability_nonlin',
                                         'predictability_nonlin_nodeconf']):

                # TODO old version used for simulation figures
                # if tune_regularization_predictability:
                #     parameters_1 = {'kernel': ('linear', 'rbf'), 'C': [0.01, 0.1, 1, 10]}
                #     svc_1 = SVC(probability=True)
                #     inner_cv_1 = StratifiedKFold(n_splits=3, shuffle=True, random_state=1)
                #     full_model = GridSearchCV(svc_1, parameters_1, refit=True, scoring=make_scorer(balanced_accuracy_score),
                #                               cv=inner_cv_1)
                #
                #     parameters = {'kernel': ('linear', 'rbf')}
                #     svc_2 = SVC(probability=True)
                #     inner_cv_2 = StratifiedKFold(n_splits=3, shuffle=True, random_state=1)
                #     naive_model = GridSearchCV(svc_2, parameters, refit=True, scoring=make_scorer(balanced_accuracy_score),
                #                                cv=inner_cv_2)

                if predictability_tune_regularization:
                    parameters_1 = {#'kernel': ('rbf'),
                                    'C': SVC_predictability_C_list}
                                    #'gamma' : ['scale', 0.0001, 0.001, 0.01, 0.1, 1]}
                    svc_1 = SVC(probability=True, gamma='scale', kernel='rbf')
                    inner_cv_1 = StratifiedKFold(n_splits=tune_C_predictability_nfolds, shuffle=True, random_state=1)
                    full_model = GridSearchCV(svc_1, parameters_1, refit=True,
                                              scoring=None,
                                              cv=inner_cv_1)
                    # TODO naive model not regularized
                    if False:
                        parameters_2 = {#'kernel': ('rbf'),
                                        'C': SVC_predictability_C_list}
                                        #'gamma': ['scale', 0.0001, 0.001, 0.01, 0.1, 1]}
                        svc_2 = SVC(probability=True, gamma='scale', kernel='rbf')
                        inner_cv_2 = StratifiedKFold(n_splits=tune_C_predictability_nfolds, shuffle=True, random_state=1)
                        naive_model = GridSearchCV(svc_2, parameters_2, refit=True, scoring=None,
                                                   cv=inner_cv_2)

                    naive_model = SVC(probability=True, kernel='rbf', C=SVC_predictability_C)

                    # svc_2 = SVC(probability=True, C=10, kernel='rbf')
                    # naive_model = svc_2

                else:
                    full_model = SVC(probability=True, kernel='rbf', C=SVC_predictability_C)
                    naive_model = SVC(probability=True, kernel='rbf', C=SVC_predictability_C)

            full_model.fit(features_full_train, y_train)
            naive_model.fit(features_naive_train, y_train)

            try:
                pass
                # print('Full model best C: {}'.format(full_model.best_params_['C']))
                # print('Full model kernel: {}'.format(full_model.best_params_['kernel']))
                # print('Naive model best C: {}'.format(naive_model.best_params_['C']))
                # print('Naive model best kernel: {}'.format(naive_model.best_params_['kernel']))

            except:
                pass

            y_pred_proba_full = full_model.predict_proba(features_full_test)
            y_pred_proba_naive = naive_model.predict_proba(features_naive_test)

            fold_accuracy_full = balanced_accuracy_score(y_test, full_model.predict(features_full_test))
            fold_accuracy_naive = balanced_accuracy_score(y_test, naive_model.predict(features_naive_test))

            kfold_accuracy_full.append(fold_accuracy_full)
            kfold_accuracy_naive.append(fold_accuracy_naive)

            log_likelihood_full = log2_likelihood(y_test, y_pred_proba_full)
            log_likelihood_naive = log2_likelihood(y_test, y_pred_proba_naive)
            # print(fold_accuracy_full)
            # print(log_likelihood_full)
            # print(log_likelihood_naive)

            llratio = log_likelihood_full - log_likelihood_naive
            llratios.append(llratio)

        accuracy_full = np.mean(kfold_accuracy_full)
        accuracy_naive = np.mean(kfold_accuracy_naive)
        predictability = np.mean(llratios)
        # print(accuracy_full)
        # print(accuracy_naive)
        # print(predictability)

        mean_score = predictability
        kfold_scores = llratios

    if run_spisak_full_test:

        if np.unique(confound).shape[0] == 1 :
            confound += np.random.normal(0, 0.01, confound.shape[0])

        full_test = full_confound_test(target, predictions, confound,
                                       num_perms=num_perms_spisak,
                                       cat_y=True,
                                       cat_yhat=True,
                                       cat_c=False,
                                       mcmc_steps=50,
                                       cond_dist_method='gam',
                                       return_null_dist=False,
                                       random_state=92, progress=True,
                                       n_jobs=1)
        full_p = full_test.p
        full_sig = full_p < 0.05

    else :
        full_p = np.nan
        full_sig = False

    if run_spisak_partial_test:

        partial_test = partial_confound_test(target, predictions, confound,
                                             num_perms=num_perms_spisak,
                                             cat_y=True,
                                             cat_yhat=True,
                                             cat_c=False,
                                             mcmc_steps=50,
                                             cond_dist_method='gam',
                                             return_null_dist=False,
                                             random_state=92, progress=True,
                                             n_jobs=1)
        partial_p = partial_test.p
        partial_sig = partial_p < 0.05

    else :
        partial_p = np.nan
        partial_sig = False


    output = {'full_p' : full_p,
              'full_sig' : full_sig,
              'partial_p' : partial_p,
              'partial_sig' : partial_sig,
              'mean_score' : mean_score,
              'kfold_scores' : kfold_scores,
              'method' : deconf_method}

    return output

# # TODO LEGACY VERSION 2
# def decode_old(X, C, y,
#            deconf_method,
#            decoder,
#            distribution_match_binsize=0.3,
#            already_binned=False,
#            n_kfold_splits=3, min_trials=20,
#            run_spisak_partial_test=False,
#            run_spisak_full_test=False,
#            num_perms_spisak=1000,
#            surrogate_method='shift', n_shifts=20, n_shuffles=100,
#            standardize_confound=True, lr_penalty='l1',
#            lr_solver='saga', C_reg=0.2, tune_C=False,
#            Cs=None, max_iter=7000, tune_C_nfolds=3):
#     # print(np.diff(y.index).max())
#
#     methods = ['raw',
#                'wool_deconf',
#                'wool_nodeconf',
#                'distribution_match',
#                'confound_isolating_cv',
#                'linear_confound_regression',
#                'nonlinear_confound_regression']
#
#     assert np.isin(deconf_method, methods)
#
#     decoders = ['logistic_regression', 'random_forest',
#                 'linear_discriminant_analysis', 'quadratic_discriminant_analysis',
#                 'ridge_regression']
#     assert np.isin(decoder, decoders)
#
#
#     total_n_trials = X.shape[0]
#     if total_n_trials < 20 :
#         raise Exception('Not enough trials to decode')
#
#     if np.isin(deconf_method, ['wool_deconf', 'wool_nodeconf']):
#
#         T = X.shape[0]
#         center_point = T // 2
#
#         index_list, X_list, C_list, y_list = [], [], [], []
#
#         if surrogate_method == 'shift' :
#             for s in range(-n_shifts, n_shifts + 1) :
#                 # print(s)
#                 X_shift = X[n_shifts + s :T - n_shifts + s, :]
#                 C_shift = C[n_shifts :T - n_shifts, :]
#                 y_shift = y[n_shifts :T - n_shifts]
#
#                 X_list.append(X_shift)
#                 C_list.append(C_shift)
#                 y_list.append(y_shift)
#                 index_list.append(s)
#
#         elif surrogate_method == 'shuffle' :
#             for s in range(n_shuffles + 1) :
#                 # print(s)
#                 if s == 0 :
#                     X_shuffle = X
#                 else :
#                     X_shuffle = np.random.default_rng(seed=92).permutation(X)
#
#                 X_list.append(X_shuffle)
#                 C_list.append(C)
#                 y_list.append(y)
#                 index_list.append(s)
#
#         surrogate_scores = []
#
#         for s, X_surr, C_surr, y_surr in zip(index_list, X_list, C_list,
#                                              y_list) :
#
#             # --- predictability with behavioral predictors ---
#             kfold = StratifiedKFold(n_splits=n_kfold_splits, shuffle=True,
#                                     random_state=92)
#             iterable = kfold.split(X_surr, y_surr)
#
#             kfold_scores = []
#             y_test_all, y_pred_full_all, y_pred_naive_all, c_all = [], [], [], []
#
#             kfold_accuracy_full, kfold_accuracy_naive = [], []
#             y_test_all, y_pred_proba_full_list, y_pred_proba_naive_list, c_all = [], [], [], []
#             log_likelihood_full_list = []
#             log_likelihood_naive_list = []
#             llratios = []
#
#             for fold, (training_ind, testing_ind) in enumerate(iterable) :
#
#                 # decoder = SGDClassifier(random_state=92)
#                 # decoder = RandomForestClassifier(n_estimators=300, random_state=92)
#
#                 X_train = X_surr[training_ind, :]
#                 X_test = X_surr[testing_ind, :]
#                 y_train = y_surr[training_ind]
#                 y_test = y_surr[testing_ind]
#                 C_train = C_surr[training_ind, :]
#                 C_test = C_surr[testing_ind, :]
#
#                 ss = StandardScaler()
#                 X_train = ss.fit_transform(X_train)
#                 X_test = ss.transform(X_test)
#
#                 if standardize_confound:
#                     ss = StandardScaler()
#                     C_train = ss.fit_transform(C_train)
#                     C_test = ss.transform(C_test)
#
#                 if deconf_method == 'wool_deconf' :
#                     # C_train = C_train * SCALE_C
#                     # C_test = C_test * SCALE_C
#                     # X_train = X_train * 0.1
#                     # X_test = X_test * 0.1
#                     features_full_train = np.hstack([X_train, C_train])
#                     features_full_test = np.hstack([X_test, C_test])
#
#                     features_naive_train = C_train
#                     features_naive_test = C_test
#
#                 elif deconf_method == 'wool_nodeconf' :
#                     features_full_train = X_train
#                     features_full_test = X_test
#
#                     features_naive_train = np.ones_like(C_train)
#                     features_naive_test = np.ones_like(C_test)
#
#                 if tune_C:
#                     full_model = LogisticRegressionCV(penalty=lr_penalty,
#                                                       solver=lr_solver,
#                                                       Cs=Cs,
#                                                       max_iter=max_iter,
#                                                       cv=tune_C_nfolds)
#                     # naive_model = LogisticRegressionCV(penalty=lr_penalty,
#                     #                                    solver=lr_solver,
#                     #                                    Cs=Cs,
#                     #                                    max_iter=max_iter,
#                     #                                    cv=tune_C_nfolds)
#                     # TODO naive model not regularized!
#                     naive_model = LogisticRegression(penalty=None,
#                                                      solver=lr_solver)
#
#
#                 else :
#                     full_model = LogisticRegression(penalty=lr_penalty,
#                                                     solver=lr_solver,
#                                                     C=C_reg)
#                     # naive_model = LogisticRegression(penalty=lr_penalty,
#                     #                                  solver=lr_solver,
#                     #                                  C=C_reg)
#                     # TODO naive model not regularized!
#                     naive_model = LogisticRegression(penalty=None,
#                                                      solver=lr_solver)
#
#                 full_model.fit(features_full_train, y_train)
#                 y_pred_proba_full = full_model.predict_proba(features_full_test)
#
#                 naive_model.fit(features_naive_train, y_train)
#                 y_pred_proba_naive = naive_model.predict_proba(
#                     features_naive_test)
#
#                 y_pred_full = full_model.predict(features_full_test)
#                 y_pred_naive = naive_model.predict(features_naive_test)
#                 fold_accuracy_full = balanced_accuracy_score(y_test,
#                                                              y_pred_full)
#                 fold_accuracy_naive = balanced_accuracy_score(y_test,
#                                                               y_pred_naive)
#
#                 y_test_all.append(y_test)
#                 y_pred_proba_full_list.append(y_pred_proba_full)
#                 y_pred_proba_naive_list.append(y_pred_proba_naive)
#                 c_all.append(C_test[:, 0])
#
#                 kfold_accuracy_full.append(fold_accuracy_full)
#                 kfold_accuracy_naive.append(fold_accuracy_naive)
#
#                 log_likelihood_full = log2_likelihood(y_test,
#                                                       y_pred_proba_full)
#                 log_likelihood_naive = log2_likelihood(y_test,
#                                                        y_pred_proba_naive)
#                 log_likelihood_full_list.append(log_likelihood_full)
#                 log_likelihood_naive_list.append(log_likelihood_naive)
#                 llratio = log_likelihood_full - log_likelihood_naive
#                 llratios.append(llratio)
#
#                 # score = balanced_accuracy_score(y_test, y_pred)
#                 # kfold_scores.append(score)
#                 y_test_all.append(y_test)
#                 y_pred_full_all.append(y_pred_full)
#                 y_pred_naive_all.append(y_pred_naive)
#                 c_all.append(C_test[:, 0])
#
#             target = np.hstack(y_test_all)
#             predictions_full = np.vstack(y_pred_proba_full_list)
#             predictions_naive = np.vstack(y_pred_proba_naive_list)
#             confound = np.hstack(c_all)
#
#             # log_likelihood_full = np.mean(log_likelihood_full_list)
#             # log_likelihood_naive = np.mean(log_likelihood_naive)
#             #
#             # llratio = log_likelihood_full - log_likelihood_naive
#             # predictability = llratio
#
#             accuracy_full = np.mean(kfold_accuracy_full)
#             accuracy_naive = np.mean(kfold_accuracy_naive)
#
#             # ratios
#             predictability = np.mean(llratios)
#
#
#             # print(mean_score)
#             if s == 0 :
#                 obs_score = predictability
#             else :
#                 surrogate_scores.append(predictability)
#
#         wool_q1, wool_q2 = pd.Series(surrogate_scores).quantile(q=[0.025, 0.975])
#         surrogate_scores = np.array(surrogate_scores)
#         n_higher = (surrogate_scores > obs_score).sum()
#
#         if surrogate_method == 'shift' :
#             wool_significant = n_higher == 0
#         elif surrogate_method == 'shuffle' :
#             p_val = n_higher / len(surrogate_scores)
#             wool_significant = p_val < 0.05
#
#         full_p = np.nan
#         full_sig = False
#         partial_p = np.nan
#         partial_sig = False
#
#         mean_score = obs_score
#
#
#     else:
#
#         # methods that influence the data distribution
#         if deconf_method == 'distribution_match' :
#
#             X, y, C = posthoc_counterbalancing(X, y, C, distribution_match_binsize,
#                                                already_binned=already_binned)
#             total_n_trials = X.shape[0]
#
#             kfold = StratifiedKFold(n_splits=n_kfold_splits, shuffle=True,
#                                     random_state=92)
#             iterable = kfold.split(X, y)
#
#         elif deconf_method == 'confound_isolating_cv' :
#
#             if C.shape[1] > 1 :
#                 # if more than one confound, we only look at first
#                 # could look at the sum as well
#                 print('Confound has more than 1 dimension, matching '
#                       'only the first dimension')
#                 C = C[:, 0].reshape(-1, 1)
#
#
#             _, _, _, _, ids_test, ids_train = confound_isolating_cv(X, y, C[:, 0],
#                                                                         random_seed=92,
#                                                                         min_sample_size=None,
#                                                                         cv_folds=n_kfold_splits,
#                                                                         n_remove=None)
#
#             total_n_trials = np.unique(np.hstack([ids_test, ids_train])).shape[0]
#
#             iterable = zip(ids_train, ids_test)
#
#         else:
#             # normal data
#             total_n_trials = X.shape[0]
#
#             kfold = StratifiedKFold(n_splits=n_kfold_splits, shuffle=True,
#                                     random_state=92)
#             iterable = kfold.split(X, y)
#
#
#         if total_n_trials < 20:
#             raise Exception('Not enough trials to decode')
#
#         # run the decoding
#         kfold_scores = []
#         y_test_all, y_pred_all, c_all = [], [], []
#
#         for fold, (training_ind, testing_ind) in enumerate(iterable) :
#             if fold == 0:
#                 print(testing_ind)
#             # decoder = SGDClassifier(random_state=92)
#             X_train = X[training_ind, :]
#             X_test = X[testing_ind, :]
#             y_train = y[training_ind]
#             y_test = y[testing_ind]
#             C_train = C[training_ind, :]
#             C_test = C[testing_ind, :]
#
#             # TODO scale before or after conf regression?
#             ss = StandardScaler()
#             X_train = ss.fit_transform(X_train)
#             X_test = ss.transform(X_test)
#
#             ss = StandardScaler()
#             C_train = ss.fit_transform(C_train)
#             C_test = ss.transform(C_test)
#
#             if deconf_method == 'linear_confound_regression' :
#                 reg = LinearRegression().fit(C_train, X_train)
#                 X_train = X_train - reg.predict(C_train)
#                 X_test = X_test - reg.predict(C_test)
#
#             elif deconf_method == 'nonlinear_confound_regression' :
#                 reg = RandomForestRegressor(n_estimators=300).fit(C_train, X_train)
#                 if X.shape[1] == 1:
#                     X_train = X_train - reg.predict(C_train).reshape(-1, 1)
#                     X_test = X_test - reg.predict(C_test).reshape(-1, 1)
#                 elif X.shape[1] > 1:
#                     X_train = X_train - reg.predict(C_train)
#                     X_test = X_test - reg.predict(C_test)
#
#             if decoder == 'random_forest':
#                 model = RandomForestClassifier(n_estimators=300, random_state=92)
#             elif decoder == 'logistic_regression':
#                 model = LogisticRegression()
#             elif decoder == 'linear_discriminant_analysis':
#                 model = LinearDiscriminantAnalysis()
#             elif decoder == 'quadratic_discriminant_analysis':
#                 model = QuadraticDiscriminantAnalysis()
#             elif decoder == 'ridge_regression':
#                 model = RidgeClassifier()
#
#             model.fit(X_train, y_train)
#             y_pred = model.predict(X_test)
#             score = balanced_accuracy_score(y_test, y_pred)
#             kfold_scores.append(score)
#             y_test_all.append(y_test)
#             y_pred_all.append(y_pred)
#             c_all.append(C_test[:, 0])
#
#         target = np.hstack(y_test_all)
#         predictions = np.hstack(y_pred_all)
#         confound = np.hstack(c_all)
#         mean_score = np.mean(kfold_scores)
#
#         if run_spisak_full_test:
#
#             if np.unique(confound).shape[0] == 1 :
#                 confound += np.random.normal(0, 0.01, confound.shape[0])
#
#             full_test = full_confound_test(target, predictions, confound,
#                                            num_perms=num_perms_spisak,
#                                            cat_y=True,
#                                            cat_yhat=True,
#                                            cat_c=False,
#                                            mcmc_steps=50,
#                                            cond_dist_method='gam',
#                                            return_null_dist=False,
#                                            random_state=92, progress=True,
#                                            n_jobs=1)
#             full_p = full_test.p
#             full_sig = full_p < 0.05
#
#         else :
#             full_p = np.nan
#             full_sig = False
#
#         if run_spisak_partial_test:
#
#             partial_test = partial_confound_test(target, predictions, confound,
#                                                  num_perms=num_perms_spisak,
#                                                  cat_y=True,
#                                                  cat_yhat=True,
#                                                  cat_c=False,
#                                                  mcmc_steps=50,
#                                                  cond_dist_method='gam',
#                                                  return_null_dist=False,
#                                                  random_state=92, progress=True,
#                                                  n_jobs=1)
#             partial_p = partial_test.p
#             partial_sig = partial_p < 0.05
#
#         else :
#             partial_p = np.nan
#             partial_sig = False
#         wool_q1 = None
#         wool_q2 = None
#         wool_significant = None
#
#
#     output = {'full_p' : full_p,
#               'full_sig' : full_sig,
#               'partial_p' : partial_p,
#               'partial_sig' : partial_sig,
#               'mean_score' : mean_score,
#               'kfold_scores' : kfold_scores,
#               'method' : deconf_method,
#               'wool_q1' : wool_q1,
#               'wool_q2' : wool_q2,
#               'wool_significant' : wool_significant}
#
#     return output