import os
import pickle
from sklearn.model_selection import StratifiedKFold, KFold, LeaveOneOut
from sklearn.metrics import balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
from constants import *
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
try:
    from mlconfound.stats import full_confound_test, partial_confound_test
except ModuleNotFoundError:
    print('Spisak tests not installed')

try:
    from confound_prediction.deconfounding import confound_isolating_cv
except ModuleNotFoundError:
    print('Confound isolating cv not installed')

from sklearn.svm import SVC, LinearSVC, SVR
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import balanced_accuracy_score, make_scorer
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from sklearn.linear_model import LinearRegression, RidgeCV
from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from sklearn.linear_model import RidgeClassifier, Ridge
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis, QuadraticDiscriminantAnalysis
from utils import *
from sklearn.gaussian_process import GaussianProcessRegressor
from plotting_style import *

settings_name = 'feb23_all_2'

session_ids = ['2018-08-13_14-31-39', '2018-08-14_14-30-15',
               '2018-08-15_14-13-50', '2020-01-16_11-18-35',
               '2021-04-20_18-27-54', '2021-04-29_15-55-12',
               '2019-03-12_11-28-33', '2019-03-13_13-01-19',
               '2019-03-08_14-39-42', '2019-03-12_15-41-39',
               '2019-03-13_15-32-33', '2019-04-11_11-12-47',
               '2019-12-13_12-46-26', '2019-12-13_09-59-24'] #'2018-08-15_14-13-50'

session_ids = ['2018-08-10_10-14-01', '2018-08-14_14-30-15',
               '2020-01-16_11-18-35', '2019-03-12_11-28-33',
               '2019-03-08_14-39-42', '2019-03-12_15-41-39',
               '2019-03-13_15-32-33', '2019-12-13_12-46-26',
               '2019-12-13_09-59-24']

session_ids = ['2019-03-13_15-32-33',
               '2019-12-13_12-46-26',
               '2019-03-08_14-39-42',]
experiment = 'audiofreq_in_big_change'
data_settings_name = 'aug22'
ephys_dataset = 'modid'
area_spikes_dataset = 'V1'
time_bin_indx = 3
n_repeats = 10
n_kfold_splits = 5

n_pca_components = 30
deconf_method = 'nonlinear_confound_regression'

decoder = 'SVC'


tune_hyperpars_decoder = True
standardize_features = True
standardize_confound = True

# RF parameters
n_estimators = 75
max_depth = None
min_samples_split = int(2)
min_samples_leaf = int(1)
max_features = 'sqrt'


data = load_decoding_data(settings_name=data_settings_name,
                          experiment_name=experiment,
                          dataset=ephys_dataset,
                          area_spikes=area_spikes_dataset)

X_trains, decoders_list = [], []
rs = pd.DataFrame(columns=['session_id', 'deconf_method', 'decoder', 'shuffle_components',
                           'n_pca_components', 'repeat', 'mean_score'])

for shuffle_components in [False, True]:

    for session_id in session_ids:
        print(session_id)

        X = data['data'][session_id]['binned_spikes'][time_bin_indx]
        C = data['data'][session_id]['binned_movement'][time_bin_indx]
        y = data['data'][session_id]['target']

        total_n_trials = X.shape[0]

        for repeat in range(n_repeats):

            # TODO every repeat shuffle C differently?
            if shuffle_components:
                if n_pca_components == 1:
                    C = np.random.default_rng(seed=repeat).permutation(C[:, 0]).reshape(-1, 1)
                else:
                    # C = C[:, 0:n_pca_components]
                    # for col in range(C.shape[1]):
                    #     C[:, col] = np.random.default_rng(seed=col+2).permutation(C[:, col])
                    indx = np.random.default_rng(seed=repeat).permutation(C.shape[0])
                    C = C[indx, 0:n_pca_components]

            else:
                if n_pca_components == 1:
                    C = C[:, 0].reshape(-1, 1)
                else:
                    C = C[:, 0:n_pca_components]

            kfold = StratifiedKFold(n_splits=n_kfold_splits, shuffle=True,
                                    random_state=repeat)
            iterable = kfold.split(X, y)

            # run the decoding
            kfold_scores = []
            y_test_all, y_pred_all, c_all = [], [], []

            for fold, (training_ind, testing_ind) in enumerate(iterable):

                # decoder = SGDClassifier(random_state=92)
                X_train = X[training_ind, :]
                X_test = X[testing_ind, :]
                y_train = y[training_ind]
                y_test = y[testing_ind]
                C_train = C[training_ind, :]
                C_test = C[testing_ind, :]

                # TODO scale before or after conf regression?
                if standardize_features:
                    ss = StandardScaler()
                    X_train = ss.fit_transform(X_train)
                    X_test = ss.transform(X_test)

                if standardize_confound:
                    ss = StandardScaler()
                    C_train = ss.fit_transform(C_train)
                    C_test = ss.transform(C_test)

                # Apply confound regression (linear regression)
                if deconf_method == 'linear_confound_regression':
                    # for high-dimensional confounds, avoid overfitting on the
                    # train set
                    reg = LinearRegression().fit(C_train, X_train)
                    X_train = X_train - reg.predict(C_train)
                    X_test = X_test - reg.predict(C_test)

                elif deconf_method == 'linear_confound_regression_reg':
                    # LOO so no random state
                    reg = RidgeCV(alphas=[0.0001, 0.001, 0.1], cv=None,
                                  scoring=None, store_cv_values=False)

                    reg = Ridge(alpha=0.01, random_state=fold)
                    reg.fit(C_train, X_train)
                    X_train = X_train - reg.predict(C_train)
                    X_test = X_test - reg.predict(C_test)

                # Apply confound regression (nonlinear variation)
                elif deconf_method == 'nonlinear_confound_regression':
                    if X.shape[1] == 1:
                        reg = RandomForestRegressor(n_estimators=n_estimators, random_state=fold,
                                                     max_depth=max_depth,
                                                     min_samples_split=min_samples_split,
                                                     min_samples_leaf=min_samples_leaf,
                                                     max_features=max_features)

                        reg.fit(C_train, X_train.flatten())
                        # reg = SVC().fit(C_train, X_train.flatten())
                        X_train = X_train - reg.predict(C_train).reshape(-1, 1)
                        X_test = X_test - reg.predict(C_test).reshape(-1, 1)
                    elif X.shape[1] > 1:
                        #reg = RandomForestRegressor(n_estimators=50, random_state=92)
                        reg = RandomForestRegressor(n_estimators=n_estimators, random_state=fold,
                                                     max_depth=max_depth,
                                                     min_samples_split=min_samples_split,
                                                     min_samples_leaf=min_samples_leaf,
                                                     max_features=max_features)
                        # reg = SVR(C=1)
                        reg.fit(C_train, X_train)
                        X_train = X_train - reg.predict(C_train)
                        X_test = X_test - reg.predict(C_test)

                        # for col in range(X.shape[1]):
                        #     reg = SVR(C=1, kernel='rbf')
                        #     reg.fit(C_train, X_train[:, col])
                        #     X_train[:, col] = X_train[:, col] - reg.predict(C_train)
                        #     X_test[:, col] = X_test[:, col] - reg.predict(C_test)

                X_trains.append(X_train)
                decoders_list.append(decoder)
                # DECODE

                if decoder == 'random_forest':
                    if tune_hyperpars_decoder:
                        raise NotImplementedError
                    #model = RandomForestClassifier(n_estimators=50, random_state=92)
                    model = RandomForestClassifier(n_estimators=50, random_state=fold,
                                                 max_depth=5,
                                                 min_samples_split=min_samples_split,
                                                 min_samples_leaf=5,
                                                 max_features=max_features)

                elif decoder == 'logistic_regression':
                    if tune_hyperpars_decoder:
                        parameters = {'C': [0.00001, 0.0001, 0.001, 0.01]}
                        lr = LogisticRegression(penalty='l2')
                        inner_cv = StratifiedKFold(n_splits=3, shuffle=True, random_state=fold)
                        model = GridSearchCV(lr, parameters, refit=True, scoring=None, #make_scorer(balanced_accuracy_score)
                                             cv=inner_cv)
                    else:
                        model = LogisticRegression(penalty='l2', C=0.001, random_state=92)

                elif decoder == 'linear_discriminant_analysis':
                    if tune_hyperpars_decoder:
                        raise NotImplementedError
                    model = LinearDiscriminantAnalysis()
                elif decoder == 'quadratic_discriminant_analysis':
                    if tune_hyperpars_decoder:
                        raise NotImplementedError
                    model = QuadraticDiscriminantAnalysis()
                elif decoder == 'ridge_regression':
                    if tune_hyperpars_decoder:
                        raise NotImplementedError
                    model = RidgeClassifier()
                elif decoder == 'SVC':
                    if tune_hyperpars_decoder:
                        parameters = {'kernel' : ['linear', 'rbf'],
                                      'C': [0.00001, 0.0001, 0.001, 0.01, 0.1, 1, 10],
                                      'gamma':['scale', 'auto']}
                        svc = SVC(probability=True)
                        inner_cv = StratifiedKFold(n_splits=5, shuffle=True, random_state=fold)
                        model = GridSearchCV(svc, parameters, refit=True,
                                             scoring=None, cv=inner_cv)

                        parameters = {'kernel' : ['linear', 'rbf'],
                                      'C': [0.00001, 0.0001, 0.001, 0.01, 0.1, 1, 10],
                                      'gamma':['scale', 'auto']}
                        svc = SVC(probability=True)
                        inner_cv = StratifiedKFold(n_splits=5, shuffle=True, random_state=fold)
                        model = GridSearchCV(svc, parameters, refit=True,
                                             scoring=None, cv=inner_cv)

                    else:
                        model = SVC(kernel='rbf', C=0.1, random_state=92)
                else:
                    raise ValueError

                if deconf_method == 'decode_from_confound':
                    model.fit(C_train, y_train)
                    y_pred = model.predict(C_test)

                else:
                    model.fit(X_train, y_train)
                    y_pred = model.predict(X_test)

                if tune_hyperpars_decoder:
                    try:
                        best_C = model.best_params_['C']
                        best_gamma = model.best_params_['gamma']
                        print('Model: {} - BEST C: {} - BEST GAMMA: {}'.format(decoder, best_C, best_gamma))
                    except KeyError:
                        pass
                score = balanced_accuracy_score(y_test, y_pred)
                kfold_scores.append(score)
                y_test_all.append(y_test)
                y_pred_all.append(y_pred)
                c_all.append(C_test[:, 0])

            target = np.hstack(y_test_all)
            predictions = np.hstack(y_pred_all)
            confound = np.hstack(c_all)
            mean_score = np.mean(kfold_scores)

            rs.loc[rs.shape[0], :] = [session_id, deconf_method, decoder, shuffle_components,
                                      n_pca_components,
                                      repeat, mean_score]


rs['mean_score'] = pd.to_numeric(rs['mean_score'])
rm = rs.groupby(['session_id', 'deconf_method', 'decoder',
            'shuffle_components', 'n_pca_components']).mean().reset_index()

print(rm[['session_id', 'shuffle_components', 'mean_score']])

# rs['mean_score'] = pd.to_numeric(rs['mean_score'])
#
# output_folder = os.path.join(DATA_PATH, 'test_CR',
#                              'SIMULATION_{}'.format(settings_name))
#
# if not os.path.isdir(output_folder):
#     os.makedirs(output_folder)
#
# pars = {'session_ids': session_ids,
#         'data_settings_name': data_settings_name,
#         'ephys_dataset': ephys_dataset,
#         'area_spikes_dataset': area_spikes_dataset,
#         'time_bin_indx': time_bin_indx,
#         'n_repeats': n_repeats,
#         'n_kfold_splits': n_kfold_splits,
#         'n_pca_components': n_pca_components,
#         'deconf_methods': deconf_methods,
#         'decoders' : decoders,
#         'n_pca_components_list' : n_pca_components_list,
#         'standardize_features': standardize_features,
#         'standardize_confound': standardize_confound,
#         'n_estimators' : n_estimators,
#         'max_depth' : max_depth,
#         'min_samples_split' : min_samples_split,
#         'min_samples_leaf' : min_samples_leaf,
#         'max_features' : max_features}
#
# out = {'pars' : pars,
#        'df' : rs}
#
# if shuffle_components:
#     extra_string = 'shuffledC'
# else:
#     extra_string = 'trueC'
#
# output_full_path = os.path.join(output_folder, 'test_n_components_results_{}_{}.pkl'.format(settings_name, extra_string))
# print('Saving simulation to {}'.format(output_full_path))
# pickle.dump(out, open(output_full_path, 'wb'))




