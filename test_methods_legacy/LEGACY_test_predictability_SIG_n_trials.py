import os
import pickle
from sklearn.model_selection import StratifiedKFold, KFold, LeaveOneOut
from sklearn.metrics import balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
from constants import *
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
try:
    from mlconfound.stats import full_confound_test, partial_confound_test
except ModuleNotFoundError:
    print('Spisak tests not installed')

try:
    from confound_prediction.deconfounding import confound_isolating_cv
except ModuleNotFoundError:
    print('Confound isolating cv not installed')

from sklearn.model_selection import cross_val_predict
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from sklearn.linear_model import LinearRegression, RidgeCV
from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from sklearn.svm import SVC, LinearSVC
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import balanced_accuracy_score, make_scorer
from sklearn.ensemble import AdaBoostClassifier
from utils import *
from plotting_style import *
from scipy.stats import mannwhitneyu, bootstrap


"""
Test the predictability for a given session with a fixed number of PCA components
but increasing the number of trials available.
"""

# TODO slightly different parameters of the decoders to make a bit faster...

settings_name = 'feb23_all_repeats'

session_ids = ['2018-08-10_10-14-01', '2018-08-14_14-30-15',
               '2020-01-16_11-18-35', '2019-03-12_11-28-33',
               '2019-03-08_14-39-42', '2019-03-12_15-41-39',
               '2019-03-13_15-32-33', '2019-12-13_12-46-26',
               '2019-12-13_09-59-24']

# session_ids = ['2019-03-12_15-41-39',
#                '2019-03-13_15-32-33',
#                '2019-12-13_12-46-26']

experiment = 'audiofreq_in_big_change'
data_settings_name = 'aug22'
ephys_dataset = 'modid'
area_spikes_dataset = 'V1'
time_bin_indx = 3
shuffle_components_list = [False, True]
shuffle_components_list = [True]
n_kfold_splits = 5
n_pca_components = 30
n_repeats = 10
n_permutations = 100
max_n_trials = 70
deconf_methods = ['predictability_lin',
                  'predictability_nonlin',
                  'predictability_nonlin_ensemble']

tune_regularization_predictability = True
standardize_features = True
standardize_confound = True
lr_penalty = 'l2'#'l2' 'elasticnet' is slower
lr_solver = 'saga' # 'saga'
Cs = [0.0001, 0.001, 0.01, 0.1, 1]
l1_ratios = [0, 0.25, 0.5, 0.75, 1]
max_iter = 6000
tune_C_nfolds = 3
C_reg_notune = 1

# RF parameters
n_estimators = 50
max_depth = 3
min_samples_split = int(2)
min_samples_leaf = int(7)
max_features = 'sqrt'


data = load_decoding_data(settings_name=data_settings_name,
                          experiment_name=experiment,
                          dataset=ephys_dataset,
                          area_spikes=area_spikes_dataset)

for shuffle_components in shuffle_components_list:

    rs = pd.DataFrame(columns=['session_id', 'deconf_method', 'decoder',
                               'n_pca_components', 'n_trials', 'repeat',
                               'permuted', 'mean_score'])

    n_trials_dict = {}
    for session_id in session_ids:

        for deconf_method in deconf_methods:
            print('Session {} - {}'.format(session_id, deconf_method))

            # get decoding data
            X_full = data['data'][session_id]['binned_spikes'][time_bin_indx]
            C_full = data['data'][session_id]['binned_movement'][time_bin_indx]
            y_full = data['data'][session_id]['target']
            total_n_trials = X_full.shape[0]
            n_trials_dict[session_id] = np.arange(30, max_n_trials+1, 10)

            for n_trials_draw in n_trials_dict[session_id]:
                print('# trials: {}'.format(n_trials_draw))
                if n_trials_draw >= total_n_trials:
                    print('Not enought trials, skipping')
                    continue
                accuracies_full, accuracies_naive = [], []
                predictabilities = []

                print('Tot: {}, drawn {}'.format(total_n_trials, n_trials_draw))

                for repeat in range(n_repeats):
                    print('Repeat {}'.format(repeat))

                    indx0 = np.where(y_full==0)[0]
                    indx1 = np.where(y_full==1)[0]

                    n_pick = int(n_trials_draw / 2)
                    indx0_sel = np.random.default_rng(seed=repeat).choice(indx0, size=n_pick, replace=False)
                    indx1_sel = np.random.default_rng(seed=repeat).choice(indx1, size=n_pick, replace=False)
                    assert set(indx0_sel).intersection(set(indx1_sel)).__len__() == 0
                    indx_sub = np.sort(np.hstack([indx0_sel, indx1_sel]))

                    X = X_full[indx_sub, :]
                    C = C_full[indx_sub, :]
                    y = y_full[indx_sub]

                    # TODO every repeat shuffle C differently?
                    if shuffle_components:
                        if n_pca_components == 1:
                            C = np.random.default_rng(seed=repeat).permutation(C[:, 0]).reshape(-1, 1)
                        else:
                            # C = C[:, 0:n_pca_components]
                            # for col in range(C.shape[1]):
                            #     C[:, col] = np.random.default_rng(seed=col+2).permutation(C[:, col])
                            indx = np.random.default_rng(seed=repeat).permutation(C.shape[0])
                            C = C[indx, 0:n_pca_components]

                    else:
                        if n_pca_components == 1:
                            C = C[:, 0].reshape(-1, 1)
                        else:
                            C = C[:, 0:n_pca_components]

                    for nperm in range(n_permutations):
                        if nperm == 0:
                            C_dec = np.copy(C)
                            y_dec = np.copy(y)
                            permuted = False
                        elif nperm > 0:
                            random_index =  np.random.default_rng(seed=nperm).permutation(np.arange(X.shape[0]))
                            # only if method is predictability we permute C together with y
                            if np.isin(deconf_method, predictability_methods):
                                C_dec = C[random_index, :]
                            else:
                                C_dec = np.copy(C)

                            y_dec = y[random_index]
                            permuted = True
                        if nperm == 25:
                            print('--- n_perms = 25')
                        if nperm == 50:
                            print('--- n_perms = 50')
                        if nperm == 75:
                            print('--- n_perms = 75')

                        kfold = StratifiedKFold(n_splits=n_kfold_splits, shuffle=True,
                                                random_state=10)
                        #kfold = LeaveOneOut()
                        iterable = kfold.split(X, y_dec)

                        # kfold_accuracy_full, kfold_accuracy_naive = [], []
                        llratios = []
                        kfold_accuracy_full, kfold_accuracy_naive = [], []
                        for fold, (training_ind, testing_ind) in enumerate(iterable):

                            X_train = X[training_ind, :]
                            X_test = X[testing_ind, :]
                            y_train = y_dec[training_ind]
                            y_test = y_dec[testing_ind]
                            C_train = C_dec[training_ind, :]
                            C_test = C_dec[testing_ind, :]

                            if standardize_features:
                                ss = StandardScaler()
                                X_train = ss.fit_transform(X_train)
                                X_test = ss.transform(X_test)

                            if standardize_confound:
                                ss = StandardScaler()
                                C_train = ss.fit_transform(C_train)
                                C_test = ss.transform(C_test)

                            features_full_train = np.hstack([X_train, C_train])
                            features_full_test = np.hstack([X_test, C_test])
                            features_naive_train = C_train
                            features_naive_test = C_test

                            # if np.isin(deconf_method, ['predictability_lin_nodeconf',
                            #                            'predictability_nonlin_nodeconf']):
                            #     features_full_train = X_train
                            #     features_full_test = X_test
                            #     features_naive_train = np.ones_like(C_train)
                            #     features_naive_test = np.ones_like(C_test)

                            if np.isin(deconf_method, ['predictability_lin',
                                                       'predictability_lin_nodeconf']):
                                if tune_regularization_predictability:
                                    full_model = LogisticRegressionCV(penalty=lr_penalty,
                                                                      solver=lr_solver,
                                                                      Cs=Cs,
                                                                      #l1_ratios=l1_ratios,
                                                                      max_iter=max_iter,
                                                                      cv=tune_C_nfolds,
                                                                      random_state=fold)
                                    naive_model = LogisticRegressionCV(penalty=lr_penalty,
                                                                       solver=lr_solver,
                                                                       Cs=Cs,
                                                                       #l1_ratios=l1_ratios,
                                                                       max_iter=max_iter,
                                                                       cv=tune_C_nfolds,
                                                                       random_state=fold)
                                    # TODO naive model not regularized!
                                    # naive_model = LogisticRegression(penalty=None,
                                    #                                  solver=lr_solver,
                                    #                                  random_state=fold)

                                else:
                                    full_model = LogisticRegression(penalty=lr_penalty,
                                                                    solver=lr_solver,
                                                                    C=C_reg_notune,
                                                                    random_state=fold)
                                    # naive_model = LogisticRegression(penalty=lr_penalty,
                                    #                                  solver=lr_solver,
                                    #                                  C=C_reg)
                                    # TODO naive model not regularized!
                                    naive_model = LogisticRegression(penalty=None,
                                                                     solver=lr_solver,
                                                                     random_state=fold)

                            elif np.isin(deconf_method, ['predictability_nonlin',
                                                         'predictability_nonlin_nodeconf']):

                                if tune_regularization_predictability:
                                    # parameters_1 =  { 'kernel': ('linear', 'rbf'),
                                    #     'C': [0.00001, 0.0001, 0.001, 0.01, 0.1, 1, 10]}

                                    parameters_1 = {'C': [0.00001, 0.0001, 0.001, 0.01, 0.1, 1, 10]}
                                    # 'gamma' : ['scale', 0.0001, 0.001, 0.01, 0.1, 1]}
                                    svc_1 = SVC(probability=True, kernel='rbf', gamma='scale', random_state=fold)
                                    inner_cv_1 = StratifiedKFold(n_splits=3, shuffle=True, random_state=1)
                                    full_model = GridSearchCV(svc_1, parameters_1, refit=True, scoring=None,
                                                              cv=inner_cv_1)

                                    # parameters_2 = { 'kernel': ('linear','rbf'),
                                    #     'C': [0.00001, 0.0001, 0.001, 0.01, 0.1, 1, 10]}
                                    parameters_2 = {'C': [0.00001, 0.0001, 0.001, 0.01, 0.1, 1, 10]}
                                    # 'gamma': ['scale', 0.0001, 0.001, 0.01, 0.1, 1]}
                                    svc_2 = SVC(probability=True, kernel='rbf', gamma='scale', random_state=fold)
                                    inner_cv_2 = StratifiedKFold(n_splits=3, shuffle=True, random_state=1)
                                    naive_model = GridSearchCV(svc_2, parameters_2, refit=True, scoring=None,
                                                               cv=inner_cv_2)


                                    # svc_2 = SVC(probability=True, C=10, kernel='rbf')
                                    # naive_model = svc_2
                                else:
                                    raise ValueError
                            elif deconf_method == 'predictability_nonlin_ensemble':
                                    full_model = RandomForestClassifier(n_estimators=n_estimators, random_state=fold,
                                                                        max_depth=max_depth,
                                                                        min_samples_split=min_samples_split,
                                                                        min_samples_leaf=min_samples_leaf,
                                                                        max_features=max_features)
                                    naive_model = RandomForestClassifier(n_estimators=n_estimators, random_state=fold,
                                                                         max_depth=max_depth,
                                                                         min_samples_split=min_samples_split,
                                                                         min_samples_leaf=min_samples_leaf,
                                                                         max_features=max_features)

                            full_model.fit(features_full_train, y_train)
                            naive_model.fit(features_naive_train, y_train)

                            y_pred_proba_full = full_model.predict_proba(features_full_test)
                            y_pred_proba_naive = naive_model.predict_proba(features_naive_test)

                            fold_accuracy_full = balanced_accuracy_score(y_test, full_model.predict(features_full_test))
                            fold_accuracy_naive = balanced_accuracy_score(y_test, naive_model.predict(features_naive_test))

                            kfold_accuracy_full.append(fold_accuracy_full)
                            kfold_accuracy_naive.append(fold_accuracy_naive)

                            log_likelihood_full = log2_likelihood(y_test, y_pred_proba_full)
                            log_likelihood_naive = log2_likelihood(y_test, y_pred_proba_naive)
                            llratio = log_likelihood_full - log_likelihood_naive
                            llratios.append(llratio)

                            # print('\nAccuracy full: {:.3f}'.format(fold_accuracy_full))
                            # print('Accuracy naiv: {:.3f}'.format(fold_accuracy_naive))
                            # print('Loglikelihood full: {:.3f}'.format(log_likelihood_full))
                            # print('Loglikelihood naive: {:.3f}'.format(log_likelihood_naive))
                            # print('LL ratio: {:.3f}'.format(llratio))
                            #
                            # try:
                            #     print('Full model best C: {}'.format(full_model.best_params_['C']))
                            #     print('Full model kernel: {}'.format(full_model.best_params_['kernel']))
                            #     print('Naive model best C: {}'.format(naive_model.best_params_['C']))
                            #     print('Naive model best kernel: {}'.format(naive_model.best_params_['kernel']))
                            #
                            # except:
                            #     pass

                        accuracy_full = np.mean(kfold_accuracy_full)
                        accuracy_naive = np.mean(kfold_accuracy_naive)
                        predictability = np.mean(llratios)

                        accuracies_full.append(accuracy_full)
                        accuracies_naive.append(accuracy_naive)
                        predictabilities.append(predictability)

                        mean_score = predictability
                        kfold_scores = llratios

                        rs.loc[rs.shape[0], :] = [session_id, deconf_method, 'none', n_pca_components,
                                                  n_trials_draw, repeat, permuted, mean_score]


    output_folder = os.path.join(DATA_PATH, 'test_predictability',
                                 'SIMULATION_{}'.format(settings_name))

    if not os.path.isdir(output_folder):
        os.makedirs(output_folder)

    pars = {'session_ids': session_ids,
            'data_settings_name': data_settings_name,
            'ephys_dataset': ephys_dataset,
            'area_spikes_dataset': area_spikes_dataset,
            'time_bin_indx': time_bin_indx,
            'n_permutations': n_permutations,
            'n_repeats' : n_repeats,
            'n_kfold_splits': n_kfold_splits,
            'n_pca_components': n_pca_components,
            'deconf_methods': deconf_methods,
            'n_trials_dict' : n_trials_dict,
            'tune_regularization_predictability': tune_regularization_predictability,
            'standardize_features': standardize_features,
            'standardize_confound': standardize_confound,
            'lr_penalty': lr_penalty,
            'lr_solver': lr_solver,
            'Cs': Cs,
            'max_iter': max_iter,
            'tune_C_nfolds': tune_C_nfolds,
            'C_reg_notune': C_reg_notune,
            'n_estimators' : n_estimators,
            'max_depth' : max_depth,
            'min_samples_split' : min_samples_split,
            'min_samples_leaf' : min_samples_leaf,
            'max_features' : max_features}

    out = {'pars' : pars,
           'df' : rs}

    if shuffle_components:
        extra_string = 'shuffledC'
    else:
        extra_string = 'trueC'

    output_full_path = os.path.join(output_folder, 'test_SIG_n_trials_results_{}_{}.pkl'.format(settings_name, extra_string))
    print('Saving simulation to {}'.format(output_full_path))
    pickle.dump(out, open(output_full_path, 'wb'))


