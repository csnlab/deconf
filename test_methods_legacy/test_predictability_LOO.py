import os
import pickle
from sklearn.model_selection import StratifiedKFold, KFold, LeaveOneOut
from sklearn.metrics import balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
from constants import *
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
try:
    from mlconfound.stats import full_confound_test, partial_confound_test
except ModuleNotFoundError:
    print('Spisak tests not installed')

try:
    from confound_prediction.deconfounding import confound_isolating_cv
except ModuleNotFoundError:
    print('Confound isolating cv not installed')

from sklearn.model_selection import cross_val_predict
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from sklearn.linear_model import LinearRegression, RidgeCV
from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from sklearn.svm import SVC
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import balanced_accuracy_score, make_scorer
from utils import *


session_id = '2018-08-13_14-31-39'#'2019-03-12_15-41-39'#'2019-12-13_12-46-26'
experiment = 'audiofreq_in_big_change'
data_settings_name = 'aug22'
ephys_dataset = 'modid'
area_spikes_dataset = 'V1'
time_bin_indx = 3
shuffle_components = True
n_repeats = 15
n_pca_components = 10
deconf_method = 'predictability_nonlin'
tune_regularization_predictability = True
tune_hyperpars_decoder = False
n_kfold_splits = 5
standardize_features = True
standardize_confound = True
lr_penalty = 'l1'
lr_solver = 'saga'
Cs = [0.0001, 0.001, 0.01, 0.1, 1]
max_iter = 3000
tune_C_nfolds = 3
C_reg = 1

data = load_decoding_data(settings_name=data_settings_name,
                          experiment_name=experiment,
                          dataset=ephys_dataset,
                          area_spikes=area_spikes_dataset)

# get decoding data
X = data['data'][session_id]['binned_spikes'][time_bin_indx]
C = data['data'][session_id]['binned_movement'][time_bin_indx]
y = data['data'][session_id]['target']

if shuffle_components:
    if n_pca_components == 1:
        C = np.random.default_rng(seed=0).permutation(C[:, 0]).reshape(-1, 1)
    else:
        #C = C[:, 0:n_pca_components]
        # for col in range(C.shape[1]):
        #     C[:, col] = np.random.default_rng(seed=col+2).permutation(C[:, col])
        indx = np.random.default_rng(seed=3).permutation(C.shape[0])
        C = C[indx, 0:n_pca_components]


else:
    if n_pca_components == 1:
        C = C[:, 0].reshape(-1, 1)
    else:
        C = C[:, 0:n_pca_components]

total_n_trials = X.shape[0]
accuracies_full, accuracies_naive = [], []
predictabilities = []


kfold = StratifiedKFold(n_splits=n_kfold_splits, shuffle=True,
                        random_state=repeat)
kfold = LeaveOneOut()
iterable = kfold.split(X, y)

# kfold_accuracy_full, kfold_accuracy_naive = [], []
llratios = []
kfold_accuracy_full, kfold_accuracy_naive = [], []

for fold, (training_ind, testing_ind) in enumerate(iterable):

    X_train = X[training_ind, :]
    X_test = X[testing_ind, :]
    y_train = y[training_ind]
    y_test = y[testing_ind]
    C_train = C[training_ind, :]
    C_test = C[testing_ind, :]

    sns.heatmap(X, ax=ax)

    if standardize_features:
        ss = StandardScaler()
        X_train = ss.fit_transform(X_train)
        X_test = ss.transform(X_test)

    if standardize_confound:
        ss = StandardScaler()
        C_train = ss.fit_transform(C_train)
        C_test = ss.transform(C_test)


    if np.isin(deconf_method, ['predictability_lin',
                               'predictability_nonlin']):
        features_full_train = np.hstack([X_train, C_train])
        features_full_test = np.hstack([X_test, C_test])
        features_naive_train = C_train
        features_naive_test = C_test

    if np.isin(deconf_method, ['predictability_lin_nodeconf',
                               'predictability_nonlin_nodeconf']):
        features_full_train = X_train
        features_full_test = X_test
        features_naive_train = np.ones_like(C_train)
        features_naive_test = np.ones_like(C_test)

    if np.isin(deconf_method, ['predictability_lin',
                               'predictability_lin_nodeconf']):
        if tune_regularization_predictability:
            full_model = LogisticRegressionCV(penalty=lr_penalty,
                                              solver=lr_solver,
                                              Cs=Cs,
                                              max_iter=max_iter,
                                              cv=tune_C_nfolds,
                                              random_state=fold)
            # naive_model = LogisticRegressionCV(penalty=lr_penalty,
            #                                    solver=lr_solver,
            #                                    Cs=Cs,
            #                                    max_iter=max_iter,
            #                                    cv=tune_C_nfolds)
            # TODO naive model not regularized!
            naive_model = LogisticRegression(penalty=None,
                                             solver=lr_solver,
                                             random_state=fold)

        else:
            full_model = LogisticRegression(penalty=lr_penalty,
                                            solver=lr_solver,
                                            C=C_reg,
                                            random_state=fold)
            # naive_model = LogisticRegression(penalty=lr_penalty,
            #                                  solver=lr_solver,
            #                                  C=C_reg)
            # TODO naive model not regularized!
            naive_model = LogisticRegression(penalty=None,
                                             solver=lr_solver,
                                             random_state=fold)

    elif np.isin(deconf_method, ['predictability_nonlin',
                                 'predictability_nonlin_nodeconf']):
        # full_model = QuadraticDiscriminantAnalysis()
        # naive_model = QuadraticDiscriminantAnalysis()
        # full_model = RandomForestClassifier(n_estimators=200)
        # naive_model = RandomForestClassifier(n_estimators=200)

        # TODO old version used for simulation figures
        # if tune_regularization_predictability:
        #     parameters_1 = {'kernel': ('linear', 'rbf'), 'C': [0.01, 0.1, 1, 10]}
        #     svc_1 = SVC(probability=True)
        #     inner_cv_1 = StratifiedKFold(n_splits=3, shuffle=True, random_state=1)
        #     full_model = GridSearchCV(svc_1, parameters_1, refit=True, scoring=make_scorer(balanced_accuracy_score),
        #                               cv=inner_cv_1)
        #
        #     parameters = {'kernel': ('linear', 'rbf')}
        #     svc_2 = SVC(probability=True)
        #     inner_cv_2 = StratifiedKFold(n_splits=3, shuffle=True, random_state=1)
        #     naive_model = GridSearchCV(svc_2, parameters, refit=True, scoring=make_scorer(balanced_accuracy_score),
        #                                cv=inner_cv_2)

        if tune_regularization_predictability:
            parameters_1 =  { 'kernel': ('linear', 'rbf'),
                'C': [0.00001, 0.0001, 0.001, 0.01, 0.1, 1, 10]}
            # 'gamma' : ['scale', 0.0001, 0.001, 0.01, 0.1, 1]}
            svc_1 = SVC(probability=True, gamma='scale', random_state=fold)
            inner_cv_1 = StratifiedKFold(n_splits=3, shuffle=True, random_state=1)
            full_model = GridSearchCV(svc_1, parameters_1, refit=True, scoring=None,
                                      cv=inner_cv_1)

            parameters_2 = { 'kernel': ('linear','rbf'),
                'C': [0.00001, 0.0001, 0.001, 0.01, 0.1, 1, 10]}
            # 'gamma': ['scale', 0.0001, 0.001, 0.01, 0.1, 1]}
            svc_2 = SVC(probability=True, gamma='scale', random_state=fold)
            inner_cv_2 = StratifiedKFold(n_splits=3, shuffle=True, random_state=1)
            naive_model = GridSearchCV(svc_2, parameters_2, refit=True, scoring=None,
                                       cv=inner_cv_2)

            # svc_2 = SVC(probability=True, C=10, kernel='rbf')
            # naive_model = svc_2

        else:
            full_model = SVC(probability=True, kernel='rbf', C=10)
            naive_model = SVC(probability=True, kernel='rbf', C=10)

    full_model.fit(features_full_train, y_train)
    naive_model.fit(features_naive_train, y_train)


    y_pred_proba_full = full_model.predict_proba(features_full_test)
    y_pred_proba_naive = naive_model.predict_proba(features_naive_test)

    fold_accuracy_full = balanced_accuracy_score(y_test, full_model.predict(features_full_test))
    fold_accuracy_naive = balanced_accuracy_score(y_test, naive_model.predict(features_naive_test))

    kfold_accuracy_full.append(fold_accuracy_full)
    kfold_accuracy_naive.append(fold_accuracy_naive)

    log_likelihood_full = log2_likelihood(y_test, y_pred_proba_full)
    log_likelihood_naive = log2_likelihood(y_test, y_pred_proba_naive)
    llratio = log_likelihood_full - log_likelihood_naive
    llratios.append(llratio)

    print('\nAccuracy full: {:.3f}'.format(fold_accuracy_full))
    print('Accuracy naiv: {:.3f}'.format(fold_accuracy_naive))
    print('Loglikelihood full: {:.3f}'.format(log_likelihood_full))
    print('Loglikelihood naive: {:.3f}'.format(log_likelihood_naive))
    print('LL ratio: {:.3f}'.format(llratio))

    try:
        print('Full model best C: {}'.format(full_model.best_params_['C']))
        print('Full model kernel: {}'.format(full_model.best_params_['kernel']))
        print('Naive model best C: {}'.format(naive_model.best_params_['C']))
        print('Naive model best kernel: {}'.format(naive_model.best_params_['kernel']))

    except:
        pass

accuracy_full = np.mean(kfold_accuracy_full)
accuracy_naive = np.mean(kfold_accuracy_naive)
predictability = np.mean(llratios)

accuracies_full.append(accuracy_full)
accuracies_naive.append(accuracy_naive)
predictabilities.append(predictability)

print('\nAccuracy full: {:.3f}'.format(accuracy_full))
print('\nAccuracy naive: {:.3f}'.format(accuracy_naive))
print('\nPredictability: {:.3f}'.format(predictability))

mean_score = predictability
kfold_scores = llratios

"""
Accuracy full: 0.627
Accuracy naive: 0.519
Predictability: 0.061
"""

print('\nFINAL\nAccuracy full: {:.3f}'.format(np.mean(accuracies_full)))
print('Accuracy naive: {:.3f}'.format(np.mean(accuracies_naive)))
print('Predictability: {:.3f}'.format(np.mean(predictabilities)))
