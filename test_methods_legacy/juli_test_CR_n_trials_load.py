import os
import pickle
from constants import *
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from utils import *
from plotting_style import *
from scipy.stats import bootstrap

settings_name = 'feb26'

plot_single_sessions_subplots = False
plot_single_sessions_singleplots = False
plot_sessions_combined_subplots = False
plot_sessions_combined_singleplots = False
plot_sessions_combined_singleplots_v2 = True
plot_sessions_combined = False
plot_kde = False
plot_dots = False
n_trials_all = 60

plot_methods = ['raw', 'linear_confound_regression_reg', 'nonlinear_confound_regression']


plot_format = 'png'
plots_folder = os.path.join(DATA_PATH, 'plots', 'test_CR_n_trials', settings_name)
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)

rss = []
for shuffle_components in [False, True]:
    if shuffle_components:
        extra_string = 'shuffledC'
    else:
        extra_string = 'trueC'

    output_folder = os.path.join(DATA_PATH, 'test_CR',
                                 'SIMULATION_{}'.format(settings_name))

    output_full_path = os.path.join(output_folder, 'test_n_trials_results_{}_{}.pkl'.format(settings_name, extra_string))
    res = pickle.load(open(output_full_path, 'rb'))

    rs = res['df']
    rs['shuffle_confound'] = shuffle_components
    rss.append(rs)
    pars = res['pars']
    session_ids = pars['session_ids']
    deconf_methods = [d for d in pars['deconf_methods'] if d in plot_methods]
    n_trials_dict = pars['n_trials_dict']
    decoders = pars['decoders']
    n_trials_list = np.unique(np.hstack([n_trials_dict[s] for s in session_ids]))


    if plot_single_sessions_subplots:
        for session_id in session_ids:
            rsel = rs[rs['session_id'] == session_id]
            f, ax = plt.subplots(1, 3, figsize=[10, 3], sharey=True)
            sns.lineplot(x='n_trials', y='mean_score', ax=ax[0], markers=True, marker='o',
                         hue='deconf_method', data=rsel[rsel['decoder']==decoders[0]], palette=method_palette)
            sns.lineplot(x='n_trials', y='mean_score', ax=ax[1], markers=True, marker='o',
                         hue='deconf_method', data=rsel[rsel['decoder']==decoders[1]], palette=method_palette)
            sns.lineplot(x='n_trials', y='mean_score', ax=ax[2], markers=True, marker='o',
                         hue='deconf_method', data=rsel[rsel['decoder']==decoders[2]], palette=method_palette)
            ax[0].set_title('Linear decoder')
            ax[1].set_title('Nonlinear decoder 1')
            ax[2].set_title('Nonlinear decoder 2')

            sns.despine()
            ax[0].set_ylabel('Mean accuracy')
            for axx in ax:
                axx.set_xlabel('# trials')
                axx.axhline(0.5, ls=':', c='grey')
                axx.legend().remove()
                axx.set_xticks(n_trials_dict[session_id])

            plot_name = 'lineplot_{}_{}.{}'.format(session_id, extra_string, plot_format)
            f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                      bbox_inches="tight")


    if plot_single_sessions_singleplots:
        for session_id in session_ids:
            for decoder in decoders:
                f, ax = plt.subplots(1, 1, figsize=[2.8, 2.2], sharey=True)

                for j, deconf_method in enumerate(deconf_methods):
                    x = []
                    err = []
                    for k, n_trials in enumerate(n_trials_dict[session_id]):

                        vals = rs[(rs['n_trials'] == n_trials) &
                                  (rs['decoder'] == decoder) &
                                  (rs['deconf_method'] == deconf_method) &
                                  (rs['session_id'] == session_id)]['mean_score'].__array__()
                        center = vals.mean()
                        print(vals.mean())

                        bootstrap_vals = (vals.__array__().reshape(-1, 1).astype(float),)
                        bres = bootstrap(bootstrap_vals,
                                         axis=0,
                                         statistic=np.mean,
                                         n_resamples=100,
                                         confidence_level=0.95,
                                         method='basic')
                        error = np.array([bres.confidence_interval.low[0], bres.confidence_interval.high[0]])
                        x.append(center)
                        err.append(error)
                    err = np.vstack(err)

                    ax.plot(n_trials_dict[session_id], x, c=method_palette[deconf_method], zorder=-5)
                    ax.scatter(n_trials_dict[session_id], x, s=30, c=method_palette[deconf_method],
                               edgecolor='w', label=method_labels[deconf_method])
                    ax.fill_between(n_trials_dict[session_id], err[:, 0], err[:, 1], alpha=0.3, zorder=-10,
                                    color=method_palette[deconf_method], linewidth=0)
                sns.despine()
                ax.set_ylabel('Mean accuracy')
                ax.set_xlabel('# trials')
                #ax.set_title(decoder_labels[decoder])
                ax.axhline(0.5, ls=':', c='grey')

                ax.set_ylim(0.3, 1)
                ax.legend().remove()
                if n_trials_dict[session_id].shape[0] > 6 and n_trials_dict[session_id].shape[0] < 12:
                    ax.set_xticks(n_trials_dict[session_id][::2])
                elif n_trials_dict[session_id].shape[0] >= 12:
                    ax.set_xticks(n_trials_dict[session_id][::6])
                else:
                    ax.set_xticks(n_trials_dict[session_id])
                plt.tight_layout()
                plot_name = 'lineplot_{}_{}_{}.{}'.format(session_id, extra_string, decoder, plot_format)
                f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                          bbox_inches="tight")


    if plot_sessions_combined_subplots:
        # plot all sessions together
        f, ax = plt.subplots(1, 3, figsize=[10, 3], sharey=True)
        for axix, decoder in enumerate(decoders):
            for j, deconf_method in enumerate(deconf_methods):
                for session_id in session_ids:
                    x = []
                    for k, n_trials in enumerate(n_trials_dict[session_id]):
                        vals = rs[(rs['n_trials'] == n_trials) &
                                  (rs['deconf_method'] == deconf_method) &
                                  (rs['decoder'] == decoder) &
                                  (rs['session_id'] == session_id)]['mean_score'].__array__()

                        center = vals.mean()
                        x.append(center)

                    ax[axix].plot(n_trials_dict[session_id], x, c=method_palette[deconf_method], zorder=-5)
                # ax.scatter(n_trials_dict[session_id], x, s=50, c=method_palette[deconf_method],
                #            edgecolor='w', label=method_labels[deconf_method])

        for axx in ax:
            axx.set_xlabel('# trials')
            axx.axhline(0.5, ls=':', c='grey')
            axx.legend().remove()

            if n_trials_dict[session_id].shape[0] > 6:
                axx.set_xticks(n_trials_dict[session_id][::2])
            elif n_trials_dict[session_id].shape[0] >= 12:
                axx.set_xticks(n_trials_dict[session_id][::3])
            else:
                axx.set_xticks(n_trials_dict[session_id])

            axx.set_xticks(np.array(n_trials_dict[session_id]))
            second_max = np.sort([n_trials_dict[s].max() for s in session_ids])[-2]
            axmin = np.sort([n_trials_dict[s].min() for s in session_ids])[0]
            axx.set_xlim([axmin, second_max])
            axx.set_ylim(0.3, 1)

        ax[0].set_ylabel('Mean accuracy')
        sns.despine()
        # ax.set_yticks([-0.5, -0.25, 0, 0.25, 0.5, 0.75, 1])
        plt.tight_layout()

        plot_name = 'allsess_lineplot_{}.{}'.format(extra_string, plot_format)
        f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                  bbox_inches="tight")


    if plot_sessions_combined_singleplots:
        # plot all sessions together
        for decoder in decoders:
            f, ax = plt.subplots(1, 1, figsize=[3, 3], sharey=True)
            for j, deconf_method in enumerate(deconf_methods):
                for session_id in session_ids:
                    x = []
                    for k, n_trials in enumerate(n_trials_dict[session_id]):
                        vals = rs[(rs['n_trials'] == n_trials) &
                                  (rs['deconf_method'] == deconf_method) &
                                  (rs['decoder'] == decoder) &
                                  (rs['session_id'] == session_id)]['mean_score'].__array__()

                        center = vals.mean()
                        x.append(center)

                    ax.plot(n_trials_dict[session_id], x, c=method_palette[deconf_method], zorder=-5)
                # ax.scatter(n_trials_dict[session_id], x, s=50, c=method_palette[deconf_method],
                #            edgecolor='w', label=method_labels[deconf_method])

            ax.set_xlabel('# trials')
            ax.axhline(0.5, ls=':', c='grey')
            ax.legend().remove()

            second_max = np.sort([n_trials_dict[s].max() for s in session_ids])[-2]
            axmin = np.sort([n_trials_dict[s].min() for s in session_ids])[0]
            ax.set_xticks(n_trials_dict[session_id][::3])
            ax.set_xlim([axmin, second_max])
            ax.set_ylim(0.3, 1)

            ax.set_ylabel('Mean accuracy')
            ax.set_title(decoder_labels[decoder])
            sns.despine()
            #ax.set_yticks([-0.5, -0.25, 0, 0.25, 0.5, 0.75, 1])
            plt.tight_layout()

            plot_name = 'allsess_lineplot_{}_{}.{}'.format(extra_string, decoder, plot_format)
            f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                      bbox_inches="tight")


    if plot_sessions_combined_singleplots_v2:
        # plot all sessions together
        for decoder in decoders:
            f, ax = plt.subplots(1, 1, figsize=[3, 2.2], sharey=True)
            for j, deconf_method in enumerate(deconf_methods):
                mean_score = {i : [] for i in n_trials_list}
                for session_id in session_ids:
                    x = []
                    for k, n_trials in enumerate(n_trials_dict[session_id]):
                        vals = rs[(rs['n_trials'] == n_trials) &
                                  (rs['deconf_method'] == deconf_method) &
                                  (rs['decoder'] == decoder) &
                                  (rs['session_id'] == session_id)]['mean_score'].__array__()

                        center = vals.mean()
                        x.append(center)
                        mean_score[n_trials].append(center)
                    ax.plot(n_trials_dict[session_id], x, c=method_palette[deconf_method], zorder=-5,
                            lw=0.7, alpha=0.6)

                mean_score = [np.mean(mean_score[m]) for m in n_trials_list]
                ax.plot(n_trials_list, mean_score, c=method_palette[deconf_method], zorder=-4,
                        lw=3)
                # ax.scatter(n_trials_dict[session_id], x, s=50, c=method_palette[deconf_method],
                #            edgecolor='w', label=method_labels[deconf_method])

            ax.set_xlabel('# trials')
            ax.axhline(0.5, ls=':', c='grey')
            ax.legend().remove()

            second_max = np.sort([n_trials_dict[s].max() for s in session_ids])[-2]
            axmin = np.sort([n_trials_dict[s].min() for s in session_ids])[0]
            ax.set_xticks(n_trials_dict[session_id])
            ax.set_xlim([axmin, second_max])
            ax.set_ylim(0.4, 0.85)

            ax.set_ylabel('Mean accuracy')
            #ax.set_title(decoder_labels[decoder])
            sns.despine()
            # ax.set_yticks([-0.5, -0.25, 0, 0.25, 0.5, 0.75, 1])
            plt.tight_layout()

            plot_name = 'allsess_lineplot_v2_{}_{}.{}'.format(extra_string, decoder, plot_format)
            f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                      bbox_inches="tight")


    if plot_kde:
        for axix, decoder in enumerate(decoders):

            f, ax = plt.subplots(1, 1, figsize=[1.5, 2.2], sharey=True)

            dfsel = rs[(rs['n_trials'] == n_trials_all) &
                       (rs['decoder'] == decoder)]
            sns.kdeplot(data=dfsel, hue='deconf_method', hue_order=deconf_methods, x='mean_score',
                        vertical=True, ax=ax, palette=method_palette)

            ax.legend().remove()
            sns.despine()
            ax.set_ylim(0.4, 0.85)
            ax.set_ylabel('')
            ax.axhline(0.5, ls=':', c='grey')
            plt.tight_layout()
            plot_name = 'kde_{}_{}.{}'.format(extra_string, decoder, plot_format)
            f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                      bbox_inches="tight")


rx = pd.concat(rss)

ylims2 = [0.4, 0.85]
yticks2 = [0.5, 0.6, 0.7, 0.8]
if plot_dots:
    for n_trials_plot in [40, 50, 60, 70, 80]:
        for decoder in decoders:
            for deconf_method in deconf_methods:
                f, ax = plt.subplots(1, 1, figsize=[2.1, 2.1], sharey=True)

                dfsel = rx[(rx['n_trials'] == n_trials_plot) &
                           (rx['deconf_method'] == deconf_method) &
                            (rx['decoder'] == decoder)]
                dfsel = dfsel.groupby(['session_id', 'shuffle_confound','decoder',
                                       'n_trials', 'deconf_method']).mean().reset_index()
                n_sessions = dfsel['session_id'].unique().shape[0]
                print('KDE comparison computed with {} sessions'.format(n_sessions))

                y1 = data = dfsel[dfsel['shuffle_confound'] == True]['mean_score'].__array__()
                y0 = data = dfsel[dfsel['shuffle_confound'] == False]['mean_score'].__array__()
                x0 = [0 for i in range(y0.shape[0])]
                x1 = [1 for i in range(y1.shape[0])]

                from scipy.stats import mannwhitneyu
                stat, p = mannwhitneyu(y0, y1)
                print('Method {}, p-value = {}'.format(deconf_method, p))

                ax.scatter(x0, y0, s=30, c=method_palette[deconf_method],
                            edgecolor='w')
                ax.scatter(x1, y1, s=30, c='w',
                            edgecolor=method_palette[deconf_method])
                for i in range(y0.shape[0]):
                    ax.plot([0, 1], [y0[i], y1[i]], ls='-', linewidth=1,
                            color=method_palette[deconf_method], zorder=-10)
                ax.legend().remove()
                sns.despine()
                ax.set_xlim([-0.1, 1.1])
                ax.set_xticks([0, 1])
                ax.set_ylim(ylims2)
                ax.set_yticks(yticks2)
                ax.set_xticklabels(['True C', 'Shuffled C'])
                #ax.set_title(method_labels[deconf_method].replace('\n', ' '))
                ax.set_ylabel('Mean accuracy')
                ax.axhline(0.5, ls=':', c='grey', zorder=-20)
                ax.text(x=0.5, y=0.43, text='p={:.5f}'.format(p), s=8)
                plt.tight_layout()
                plot_name = 'dotsss_{}_{}_{}.{}'.format(deconf_method, n_trials_plot, decoder, plot_format)
                f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                          bbox_inches="tight")










f, ax = plt.subplots(1, 1, figsize=[2, 2])
legend_elements = [plt.Line2D([0], [0], marker='o', color=method_palette[m],
                              label=method_labels[m], lw=2,
                          markerfacecolor=method_palette[m],
                              markeredgecolor='w',
                              markersize=10) for m in deconf_methods]
ax.legend(handles=legend_elements, loc='center', frameon=False)
sns.despine(left=True, bottom=True)
ax.axis('off')
plt.show()
plot_name = 'singlesess_overtime_legend.{}'.format(plot_format)
f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
          bbox_inches="tight")


f, ax = plt.subplots(1, 1, figsize=[2, 2])
legend_elements = [plt.Line2D([0], [0], marker='o', color=method_palette[m],
                              label=method_labels[m], lw=2,
                              markerfacecolor=method_palette[m],
                              markeredgecolor='w',
                              markersize=0) for m in deconf_methods]
ax.legend(handles=legend_elements, loc='center', frameon=False)
sns.despine(left=True, bottom=True)
ax.axis('off')
plt.show()
plot_name = 'singlesess_nomarker_overtime_legend.{}'.format(plot_format)
f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
          bbox_inches="tight")
