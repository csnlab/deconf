import os
import pickle
from sklearn.model_selection import StratifiedKFold, KFold, LeaveOneOut
from sklearn.metrics import balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
from constants import *
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
try:
    from mlconfound.stats import full_confound_test, partial_confound_test
except ModuleNotFoundError:
    print('Spisak tests not installed')

try:
    from confound_prediction.deconfounding import confound_isolating_cv
except ModuleNotFoundError:
    print('Confound isolating cv not installed')

from sklearn.model_selection import cross_val_predict
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from sklearn.linear_model import LinearRegression, RidgeCV
from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from sklearn.svm import SVC, LinearSVC
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import balanced_accuracy_score, make_scorer
from sklearn.ensemble import AdaBoostClassifier
from utils import *
from plotting_style import *

"""
Test the predictability for a given session while increasing the number
of PCA components included in the confound. 
"""

settings_name = 'feb20_all'

session_ids = ['2018-08-13_14-31-39', '2018-08-14_14-30-15',
               '2018-08-15_14-13-50', '2020-01-16_11-18-35',
               '2021-04-20_18-27-54', '2021-04-29_15-55-12',
               '2019-03-12_11-28-33', '2019-03-13_13-01-19',
               '2019-03-08_14-39-42', '2019-03-12_15-41-39',
               '2019-03-13_15-32-33', '2019-04-11_11-12-47',
               '2019-12-13_12-46-26', '2019-12-13_09-59-24']

session_ids = ['2018-08-10_10-14-01', '2018-08-14_14-30-15',
               '2020-01-16_11-18-35', '2019-03-12_11-28-33',
               '2019-03-08_14-39-42', '2019-03-12_15-41-39',
               '2019-03-13_15-32-33', '2019-12-13_12-46-26',
               '2019-12-13_09-59-24']


experiment = 'audiofreq_in_big_change'
data_settings_name = 'aug22'
ephys_dataset = 'modid'
area_spikes_dataset = 'V1'
time_bin_indx = 3
shuffle_components_list = [False, True]
n_repeats = 5
n_kfold_splits = 5
n_pca_components_list = [1, 3, 5, 10, 15, 20, 25, 30]
deconf_methods = ['predictability_lin',
                  'predictability_nonlin',
                  'predictability_nonlin_ensemble']
tune_regularization_predictability = True
standardize_features = True
standardize_confound = True
lr_penalty = 'elasticnet'#'l2'
lr_solver = 'saga' # 'saga'
Cs = [0.0001, 0.001, 0.01, 0.1, 1]
l1_ratios = [0, 0.25, 0.5, 0.75, 1]
max_iter = 6000
tune_C_nfolds = 3
C_reg_notune = 1

# RF parameters
n_estimators = 100
max_depth = None
min_samples_split = int(2)
min_samples_leaf = int(7)
max_features = 'sqrt'
bootstrap = True


plot = True
plot_format = 'png'
plots_folder = os.path.join(DATA_PATH, 'plots', 'test_predictability', settings_name)
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)

data = load_decoding_data(settings_name=data_settings_name,
                          experiment_name=experiment,
                          dataset=ephys_dataset,
                          area_spikes=area_spikes_dataset)


for shuffle_components in shuffle_components_list:

    rs = pd.DataFrame(columns=['session_id', 'deconf_method', 'decoder',
                               'n_pca_components', 'repeat', 'mean_score'])

    for session_id in session_ids:


        print(session_id)

        for deconf_method in deconf_methods:

            for n_pca_components in n_pca_components_list:
                print('Method: {}, # components: {}'.format(deconf_method, n_pca_components))

                # get decoding data
                X = data['data'][session_id]['binned_spikes'][time_bin_indx]
                C = data['data'][session_id]['binned_movement'][time_bin_indx]
                y = data['data'][session_id]['target']

                total_n_trials = X.shape[0]
                accuracies_full, accuracies_naive = [], []
                predictabilities = []

                for repeat in range(n_repeats):

                    # TODO every repeat shuffle C differently?
                    if shuffle_components:
                        if n_pca_components == 1:
                            C = np.random.default_rng(seed=repeat).permutation(C[:, 0]).reshape(-1, 1)
                        else:
                            # C = C[:, 0:n_pca_components]
                            # for col in range(C.shape[1]):
                            #     C[:, col] = np.random.default_rng(seed=col+2).permutation(C[:, col])
                            indx = np.random.default_rng(seed=repeat).permutation(C.shape[0])
                            C = C[indx, 0:n_pca_components]

                    else:
                        if n_pca_components == 1:
                            C = C[:, 0].reshape(-1, 1)
                        else:
                            C = C[:, 0:n_pca_components]

                    kfold = StratifiedKFold(n_splits=n_kfold_splits, shuffle=True,
                                            random_state=repeat)
                    #kfold = LeaveOneOut()
                    iterable = kfold.split(X, y)

                    # kfold_accuracy_full, kfold_accuracy_naive = [], []
                    llratios = []
                    kfold_accuracy_full, kfold_accuracy_naive = [], []
                    for fold, (training_ind, testing_ind) in enumerate(iterable):

                        X_train = X[training_ind, :]
                        X_test = X[testing_ind, :]
                        y_train = y[training_ind]
                        y_test = y[testing_ind]
                        C_train = C[training_ind, :]
                        C_test = C[testing_ind, :]

                        if standardize_features:
                            ss = StandardScaler()
                            X_train = ss.fit_transform(X_train)
                            X_test = ss.transform(X_test)

                        if standardize_confound:
                            ss = StandardScaler()
                            C_train = ss.fit_transform(C_train)
                            C_test = ss.transform(C_test)

                        features_full_train = np.hstack([X_train, C_train])
                        features_full_test = np.hstack([X_test, C_test])
                        features_naive_train = C_train
                        features_naive_test = C_test

                        # if np.isin(deconf_method, ['predictability_lin_nodeconf',
                        #                            'predictability_nonlin_nodeconf']):
                        #     features_full_train = X_train
                        #     features_full_test = X_test
                        #     features_naive_train = np.ones_like(C_train)
                        #     features_naive_test = np.ones_like(C_test)

                        if np.isin(deconf_method, ['predictability_lin',
                                                   'predictability_lin_nodeconf']):
                            if tune_regularization_predictability:
                                full_model = LogisticRegressionCV(penalty=lr_penalty,
                                                                  solver=lr_solver,
                                                                  Cs=Cs,
                                                                  l1_ratios=l1_ratios,
                                                                  max_iter=max_iter,
                                                                  cv=tune_C_nfolds,
                                                                  random_state=fold)

                                naive_model = LogisticRegressionCV(penalty=lr_penalty,
                                                                   solver=lr_solver,
                                                                   Cs=Cs,
                                                                   l1_ratios=l1_ratios,
                                                                   max_iter=max_iter,
                                                                   cv=tune_C_nfolds,
                                                                   random_state=fold)
                                # TODO naive model not regularized!
                                # naive_model = LogisticRegression(penalty=None,
                                #                                  solver=lr_solver,
                                #                                  random_state=fold)

                            else:
                                full_model = LogisticRegression(penalty=lr_penalty,
                                                                solver=lr_solver,
                                                                C=C_reg,
                                                                random_state=fold)
                                # naive_model = LogisticRegression(penalty=lr_penalty,
                                #                                  solver=lr_solver,
                                #                                  C=C_reg)
                                # TODO naive model not regularized!
                                naive_model = LogisticRegression(penalty=None,
                                                                 solver=lr_solver,
                                                                 random_state=fold)

                        elif np.isin(deconf_method, ['predictability_nonlin',
                                                     'predictability_nonlin_nodeconf']):

                            if tune_regularization_predictability:
                                parameters_1 =  { 'kernel': ('linear', 'rbf'),
                                    'C': [0.00001, 0.0001, 0.001, 0.01, 0.1, 1, 10]}
                                # 'gamma' : ['scale', 0.0001, 0.001, 0.01, 0.1, 1]}
                                svc_1 = SVC(probability=True, gamma='scale', random_state=fold)
                                inner_cv_1 = StratifiedKFold(n_splits=3, shuffle=True, random_state=1)
                                full_model = GridSearchCV(svc_1, parameters_1, refit=True, scoring=None,
                                                          cv=inner_cv_1)

                                parameters_2 = { 'kernel': ('linear','rbf'),
                                    'C': [0.00001, 0.0001, 0.001, 0.01, 0.1, 1, 10]}
                                # 'gamma': ['scale', 0.0001, 0.001, 0.01, 0.1, 1]}
                                svc_2 = SVC(probability=True, gamma='scale', random_state=fold)
                                inner_cv_2 = StratifiedKFold(n_splits=3, shuffle=True, random_state=1)
                                naive_model = GridSearchCV(svc_2, parameters_2, refit=True, scoring=None,
                                                           cv=inner_cv_2)


                                # svc_2 = SVC(probability=True, C=10, kernel='rbf')
                                # naive_model = svc_2
                            else:
                                raise ValueError
                        elif deconf_method == 'predictability_nonlin_ensemble':

                                full_model = RandomForestClassifier(n_estimators=n_estimators, random_state=fold,
                                                                    max_depth=max_depth,
                                                                    min_samples_split=min_samples_split,
                                                                    min_samples_leaf=min_samples_leaf,
                                                                    max_features=max_features,
                                                                    bootstrap=bootstrap)
                                naive_model = RandomForestClassifier(n_estimators=n_estimators, random_state=fold,
                                                                     max_depth=max_depth,
                                                                     min_samples_split=min_samples_split,
                                                                     min_samples_leaf=min_samples_leaf,
                                                                     max_features=max_features,
                                                                     bootstrap=bootstrap)
                        full_model.fit(features_full_train, y_train)
                        naive_model.fit(features_naive_train, y_train)

                        y_pred_proba_full = full_model.predict_proba(features_full_test)
                        y_pred_proba_naive = naive_model.predict_proba(features_naive_test)

                        fold_accuracy_full = balanced_accuracy_score(y_test, full_model.predict(features_full_test))
                        fold_accuracy_naive = balanced_accuracy_score(y_test, naive_model.predict(features_naive_test))

                        kfold_accuracy_full.append(fold_accuracy_full)
                        kfold_accuracy_naive.append(fold_accuracy_naive)

                        log_likelihood_full = log2_likelihood(y_test, y_pred_proba_full)
                        log_likelihood_naive = log2_likelihood(y_test, y_pred_proba_naive)
                        llratio = log_likelihood_full - log_likelihood_naive
                        llratios.append(llratio)

                        # print('\nAccuracy full: {:.3f}'.format(fold_accuracy_full))
                        # print('Accuracy naiv: {:.3f}'.format(fold_accuracy_naive))
                        # print('Loglikelihood full: {:.3f}'.format(log_likelihood_full))
                        # print('Loglikelihood naive: {:.3f}'.format(log_likelihood_naive))
                        # print('LL ratio: {:.3f}'.format(llratio))
                        #
                        # try:
                        #     print('Full model best C: {}'.format(full_model.best_params_['C']))
                        #     print('Full model kernel: {}'.format(full_model.best_params_['kernel']))
                        #     print('Naive model best C: {}'.format(naive_model.best_params_['C']))
                        #     print('Naive model best kernel: {}'.format(naive_model.best_params_['kernel']))
                        #
                        # except:
                        #     pass

                    accuracy_full = np.mean(kfold_accuracy_full)
                    accuracy_naive = np.mean(kfold_accuracy_naive)
                    predictability = np.mean(llratios)

                    accuracies_full.append(accuracy_full)
                    accuracies_naive.append(accuracy_naive)
                    predictabilities.append(predictability)

                    # print('\nAccuracy full: {:.3f}'.format(accuracy_full))
                    # print('\nAccuracy naive: {:.3f}'.format(accuracy_naive))
                    # print('\nPredictability: {:.3f}'.format(predictability))

                    mean_score = predictability
                    kfold_scores = llratios

                    rs.loc[rs.shape[0], :] = [session_id, deconf_method, 'none', n_pca_components,
                                              repeat, mean_score]


    output_folder = os.path.join(DATA_PATH, 'test_predictability',
                                 'SIMULATION_{}'.format(settings_name))

    if not os.path.isdir(output_folder):
        os.makedirs(output_folder)

    pars = {'session_ids': session_ids,
            'data_settings_name': data_settings_name,
            'ephys_dataset': ephys_dataset,
            'area_spikes_dataset': area_spikes_dataset,
            'time_bin_indx': time_bin_indx,
            'n_repeats': n_repeats,
            'n_kfold_splits': n_kfold_splits,
            'n_pca_components': n_pca_components,
            'deconf_methods': deconf_methods,
            'n_pca_components_list' : n_pca_components_list,
            'tune_regularization_predictability': tune_regularization_predictability,
            'standardize_features': standardize_features,
            'standardize_confound': standardize_confound,
            'lr_penalty': lr_penalty,
            'lr_solver': lr_solver,
            'Cs': Cs,
            'max_iter': max_iter,
            'tune_C_nfolds': tune_C_nfolds,
            'C_reg_notune': C_reg_notune,
            'n_estimators' : n_estimators,
            'max_depth' : max_depth,
            'min_samples_split' : min_samples_split,
            'min_samples_leaf' : min_samples_leaf,
            'max_features' : max_features}

    out = {'pars' : pars,
           'df' : rs}

    if shuffle_components:
        extra_string = 'shuffledC'
    else:
        extra_string = 'trueC'

    output_full_path = os.path.join(output_folder, 'test_n_components_results_{}_{}.pkl'.format(settings_name, extra_string))
    print('Saving simulation to {}'.format(output_full_path))
    pickle.dump(out, open(output_full_path, 'wb'))


# if plot:
#     for session_id in session_ids:
#         rsel = rs[rs['session_id'] == session_id]
#         f, ax = plt.subplots(1, 1, figsize=[4, 4], sharey=True)
#         sns.lineplot(x='n_pca_components', y='mean_score', ax=ax, markers=True, marker='o',
#                      hue='deconf_method', data=rsel, palette=method_palette)
#
#         sns.despine()
#         ax.set_ylabel('Mean predictability')
#         ax.set_xlabel('# movement PCA components')
#         ax.axhline(0.0, ls=':', c='grey')
#         ax.legend().remove()
#         ax.set_xticks(n_pca_components_list)
#
#         plot_name = 'reg_overfit_lineplot_{}.{}'.format(session_id, plot_format)
#         f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
#                   bbox_inches="tight")
#
#
#
#
#
#     f, ax = plt.subplots(1, 1, figsize=[2, 2])
#     legend_elements = [plt.Line2D([0], [0], marker='o', color=method_palette[m],
#                                   label=method_labels[m], lw=2,
#                               markerfacecolor=method_palette[m],
#                                   markeredgecolor='w',
#                                   markersize=10) for m in deconf_methods]
#     ax.legend(handles=legend_elements, loc='center', frameon=False)
#     sns.despine(left=True, bottom=True)
#     ax.axis('off')
#     plt.show()
#     plot_name = 'singlesess_overtime_legend.{}'.format(plot_format)
#     f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
#               bbox_inches="tight")


