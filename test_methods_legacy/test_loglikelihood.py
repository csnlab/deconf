import numpy as np
from utils import log2_likelihood

target = np.array([0, 1, 0, 1, 1, 0, 1, 1])

predictions_naive = np.array([[0.5, 0.5],
                      [0.5, 0.5],
                      [0.5, 0.5],
                      [0.5, 0.5],
                      [0.5, 0.5],
                      [0.5, 0.5],
                      [0.5, 0.5],
                      [0.5, 0.5]])

predictions_naive = np.array([[0, 1],
                              [0, 1],
                              [0, 1],
                              [0, 1],
                              [0, 1],
                              [0, 1],
                              [0, 1],
                              [0, 1]]).astype(float)

predictions_naive = np.array([[0, 1],
                              [1, 0],
                              [0, 1],
                              [0, 1],
                              [0, 1],
                              [0, 1],
                              [0, 1],
                              [0, 1]]).astype(float)

y_true = target
y_pred = predictions_naive[:, 1]
eps = np.finfo(y_pred.dtype).eps
y_pred = np.clip(y_pred, eps, 1 - eps)
terms = y_true * np.log2(y_pred) + (1 - y_true) * np.log2(1 - y_pred)
ll = np.sum(terms) / len(y_true)
print(ll)

y_true = np.array([1])
y_pred = np.array([[1, 0]]).astype(float)[:, 1]
eps = np.finfo(y_pred.dtype).eps
y_pred = np.clip(y_pred, eps, 1 - eps)
terms = y_true * np.log2(y_pred) + (1 - y_true) * np.log2(1 - y_pred)
ll = np.sum(terms) / len(y_true)
print(ll)


log_likelihood_naive = log2_likelihood(target, predictions_naive)

print(log_likelihood_naive)