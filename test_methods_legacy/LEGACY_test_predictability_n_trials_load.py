import os
import pickle
from sklearn.model_selection import StratifiedKFold, KFold, LeaveOneOut
from sklearn.metrics import balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
from constants import *
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from utils import *
from plotting_style import *
from scipy.stats import mannwhitneyu, bootstrap


"""
Test the predictability for a given session with a fixed number of PCA components
but increasing the number of trials available.
"""

# TODO if we shuffle, the variance in the end will both be due to the
# shuffling of the confound and to the random subsampling, less meaningful

settings_name = 'feb20_all'

plot_single_sessions = True
plot_sessions_combined = True
plot_kde=True
n_trials_all = 60

plot_format = 'png'
ylims = -0.5, 0.75
yticks = [-0.5, -0.25, 0, 0.25, 0.5, 0.75]
ylimsss = [-0.4, 0.4]
yticksss = [-0.4, 0, 0.4]
plots_folder = os.path.join(DATA_PATH, 'plots', 'test_predictability_n_trials', settings_name)
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)

plot_methods = ['predictability_lin', 'predictability_nonlin_ensemble']


for shuffle_components in [False, True]:
    if shuffle_components:
        extra_string = 'shuffledC'
    else:
        extra_string = 'trueC'
    output_folder = os.path.join(DATA_PATH, 'test_predictability',
                                 'SIMULATION_{}'.format(settings_name))

    output_full_path = os.path.join(output_folder, 'test_n_trials_results_{}_{}.pkl'.format(settings_name, extra_string))
    print('Saving simulation to {}'.format(output_full_path))
    res = pickle.load(open(output_full_path, 'rb'))

    rs = res['df']
    pars = res['pars']
    session_ids = pars['session_ids']
    deconf_methods = [d for d in pars['deconf_methods'] if np.isin(d, plot_methods)]
    n_trials_dict = pars['n_trials_dict']
    rs['mean_score'] = pd.to_numeric(rs['mean_score'])

    # session_ids =  ['2018-08-14_14-30-15',
    #            '2020-01-16_11-18-35', '2019-03-12_11-28-33',
    #            '2019-03-08_14-39-42', '2019-03-12_15-41-39',
    #            '2019-03-13_15-32-33', '2019-12-13_12-46-26',
    #            '2019-12-13_09-59-24']

    # for session_id in session_ids:
    #     rsel = rs[rs['session_id'] == session_id]
    #     f, ax = plt.subplots(1, 1, figsize=[4, 4], sharey=True)
    #     sns.lineplot(x='n_trials', y='mean_score', ax=ax, markers=True, marker='o',
    #                  hue='deconf_method', data=rsel, palette=method_palette)
    #
    #     sns.despine()
    #     ax.set_ylabel('Mean predictability')
    #     ax.set_xlabel('# movement PCA components')
    #     ax.axhline(0.0, ls=':', c='grey')
    #     ax.legend().remove()
    #     ax.set_xticks(n_trials_list)
    #
    #     plot_name = 'reg_overfit_lineplot_{}.{}'.format(session_id, plot_format)
    #     f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
    #               bbox_inches="tight")

    # make your own CIs
    if plot_single_sessions:
        for session_id in session_ids:

            f, ax = plt.subplots(1, 1, figsize=[2.8, 2.2], sharey=True)

            for j, deconf_method in enumerate(deconf_methods):
                x = []
                err = []
                for k, n_trials in enumerate(n_trials_dict[session_id]):

                    vals = rs[(rs['n_trials'] == n_trials) &
                              (rs['deconf_method'] == deconf_method) &
                              (rs['session_id'] == session_id)]['mean_score'].__array__()
                    center = vals.mean()
                    print(vals.mean())

                    bootstrap_vals = (vals.__array__().reshape(-1, 1).astype(float),)
                    bres = bootstrap(bootstrap_vals,
                                     axis=0,
                                     statistic=np.mean,
                                     n_resamples=100,
                                     confidence_level=0.95,
                                     method='basic')
                    error = np.array([bres.confidence_interval.low[0], bres.confidence_interval.high[0]])
                    x.append(center)
                    err.append(error)
                err = np.vstack(err)

                ax.plot(n_trials_dict[session_id], x, c=method_palette[deconf_method], zorder=-5)
                ax.scatter(n_trials_dict[session_id], x, s=30, c=method_palette[deconf_method],
                           edgecolor='w', label=method_labels[deconf_method])
                ax.fill_between(n_trials_dict[session_id], err[:, 0], err[:, 1], alpha=0.3, zorder=-10,
                                color=method_palette[deconf_method], linewidth=0)
            sns.despine()
            ax.set_ylabel(predictability_label)
            ax.set_xlabel('# trials')
            ax.axhline(0.0, ls=':', c='grey')
            ax.legend().remove()

            if n_trials_dict[session_id].shape[0] > 6 and n_trials_dict[session_id].shape[0] < 12:
                ax.set_xticks(n_trials_dict[session_id][::2])
            elif n_trials_dict[session_id].shape[0] >= 12:
                ax.set_xticks(n_trials_dict[session_id][::6])
            else:
                ax.set_xticks(n_trials_dict[session_id])

            ax.set_ylim(ylimsss)
            ax.set_yticks(yticksss)
            plt.tight_layout()

            plot_name = 'lineplot_{}_{}.{}'.format(session_id, extra_string, plot_format)
            f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                      bbox_inches="tight")


    if plot_sessions_combined:
        # plot all sessions together
        f, ax = plt.subplots(1, 1, figsize=[3, 2.2], sharey=True)

        for j, deconf_method in enumerate(deconf_methods):
            for session_id in session_ids:
                x = []
                for k, n_trials in enumerate(n_trials_dict[session_id]):
                    vals = rs[(rs['n_trials'] == n_trials) &
                              (rs['deconf_method'] == deconf_method) &
                              (rs['session_id'] == session_id)]['mean_score'].__array__()

                    center = vals.mean()
                    x.append(center)

                ax.plot(n_trials_dict[session_id], x, c=method_palette[deconf_method], zorder=-5)
                # ax.scatter(n_trials_dict[session_id], x, s=50, c=method_palette[deconf_method],
                #            edgecolor='w', label=method_labels[deconf_method])

        sns.despine()
        ax.set_ylabel(predictability_label)
        ax.set_xlabel('# trials')
        ax.axhline(0.0, ls=':', c='grey')
        #ax.axvline(n_trials_all, c='green', zorder=-10)
        ax.legend().remove()

        # restrict to where we have at least 2 sessions
        second_max = np.sort([n_trials_dict[s].max() for s in session_ids])[-2]
        axmin = np.sort([n_trials_dict[s].min() for s in session_ids])[0]
        ax.set_xticks(n_trials_dict[session_id][::3])
        ax.set_xlim([axmin, second_max])
        ax.set_ylim(ylims)
        ax.set_yticks(yticks)
        plt.tight_layout()

        plot_name = 'allsess_lineplot_{}.{}'.format(extra_string, plot_format)
        f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                  bbox_inches="tight")


    if plot_kde:
        f, ax = plt.subplots(1, 1, figsize=[1.5, 2.2], sharey=True)

        dfsel = rs[(rs['n_trials'] == n_trials_all)]

        dfsel = dfsel.groupby(['session_id', 'decoder', 'n_pca_components',
                               'n_trials', 'deconf_method']).mean().reset_index()
        n_sessions = dfsel['session_id'].unique().shape[0]
        print('KDE computed with {} sessions'.format(n_sessions))

        for deconf_method in deconf_methods:
            dplot = dfsel[dfsel['deconf_method'] == deconf_method]
            sns.kdeplot(data=dplot, x='mean_score',
                        vertical=True, ax=ax, color=method_palette[deconf_method])

        # sns.kdeplot(data=dfsel, hue='deconf_method', x='mean_score',
        #             vertical=True, ax=ax, palette=method_palette)

        ax.legend().remove()
        sns.despine()
        ax.set_ylim(ylims)
        ax.set_yticks(yticks)
        ax.set_ylabel('')
        ax.axhline(0.0, ls=':', c='grey')
        plt.tight_layout()
        plot_name = 'kde_{}.{}'.format(extra_string, plot_format)
        f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                  bbox_inches="tight")



f, ax = plt.subplots(1, 1, figsize=[2, 2])
legend_elements = [plt.Line2D([0], [0], marker='o', color=method_palette[m],
                              label=method_labels[m], lw=2,
                          markerfacecolor=method_palette[m],
                              markeredgecolor='w',
                              markersize=10) for m in deconf_methods]
ax.legend(handles=legend_elements, loc='center', frameon=False)
sns.despine(left=True, bottom=True)
ax.axis('off')
plt.show()
plot_name = 'singlesess_overtime_legend.{}'.format(plot_format)
f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
          bbox_inches="tight")


