import os
import pickle
from sklearn.model_selection import StratifiedKFold, KFold, LeaveOneOut
from sklearn.metrics import balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
from constants import *
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from utils import *
from plotting_style import *
from scipy.stats import mannwhitneyu, bootstrap

settings_name = 'feb23_all_3'


alpha_level_permutations = 0.05
plot = True
n_trials_all = 60
plot_alpha_levels = True
plot_format = 'png'

plots_folder = os.path.join(DATA_PATH, 'plots', 'test_CR_SIG_n_trials', settings_name)
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)

plot_methods = ['raw', 'linear_confound_regression_reg', 'nonlinear_confound_regression']

#plot_methods = ['linear_confound_regression', 'linear_confound_regression_reg']

dps = []
for shuffle_components in [False, True]:
    if shuffle_components:
        extra_string = 'shuffledC'
    else:
        extra_string = 'trueC'
    output_folder = os.path.join(DATA_PATH, 'test_CR',
                                 'SIMULATION_{}'.format(settings_name))

    output_full_path = os.path.join(output_folder, 'test_SIG_n_trials_results_{}_{}.pkl'.format(settings_name, extra_string))
    res = pickle.load(open(output_full_path, 'rb'))

    rs = res['df']
    pars = res['pars']
    session_ids = pars['session_ids']
    deconf_methods = [m for m in pars['deconf_methods'] if np.isin(m, plot_methods)]
    n_trials_dict = {s : rs[rs['session_id'] == s]['n_trials'].unique().__array__().astype(int) for s in session_ids}
    n_repeats = pars['n_repeats']
    n_permutations = pars['n_permutations']
    decoders = pars['decoders']


    n_trials_list = pars['n_trials_dict'][session_ids[0]]

    if n_repeats > 0:
        dp = pd.DataFrame(columns=['session_id',
                                   'deconf_method',
                                   'decoder',
                                   'n_trials',
                                   'pvalue',
                                   'repeat',
                                   'mean_score',
                                   'is_sig'])

        for session_id in session_ids:
            for n_trials in n_trials_dict[session_id]:
                for kk, deconf_method in enumerate(deconf_methods):
                    for decoder in decoders:
                        for repeat in range(n_repeats):
                            df_sel = rs[(rs['session_id'] == session_id) &
                                        (rs['deconf_method'] == deconf_method) &
                                        (rs['n_trials'] == n_trials) &
                                        (rs['decoder'] == decoder) &
                                        (rs['repeat'] == repeat)]

                            obs = df_sel[df_sel['permuted'] == False]
                            perms = df_sel[(df_sel['permuted'] == True)]

                            obs_val = obs['mean_score'].iloc[0]
                            perms_val = perms['mean_score'].__array__()
                            p_val = (perms_val > obs_val).sum() / (n_permutations + 1)
                            is_sig = p_val <= alpha_level_permutations
                            dp.loc[dp.shape[0], :] = [session_id, deconf_method,
                                                      decoder, n_trials,
                                                      p_val, repeat, obs_val, is_sig]


        dp['pvalue'] = pd.to_numeric(dp['pvalue'])
        dp['shuffled_confound'] = shuffle_components
        dps.append(dp)


        ds = pd.DataFrame(columns=['deconf_method',
                                   'decoder',
                                   'n_trials',
                                   'repeat',
                                   'perc_sig',
                                   'n_sessions'])

        for n_trials in n_trials_list:
            for kk, deconf_method in enumerate(deconf_methods):
                for decoder in decoders:
                    for repeat in range(n_repeats):
                        dpsel = dp[(dp['deconf_method'] == deconf_method) &
                                   (dp['decoder'] == decoder) &
                                   (dp['n_trials'] == n_trials) &
                                    (dp['repeat'] == repeat)]
                        n_sess = dpsel['session_id'].unique().shape[0]
                        perc_sig = 100 * dpsel['is_sig'].astype(bool).sum() / dpsel.shape[0]
                        ds.loc[ds.shape[0], :] = [deconf_method, decoder, n_trials,
                                                  repeat, perc_sig, n_sess]


        f, ax = plt.subplots(1, 1, figsize=[1.8, 3])
        n_sessions = ds.groupby(['n_trials']).mean().reset_index()['n_sessions'].__array__().astype(int)
        ax.scatter(n_trials_list, n_sessions, c='k', s=20, edgecolor='w', zorder=3)
        ax.plot(n_trials_list, n_sessions, c='k', zorder=2)
        ax.set_yticks([6, 7, 8, 9])
        ax.set_xticks(n_trials_list)
        ax.set_ylabel('# of recordings')
        ax.set_xlabel('# trials')
        sns.despine()
        plt.tight_layout()
        plot_name = 'number_of_sessions_{}_{}.{}'.format(extra_string, alpha_level_permutations, plot_format)
        f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                  bbox_inches="tight")


        for decoder in decoders:
            f, ax = plt.subplots(1, 1, figsize=[2.8, 2.2])
            for kk, deconf_method in enumerate(deconf_methods):
                #x = ds[ds['deconf_method'] == deconf_method]['perc_sig']

                x = []
                err = []
                for k, n_trials in enumerate(n_trials_list):
                    vals = ds[(ds['n_trials'] == n_trials) &
                              (ds['decoder'] == decoder) &
                              (ds['deconf_method'] == deconf_method)]['perc_sig'].__array__()
                    center = vals.mean()
                    bootstrap_vals = (vals.__array__().reshape(-1, 1).astype(float),)
                    bres = bootstrap(bootstrap_vals,
                                     axis=0,
                                     statistic=np.mean,
                                     n_resamples=100,
                                     confidence_level=0.95,
                                     method='basic')
                    error = np.array([bres.confidence_interval.low[0], bres.confidence_interval.high[0]])
                    x.append(center)
                    err.append(error)
                err = np.vstack(err)

                if deconf_method == 'raw':
                    ax.plot(n_trials_list, x, c='red', zorder=-5)
                else:
                    ax.plot(n_trials_list, x, c=method_palette[deconf_method], zorder=-10)
                    ax.scatter(n_trials_list, x, s=30, c=method_palette[deconf_method],
                               edgecolor='w', label=method_labels[deconf_method])
                    ax.fill_between(n_trials_list, err[:, 0], err[:, 1], alpha=0.3, zorder=-10,
                                    color=method_palette[deconf_method], linewidth=0)

            sns.despine()
            ax.set_ylabel('% significant')
            ax.set_xlabel('# trials')
            # ax.axhline(0.0, ls=':', c='grey')
            ax.set_ylim(-5, 100)
            ax.axhline(5, ls='--', c=sns.xkcd_rgb['light grey'], zorder=-5)
            ax.set_yticks([0, 25, 50, 75, 100])
            ax.set_xticks(n_trials_list)
            ax.legend().remove()
            plt.tight_layout()
            if np.isin('linear_confound_regression', plot_methods):
                ax.set_ylim(-5, 103)
                plt.tight_layout()
                plot_name = 'SUPP_perc_sig_{}_{}_{}.{}'.format(session_id, extra_string, decoder, plot_format)
                f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                          bbox_inches="tight")
            else:
                plot_name = 'perc_sig_{}_{}_{}.{}'.format(session_id, extra_string, decoder, plot_format)
                f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                          bbox_inches="tight")



    if n_repeats == 0:
        dp = pd.DataFrame(columns=['session_id',
                                   'deconf_method',
                                   'n_trials',
                                   'pvalue',
                                   'mean_score',
                                   'is_sig'])

        for session_id in session_ids:
            for n_trials in n_trials_dict[session_id]:
                for kk, deconf_method in enumerate(deconf_methods):
                    df_sel = rs[(rs['session_id'] == session_id) &
                                (rs['deconf_method'] == deconf_method) &
                                (rs['n_trials'] == n_trials)]

                    obs = df_sel[df_sel['permuted'] == False]
                    perms = df_sel[(df_sel['permuted'] == True)]

                    obs_val = obs['mean_score'].iloc[0]
                    perms_val = perms['mean_score'].__array__()
                    p_val = (perms_val > obs_val).sum() / (n_permutations + 1)
                    is_sig = p_val <= alpha_level_permutations

                    dp.loc[dp.shape[0], :] = [session_id, deconf_method, n_trials,
                                              p_val, obs_val, is_sig]



        ds = pd.DataFrame(columns=['deconf_method',
                                   'n_trials',
                                   'perc_sig'])

        for n_trials in n_trials_dict[session_id]:
            for kk, deconf_method in enumerate(deconf_methods):
                dpsel = dp[(dp['deconf_method'] == deconf_method) &
                            (dp['n_trials'] == n_trials)]

                perc_sig = 100 * dpsel['is_sig'].astype(bool).sum() / dpsel.shape[0]
                ds.loc[ds.shape[0], :] = [deconf_method, n_trials, perc_sig]



        f, ax = plt.subplots(1, 1, figsize=[3, 2.8])
        for kk, deconf_method in enumerate(deconf_methods):

            x = ds[ds['deconf_method'] == deconf_method]['perc_sig']

            ax.plot(n_trials_list, x, c=method_palette[deconf_method], zorder=-5)
            ax.scatter(n_trials_list, x, s=30, c=method_palette[deconf_method], edgecolor='w')

            sns.despine()
            ax.set_ylabel('% significant')
            ax.set_xlabel('# trials')
            #ax.axhline(0.0, ls=':', c='grey')
            ax.set_ylim(0, 100)
            ax.set_yticks([0, 25, 50, 75, 100])
            ax.set_xticks(n_trials_list)
            ax.legend().remove()
            plt.tight_layout()
            plot_name = 'perc_sig_{}_{}.{}'.format(session_id, extra_string, plot_format)
            f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                      bbox_inches="tight")




dp = pd.concat(dps)
if plot_alpha_levels:

    for decoder in decoders:
        for kk, deconf_method in enumerate(deconf_methods):

            f, ax = plt.subplots(1, 1, figsize=[2.2, 2.2])

            for ix, alpha in enumerate([0.001, 0.01, 0.05]):
                for shuffle_components in [False, True]:
                    dp['is_sig'] = dp['pvalue'] <= alpha
                    ds = pd.DataFrame(columns=['deconf_method',
                                               'n_trials',
                                               'decoder',
                                               'repeat',
                                               'perc_sig',
                                               'n_sessions'])

                    for n_trials in n_trials_list:
                        print(n_trials)
                        for repeat in range(n_repeats):
                            dpsel = dp[(dp['deconf_method'] == deconf_method) &
                                       (dp['n_trials'] == n_trials) &
                                       (dp['decoder'] == decoder) &
                                       (dp['shuffled_confound'] == shuffle_components) &
                                       (dp['repeat'] == repeat)]
                            n_sess = dpsel.shape[0]
                            perc_sig = 100 * dpsel['is_sig'].astype(bool).sum() / n_sess
                            ds.loc[ds.shape[0], :] = [deconf_method, n_trials, decoder,
                                                      repeat, perc_sig, n_sess]

                    x = []
                    for k, n_trials in enumerate(n_trials_list):
                        vals = ds[(ds['n_trials'] == n_trials) &
                                  (ds['decoder'] == decoder) &
                                  (ds['deconf_method'] == deconf_method)]['perc_sig'].__array__()
                        center = vals.mean()
                        x.append(center)
                    if shuffle_components:
                        ls = '--'
                    else:
                        ls = '-'

                    ax.plot(n_trials_list, x,
                            c=method_palette[deconf_method],
                            zorder=-10, lw=(ix+1)*1, ls=ls)
                # ax.scatter(n_trials_list, x, s=30, c=method_palette[deconf_method],
                #            edgecolor='w', label=method_labels[deconf_method])

            sns.despine()
            ax.set_ylabel('% significant')
            ax.set_xlabel('# trials')
            ax.axhline(5, ls='--', c=sns.xkcd_rgb['grey'], zorder=-20, alpha=1)
            ax.set_ylim(0, 85)
            ax.set_yticks([0, 25, 50, 75])
            ax.set_xticks(n_trials_list)
            ax.legend().remove()
            plt.tight_layout()
            if np.isin('linear_confound_regression', plot_methods):
                plot_name = 'SUPP_vary_alpha_perc_sig_{}_{}_{}.{}'.format(session_id, deconf_method, decoder, plot_format)
                f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                          bbox_inches="tight")
            else:
                plot_name = 'vary_alpha_perc_sig_{}_{}_{}.{}'.format(session_id, deconf_method, decoder, plot_format)
                f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                          bbox_inches="tight")


alphas = [0.001, 0.01, 0.05]
f, ax = plt.subplots(1, 1, figsize=[2, 2])
legend_elements = [plt.Line2D([0], [0], color=sns.xkcd_rgb['grey'],
                              label='$\\alpha={}$'.format(a), lw=(ix+1)*1) for ix, a in enumerate(alphas)]
ax.legend(handles=legend_elements, loc='center', frameon=False)
sns.despine(left=True, bottom=True)
ax.axis('off')
plt.show()
plot_name = 'alpha_legend.{}'.format(plot_format)
f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
          bbox_inches="tight")

if np.isin('linear_confound_regression', plot_methods):
    f, ax = plt.subplots(1, 1, figsize=[2, 2])
    legend_elements = [plt.Line2D([0], [0], marker='o', color=method_palette[m],
                                  label=method_labels[m], lw=2,
                                  markerfacecolor=method_palette[m],
                                  markeredgecolor='w',
                                  markersize=10) for m in deconf_methods]
    ax.legend(handles=legend_elements, loc='center', frameon=False)
    sns.despine(left=True, bottom=True)
    ax.axis('off')
    plt.show()
    plot_name = 'SUPP_singlesess_overtime_legend.{}'.format(plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
              bbox_inches="tight")







