import os
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import quantities as pq
import pandas as pd
from scipy.stats import alpha, gamma, norm
from sklearn.preprocessing import minmax_scale
import neo
from elephant.statistics import BinnedSpikeTrain
from elephant.spike_train_generation import NonStationaryPoissonProcess
from constants import *
import pickle


"""
first version of the simulated modid data. 
the responses to the stimuli look more realistic, but also
harder to compare methods because the ground truth is not very clear
"""


settings_name = 'true+conf_effect'
experiment = 'visualori'#'audiofreq'#'visualori'

plot_format = 'png'
plots_folder = os.path.join(DATA_PATH, 'plots', 'modid_sim', settings_name)
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)

n_trials = 200
t_start = 0 * pq.s
t_stop = 1 * pq.s
binsize = 50 * pq.ms  # the binsize of the spikes and of the confound are the same

make_poisson = True
auditory_freq_modulation = True

auditory_bump_peak_rate = 10
movement_bump_peak_rate = 10
visual_bump_peak_rate = 10

unit_ids = ['U1', 'U2', 'U3']
baseline_rates = [2, 10, 30]
auditory_evoked = [True, True, True]
orientation_modulation = [True, True, False]
motor_modulation = [True, True, True]

# ----- make trials -----
if experiment == 'audiofreq' :
    tf = pd.DataFrame(columns=['trial_id',
                               'trial_type',
                               'auditory_frequency',
                               'visual_orientation'])

    tf['trial_id'] = [str(i) for i in np.arange(n_trials)]
    tf['trial_type'] = np.repeat('A', n_trials)
    tf['auditory_frequency'] = np.repeat([0, 1], n_trials // 2)
    tf['visual_orientation'] = None

    target = 'auditory_frequency'



elif experiment == 'visualori' :
    tf = pd.DataFrame(columns=['trial_id',
                               'trial_type',
                               'auditory_frequency',
                               'visual_orientation'])

    tf['trial_id'] = [str(i) for i in np.arange(n_trials)]
    tf['trial_type'] = np.repeat('V', n_trials)
    tf['auditory_frequency'] = None
    tf['visual_orientation'] = np.repeat([0, 1], n_trials // 2)

    target = 'visual_orientation'


# ----- make units -----
uf = pd.DataFrame(columns=['unit_id',
                           'baseline_rate',
                           'auditory_evoked',
                           'orientation_modulation',
                           'motor_modulation'])

uf['unit_id'] = unit_ids
uf['baseline_rate'] = baseline_rates
uf['auditory_evoked'] = auditory_evoked
uf['orientation_modulation'] = orientation_modulation
uf['motor_modulation'] = motor_modulation
n_units = uf.shape[0]


n_time_bins_per_trial = int(((t_stop - t_start) / binsize.rescale(pq.s)).item())
sampling_period = binsize

left_edges = np.arange(t_start, t_stop, binsize.rescale(pq.s)) * pq.s
time_bin_times = left_edges + binsize / 2

assert sampling_period.rescale(pq.ms) == binsize.rescale(pq.ms)

# ----- make modulation components -----

trial_rate = np.zeros(n_time_bins_per_trial)


# auditory_evoked_bump = alpha.pdf(np.linspace(0, 10, n_time_bins_per_trial),
#                                  a=0.3)[0 :n_time_bins_per_trial]
# auditory_evoked_bump = minmax_scale(auditory_evoked_bump,
#                                     feature_range=(0, auditory_bump_peak_rate))
# aud_evok_rate = np.zeros(n_time_bins_per_trial)
# aud_evok_rate[1 :n_time_bins_per_trial] = auditory_evoked_bump[:-1]


auditory_evoked_bump = norm.pdf(np.linspace(-0.1, 3, n_time_bins_per_trial),
                                 loc=0, scale=0.3)[0 :n_time_bins_per_trial]
auditory_evoked_bump = minmax_scale(auditory_evoked_bump,
                                    feature_range=(0, auditory_bump_peak_rate))
aud_evok_rate = np.zeros(n_time_bins_per_trial)
aud_evok_rate[1 :7] = auditory_evoked_bump[:-14]
indxmax = np.where(aud_evok_rate>0)[0].max()


visual_evoked_bump = alpha.pdf(np.linspace(0, 10, n_time_bins_per_trial),
                               a=0.01)[0 :n_time_bins_per_trial]
visual_evoked_bump = minmax_scale(visual_evoked_bump,
                                  feature_range=(0, visual_bump_peak_rate))
vis_evok_rate = np.zeros(n_time_bins_per_trial)
vis_evok_rate[2 :n_time_bins_per_trial] = visual_evoked_bump[0:-2]

movement_evoked_bump = gamma.pdf(np.linspace(0.01, 10, n_time_bins_per_trial),
                                 a=2.5)[0 :n_time_bins_per_trial]
movement_evoked_bump = minmax_scale(movement_evoked_bump,
                                    feature_range=(0, movement_bump_peak_rate))
mov_evok_rate = np.zeros(n_time_bins_per_trial)
# separate the movement onset a bit
mov_evok_rate[3 :n_time_bins_per_trial] = movement_evoked_bump[:-3]


# ----- plot components ------
f, ax = plt.subplots(1, 1, figsize=[4, 4])
ax.plot(time_bin_times, aud_evok_rate, label='auditory component', c='red')
ax.plot(time_bin_times, mov_evok_rate, label='movement component', c='green')
ax.plot(time_bin_times, vis_evok_rate, label='visual component',   c='blue')
ax.scatter(time_bin_times, aud_evok_rate, s=11,c='red')
ax.scatter(time_bin_times, mov_evok_rate, s=11,c='green')
ax.scatter(time_bin_times, vis_evok_rate, s=11,c='blue')
ax.axvline(time_bin_times[indxmax+1], ls=':', c='grey')
ax.set_xlabel('Time [s]')
ax.legend()
sns.despine()
plt.tight_layout()

plot_name = 'modid_sim_components.{}'.format(plot_format)
plt.savefig(os.path.join(plots_folder, plot_name), dpi=400, bbox_inches="tight")


# ----- generate data -----
binned_confound = {}
binned_rate = {}
binned_spikes = {}

for i, row in tf.iterrows() :

    trial_id = row['trial_id']

    if row['trial_type'] == 'A' :
        # sound modulation is independent of stimulus
        V = 0
        if not auditory_freq_modulation:
            A = 1
        # movement moodulation depends on auditory frequency
        if row['auditory_frequency'] == 0 :
            M = 1
            if auditory_freq_modulation:
                A = 1
        else :
            M = 2
            if auditory_freq_modulation:
                A = 2

    if row['trial_type'] == 'V' :
        # movement modulation is independent of visual stimulus
        M = 1
        A = 0
        # visual modulation depends on visual orientation
        if row['visual_orientation'] == 0 :
            V = 1
        else :
            V = 4

    # the movement evoked rate is the confound directly
    mov_noise = np.random.uniform(0, 20)
    trial_confound = M * mov_evok_rate + mov_noise
    binned_confound[trial_id] = trial_confound

    trial_rates = np.zeros([n_time_bins_per_trial, n_units])
    trial_spikes = np.zeros([n_time_bins_per_trial, n_units])

    for j, urow in uf.iterrows() :

        baseline_rate = urow['baseline_rate']

        if urow['auditory_evoked'] :
            uA = 10
        else :
            uA = 0

        if urow['orientation_modulation'] :
            uV = 5
        else :
            uV = 0

        if urow['motor_modulation']:
            # if row['auditory_frequency'] == 0 :
            #     uM = 1
            # else :
            #     uM = 5
            uM = 10
        else :
            uM = 0

        trial_rate = baseline_rate + \
                     uA * A * aud_evok_rate + \
                     uV * V * vis_evok_rate + \
                     uM * trial_confound

        if make_poisson:
            rate_signal = neo.AnalogSignal(t_start=t_start, t_stop=t_stop,
                                           signal=trial_rate * pq.Hz,
                                           sampling_period=sampling_period)
            spiketrain = NonStationaryPoissonProcess(
                rate_signal=rate_signal).generate_spiketrain()

            bst = BinnedSpikeTrain(spiketrain, bin_size=binsize).to_array()
        else:
            bst = trial_rate

        trial_rates[:, j] = trial_rate
        trial_spikes[:, j] = bst

    binned_rate[trial_id] = trial_rates
    binned_spikes[trial_id] = trial_spikes



# --- STORE DATA IN OUTPUT ---------------------------------------------


output_file_name = 'modid_sim_{}_{}.pkl'.format(settings_name, experiment)
output_folder = os.path.join(DATA_PATH, 'sim_data', 'modid')
output_full_path = os.path.join(output_folder, output_file_name)

if not os.path.isdir(output_folder) :
    os.makedirs(output_folder)

out = {}

out['pars'] = {}
out['pars']['t_start'] = t_start
out['pars']['t_stop'] = t_stop
out['pars']['binsize'] = binsize

out['data'] = {}
out['data']['target'] = tf[target]
out['data']['trial_df'] = tf
out['data']['unit_df'] = uf
out['data']['time_bin_times'] = time_bin_times
out['data']['target_name'] = target

out['data']['spikes'] = []
out['data']['confound'] = []

for time_bin in range(n_time_bins_per_trial) :
    S = np.vstack([binned_spikes[t][time_bin, :] for t in tf['trial_id']])
    M = np.vstack([binned_confound[t][time_bin] for t in tf['trial_id']])

    out['data']['spikes'].append(S)
    out['data']['confound'].append(M)

print('Saving simulation to {}'.format(output_full_path))
pickle.dump(out, open(output_full_path, 'wb'))



# ----- pack data in dataframe for easier plotting -----
dx = pd.DataFrame(np.vstack([binned_spikes[t] for t in tf['trial_id']]))
dx.columns = uf['unit_id']
dx['confound'] = np.hstack([binned_confound[t] for t in tf['trial_id']])

for col in ['trial_id', 'trial_type', 'auditory_frequency',
            'visual_orientation'] :
    dx[col] = np.repeat(tf[col], n_time_bins_per_trial).__array__()
dx['time'] = np.tile(time_bin_times, tf.shape[0])

# ----- plot confound over time -----
f, ax = plt.subplots(1, 1, figsize=[4, 4], sharex=True, sharey=True)
# sns.lineplot(data=dx, x='time', y='confound', hue=target, ax=ax,
#              errorbar = ('ci', 95))
sns.lineplot(data=dx, x='time', y='confound', estimator=None,
             units='trial_id',
             hue=target, ax=ax)
ax.set_xlabel('Time [s]')
ax.set_ylabel('Binned movement confound')
sns.despine()
plt.tight_layout()


plot_name = 'modid_sim_confound_{}.{}'.format(experiment, plot_format)
plt.savefig(os.path.join(plots_folder, plot_name), dpi=400, bbox_inches="tight")


# ----- plot PSTHs -----
f, ax = plt.subplots(1, 3, figsize=[8, 4], sharex=True, sharey=True)
for axx, unit_id in zip(ax, uf['unit_id']) :
    sns.lineplot(data=dx, x='time', y=unit_id, hue=target, ax=axx)
    axx.set_xlabel('Time [s]')
ax[0].set_ylabel('Binned spike count')
ax[1].legend().remove()
ax[2].legend().remove()

sns.despine()
plt.tight_layout()

plot_name = 'modid_sim_neurons_{}.{}'.format(experiment, plot_format)
plt.savefig(os.path.join(plots_folder, plot_name), dpi=400, bbox_inches="tight")


# also trial by trial
f, ax = plt.subplots(1, 3, figsize=[8, 4], sharex=True, sharey=True)
for axx, unit_id in zip(ax, uf['unit_id']) :
    sns.lineplot(data=dx, x='time', y=unit_id, hue=target, ax=axx,
                 estimator=None, units='trial_id', alpha=0.5)
    axx.set_xlabel('Time [s]')
ax[0].set_ylabel('Binned spike count')
ax[1].legend().remove()
ax[2].legend().remove()

sns.despine()
plt.tight_layout()

plot_name = 'modid_sim_neurons_single_trials_{}.{}'.format(experiment, plot_format)
plt.savefig(os.path.join(plots_folder, plot_name), dpi=400, bbox_inches="tight")



# ----- decode -----

# from sklearn.model_selection import StratifiedKFold
# from sklearn.metrics import balanced_accuracy_score
# from sklearn.preprocessing import StandardScaler
# from sklearn.linear_model import SGDClassifier
#
# data = out.copy()
# n_splits = 3
# decoder_name = 'SGD'
#
#
# df = pd.DataFrame(columns=['experiment', 'features',
#                            'decoder', 'time', 'score', 'n_neurons'])
#
# for features in ['spikes', 'confound'] :
#
#     for time_bin in range(n_time_bins_per_trial) :
#
#         time = time_bin_times[time_bin]
#
#         X = data['data'][features][time_bin]
#
#         y = data['data']['target']
#
#         print('\n\nTime bin {} of {}'.format(time_bin + 1, n_time_bins_per_trial))
#
#
#         kfold = StratifiedKFold(n_splits=n_splits, shuffle=True,
#                                 random_state=92)
#         kfold_scores = []
#         y_test_all, y_pred_all = [], []
#
#         for fold, (training_ind, testing_ind) in enumerate(
#                 kfold.split(X, y)) :
#
#             if decoder_name == 'random_forest' :
#                 decoder = RandomForestClassifier(n_estimators=n_estimators)
#             elif decoder_name == 'SGD' :
#                 decoder = SGDClassifier()
#             else :
#                 raise NotImplementedError
#
#             X_train = X[training_ind, :]
#             X_test = X[testing_ind, :]
#             y_train = y[training_ind]
#             y_test = y[testing_ind]
#
#             ss = StandardScaler()
#             X_train = ss.fit_transform(X_train)
#             X_test = ss.transform(X_test)
#
#             decoder.fit(X_train, y_train)
#             y_pred = decoder.predict(X_test)
#             score = balanced_accuracy_score(y_test, y_pred)
#             kfold_scores.append(score)
#             y_test_all.append(y_test)
#             y_pred_all.append(y_pred)
#
#         y_test_all = np.hstack(y_test_all)
#         y_pred_all = np.hstack(y_pred_all)
#
#         mean_score = np.mean(kfold_scores)
#
#         row = [experiment, features,
#                decoder_name, time, mean_score, X.shape[1]]
#
#         df.loc[df.shape[0], :] = row
#
# df['time'] = [t.item() for t in df['time']]
#
# features_palette = {'spikes' : sns.xkcd_rgb['purple'],
#                     'confound' : sns.xkcd_rgb['grey']}
#
# f, ax = plt.subplots(1, 1, figsize=[4, 4], sharex=True, sharey=True)
# sns.lineplot(data=df, x='time', y='score', hue='features', ax=ax,
#              palette=features_palette)
# ax.set_xlabel('Time [s]')
# ax.set_ylabel('Decoding accuracy {}'.format(target))
# ax.axhline(0.5, ls=':', c='grey')
# sns.despine()
# plt.tight_layout()
#
# plot_name = 'decoding_{}.{}'.format(experiment, plot_format)
# f.savefig(os.path.join(plots_folder, plot_name), dpi=400, bbox_inches="tight")
#
