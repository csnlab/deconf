import os
import pickle
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
from constants import *
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from mlconfound.stats import full_confound_test, partial_confound_test
from sklearn.model_selection import cross_val_predict
from confound_prediction.deconfounding import confound_isolating_cv
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import log_loss
from utils import *

"""
Decoding visual orientation and auditory frequency simulate data using the 
method of Wool et al. 2023, namely computing the loglikelihood ratio of a full
and a naive model, so-called predictability.

Two options:
'wool_nodeconf': the full model predicts the target using neural data, and is 
compared against a model which has no access to neural data (i.e. predicts the 
target from a constant matrix, so only predicts the relative frequency of the 
two classes).
'wool_deconf': the full model predicts the target using neural data and 
confound combined, whereas the naive model only predicts the target using the 
confound (we measure the added value of neural data on top of the confound).

Two methods for generating surrogates:
'shift': this is the recommended method (used in Wool 2023) where you 
shift the neural data wrt target and confound. Using 20 shifts and then checking
that no surrogate value is higher than the observed gives a conservative test 
with alpha=0.05.
'shuffle': this is an alternative method which might be considered if 
there are too few trials to use shifting (which forces you to decode on a subset 
of the trials). The neural data is simply randomly permuted wrt to target and 
confound, and we compute a p-value as the fraction of surrogate values larger
than the observed value. 

"""

# TODO we are not checking that we have enough trials

settings_name = 'sep27_all'

simulation_settings_name = 'true+conf_effect'#'true+conf_effect'
experiments = ['visualori', 'audiofreq']#['visualori', 'audiofreq']

n_splits = 3
methods = ['wool_deconf', 'wool_nodeconf'] #None

n_shifts = 20
n_shuffles = 200
surrogate_methods = ['shuffle', 'shift']

run_spisak_tests = False
num_perms_spisak = 5000
save_plots = True

plot_format = 'png'
plots_folder = os.path.join(DATA_PATH, 'plots', 'modid_sim',
                            'SIMULATION_SETTINGS_{}'.format(simulation_settings_name),
                            'DECODE_SETTINGS_{}'.format(settings_name))
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)


for method in methods:

    for surrogate_method in surrogate_methods:

        for experiment in experiments:

            output_file_name = 'modid_sim_{}_{}.pkl'.format(simulation_settings_name, experiment)
            output_folder = os.path.join(DATA_PATH, 'sim_data', 'modid')
            output_full_path = os.path.join(output_folder, output_file_name)
            sim = pickle.load(open(output_full_path, 'rb'))

            time_bin_times = sim['data']['time_bin_times']
            n_time_bins_per_trial = len(time_bin_times)
            target_name = sim['data']['target_name']


            df = pd.DataFrame(columns=['experiment', 'features',
                                       'time', 'score', 'n_neurons',
                                       'full_p', 'full_sig', 'partial_p',
                                       'partial_sig'])

            df_null = pd.DataFrame(columns=['experiment', 'features',
                                       'time', 'score', 'shift', 'n_neurons', ])


            # randomize the trial order so we are more likely to cut
            # balanced segments
            n_trials = sim['data']['trial_df'].shape[0]
            random_index = np.random.permutation(np.arange(n_trials))

            # --- Decode from spikes ---
            for time_bin in range(n_time_bins_per_trial) :
                time = time_bin_times[time_bin]
                X_full = sim['data']['spikes'][time_bin][random_index, :]
                C_full = sim['data']['confound'][time_bin][random_index, :]
                y_full = sim['data']['target'].__array__()[random_index]

                T = X_full.shape[0]
                center_point = T // 2

                index_list, X_list, C_list, y_list = [], [], [], []

                if surrogate_method == 'shift':
                    for s in range(-n_shifts, n_shifts + 1):
                        #print(s)
                        X_shift = X_full[n_shifts + s :T - n_shifts + s, :]
                        C_shift = C_full[n_shifts :T - n_shifts, :]
                        y_shift = y_full[n_shifts :T - n_shifts]

                        X_list.append(X_shift)
                        C_list.append(C_shift)
                        y_list.append(y_shift)
                        index_list.append(s)

                elif surrogate_method == 'shuffle':
                    for s in range(n_shuffles + 1) :
                        # print(s)
                        if s == 0 :
                            X_shuffle = X_full
                        else :
                            X_shuffle = np.random.permutation(X_full)

                        X_list.append(X_shuffle)
                        C_list.append(C_full)
                        y_list.append(y_full)
                        index_list.append(s)

                for s, X, C, y in zip(index_list, X_list, C_list, y_list):

                    # --- predictability with behavioral predictors ---
                    kfold = StratifiedKFold(n_splits=n_splits, shuffle=True,
                                            random_state=92)
                    iterable = kfold.split(X, y)

                    kfold_scores = []
                    y_test_all, y_pred_full_all, y_pred_naive_all, c_all = [], [], [], []

                    for fold, (training_ind, testing_ind) in enumerate(iterable) :

                        #decoder = SGDClassifier(random_state=92)
                        #decoder = RandomForestClassifier(n_estimators=300, random_state=92)

                        X_train = X[training_ind, :]
                        X_test = X[testing_ind, :]
                        y_train = y[training_ind]
                        y_test = y[testing_ind]
                        C_train = C[training_ind, :]
                        C_test = C[testing_ind, :]

                        ss = StandardScaler()
                        X_train = ss.fit_transform(X_train)
                        X_test = ss.transform(X_test)

                        ss = StandardScaler()
                        C_train = ss.fit_transform(C_train)
                        C_test = ss.transform(C_test)

                        if method == 'wool_deconf' :
                            full_model = LogisticRegression(penalty='l1',
                                                            solver='liblinear')
                            features_full_train = np.hstack([X_train, C_train])
                            features_full_test = np.hstack([X_test, C_test])
                            full_model.fit(features_full_train, y_train)
                            y_pred_full = full_model.predict_proba(features_full_test)

                            naive_model = LogisticRegression(penalty='l1',
                                                             solver='liblinear')
                            features_naive_train = C_train
                            features_naive_test = C_test
                            naive_model.fit(features_naive_train, y_train)
                            y_pred_naive = naive_model.predict_proba(features_naive_test)


                        elif method == 'wool_nodeconf' :
                            full_model = LogisticRegression(penalty='l1',
                                                            solver='liblinear')
                            features_full_train = X_train
                            features_full_test = X_test
                            full_model.fit(features_full_train, y_train)
                            y_pred_full = full_model.predict_proba(features_full_test)

                            naive_model = LogisticRegression(penalty='l1',
                                                             solver='liblinear')
                            features_naive_train = np.ones_like(X_train)
                            features_naive_test = np.ones_like(X_test)
                            naive_model.fit(features_naive_train, y_train)
                            y_pred_naive = naive_model.predict_proba(features_naive_test)

                        #score = balanced_accuracy_score(y_test, y_pred)
                        #kfold_scores.append(score)
                        y_test_all.append(y_test)
                        y_pred_full_all.append(y_pred_full)
                        y_pred_naive_all.append(y_pred_naive)
                        c_all.append(C_test[:, 0])

                    target = np.hstack(y_test_all)
                    predictions_full = np.vstack(y_pred_full_all)
                    predictions_naive = np.vstack(y_pred_naive_all)
                    confound = np.hstack(c_all)

                    # log_likelihood_full = -log_loss(target, predictions_full)
                    # log_likelihood_naive = -log_loss(target, predictions_naive)

                    log_likelihood_full = log2_likelihood(target, predictions_full)
                    log_likelihood_naive = log2_likelihood(target, predictions_naive)
                    print(log_likelihood_naive)

                    llratio = log_likelihood_full - log_likelihood_naive
                    predictability = llratio
                    mean_score = predictability
                    #print(mean_score)

                    if s == 0:
                        full_p = np.nan
                        full_sig = False
                        partial_p = np.nan
                        partial_sig = False

                        row = [experiment, 'spikes',
                               time, mean_score, X.shape[1], full_p,
                               full_sig, partial_p, partial_sig]

                        df.loc[df.shape[0], :] = row

                    else:
                        row = [experiment, 'spikes',
                               time, mean_score, s, X.shape[1]]

                        df_null.loc[df_null.shape[0], :] = row


            df['time'] = [t.item() for t in df['time']]

            df_null['time'] = [t.item() for t in df_null['time']]

            df['q1'] = np.nan
            df['q2'] = np.nan
            df['significant'] = np.nan

            for time in df_null['time'].unique():
                surrogate_scores = df_null[df_null['time'] == time]['score']
                q1, q2 = surrogate_scores.quantile(q=[0.025, 0.975])
                df.loc[df['time'] == time, 'q1'] = q1
                df.loc[df['time'] == time, 'q2'] = q2
                obs_score = df.loc[df['time'] == time, 'score']
                n_higher = (surrogate_scores > obs_score.array[0]).sum()

                if surrogate_method == 'shift':
                    df.loc[df['time'] == time, 'significant'] = n_higher == 0
                elif surrogate_method == 'shuffle':
                    # TODO this is an approximate test right?
                    p_val = n_higher / len(surrogate_scores)
                    df.loc[df['time'] == time, 'significant'] = p_val < 0.05

                sig_wool = n_higher == 0
                p_val = n_higher / n_shifts
                sig_pval = p_val < 0.05
                assert sig_wool == sig_pval


            features_palette = {'spikes' : sns.xkcd_rgb['purple'],
                                'confound' : sns.xkcd_rgb['grey']}

            #MAX = df['score'].max()+0.06

            f, ax = plt.subplots(1, 1, figsize=[5, 5], sharex=True, sharey=True)

            for features in ['spikes']: #'confound']:
                dx = df[df['features'] == features]
                ax.plot(dx['time'], dx['score'], c=features_palette[features],
                        label='decoding from {}'.format(features))
                ax.scatter(dx['time'], dx['score'], c=features_palette[features])

            # sns.lineplot(data=df, x='time', y='score', hue='features', ax=ax,
            #              palette=features_palette, markers=True, style='experiment')
            ax.set_xlabel('Time [s]')
            ax.set_ylabel('Decoding accuracy {}'.format(target_name))
            ax.axhline(0.0, ls=':', c='grey')
            ax.set_title('deconf method: {} ({})'.format(method, surrogate_method))

            #TODO
            # this should also be only for features=spikes,
            ax.fill_between(df['time'], df['q1'], df['q2'], color='grey',
                            zorder=-10, alpha=0.5, label='Null distribution',
                            linewidth=0)

            dxx = df[(df['features'] == 'spikes') & (df['significant'])]
            ax.scatter(dxx['time'], np.repeat(-0.06, dxx.shape[0]),  marker='X',
                      c=features_palette['spikes'], zorder=10, alpha=1, s=40,
                       label='Significant p<0.05')

            # dxx = df[(df['features'] == 'spikes') & (df['full_sig'])]
            # ax.scatter(dxx['time'], np.repeat(0.33, dxx.shape[0]),  marker='o',
            #           c=features_palette['spikes'], zorder=10, alpha=1, s=40,
            #            label='full test p<0.05')
            #
            # dxx = df[(df['features'] == 'spikes') & (df['partial_sig'])]
            # ax.scatter(dxx['time'], np.repeat(0.36, dxx.shape[0]), marker='s',
            #            c=features_palette['spikes'], zorder=10, alpha=1, s=40,
            #            label='partial test p<0.05')
            ax.legend()

            ax.set_ylim([-0.08, ax.get_ylim()[1]])
            sns.despine()
            plt.tight_layout()

            if save_plots:
                plot_name = 'decoding_{}_{}_{}_{}.{}'.format(method, surrogate_method, experiment, settings_name,
                                                             plot_format)
                f.savefig(os.path.join(plots_folder, plot_name), dpi=400, bbox_inches="tight")

