import os
import pickle
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
from constants import *
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from mlconfound.stats import full_confound_test, partial_confound_test
from sklearn.model_selection import cross_val_predict
from confound_prediction.deconfounding import confound_isolating_cv
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from sklearn.linear_model import LinearRegression
from decoding_utils import decode

"""
Decoding visual orientation and auditory frequency simulate data.

several options for deconfounding:
- 'raw': neural data is not touched
- 'distribution_match': matches the distribution of the confound in the two classes 
of the target
- 'linear_confound_regression': linear confound regression on the neural data:
- 'nonlinear_confound_regression': non-linear confound regression on the neural data:
- 'confound_isolating_cv': applies confound isolating cross-validation
- 'wool_nodeconf': the full model predicts the target using neural data, and is 
compared against a model which has no access to neural data (i.e. predicts the 
target from a constant matrix, so only predicts the relative frequency of the 
two classes).
- 'wool_deconf': the full model predicts the target using neural data and 
confound combined, whereas the naive model only predicts the target using the 
confound (we measure the added value of neural data on top of the confound).

For the 'wool_deconf' and 'wool_nodeconf' methods, 
 we have two methods for generating surrogates:
'shift': this is the recommended method (used in Wool 2023) where you 
shift the neural data wrt target and confound. Using 20 shifts and then checking
that no surrogate value is higher than the observed gives a conservative test 
with alpha=0.05.
'shuffle': this is an alternative method which might be considered if 
there are too few trials to use shifting (which forces you to decode on a subset 
of the trials). The neural data is simply randomly permuted wrt to target and 
confound, and we compute a p-value as the fraction of surrogate values larger
than the observed value. 

"""


settings_name = 'feb29'

simulation_settings_name = 'feb14'#'true+conf_effect'
experiments = ['audiofreq']#['visualori', 'audiofreq']

n_kfold_splits = 3
methods = ['distribution_match', 'linear_confound_regression',
           'nonlinear_confound_regression',
           #'confound_isolating_cv',
           'raw', 'wool_deconf'] #None
methods = ['nonlinear_confound_regression']
surrogate_method = 'shift'
n_shifts = 20
n_shuffles = 200

run_spisak_tests = False
num_perms_spisak = 5000

# grid elements
grid = np.arange(1)

distribution_match_binsize = 1

plot_format = 'png'
plots_folder = os.path.join(DATA_PATH, 'plots', 'modid_sim_v2',
                            'SIMULATION_SETTINGS_{}'.format(simulation_settings_name),
                            'DECODE_SETTINGS_{}'.format(settings_name))
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)


for method in methods:

    for experiment in experiments:

        for sim_indx in grid:
            print(method)
            print(experiment)
            print(sim_indx)

            output_file_name = 'modid_sim_{}_{}_{}.pkl'.format(simulation_settings_name, sim_indx, experiment)
            output_folder = os.path.join(DATA_PATH, 'sim_data', 'modid')
            output_full_path = os.path.join(output_folder, output_file_name)
            sim = pickle.load(open(output_full_path, 'rb'))

            time_bin_times = sim['data']['time_bin_times']
            n_time_bins_per_trial = len(time_bin_times)
            target_name = sim['data']['target_name']

            # data is generated with all the class-1 trials first
            # we randomize to generate a more realistic dataset where the
            # stimulus is randomized
            n_trials = sim['data']['trial_df'].shape[0]
            random_index = np.random.default_rng(seed=92).permutation(np.arange(n_trials))

            df = pd.DataFrame(columns=['experiment', 'features',
                                       'time', 'score', 'n_neurons',
                                       'full_p', 'full_sig', 'partial_p',
                                       'partial_sig', 'wool_q1', 'wool_q2',
                                       'wool_significant'])

            # --- Decode from confounds ---
            for time_bin in range(n_time_bins_per_trial) :

                time = time_bin_times[time_bin]
                X = sim['data']['confound'][time_bin][random_index, :]
                y = sim['data']['target'].__array__()[random_index]

                kfold = StratifiedKFold(n_splits=n_kfold_splits, shuffle=True,
                                        random_state=92)

                kfold_scores = []

                for fold, (training_ind, testing_ind) in enumerate(kfold.split(X, y)) :

                    decoder = SGDClassifier(random_state=92)

                    X_train = X[training_ind, :]
                    X_test = X[testing_ind, :]
                    y_train = y[training_ind]
                    y_test = y[testing_ind]

                    ss = StandardScaler()
                    X_train = ss.fit_transform(X_train)
                    X_test = ss.transform(X_test)

                    decoder.fit(X_train, y_train)
                    y_pred = decoder.predict(X_test)
                    score = balanced_accuracy_score(y_test, y_pred)
                    kfold_scores.append(score)

                mean_score = np.mean(kfold_scores)
                full_p = np.nan
                full_sig = False
                partial_p = np.nan
                partial_sig = False
                wool_q1 = None
                wool_q2 = None
                wool_significant = None

                row = [experiment, 'confound', time, mean_score, X.shape[1], full_p,
                       full_sig, partial_p, partial_sig, wool_q1, wool_q2,
                       wool_significant]

                df.loc[df.shape[0], :] = row

            # --- Decode from spikes ---
            for time_bin in range(n_time_bins_per_trial):

                time = time_bin_times[time_bin]
                X = sim['data']['spikes'][time_bin][random_index, :]
                C = sim['data']['confound'][time_bin][random_index, :]
                y = sim['data']['target'].__array__()[random_index]

                dec_out = decode(X=X, y=y, C=C,
                                 deconf_method=method,
                                 posthoc_counterbalancing_binsize=distribution_match_binsize,
                                 n_kfold_splits=n_kfold_splits,
                                 run_spisak_tests=run_spisak_tests,
                                 num_perms_spisak=num_perms_spisak,
                                 surrogate_method=surrogate_method,
                                 n_shifts=n_shifts,
                                 n_shuffles=n_shuffles)

                mean_score = dec_out['mean_score']
                full_p = dec_out['full_p']
                full_sig = dec_out['full_sig']
                partial_p = dec_out['partial_p']
                partial_sig = dec_out['partial_sig']
                wool_q1 = dec_out['wool_q1']
                wool_q2 = dec_out['wool_q2']
                wool_significant = dec_out['wool_significant']


                row = [experiment, 'spikes',
                       time, mean_score, X.shape[1], full_p,
                       full_sig, partial_p, partial_sig, wool_q1, wool_q2, wool_significant]

                df.loc[df.shape[0], :] = row

            df['time'] = [t.item() for t in df['time']]

            features_palette = {'spikes' : sns.xkcd_rgb['purple'],
                                'confound' : sns.xkcd_rgb['grey']}

            if np.isin(method, ['wool_deconf', 'wool_nodeconf']):
                method_string = '_'.join([method, surrogate_method])
            else:
                method_string = method

            f, ax = plt.subplots(1, 1, figsize=[5, 5], sharex=True, sharey=True)

            ax.set_xlabel('Time [s]')
            ax.set_ylabel('Decoding accuracy {}'.format(target_name))
            ax.set_title('deconf method: {}'.format(method))


            if np.isin(method, ['wool_deconf', 'wool_nodeconf']):
                for col in ['wool_q1', 'wool_q2']:
                    df[col] = pd.to_numeric(df[col])

                dspikes = df[df['features'] == 'spikes']
                ax.fill_between(dspikes['time'], dspikes['wool_q1'], dspikes['wool_q2'],
                                color='grey',
                                zorder=-10, alpha=0.5, label='Null distribution',
                                linewidth=0)

                dxx = df[(df['features'] == 'spikes') & (df['wool_significant'])]
                ax.scatter(dxx['time'], np.repeat(-0.06, dxx.shape[0]),  marker='X',
                          c=features_palette['spikes'], zorder=10, alpha=1, s=40,
                           label='Significant p<0.05')

                ax.set_ylim([-0.08, 0.51])
                ax.axhline(0.0, ls=':', c='grey')
                features_to_plot = ['spikes']


            else:
                dxx = df[(df['features'] == 'spikes') & (df['full_sig'])]
                ax.scatter(dxx['time'], np.repeat(0.33, dxx.shape[0]),  marker='o',
                          c=features_palette['spikes'], zorder=10, alpha=1, s=40,
                           label='full test p<0.05')

                dxx = df[(df['features'] == 'spikes') & (df['partial_sig'])]
                ax.scatter(dxx['time'], np.repeat(0.36, dxx.shape[0]), marker='s',
                           c=features_palette['spikes'], zorder=10, alpha=1, s=40,
                           label='partial test p<0.05')

                ax.set_ylim([0.3, 1.01])
                ax.axhline(0.5, ls=':', c='grey')
                features_to_plot = ['confound', 'spikes']

            for features in features_to_plot:
                dx = df[df['features'] == features]
                ax.plot(dx['time'], dx['score'], c=features_palette[features],
                        label='decoding from {}'.format(features))
                ax.scatter(dx['time'], dx['score'], c=features_palette[features])

            ax.axvspan(0.0, 0.6, color=sns.xkcd_rgb['light red'],
                       alpha=0.2, zorder=-10, lw=0, label='Audio response',
                       ymin=0.2, ymax=0.25)

            ax.axvspan(0.3, 0.9, color=sns.xkcd_rgb['light green'], alpha=0.5,
                       zorder=-10, lw=0, label='Confound response',
                       ymin=0.26, ymax=0.31)

                # ax.axvline(0.3, color=sns.xkcd_rgb['light green'], ls='--')
                # ax.axvline(0.9, color=sns.xkcd_rgb['light green'], ls='--')

            ax.legend()

            sns.despine()
            plt.tight_layout()
            plot_name = 'decoding_{}_{}_{}_{}.{}'.format(method, experiment,
                                                         settings_name,
                                                         sim_indx, plot_format)
            f.savefig(os.path.join(plots_folder, plot_name), dpi=400,
                      bbox_inches="tight")

