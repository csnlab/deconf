import os
import pickle
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
from constants import *
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from mlconfound.stats import full_confound_test, partial_confound_test
from sklearn.model_selection import cross_val_predict
from confound_prediction.deconfounding import confound_isolating_cv
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from sklearn.linear_model import LinearRegression

"""
Decoding visual orientation and auditory frequency simulate data.

several options for deconfounding:
'raw': neural data is not touched
'linear_confound_regression': linear confound regression on the neural data:
'nonlinear_confound_regression': non-linear confound regression on the neural data:
'confound_isolating_cv': applies confound isolating cross-validation
"""


settings_name = 'feb13_check'

simulation_settings_name = 'step_grid_TEST_1'#'true+conf_effect'
experiments = ['audiofreq']#['visualori', 'audiofreq']

n_splits = 3
methods = ['raw', 'linear_confound_regression', 'nonlinear_confound_regression',
           'confound_isolating_cv', 'distribution_match'] #None
methods = ['distribution_match']
run_spisak_tests = False
num_perms_spisak = 5000

distribution_match_binsize = 4


plot_format = 'png'
plots_folder = os.path.join(DATA_PATH, 'plots', 'modid_sim',
                            'DATA_SETTINGS_{}'.format(simulation_settings_name),
                            'DECODE_SETTINGS_{}'.format(settings_name))
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)


for method in methods:

    for experiment in experiments:

        output_file_name = 'modid_sim_{}_{}.pkl'.format(simulation_settings_name, experiment)
        output_folder = os.path.join(DATA_PATH, 'sim_data', 'modid')
        output_full_path = os.path.join(output_folder, output_file_name)
        sim = pickle.load(open(output_full_path, 'rb'))

        time_bin_times = sim['data']['time_bin_times']
        n_time_bins_per_trial = len(time_bin_times)
        target_name = sim['data']['target_name']



        df = pd.DataFrame(columns=['experiment', 'features',
                                   'time', 'score', 'n_neurons',
                                   'full_p', 'full_sig', 'partial_p',
                                   'partial_sig'])

        # --- Decode from confounds ---
        for time_bin in range(n_time_bins_per_trial) :

            time = time_bin_times[time_bin]
            X = sim['data']['confound'][time_bin]
            y = sim['data']['target']

            kfold = StratifiedKFold(n_splits=n_splits, shuffle=True,
                                    random_state=92)

            kfold_scores = []

            for fold, (training_ind, testing_ind) in enumerate(kfold.split(X, y)) :

                decoder = SGDClassifier(random_state=92)

                X_train = X[training_ind, :]
                X_test = X[testing_ind, :]
                y_train = y[training_ind]
                y_test = y[testing_ind]

                ss = StandardScaler()
                X_train = ss.fit_transform(X_train)
                X_test = ss.transform(X_test)

                decoder.fit(X_train, y_train)
                y_pred = decoder.predict(X_test)
                score = balanced_accuracy_score(y_test, y_pred)
                kfold_scores.append(score)

            mean_score = np.mean(kfold_scores)
            full_p = np.nan
            full_sig = False
            partial_p = np.nan
            partial_sig = False

            row = [experiment, 'confound', time, mean_score, X.shape[1], full_p,
                   full_sig, partial_p, partial_sig]

            df.loc[df.shape[0], :] = row


        # --- Decode from spikes ---
        for time_bin in range(n_time_bins_per_trial):

            time = time_bin_times[time_bin]
            X = sim['data']['spikes'][time_bin]
            C = sim['data']['confound'][time_bin]
            y = sim['data']['target'].__array__()

            if method == 'distribution_match':

                bins = np.arange(0, C.max() + 1, distribution_match_binsize)
                indx = np.arange(y.shape[0])

                indx_remove_all = []
                for j in range(bins.shape[0] - 1) :

                    indx_bin = np.where(np.logical_and(C.flatten() >= bins[j],
                                                       C.flatten() < bins[j + 1]))[
                        0]
                    indx_bin_0 = [ix for ix in indx_bin if y[ix] == 0]
                    indx_bin_1 = [ix for ix in indx_bin if y[ix] == 1]

                    y_bin = y[indx_bin]
                    n0 = len(y_bin[y_bin == 0])
                    n1 = len(y_bin[y_bin == 1])

                    if n0 > n1 :
                        n_remove = n0 - n1
                        indx_remove = np.random.choice(indx_bin_0, n_remove,
                                                       replace=False)
                    elif n1 > n0 :
                        n_remove = n1 - n0
                        indx_remove = np.random.choice(indx_bin_1, n_remove,
                                                       replace=False)

                    indx_remove_all.append(indx_remove)

                indx_remove_all = np.hstack(indx_remove_all)
                indx_keep = [ix for ix in indx if ix not in indx_remove_all]

                X_bal = X[indx_keep].copy()
                y_bal = y[indx_keep].copy()
                C_bal = C[indx_keep].copy()

                if False :
                    # check the distribution to make sure you have successfully
                    # balanced the 0 and 1 label within each confound bin
                    f, ax = plt.subplots(1, 2)
                    ax[0].hist(C[y == 0], color='green', alpha=0.5, bins=bins)
                    ax[0].hist(C[y == 1], color='red', alpha=0.5, bins=bins)

                    ax[1].hist(C_bal[y_bal == 0], color='green', alpha=0.5,
                               bins=bins)
                    ax[1].hist(C_bal[y_bal == 1], color='red', alpha=0.5, bins=bins)

                X = X_bal
                y = y_bal
                C = C_bal


            if method == 'confound_isolating_cv':

                _, _, _, _, ids_test, ids_train = confound_isolating_cv(X, y, C[:, 0],
                                                          random_seed=92,
                                                          min_sample_size=None,
                                                          cv_folds=n_splits,
                                                          n_remove=None)

                iterable = zip(ids_train, ids_test)


            else:

                kfold = StratifiedKFold(n_splits=n_splits, shuffle=True,
                                        random_state=92)
                iterable = kfold.split(X, y)


            kfold_scores = []
            y_test_all, y_pred_all, c_all = [], [], []

            for fold, (training_ind, testing_ind) in enumerate(iterable) :

                #decoder = SGDClassifier(random_state=92)
                decoder = RandomForestClassifier(n_estimators=300, random_state=92)
                X_train = X[training_ind, :]
                X_test = X[testing_ind, :]
                y_train = y[training_ind]
                y_test = y[testing_ind]
                C_train = C[training_ind, :]
                C_test = C[testing_ind, :]

                if method == 'linear_confound_regression' :
                    reg = LinearRegression().fit(C_train, X_train)
                    X_train = X_train - reg.predict(C_train)
                    X_test = X_test - reg.predict(C_test)

                elif method == 'nonlinear_confound_regression':
                    reg = RandomForestRegressor(n_estimators=500).fit(C_train, X_train)
                    X_train = X_train - reg.predict(C_train)
                    X_test = X_test - reg.predict(C_test)

                ss = StandardScaler()
                X_train = ss.fit_transform(X_train)
                X_test = ss.transform(X_test)

                ss = StandardScaler()
                C_train = ss.fit_transform(C_train)
                C_test = ss.transform(C_test)

                decoder.fit(X_train, y_train)
                y_pred = decoder.predict(X_test)
                score = balanced_accuracy_score(y_test, y_pred)
                kfold_scores.append(score)
                y_test_all.append(y_test)
                y_pred_all.append(y_pred)
                c_all.append(C_test[:, 0])

            target = np.hstack(y_test_all)
            predictions = np.hstack(y_pred_all)
            confound = np.hstack(c_all)
            mean_score = np.mean(kfold_scores)


            if run_spisak_tests:
                full_test = full_confound_test(target, predictions, confound,
                                               num_perms=num_perms_spisak,
                                               cat_y=True,
                                               cat_yhat=True,
                                               cat_c=False,
                                               mcmc_steps=50,
                                               cond_dist_method='gam',
                                               return_null_dist=False,
                                               random_state=92, progress=True,
                                               n_jobs=-1)
                full_p = full_test.p
                full_sig = full_p < 0.05

                partial_test = partial_confound_test(target, predictions, confound,
                                               num_perms=num_perms_spisak,
                                               cat_y=True,
                                               cat_yhat=True,
                                               cat_c=False,
                                               mcmc_steps=50,
                                               cond_dist_method='gam',
                                               return_null_dist=False,
                                               random_state=92, progress=True,
                                               n_jobs=-1)
                partial_p = partial_test.p
                partial_sig = partial_p < 0.05

            else:
                full_p = np.nan
                full_sig = False
                partial_p = np.nan
                partial_sig = False

            row = [experiment, 'spikes',
                   time, mean_score, X.shape[1], full_p,
                   full_sig, partial_p, partial_sig]

            df.loc[df.shape[0], :] = row

        df['time'] = [t.item() for t in df['time']]

        features_palette = {'spikes' : sns.xkcd_rgb['purple'],
                            'confound' : sns.xkcd_rgb['grey']}

        f, ax = plt.subplots(1, 1, figsize=[5, 5], sharex=True, sharey=True)

        for features in ['spikes', 'confound']:
            dx = df[df['features'] == features]
            ax.plot(dx['time'], dx['score'], c=features_palette[features],
                    label='decoding from {}'.format(features))
            ax.scatter(dx['time'], dx['score'], c=features_palette[features])

        # sns.lineplot(data=df, x='time', y='score', hue='features', ax=ax,
        #              palette=features_palette, markers=True, style='experiment')
        ax.set_xlabel('Time [s]')
        ax.set_ylabel('Decoding accuracy {}'.format(target_name))
        ax.axhline(0.5, ls=':', c='grey')
        ax.set_title('deconf method: {}'.format(method))

        dxx = df[(df['features'] == 'spikes') & (df['full_sig'])]
        ax.scatter(dxx['time'], np.repeat(0.33, dxx.shape[0]),  marker='o',
                  c=features_palette['spikes'], zorder=10, alpha=1, s=40,
                   label='full test p<0.05')

        dxx = df[(df['features'] == 'spikes') & (df['partial_sig'])]
        ax.scatter(dxx['time'], np.repeat(0.36, dxx.shape[0]), marker='s',
                   c=features_palette['spikes'], zorder=10, alpha=1, s=40,
                   label='partial test p<0.05')
        ax.legend()

        ax.set_ylim([0.3, 1.01])
        sns.despine()
        plt.tight_layout()


        plot_name = 'decoding_{}_{}_{}.{}'.format(method, experiment, settings_name, plot_format)
        f.savefig(os.path.join(plots_folder, plot_name), dpi=400, bbox_inches="tight")

