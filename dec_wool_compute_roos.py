import os
import numpy as np
import pandas as pd
from load_data import load_data
from constants import *
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import accuracy_score, recall_score, balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
import matplotlib.pyplot as plt
import seaborn as sns
import pickle
from utils import select_data_experiment
from warp import warp_trials
from utils import rolling_window, rolling_window_sparse
from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from utils import *
from sklearn.preprocessing import LabelEncoder

"""


"""

# TODO not balanced
# TODO trial order not randomized, maybe that's the right thing

settings_name = 'may1'

# experiments = ['hit_miss_V',
#                'hit_miss_M',
#                'hit_miss_A']
# experiments = ['target_vs_distractor_with_misses_V',
#                'target_vs_distractor_with_misses_M',
#                'target_vs_distractor_with_misses_A']
# experiments = ['correct_vs_incorrect_V',
#                'correct_vs_incorrect_M',
#                'correct_vs_incorrect_A']

experiments = ['hit_miss_V',
               'hit_miss_M',
               'hit_miss_A',
               'correct_vs_incorrect_V',
               'correct_vs_incorrect_A',
               'correct_vs_incorrect_M',
               # 'target_vs_distractor_V',
               # 'target_vs_distractor_M',
               # 'target_vs_distractor_A',
               'target_vs_distractor_with_misses_V',
               'target_vs_distractor_with_misses_M',
               'target_vs_distractor_with_misses_A']

experiments = ['response_side',
               'response_side_V',
               'response_side_M',
               'response_side_A']

# DECODING PARAMETERS
methods = ['wool_nodeconf'] #['wool_deconf', 'wool_nodeconf']
surrogate_method = 'shift'
n_shifts = 10 # 20 for conservative test, but not enough trials
n_shuffles = 100
tune_C = True # tune regularization parameter of logistic regression
tune_C_nfolds = 3
C_reg = 0.2 #regularization parameter
Cs = np.linspace(0.01, 0.3, 10)#[0.01, 0.025, 0.05, 0.075, 0.1, 0.125, 0.15, 0.175, 0.2, 0.225, 0.25, 0.3] # number of values to try for the regularization parameter optimisation
lr_penalty = 'l1'#'l1' #'l2'
lr_solver = 'saga'#'liblinear'#'lbfgs'
max_iter = 7000

SCALE_C = 1

# TODO binsize 100 breaks the warping...
align_to = 'stimulus'
binsize = 50
min_units = 8
#min_trials_per_class = 25
min_trials_per_class = 20 #15
min_tot_trials = n_shifts * 2 + 40
start_time_in_ms = -500
end_time_in_ms = 1500 #1500

# TODO warp could be excluded automatically for target/distractor
warp = True
warp_per_modality = True
larger_bins = True

n_splits = 3

seed = 92

# --- LOAD DATA ------------------------------------------------------------
trial_df, trial_data, motion_data, time_bin_centers = load_data(binsize=binsize,
                                                   align_to=align_to,
                                                   enforce_min_max_reaction_times=True)
# get unique session ids
session_ids = trial_df['session_id'].unique()

# consider late hits as hits
trial_df.loc[trial_df['hit_miss'] == 'late', 'hit_miss'] = 'miss'

# remove early trials (marked as early in the hit_miss column)
trial_df = trial_df[np.isin(trial_df['hit_miss'], ['hit', 'miss'])]
trial_data = {id : trial_data[id] for id in trial_df['trial_id']}
motion_data = {id : motion_data[id] for id in trial_df['trial_id']}

# consider misses to be incorrect (in the sense of no reward)
trial_df.loc[trial_df['hit_miss'] == 'miss', 'response'] = 0

assert trial_df['response'].isna().sum() == 0

if warp:
    trial_data, motion_data, trial_df, median_reaction_times = warp_trials(trial_data, trial_df, time_bin_centers,
                                                              warp_per_modality=warp_per_modality,
                                                              keep_misses=True,
                                                              motion_data=motion_data)
else:
    median_reaction_times = {}

# TODO rolling window sparse to save some time!
if larger_bins :
    trial_data, motion_data, time_bin_centers = rolling_window_sparse(trial_data, motion_data, time_bin_centers)


for method in methods:

    for experiment in experiments:

        # determine the confound variable to consider based on the target variable
        if np.isin(experiment, hit_miss_experiments):
            confound_variable = 'correct'

        elif np.isin(experiment, correct_vs_incorrect_experiments):
            confound_variable = 'target'

        elif np.isin(experiment, target_vs_distractor_experiments):
            confound_variable = 'correct'

        # --- OUTPUT FILES ---------------------------------------------------------
        output_file_name = 'decode_wool_setting_{}_{}_{}.pkl'.format(settings_name, method,
                                                             experiment)
        output_folder = os.path.join(DATA_FOLDER, 'results', 'decode_wool',
                                     settings_name)
        output_full_path = os.path.join(output_folder, output_file_name)
        pars_file_name = 'parameters_decode_wool_setting_{}_{}_{}.pkl'.format(settings_name, method,
                                                                      experiment)
        pars_full_path = os.path.join(output_folder, pars_file_name)

        if not os.path.isdir(output_folder):
            os.makedirs(output_folder)


        # --- SELECT SESSIONS AND TRIALS ------------------------------------------
        session_df, sdf, dp = select_data_experiment(experiment, trial_df)

        np.testing.assert_array_equal(sdf['trial_id'], dp['trial_numbers'])

        n_time_bins_per_trial = trial_data['0_0'].shape[0]

        selsess_df = session_df[(session_df['n1'] >= min_trials_per_class) &
                                    (session_df['n2'] >= min_trials_per_class) &
                                    (session_df['n_units'] >= min_units)]
        selsess_df['tot_n_trials'] = selsess_df['n1'] + selsess_df['n2']
        selsess_df = selsess_df[selsess_df['tot_n_trials'] >= min_tot_trials]
        selected_session_ids = selsess_df['session_id'].__array__()

        tot_n_units = selsess_df['n_units'].sum()

        print('\nEXPERIMENT: {}'.format(experiment))
        print('\nmethod: {}'.format(method))
        print('\nselected sessions:')
        print(selsess_df)


        df = pd.DataFrame(
            columns=['animal_id', 'session_id',
                     'experiment', 'features',
                     'time', 'predictability_score', 'accuracy_score',
                     'n_neurons',
                     'full_p', 'full_sig', 'partial_p',
                     'partial_sig'])

        df_null = pd.DataFrame(
            columns=['animal_id', 'session_id',
                     'experiment', 'features',
                     'time', 'predictability_score', 'accuracy_score',
                     'shift', 'n_neurons', ])

        for session_id in selected_session_ids:

            animal_id = trial_df[trial_df['session_id'] == session_id]['animal_id'].unique()
            assert len(animal_id) == 1
            animal_id = animal_id[0]

            print('______ decoding session {}, animal {}'.format(session_id, animal_id))

            ds = dp[dp['session_id'] == session_id]
            tdf_sess = sdf[sdf['session_id'] == session_id]

            tdf_sess.index = tdf_sess['trial_id']

            np.testing.assert_array_equal(tdf_sess['trial_id'], ds['trial_numbers'])

            sel_trial_ids = dp[dp['session_id'] == session_id]['trial_numbers']

            np.testing.assert_array_equal(tdf_sess['trial_id'], sel_trial_ids)

            random_index = np.random.default_rng(seed=92).permutation(np.arange(len(sel_trial_ids)))

            for time_bin in range(n_time_bins_per_trial) :

                time = time_bin_centers[time_bin]
                # time0, time1 = time_bin_edges[time_bin]

                # TODO right now I don't downsample the majority class!
                if time >= start_time_in_ms and time <= end_time_in_ms :

                    X_full = np.vstack([trial_data[t][time_bin, :] for t in sel_trial_ids])
                    y_full = ds['target'].__array__()

                    if confound_variable == 'motion':
                        C_full = np.array([motion_data[t][time_bin] for t in sel_trial_ids]).reshape(-1, 1)
                    elif confound_variable == 'correct':
                        C_full = tdf_sess['response'].__array__().reshape(-1, 1)
                    elif confound_variable == 'target':
                        C_full = np.array([tdf_sess.loc[t]['target'] for t in sel_trial_ids])
                        C_full = LabelEncoder().fit_transform(C_full).reshape(-1, 1)
                    else:
                        raise ValueError

                    C_full = C_full.astype(float)
                    # if there are misses, as their order is artificial,
                    # we shuffle the order of the trials
                    if tdf_sess['hit_miss'].unique().shape[0] == 2:
                        X_full = X_full[random_index,:]
                        y_full = y_full[random_index]
                        C_full = C_full[random_index,:]

                    T = X_full.shape[0]
                    center_point = T // 2

                    index_list, X_list, C_list, y_list = [], [], [], []

                    if surrogate_method == 'shift' :
                        for s in range(-n_shifts, n_shifts + 1) :
                            # print(s)
                            X_shift = X_full[n_shifts + s :T - n_shifts + s, :]
                            C_shift = C_full[n_shifts :T - n_shifts, :]
                            y_shift = y_full[n_shifts :T - n_shifts]

                            #print(X_shift.shape)
                            X_list.append(X_shift)
                            C_list.append(C_shift)
                            y_list.append(y_shift)
                            index_list.append(s)

                    elif surrogate_method == 'shuffle' :
                        for s in range(n_shuffles + 1) :
                            # print(s)
                            if s == 0 :
                                X_shuffle = X_full
                            else :
                                X_shuffle = np.random.permutation(X_full)

                            X_list.append(X_shuffle)
                            C_list.append(C_full)
                            y_list.append(y_full)
                            index_list.append(s)

                    for s, X, C, y in zip(index_list, X_list, C_list, y_list) :

                        # --- predictability with behavioral predictors ---
                        kfold = StratifiedKFold(n_splits=n_splits, shuffle=True,
                                                random_state=92)

                        # kfold = KFold(n_splits=n_splits, shuffle=True,
                        #                         random_state=92)

                        iterable = kfold.split(X, y)

                        # TODO qua ci sono troppe cose, sfoltire
                        kfold_accuracy_full, kfold_accuracy_naive = [], []
                        y_test_all, y_pred_proba_full_list, y_pred_proba_naive_list, c_all = [], [], [], []
                        log_likelihood_full_list = []
                        log_likelihood_naive_list = []
                        llratios = []

                        for fold, (training_ind, testing_ind) in enumerate(
                                iterable) :

                            # decoder = SGDClassifier(random_state=92)
                            # decoder = RandomForestClassifier(n_estimators=300, random_state=92)

                            X_train = X[training_ind, :]
                            X_test = X[testing_ind, :]
                            y_train = y[training_ind]
                            y_test = y[testing_ind]
                            C_train = C[training_ind, :]
                            C_test = C[testing_ind, :]

                            ss = StandardScaler()
                            X_train = ss.fit_transform(X_train)
                            X_test = ss.transform(X_test)

                            if confound_variable == 'motion':
                                ss = StandardScaler()
                                C_train = ss.fit_transform(C_train)
                                C_test = ss.transform(C_test)

                            if method == 'wool_deconf':
                                C_train = C_train * SCALE_C
                                C_test = C_test * SCALE_C
                                # X_train = X_train * 0.1
                                # X_test = X_test * 0.1
                                features_full_train = np.hstack([X_train, C_train])
                                features_full_test = np.hstack([X_test, C_test])

                                features_naive_train = C_train
                                features_naive_test = C_test

                            elif method == 'wool_nodeconf':
                                features_full_train = X_train
                                features_full_test = X_test

                                features_naive_train = np.ones_like(C_train)
                                features_naive_test = np.ones_like(C_test)

                            if tune_C:
                                full_model = LogisticRegressionCV(penalty=lr_penalty,
                                                                  solver=lr_solver,
                                                                  Cs=Cs,
                                                                  max_iter=max_iter,
                                                                  cv=tune_C_nfolds)
                                # naive_model = LogisticRegressionCV(penalty=lr_penalty,
                                #                                    solver=lr_solver,
                                #                                    Cs=Cs,
                                #                                    max_iter=max_iter,
                                #                                    cv=tune_C_nfolds)
                                # TODO naive model not regularized!
                                naive_model = LogisticRegression(penalty=None,
                                                                 solver=lr_solver)


                            else :
                                full_model = LogisticRegression(penalty=lr_penalty,
                                                                solver=lr_solver,
                                                                C=C_reg)
                                # naive_model = LogisticRegression(penalty=lr_penalty,
                                #                                  solver=lr_solver,
                                #                                  C=C_reg)
                                # TODO naive model not regularized!
                                naive_model = LogisticRegression(penalty=None,
                                                                 solver=lr_solver)

                            full_model.fit(features_full_train, y_train)
                            y_pred_proba_full = full_model.predict_proba(features_full_test)

                            naive_model.fit(features_naive_train, y_train)
                            y_pred_proba_naive = naive_model.predict_proba(features_naive_test)

                            y_pred_full = full_model.predict(features_full_test)
                            y_pred_naive = naive_model.predict(features_naive_test)
                            fold_accuracy_full = balanced_accuracy_score(y_test, y_pred_full)
                            fold_accuracy_naive = balanced_accuracy_score(y_test, y_pred_naive)

                            y_test_all.append(y_test)
                            y_pred_proba_full_list.append(y_pred_proba_full)
                            y_pred_proba_naive_list.append(y_pred_proba_naive)
                            c_all.append(C_test[:, 0])

                            kfold_accuracy_full.append(fold_accuracy_full)
                            kfold_accuracy_naive.append(fold_accuracy_naive)

                            log_likelihood_full = log2_likelihood(y_test,
                                                                  y_pred_proba_full)
                            log_likelihood_naive = log2_likelihood(y_test,
                                                                   y_pred_proba_naive)
                            log_likelihood_full_list.append(log_likelihood_full)
                            log_likelihood_naive_list.append(log_likelihood_naive)
                            llratio = log_likelihood_full - log_likelihood_naive
                            llratios.append(llratio)


                        target = np.hstack(y_test_all)
                        predictions_full = np.vstack(y_pred_proba_full_list)
                        predictions_naive = np.vstack(y_pred_proba_naive_list)
                        confound = np.hstack(c_all)

                        log_likelihood_full = np.mean(log_likelihood_full_list)
                        log_likelihood_naive = np.mean(log_likelihood_naive)

                        accuracy_full = np.mean(kfold_accuracy_full)
                        accuracy_naive = np.mean(kfold_accuracy_naive)

                        predictability = np.mean(llratios)
                        mean_score = predictability
                        # if llratio > 2:

                        if s == 0 :
                            full_p = np.nan
                            full_sig = False
                            partial_p = np.nan
                            partial_sig = False

                            row = [animal_id, session_id,
                                   experiment, 'spikes',
                                   time, mean_score, accuracy_full,  X.shape[1], full_p,
                                   full_sig, partial_p, partial_sig]

                            df.loc[df.shape[0], :] = row

                            print('___________ time bin {} of {} ___ '
                                  't = {} ms - pred={:.2f} - acc={:.2f} '.format(
                                time_bin,
                                n_time_bins_per_trial,
                                time, predictability, accuracy_full))



                        else :
                            row = [animal_id, session_id,
                                   experiment, 'spikes_deconf',
                                   time, mean_score, np.nan, s, X.shape[1]]

                            df_null.loc[df_null.shape[0], :] = row




        df['q1'] = np.nan
        df['q2'] = np.nan
        df['significant'] = np.nan

        pars = {'settings_name' : settings_name,
                'experiment' : experiment,
                'align_to' : align_to,
                'seed' : seed,
                'binsize_in_ms' : binsize,
                'min_trials_per_class' : min_trials_per_class,
                'min_tot_trials' : min_tot_trials,
                'start_time_in_ms' : start_time_in_ms,
                'end_time_in_ms' : end_time_in_ms,
                'min_units' : min_units,
                'sessions' : selected_session_ids,
                'n_splits' : n_splits,
                # not strictly user defined
                'selected_trials' : dp,
                'n_time_bins_per_trial' : n_time_bins_per_trial,
                'warp' : warp,
                'warp_per_modality' : warp_per_modality,
                'larger_bins' : larger_bins,
                'median_reaction_times' : median_reaction_times,
                'method' : method,
                'surrogate_method' : surrogate_method,
                'n_shifts' : n_shifts,
                'n_shuffles' : n_shuffles,
                'tune_C' : tune_C,
                'Cs' : Cs,
                'lr_penalty' : lr_penalty,
                'lr_solver' : lr_solver,
                'n_splits' : n_splits,
                'seed' : seed,
                'confound_variable' : confound_variable}
                #'time_bin_edges' : time_bin_edges,
                #'time_bin_centers' : time_bin_centers}

        out = {'pars' : pars,
               'df' : df,
               'df_null' : df_null}

        print('Saving output to {}'.format(output_full_path))
        pickle.dump(out, open(output_full_path, 'wb'))




