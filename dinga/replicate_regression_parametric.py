import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from scipy.stats import pearsonr, t

n_samples = 200
n_simulations = 1000
confound_weight = 1  # 0.2, 0.3

p_vals = []
for i in range(n_simulations) :

    # generate confound
    C = np.random.uniform(0, 100, size=n_samples)

    # generate input as a function of the confound
    input_noise = np.random.normal(loc=0, scale=1, size=n_samples)
    X = 10 + 5 * C + input_noise *(10 + C * confound_weight)

    # generate outcome as a function of confound
    outcome_noise = np.random.normal(loc=0, scale=5, size=n_samples)
    y = 100 + 0.2 * C + outcome_noise

    X = X[:, np.newaxis]
    C = C[:, np.newaxis]

    # split data in train and test sets
    n_samples_train = int(n_samples / 2)

    X_train = X[0 :n_samples_train, :]
    X_test = X[n_samples_train:, :]

    C_train = C[0 :n_samples_train]
    C_test = C[n_samples_train :]

    y_train = y[0 :n_samples_train]
    y_test = y[n_samples_train :]

    #X_train = X_train -  LinearRegression().fit(C_train, X_train).predict(C_train)

    # compute ML predictions
    p = LinearRegression().fit(X_train, y_train).predict(
        X_test)

    #pearson_r, pearson_p_val = pearsonr(p, y_test)

    # regress out confounds from outcome and predictions (compute the residuals!)
    # TODO should this be cross-validated?
    p_reg = p - LinearRegression().fit(C_test, p).predict(C_test)
    y_reg = y_test - LinearRegression().fit(C_test, y_test).predict(C_test)

    # compute partial correlation
    pcor = pearsonr(p_reg, y_reg)

    # compute the t-statistic
    # t(y, p | C) = pcor(y, p | C)*sqrt((n-2-g)/(1-pcor(y,p | C)^2

    # TODO manual computation of p-value was somehow wrong!
    # n = n_samples
    # dof = n_samples
    # g = C.shape[1]
    # t_statistic = pcor * np.sqrt((n - 2 - g) / (1 - pcor) ** 2)
    #
    # p_value = (1 - t.cdf(abs(t_statistic), dof)) * 2
    # p_vals.append(p_value)

    p_vals.append(pcor.pvalue)

p_vals = np.array(p_vals)

false_positive_rate = 100 * (p_vals < 0.05).sum() / n_simulations

print(false_positive_rate)