import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from scipy.stats import pearsonr, t

n_samples = 200
n_simulations = 1000
confound_weight = 0.4  # 0.2, 0.3
n_permutations = 1000

p_vals = []
sig = []
for i in range(n_simulations) :

    # generate confound
    trend = np.sin(np.linspace(0, 6, n_samples))
    C = np.random.uniform(0, 100, size=n_samples) + 70 * trend

    # generate input as a function of the confound
    input_noise = np.random.normal(loc=0, scale=1, size=n_samples)
    X = 10 + 5 * C + input_noise  *(10+C*confound_weight)

    # generate outcome as a function of confound
    outcome_noise = np.random.normal(loc=0, scale=5, size=n_samples)
    y = 100 + 0.2 * C + outcome_noise

    X = X[:, np.newaxis]
    C = C[:, np.newaxis]

    # f, ax = plt.subplots(3, 1)
    # ax[0].plot(X)
    # ax[1].plot(C)
    # ax[2].plot(y)

    # split data in train and test sets
    n_samples_train = int(n_samples / 2)

    X_train = X[0 :n_samples_train, :]
    X_test = X[n_samples_train :, :]

    C_train = C[0 :n_samples_train]
    C_test = C[n_samples_train :]

    y_train = y[0 :n_samples_train]
    y_test = y[n_samples_train :]

    # compute ML predictions
    p = LinearRegression().fit(X_train, y_train).predict(
        X_test)

    #pearson_r, pearson_p_val = pearsonr(p, y_test)

    # regress out confounds from outcome and predictions
    p_reg = p - LinearRegression().fit(C_test, p).predict(C_test)
    y_reg = y_test - LinearRegression().fit(C_test, y_test).predict(C_test)

    # compute partial correlation
    pcor = pearsonr(p_reg, y_reg)[0]

    # compute the empirical null distribution of pcor values
    pcors = []
    for perm in range(n_permutations):
        y_test_shuf = np.random.permutation(y_test)
        # regress out confounds from outcome and predictions
        p_reg = p - LinearRegression().fit(C_test, p).predict(C_test)
        y_reg = y_test_shuf - LinearRegression().fit(C_test, y_test_shuf).predict(C_test)

        # compute partial correlation
        pcor = pearsonr(p_reg, y_reg)[0]
        pcors.append(pcor)

    critical_val = np.quantile(pcors, 0.95)

    if pcor >= critical_val:
        sig.append(True)
    else:
        sig.append(False)

    # compute the t-statistic
    # t(y, p | C) = pcor(y, p | C)*sqrt((n-2-g)/(1-pcor(y,p | C)^2
    #
    # n = n_samples
    # dof = n_samples
    # g = C.shape[1]
    # t_statistic = pcor * np.sqrt((n - 2 - g) / (1 - pcor) ** 2)
    #
    # p_value = (1 - t.cdf(abs(t_statistic), dof)) * 2
    # p_vals.append(p_value)


false_positive_rate = 100 * np.sum(sig) / len(sig)
print(false_positive_rate)