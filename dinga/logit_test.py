import numpy as np
from sklearn.linear_model import LogisticRegression
import statsmodels.api as sm
import scipy


x1 = np.array([1.5, 3, 4.5, 2.5, 6, 7.8, 9, 10.2, 1.2, 3.4])
x2 = np.array([3.4, 2.8, 6.5, 4, 3, 1, 8.9, 9.5, 2.3, 4])
X_test = np.vstack([x1, x2]).T
C_test = np.array([1, 1, 1, 1, 1, 2, 2, 2, 2, 2])
y_test = np.array([1, 0, 0, 0, 1, 0, 1, 1, 1, 1])

predictions = np.array([0.1, 0.05, 0.2, 0.8, 0.23, 0.95,
                        0.3, 0.7, 0.8, 0.87]).reshape(-1, 1)

lr = LogisticRegression().fit(X=predictions, y=y_test)
print(lr.intercept_)
lr.coef_



# building the model and fitting the data
log_reg = sm.Logit(endog=y_test,
                   exog=sm.add_constant(scipy.special.logit(predictions))).fit()
log_reg.summary()

pvals = log_reg.pvalues


features = np.hstack([sm.add_constant(scipy.special.logit(predictions)),
                      C_test.reshape(-1, 1)])
log_reg = sm.Logit(endog=y_test,
                   exog=features).fit()
log_reg.summary()

pvals = log_reg.pvalues