import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from scipy.stats import pearsonr, t
from sklearn.svm import SVR
from pingouin import partial_corr
import os
import pandas as pd
import seaborn as sns
from constants import *
from sklearn.linear_model import SGDRegressor
from sklearn.ensemble import RandomForestRegressor
from sklearn.model_selection import StratifiedKFold, KFold

"""
replicates the test_deconfounding.Rmd script from Dinga
"""


confound_weights = [0.1, 0.2, 0.3]

plot_format = 'png'
plots_folder = os.path.join(DATA_PATH, 'plots', 'dinga_replicate')
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)


# plot the simulate data or not
plot_data = False

def get_confounded_data_regression(n_samples, confound_weight):

    input_noise = np.random.normal(loc=0, scale=1, size=n_samples)
    outcome_noise = np.random.normal(loc=0, scale=5, size=n_samples)

    confound = np.linspace(start=0, stop=100, num=n_samples)
    x = input_noise * (10 + confound * confound_weight) + confound * 5
    y = 100 + outcome_noise + confound * 0.2

    df = pd.DataFrame()
    df['x'] = x
    df['y'] = y
    df['confound'] = confound
    return df



if plot_data:
    df_low = get_confounded_data_regression(n_samples=1000,
                                            confound_weight=confound_weights[0])
    df_med = get_confounded_data_regression(n_samples=1000,
                                            confound_weight=confound_weights[1])
    df_high = get_confounded_data_regression(n_samples=1000,
                                             confound_weight=confound_weights[2])

    f, ax = plt.subplots(1, 1)
    ax.plot(df_low['x'], label='x')
    ax.plot(df_low['y'], label='y')
    ax.plot(df_low['confound'], label='C')
    ax.legend()
    plt.tight_layout()
    sns.despine()


    f, axes = plt.subplots(1, 3, figsize=[12, 4], sharex=True, sharey=True)
    sns.scatterplot(data=df_low, x='confound', y='x', ax=axes[0])
    sns.scatterplot(data=df_med, x='confound', y='x', ax=axes[1])
    sns.scatterplot(data=df_high, x='confound', y='x', ax=axes[2])
    sns.despine()
    plt.tight_layout()
    plot_name = 'regression_data_V1.{}'.format(plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=400,
              bbox_inches="tight")

    f, axes = plt.subplots(1, 3, figsize=[12, 4], sharex=True, sharey=True)
    sns.scatterplot(data=df_low, x='confound', y='y', ax=axes[0])
    sns.scatterplot(data=df_med, x='confound', y='y', ax=axes[1])
    sns.scatterplot(data=df_high, x='confound', y='y', ax=axes[2])
    sns.despine()
    plt.tight_layout()
    plot_name = 'regression_data_V2.{}'.format(plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=400,
              bbox_inches="tight")


    f, axes = plt.subplots(1, 3, figsize=[12, 4], sharex=True, sharey=True)
    sns.scatterplot(data=df_low, x='x', y='y', ax=axes[0])
    sns.scatterplot(data=df_med, x='x', y='y', ax=axes[1])
    sns.scatterplot(data=df_high, x='x', y='y', ax=axes[2])
    sns.despine()
    plt.tight_layout()
    plot_name = 'regression_data_V3.{}'.format(plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=400,
              bbox_inches="tight")



# --- run simulation ---

n_simulations = 1000
n_train = 100
n_test = 100


for confound_weight in confound_weights:


    corr_input_adj = np.empty(shape=n_simulations)
    corr_output_adj = np.empty(shape=n_simulations)

    p_input_adj = np.empty(shape=n_simulations)
    p_output_adj = np.empty(shape=n_simulations)


    for i in range(n_simulations):
        # get train and test data
        df_train = get_confounded_data_regression(n_samples=n_train,
                                                  confound_weight=confound_weight)
        df_test = get_confounded_data_regression(n_samples=n_test,
                                                  confound_weight=confound_weight)

        C_train = df_train['confound'].__array__().reshape(-1, 1)
        X_train = df_train['x'].__array__()
        y_train = df_train['y'].__array__()

        C_test = df_test['confound'].__array__().reshape(-1, 1)
        X_test = df_test['x'].__array__()
        y_test = df_test['y'].__array__()

        # confound regression of the input variables
        lm = LinearRegression().fit(X=C_train, y=X_train)
        X_train_adjusted = X_train - lm.predict(C_train)
        X_test_adjusted = X_test - lm.predict(C_test)

        # get predictions
        svm = RandomForestRegressor().fit(y=y_train, X=X_train_adjusted.reshape(-1, 1))
        predictions = svm.predict(X_test_adjusted.reshape(-1, 1))

        # get results correlation and p.values for input adjusted data
        results_input_adj = pearsonr(predictions, y_test)

        # get confound adjusted results using proposed test-set adjustment
        # partial correlation and p.values

        # using partial_corr
        df = pd.DataFrame()
        df['predictions'] = predictions
        df['y_test'] = y_test
        df['C_test'] = C_test
        results_output_adj = partial_corr(data=df, x='predictions',
                                          y='y_test', covar='C_test')
        results_output_adj_pval = results_output_adj['p-val'][0]
        results_output_adj_r = results_output_adj['r'][0]

        # we can also manually regress out
        if False:
            # and we can use the predictions obtained from unadjusted data as well
            # svm = SVR().fit(y=y_train, X=X_train.reshape(-1, 1))
            # predictions = svm.predict(X_test.reshape(-1, 1))

            p_reg = predictions - LinearRegression().fit(C_test, predictions).predict(C_test)
            y_reg = y_test - LinearRegression().fit(C_test, y_test).predict(C_test)
            # compute partial correlation
            pcor = pearsonr(p_reg, y_reg)
            results_output_adj_pval = pcor.pvalue
            results_output_adj_r = pcor.statistic

        # save output
        p_input_adj[i] = results_input_adj.pvalue
        corr_input_adj[i] = results_input_adj.statistic

        p_output_adj[i] = results_output_adj_pval
        corr_output_adj[i] = results_output_adj_r


    # make results dataframe
    rf = pd.DataFrame()
    rf['p'] = np.hstack([p_input_adj, p_output_adj])
    rf['corr'] = np.hstack([corr_input_adj, corr_output_adj])
    rf['adj'] = np.repeat(['Input adj.', 'Post-hoc adj.'], n_simulations)
    rf['fp'] = rf['p'] < 0.05

    # plot the results
    palette = [sns.xkcd_rgb['orange'], sns.xkcd_rgb['blue']]

    f, ax = plt.subplots(1, 3, figsize=[12, 4])
    sns.histplot(data=rf, x='corr', ax=ax[0], hue='adj', palette=palette,
                 linewidth=0, alpha=0.6)
    sns.histplot(data=rf, x='p', ax=ax[1], hue='adj', palette=palette,
                 linewidth=0, alpha=0.6)
    iap = rf[rf['adj']=='Input adj.']['p']
    im = (iap < 0.05).sum() / iap.shape[0]
    oap = rf[rf['adj']=='Post-hoc adj.']['p']
    om = (oap < 0.05).sum() / oap.shape[0]
    ax[2].bar(x=[0, 1], height=[im, om], color=palette, alpha=0.6)
    ax[2].set_xticks([0, 1])
    ax[2].set_xticklabels(['Input adj.', 'Post-hoc adj.'])
    ax[2].axhline(0.05, c='grey', ls=':')
    ax[1].set_title('Confound weight: {}'.format(confound_weight))
    ax[0].set_xlim([-0.6, 0.6])
    sns.despine()
    plt.tight_layout()

    plot_name = 'regression_parametric_cw_{}.{}'.format(confound_weight, plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=400,
              bbox_inches="tight")





if False:
    def run_cv_regression(df, n_folds):

        # n_folds = 4
        # df = get_confounded_data_regression(n_samples=100,
        #                                     confound_weight=confound_weight)

        C = df['confound'].__array__().reshape(-1, 1)
        X = df['x'].__array__()
        y = df['y'].__array__()

        if X.shape.__len__() == 1:
            X = X.reshape(-1, 1)

        if C.shape.__len__() == 1:
            C = C.reshape(-1, 1)

        #kfold = StratifiedKFold(n_splits=n_folds, shuffle=True)
        kfold = KFold(n_splits=n_folds, shuffle=True)

        rs = pd.DataFrame(columns=['input_adj', 'output_adj'])

        for fold, (training_ind, testing_ind) in enumerate(kfold.split(X, y)):

            X_train = X[training_ind, :]
            X_test = X[testing_ind, :]

            C_train = C[training_ind, :]
            C_test = C[testing_ind, :]

            y_train = y[training_ind]
            y_test = y[testing_ind]

            # confound regression of the input variables
            lm = LinearRegression().fit(X=C_train, y=X_train)
            X_train_adjusted = X_train - lm.predict(C_train)
            X_test_adjusted = X_test - lm.predict(C_test)

            # get predictions
            svm = SVR().fit(y=y_train, X=X_train_adjusted.reshape(-1, 1))
            predictions = svm.predict(X_test_adjusted.reshape(-1, 1))

            # get results correlation and p.values for input adjusted data
            results_input_adj = pearsonr(predictions, y_test)

            # get confound adjusted results using proposed test-set adjustment
            # partial correlation and p.values

            # using partial_corr
            df = pd.DataFrame()
            df['predictions'] = predictions
            df['y_test'] = y_test
            df['C_test'] = C_test
            results_output_adj = partial_corr(data=df, x='predictions',
                                              y='y_test', covar='C_test')
            results_output_adj_pval = results_output_adj['p-val'][0]
            results_output_adj_r = results_output_adj['r'][0]

            # we can also manually regress out
            if False:
                # and we can use the predictions obtained from unadjusted data as well
                # svm = SVR().fit(y=y_train, X=X_train.reshape(-1, 1))
                # predictions = svm.predict(X_test.reshape(-1, 1))

                p_reg = predictions - LinearRegression().fit(C_test, predictions).predict(C_test)
                y_reg = y_test - LinearRegression().fit(C_test, y_test).predict(C_test)
                # compute partial correlation
                pcor = pearsonr(p_reg, y_reg)
                results_output_adj_pval = pcor.pvalue
                results_output_adj_r = pcor.statistic

            # save output
            #p_input_adj = results_input_adj.pvalue
            corr_input_adj = results_input_adj.statistic

            #p_output_adj = results_output_adj_pval
            corr_output_adj = results_output_adj_r

            rs.loc[rs.shape[0], :] = [corr_input_adj, corr_output_adj]

        return pd.DataFrame(rs.mean(axis=0)).T



    def run_cv_regression_permtest(df, n_folds, n_perms):
        # n_folds = 4
        # df = get_confounded_data_regression(n_samples=200,
        #                                     confound_weight=confound_weight)
        # n_perms = 1000

        results_cv = run_cv_regression(df, n_folds=n_folds)

        results_perm = []
        for i in range(n_perms):

            df_permuted = df.copy()
            df_permuted['x'] = np.random.permutation(df['x'])
            results_cv_perm = run_cv_regression(df_permuted, n_folds=n_folds)
            results_perm.append(results_cv_perm)

        results_perm = pd.concat(results_perm)

        input_adj_obs = results_cv['input_adj'].iloc[0]
        input_adj_perms = results_perm['input_adj']
        results_cv['input_adj_pval'] = (input_adj_perms > input_adj_obs).sum() / n_perms

        output_adj_obs = results_cv['output_adj'].iloc[0]
        output_adj_perms = results_perm['output_adj']
        results_cv['output_adj_pval'] = (output_adj_perms > output_adj_obs).sum() / n_perms

        return results_cv

    n_folds = 4
    df = get_confounded_data_regression(n_samples=200,
                                        confound_weight=confound_weight)
    n_perms = 1000

    results_cv = run_cv_regression_permtest(df, n_folds=n_folds, n_perms=n_perms)



    n_perms = 500
    n_simulations = 100
    n_samples = 200
    n_folds = 3
    confound_weight = 0.1

    rs = []
    for i in range(n_simulations):
        print(i)
        df = get_confounded_data_regression(n_samples=n_samples,
                                            confound_weight=confound_weight)
        results_cv = run_cv_regression_permtest(df, n_folds=n_folds,
                                                n_perms=n_perms)
        rs.append(results_cv)

    rs = pd.concat(rs)


    rf = pd.DataFrame()
    rf['p'] = np.hstack([rs['input_adj_pval'], rs['output_adj_pval']])
    rf['corr'] = np.hstack([rs['input_adj'], rs['output_adj']])
    rf['adj'] = np.repeat(['Input adj.', 'Post-hoc adj.'], n_simulations)
    rf['fp'] = rf['p'] < 0.05

    # plot the results
    palette = [sns.xkcd_rgb['orange'], sns.xkcd_rgb['blue']]

    f, ax = plt.subplots(1, 3, figsize=[12, 4])
    sns.histplot(data=rf, x='corr', ax=ax[0], hue='adj', palette=palette,
                 linewidth=0, alpha=0.6)
    sns.histplot(data=rf, x='p', ax=ax[1], hue='adj', palette=palette,
                 linewidth=0, alpha=0.6)
    iap = rf[rf['adj']=='Input adj.']['p']
    im = (iap < 0.05).sum() / iap.shape[0]
    oap = rf[rf['adj']=='Post-hoc adj.']['p']
    om = (oap < 0.05).sum() / oap.shape[0]
    ax[2].bar(x=[0, 1], height=[im, om], color=palette, alpha=0.6)
    ax[2].set_xticks([0, 1])
    ax[2].set_xticklabels(['Input adj.', 'Post-hoc adj.'])
    ax[2].axhline(0.05, c='grey', ls=':')
    ax[1].set_title('Confound weight: {}'.format(confound_weight))
    sns.despine()
    plt.tight_layout()


    """
    n_sims <- 100
    n_size <- 200
    n_folds <- 5 
    
    confound_weights <- c(0.1, 0.2, 0.3) # low, medium, high confounding
    
    all_results <- list()
    for (i_confound_weight in seq_along(confound_weights)){
      confound_weight <- confound_weights[i_confound_weight]
      print(confound_weight)
      
      results <- matrix(ncol = 4, nrow = n_sims)
      colnames(results) <- c('input_adj_cor', 'input_adj_pval',
                             'output_adj_cor', 'output_adj_pval')
      for (i in seq_len(n_sims)){
        print(i)
        df <- get_confounded_data_regression(n_size, confound_weight) 
        results[i,] <- run_cv_regression_permtest_earlystop(df, n_folds, 
                                                            max_perms = 1000)
      }
      all_results[[i_confound_weight]] <- results
    }
    """