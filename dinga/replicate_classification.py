import numpy as np
import matplotlib.pyplot as plt
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import StratifiedKFold
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import log_loss
from sklearn.model_selection import cross_val_predict

n_samples = 500



confound_weight = 0.6
C = np.repeat([0, 1], int(n_samples/2))

means = [0, 0]
# stdevs = sqrt(5),sqrt(2)
# corr = .3 / (sqrt(5)*sqrt(2) = .134
cov1 = [[1, 0.9], [0.9, 1]]
cov2 = [[1, -0.9], [-0.9, 1]]

X1 = np.random.multivariate_normal(means, cov1, int(n_samples/2))
X2 = np.random.multivariate_normal(means, cov2, int(n_samples/2))
X = np.vstack([X1, X2])

y1 = np.random.choice(a=[0, 1], size=int(n_samples/2), p=[1-confound_weight, confound_weight])
y2 = np.ones(int(n_samples/2)).astype(int)

y = np.hstack([y1, y2])

indx = np.random.permutation(np.arange(y.shape[0]))
X = X[indx, :]
C = C[indx].reshape(-1, 1)
y = y[indx]






rf = RandomForestClassifier(n_estimators=200)
ml_pred = cross_val_predict(rf, X, y)


kfold = StratifiedKFold(n_splits=2, shuffle=True,
                        random_state=92)
kfold_scores = []
C_all, y_test_all, y_pred_all = [], [], []

for fold, (training_ind, testing_ind) in enumerate(kfold.split(C, y)) :

    C_train = C[training_ind, :]
    C_test = C[testing_ind, :]

    y_train = y[training_ind]
    y_test = y[testing_ind]

    ml_train = ml_pred[training_ind]
    ml_test = ml_pred[testing_ind]

    M1 = LogisticRegression().fit(C_train, y_train)
    M1_pred = M1.predict(C_test)

    M2_train = np.hstack([C_train, ml_train[:, np.newaxis]])
    M2_test = np.hstack([C_test, ml_test[:, np.newaxis]])
    M2 = LogisticRegression().fit(M2_train, y_train)
    M2_pred = M2.predict(M2_test)

    M1_log_likelihood = -log_loss(y_test, M1_pred) * len(y_test)
    M2_log_likelihood = -log_loss(y_test, M2_pred) * len(y_test)

    LR = M1_log_likelihood - M2_log_likelihood
    print(LR)



