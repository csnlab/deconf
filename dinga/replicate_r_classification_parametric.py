import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from scipy.stats import pearsonr, t
from sklearn.svm import SVR
from pingouin import partial_corr
import statsmodels.api as sm
import scipy
import os
import pandas as pd
import seaborn as sns
from constants import *

from sklearn.model_selection import StratifiedKFold, KFold
plot_data = True

plot_format = 'png'
plots_folder = os.path.join(DATA_PATH, 'plots', 'dinga_replicate')
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)


confound_weights = [0.6, 0.7, 0.8]


def get_confounded_data_classification(n_samples, confound_weight):
    C = np.repeat([0, 1], int(n_samples/2)).reshape(-1, 1)

    means = [0, 0]
    cov1 = [[1, 0.9], [0.9, 1]]
    cov2 = [[1, -0.9], [-0.9, 1]]

    X1 = np.random.multivariate_normal(means, cov1, int(n_samples/2))
    X2 = np.random.multivariate_normal(means, cov2, int(n_samples/2))
    X = np.vstack([X1, X2])

    # TODO is confound weight really a confound weight?
    # if it's 0.5, y is not related to the confound, otherwise it is
    y1 = np.random.choice(a=[0, 1], size=int(n_samples/2), p=[confound_weight, 1-confound_weight])
    y2 = np.random.choice(a=[0, 1], size=int(n_samples/2), p=[1-confound_weight, confound_weight])

    #y2 = np.ones(int(n_samples/2)).astype(int)

    y = np.hstack([y1, y2])

    # indx = np.random.permutation(np.arange(y.shape[0]))
    # X = X[indx, :]
    # C = C[indx].reshape(-1, 1)
    # y = y[indx]

    df = pd.DataFrame()
    df['x1'] = X[:, 0]
    df['x2'] = X[:, 1]
    df['y'] = y
    df['confound'] = C

    return df

# TODO weight is not really a weight cause 0.5 is not confunded but 0 and 1
# are both confounded

if plot_data:
    n_samples=1000

    f, axes = plt.subplots(1, 3, figsize=[12, 4])
    for i, w in enumerate(confound_weights):
        df = get_confounded_data_classification(n_samples=n_samples,
                                                confound_weight=w)
        sns.scatterplot(data=df, x='x1', y='x2', hue='y', ax=axes[i])
        axes[i].set_title('Conf. weight: {}'.format(w))
    sns.despine()
    plt.tight_layout()

    plot_name = 'classification_data.{}'.format(plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=400,
              bbox_inches="tight")


def get_balanced(df, n):
    indx1 = np.random.choice(np.where(df['y']==0)[0], size=n)
    indx2 = np.random.choice(np.where(df['y']==1)[0], size=n)
    df = df.copy().iloc[np.hstack([indx1, indx2]), :]
    return df


for confound_weight in confound_weights:
    n_simulations = 1000
    n_samples = 200

    df_pool = get_confounded_data_classification(n_samples=10000,
                                                confound_weight=confound_weight)


    p_input_adj = []
    p_output_adj = []

    for i in range(n_simulations):
        # get train and test data

        df = get_balanced(df_pool, n_samples)

        from sklearn.model_selection import ShuffleSplit
        from sklearn.gaussian_process import GaussianProcessClassifier

        ss = ShuffleSplit(n_splits=2, test_size=0.5)
        train_index, test_index = next(ss.split(df))

        C_train = df['confound'].__array__().reshape(-1, 1)[train_index, :]
        X1_train = df['x1'].__array__().reshape(-1, 1)[train_index, :]
        X2_train = df['x2'].__array__().reshape(-1, 1)[train_index, :]
        y_train = df['y'].__array__()[train_index]

        C_test = df['confound'].__array__().reshape(-1, 1)[test_index, :]
        X1_test = df['x1'].__array__().reshape(-1, 1)[test_index, :]
        X2_test = df['x2'].__array__().reshape(-1, 1)[test_index, :]
        y_test= df['y'].__array__()[test_index]

        # confound regression of the input variables
        lm1 = LinearRegression().fit(X=C_train, y=X1_train)
        X1_train_adjusted = X1_train - lm1.predict(C_train)
        X1_test_adjusted = X1_test - lm1.predict(C_test)

        lm2 = LinearRegression().fit(X=C_train, y=X2_train)
        X2_train_adjusted = X2_train - lm2.predict(C_train)
        X2_test_adjusted = X2_test - lm2.predict(C_test)

        X_train = np.hstack([X1_train_adjusted, X2_train_adjusted])
        X_test = np.hstack([X1_test_adjusted, X2_test_adjusted])

        gp = GaussianProcessClassifier().fit(X=X_train, y=y_train)
        predictions = gp.predict_proba(X_test)[:, 0]

        features = sm.add_constant(scipy.special.logit(predictions))
        log_reg = sm.Logit(endog=y_test,
                           exog=features).fit()
        log_reg.summary()
        p_input_adj.append(log_reg.pvalues[1])

        features_with_conf = np.hstack([sm.add_constant(scipy.special.logit(predictions)),
                                        C_test])

        log_reg = sm.Logit(endog=y_test,
                           exog=features_with_conf).fit(method='bfgs')
        log_reg.summary()

        p_output_adj.append(log_reg.pvalues[1])


    # make results dataframe
    rf = pd.DataFrame()
    rf['p'] = np.hstack([p_input_adj, p_output_adj])
    rf['adj'] = np.repeat(['Input adj.', 'Post-hoc adj.'], n_simulations)
    rf['fp'] = rf['p'] < 0.05

    # plot the results
    palette = [sns.xkcd_rgb['orange'], sns.xkcd_rgb['blue']]

    f, ax = plt.subplots(1, 2, figsize=[9, 4])

    sns.histplot(data=rf, x='p', ax=ax[0], hue='adj', palette=palette,
                 linewidth=0, alpha=0.6)
    iap = rf[rf['adj']=='Input adj.']['p']
    im = (iap < 0.05).sum() / iap.shape[0]
    oap = rf[rf['adj']=='Post-hoc adj.']['p']
    om = (oap < 0.05).sum() / oap.shape[0]
    ax[1].bar(x=[0, 1], height=[im, om], color=palette, alpha=0.6)
    ax[1].set_xticks([0, 1])
    ax[1].set_xticklabels(['Input adj.', 'Post-hoc adj.'])
    ax[1].axhline(0.05, c='grey', ls=':')
    f.suptitle('Classification (parametric) - Confound weight: {}'.format(confound_weight))
    sns.despine()
    plt.tight_layout()

    plot_name = 'classification_parametric_cw_{}.{}'.format(confound_weight, plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=400,
              bbox_inches="tight")

