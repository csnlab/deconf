import os
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import quantities as pq
from elephant.spike_train_generation import StationaryPoissonProcess
from elephant.spike_train_generation import NonStationaryPoissonProcess
from elephant.spike_train_generation import NonStationaryGammaProcess
from elephant.statistics import BinnedSpikeTrain
from elephant.statistics import mean_firing_rate
import neo
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression, LogisticRegression, SGDClassifier
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from scipy.stats import norm
from sklearn.metrics import r2_score, make_scorer, accuracy_score
from constants import *
from sklearn.model_selection import KFold


"""
"""

plot_format = 'png'
plots_folder = os.path.join(DATA_PATH, 'plots', 'place_field_deconf')
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)

n_confounds = 1


ns = 500
n_splits = 5

C = np.sin(np.arange(0, ns))
C = C.reshape(-1, 1)

X = C**3 + np.random.normal(size=ns, loc=0, scale=0.3).reshape(-1, 1)


noise_corr = np.array([np.random.normal(loc=0, scale=i) for i in np.array(C+1)/4])
X = C**3 + noise_corr
y = np.int32(C>0).flatten()


f, ax = plt.subplots()
ax.scatter(C[:, 0][y==0], X[:, 0][y==0], c='r')
ax.scatter(C[:, 0][y==1], X[:, 0][y==1], c='b')

# regress out the confound
lr = LinearRegression().fit(C, X)
X_deconf = X - lr.predict(C)
f, ax = plt.subplots()
ax.scatter(C[:, 0][y==0], X_deconf[:, 0][y==0], c='r')
ax.scatter(C[:, 0][y==1], X_deconf[:, 0][y==1], c='b')

# split data

kfold = KFold(n_splits=n_splits, shuffle=True, random_state=92)

iterable = kfold.split(X, y)

accuracies_original = []
accuracies_deconf   = []
for fold, (training_ind, testing_ind) in enumerate(iterable) :

    X_train = X[training_ind, :]
    X_test = X[testing_ind, :]
    C_train = C[training_ind, :]
    C_test = C[testing_ind, :]
    y_train = y[training_ind]
    y_test = y[testing_ind]

    model = LogisticRegression()
    model.fit(X_train, y_train)
    y_pred = model.predict(X_test)

    acc = accuracy_score(y_test, y_pred)
    accuracies_original.append(acc)

    # regress out the confound
    lr = LinearRegression().fit(C_train, X_train)
    X_train_deconf = X_train - lr.predict(C_train)
    X_test_deconf = X_test - lr.predict(C_test)

    model = RandomForestClassifier()
    model.fit(X_train_deconf, y_train)
    y_pred = model.predict(X_test_deconf)

    acd = accuracy_score(y_test, y_pred)
    accuracies_deconf.append(acd)

print('Accuracy on original features: {}'.format(np.mean(accuracies_original)))

print('Accuracy on deconfounded features: {}'.format(np.mean(accuracies_deconf)))







# predict confound from deconfounded features
rf = RandomForestRegressor(n_estimators=500).fit(X_train_deconf, C_train)
C_pred_deconf = rf.predict(X_test_deconf)
r2_deconf = r2_score(y_true=C_test[:, 0], y_pred=C_pred_deconf)




