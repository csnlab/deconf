import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import quantities as pq
from elephant.spike_train_generation import StationaryPoissonProcess
from elephant.spike_train_generation import NonStationaryPoissonProcess
from elephant.spike_train_generation import NonStationaryGammaProcess
from elephant.statistics import BinnedSpikeTrain
from elephant.statistics import mean_firing_rate
import neo
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor


"""
Linear and nonlinear confound regression. 

Simply using np.linalg.lstsq as in Snoek leads to bad results when compared
with sklearn LinearRegression, probably because the latter automatically
from sklearn is better than adapting code from Snoek probably preprocesses the data 
(centering) before fitting.
"""


n_features = 3 # each feature is confounded by only one confound
n_confounds = 3

t_start = 0 * pq.s
t_stop = 40 * pq.s
n_samples = 200
min_rate = 0
max_rate = 50

average_rates = [45, 15, 25]

# generating the confound
line_coefficients = [-30, 40, 80]
confound_train = np.zeros(shape=[n_samples, n_confounds])
for i in range(n_confounds):
    line = line_coefficients[i] * np.arange(0, n_samples) / n_samples
    noise = np.random.normal(0, 0.5, size=n_samples)
    confound_i = line #+ noise
    confound_train[:, i] = confound_i


# generating the neural data
X_train = np.zeros([n_samples, n_features])

for j in range(n_features):

    rate = np.repeat(average_rates[j], n_samples) + confound_train[:, j]
    sampling_period = (t_stop-t_start) / n_samples
    rate_signal = neo.AnalogSignal(t_start=t_start, t_stop=t_stop,
                                   signal=rate*pq.Hz, sampling_period=sampling_period)
    spiketrain = NonStationaryPoissonProcess(rate_signal=rate_signal).generate_spiketrain()

    spiketrain = NonStationaryGammaProcess(rate_signal=rate_signal,
                                           shape_factor=2).generate_spiketrain()

    bst = BinnedSpikeTrain(spiketrain, bin_size=200 * pq.ms)
    X_train[:, j] = bst.to_array()

ss = StandardScaler()

# only regression, no centering: bad results
weights_ = np.linalg.lstsq(confound_train, X_train, rcond=None)[0]
X_corr = X_train - confound_train.dot(weights_)


# sklearn linear regression
reg = LinearRegression().fit(confound_train, X_train)
# if we use just the normal dot product it does not center
X_corr_sk = X_train - confound_train.dot(reg.coef_)
# if we use the predict method it centers
X_corr_sk = X_train - reg.predict(confound_train)


# non-linear regression
rfreg = RandomForestRegressor().fit(confound_train, X_train)
X_corr_rf = X_train - rfreg.predict(confound_train)



f, ax = plt.subplots(5, 1, figsize=[8, 8], sharey=False, sharex=True)
for i in range(n_confounds):
    ax[0].plot(np.arange(n_samples), confound_train[:, i],
               label='confound {}'.format(i+1))

for i in range(n_features):
    ax[1].plot(np.arange(n_samples), X_train[:, i],
               label='feature {},confound {}'.format(i+1, i+1))

for i in range(n_features):
    ax[2].plot(np.arange(n_samples), X_corr[:, i],
               label='feature {},confound {}'.format(i+1, i+1))

for i in range(n_features):
    ax[3].plot(np.arange(n_samples), X_corr_sk[:, i],
               label='feature {} deconf.'.format(i+1, i+1))

for i in range(n_features) :
    ax[4].plot(np.arange(n_samples), X_corr_rf[:, i],
               label='feature {} deconf.'.format(i + 1, i + 1))
for axx in ax:
    axx.axhline(0, c='grey', ls=':')
    #axx.legend()

ax[0].set_title('Confound')
ax[1].set_title('Features')
ax[2].set_title('Least squares, no centering')
ax[3].set_title('Linear regression with centering')
ax[4].set_title('Random forest regression with centering')
sns.despine()
plt.tight_layout()

