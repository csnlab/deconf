import os
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import quantities as pq
from elephant.spike_train_generation import StationaryPoissonProcess
from elephant.spike_train_generation import NonStationaryPoissonProcess
from elephant.spike_train_generation import NonStationaryGammaProcess
from elephant.statistics import BinnedSpikeTrain
from elephant.statistics import mean_firing_rate
import neo
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from scipy.stats import norm
from sklearn.metrics import r2_score, make_scorer
from constants import *

"""
Here we generate a dataset resembling place fields activated along a 
linear trajectory repeated for a number of trials. We regress out the 
features using a nonlinear regression, but the variance still covaries
with the confound, so the decoding of the confound is not impaired
by regressing out the confound from the firing rates. 

BUT: I show here that in the resulting data the confound (position) is still
encoded, but position is not related to the target. So this does not really make
sense. 

What I would need is
- position (confound) encoded in the data
- position predictive of the target
- no genuine correlation between data and target
So prediction of target are only mediated by the confound. Then, after regressing,
since the scale is still affected, I would still have predictive power.
"""

plot_format = 'png'
plots_folder = os.path.join(DATA_PATH, 'plots', 'place_field_deconf')
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)

n_features = 1 # each feature is confounded by only one confound
n_confounds = 1

n_samples_trial = 500
n_trials = 10

min_rate = 0
max_rate = 50
binsize = 100 * pq.ms
t_start = 0 * pq.s
t_stop = (n_samples_trial * n_trials * binsize).rescale(pq.s)
n_samples_total = n_samples_trial * n_trials

average_rate = 5

# generating the confound
C = np.zeros(shape=[n_samples_trial, n_confounds])
C[:, 0] = np.arange(0, n_samples_trial, 1)

confound_rates = np.zeros(shape=[n_samples_trial, n_features])
for i in range(n_features):
    bump = norm.pdf(np.arange(-10, 10, 0.05))[150:250]
    cr = np.zeros(n_samples_trial)
    cr[i*30:i*30+100] = bump
    confound_rates[:, i] = cr

# f, ax = plt.subplots(1, 1)
# for i in range(n_features):
#     ax.plot(confound_rates[:, i])
C = np.vstack(n_trials*[C])
confound_rates = np.vstack(n_trials*[confound_rates])


# generating the neural data
X = np.zeros([n_samples_trial * n_trials, n_features])

for j in range(n_features):

    rate = np.repeat(average_rate, n_samples_trial * n_trials) + 80 * confound_rates[:, j]
    sampling_period = binsize
    rate_signal = neo.AnalogSignal(t_start=t_start, t_stop=t_stop,
                                   signal=rate*pq.Hz, sampling_period=sampling_period)
    spiketrain = NonStationaryPoissonProcess(rate_signal=rate_signal).generate_spiketrain()

    # spiketrain = NonStationaryGammaProcess(rate_signal=rate_signal,
    #                                        shape_factor=2).generate_spiketrain()

    bst = BinnedSpikeTrain(spiketrain, bin_size=binsize)
    X[:, j] = bst.to_array()


# split data
n_samples_train = int(X.shape[0]/2)

X_train = X[0 :n_samples_train, :]
X_test = X[n_samples_train :, :]

C_train = C[0 :n_samples_train, :]
C_test = C[n_samples_train :, :]

# predict confound from features
rf = RandomForestRegressor(n_estimators=500).fit(X_train, C_train)
C_pred = rf.predict(X_test)
r2 = r2_score(y_true=C_test[:, 0], y_pred=C_pred)

# regress out the confound
rfdc = RandomForestRegressor(n_estimators=500).fit(C_train, X_train)
X_train_deconf = X_train - rfdc.predict(C_train)
X_test_deconf = X_test - rfdc.predict(C_test)

# predict confound from deconfounded features
rf = RandomForestRegressor(n_estimators=500).fit(X_train_deconf, C_train)
C_pred_deconf = rf.predict(X_test_deconf)
r2_deconf = r2_score(y_true=C_test[:, 0], y_pred=C_pred_deconf)



f, ax = plt.subplots(n_features+1, 1, figsize=[10, 8], sharex=True)
ax[0].plot(C[:, 0])
ax[0].set_yticks([])
ax[0].set_yticklabels([])
for i in range(n_features):
    ax[i+1].plot(X[:, i])
    ax[i + 1].set_yticks([])
    ax[i + 1].set_yticklabels([])
sns.despine()
plt.tight_layout()
plot_name = 'features.{}'.format(plot_format)
f.savefig(os.path.join(plots_folder, plot_name), dpi=400)


f, ax = plt.subplots(n_features+1, 1, figsize=[10, 8],sharex=True)
ax[0].plot(C[:, 0])
ax[0].set_yticks([])
ax[0].set_yticklabels([])
for i in range(n_features):
    ax[i+1].plot(np.arange(n_samples_train), X_train_deconf[:, i])
    ax[i+1].plot(np.arange(n_samples_train, n_samples_total), X_test_deconf[:, i], c='purple')
    ax[i + 1].set_yticks([])
    ax[i + 1].set_yticklabels([])
sns.despine()
plt.tight_layout()
plot_name = 'features_deconf.{}'.format(plot_format)
f.savefig(os.path.join(plots_folder, plot_name), dpi=400)


f, ax = plt.subplots(1, 1, figsize=[6, 4])
ax.plot(C_test[:, 0], label='$C$')
ax.plot(C_pred, label='$C_{pred}$')
ax.plot(C_pred_deconf, label='$C_{pred, deconf. }$')
ax.legend()
plt.tight_layout()
sns.despine()
plot_name = 'confound_prediction.{}'.format(plot_format)
f.savefig(os.path.join(plots_folder, plot_name), dpi=400)






# # predict X_train from the confound
# reg = LinearRegression().fit(confound_train, X_train)
# # if we use just the normal dot product it does not center
# X_corr_sk = X_train - confound_train.dot(reg.coef_)
# # if we use the predict method it centers
# X_train_lr_pred = reg.predict(confound_train)
# X_corr_sk = X_train - reg.predict(confound_train)
#
#
# rfreg = RandomForestRegressor(n_estimators=500).fit(confound_train, X_train[:, 0])
# X_train_rf_pred = rfreg.predict(confound_train)
# X_corr_rf = X_train - X_train_rf_pred[:, np.newaxis]
#
#
#
# RandomForestRegressor(n_estimators=500).fit(X_corr_rf, confound_train)
#
#
#
#
# f, ax = plt.subplots(4, 1, figsize=[8, 8], sharey=False, sharex=True)
# for i in range(n_confounds):
#     ax[0].plot(np.arange(n_samples_trial), confound_train[:, i],
#                label='confound {}'.format(i+1))
#
# for i in range(n_features):
#     ax[1].plot(np.arange(n_samples_trial), X_train[:, i],
#                label='feature'.format(i+1, i+1))
#     ax[1].plot(np.arange(n_samples_trial), X_train_rf_pred,
#                label='random forest prediction'.format(i+1, i+1))
#     ax[1].plot(np.arange(n_samples_trial), X_train_lr_pred,
#                label='linear reg. prediction'.format(i+1, i+1))
#
# for i in range(n_features):
#     ax[2].plot(np.arange(n_samples_trial), X_corr_sk[:, i],
#                label='feature {} deconf.'.format(i+1, i+1))
#
# for i in range(n_features) :
#     ax[3].plot(np.arange(n_samples_trial), X_corr_rf[:, i],
#                label='feature {} deconf.'.format(i + 1, i + 1))
# for axx in ax:
#     axx.axhline(0, c='grey', ls=':')
#
# ax[1].legend()
# ax[0].set_title('$C_{train}$')
# ax[1].set_title('$X_{train}$')
# ax[2].set_title('$X_{train, corr, sklearn.}$')
# ax[3].set_title('$X_{train, corr, random forest}$')
# sns.despine()
# plt.tight_layout()

