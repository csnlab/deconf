import os
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import quantities as pq
from elephant.spike_train_generation import StationaryPoissonProcess
from elephant.spike_train_generation import NonStationaryPoissonProcess
from elephant.spike_train_generation import NonStationaryGammaProcess
from elephant.statistics import BinnedSpikeTrain
from elephant.statistics import mean_firing_rate
import neo
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from constants import *

"""
Example with three confounds, two lines and a step function. 
One of the lines acts quadratically on the firing rate. 
The linear regression successfully regress out the step function
and the line, but if the effect of the confound is quadratic it fails. 
Nonlinear regressor (random forest) successfully removes the 
nonlinear confound. But in both cases the variance of the regressed-out
data varies with the confound, even though the effect on the mean
has been fixed.
"""


plot_format = 'png'
plots_folder = os.path.join(DATA_PATH, 'plots', 'place_field_deconf')
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)

n_features = 3 # each feature is confounded by only one confound
n_confounds = 3

t_start = 0 * pq.s
t_stop = 40 * pq.s
n_samples = 200
min_rate = 0
max_rate = 50
binsize = 200 * pq.ms

average_rates = [45, 15, 25]

# generating the confound
line_coefficients = [150, 30, 80]
confound_train = np.zeros(shape=[n_samples, n_confounds])
for i in range(n_confounds):
    line = line_coefficients[i] * np.arange(0, n_samples) / n_samples
    noise = np.random.normal(0, 0.5, size=n_samples)
    confound_i = line #+ noise
    confound_train[:, i] = confound_i

confound_train[:, 2] = np.zeros(n_samples)
confound_train[75:150, 2] += 150

# generating the neural data
X_train = np.zeros([n_samples, n_features])

for j in range(n_features):
    if j == 1:
        rate = np.repeat(average_rates[j], n_samples) + confound_train[:, j]**2
    else:
        rate = np.repeat(average_rates[j], n_samples) + confound_train[:, j]

    sampling_period = (t_stop-t_start) / n_samples
    rate_signal = neo.AnalogSignal(t_start=t_start, t_stop=t_stop,
                                   signal=rate*pq.Hz, sampling_period=sampling_period)
    spiketrain = NonStationaryPoissonProcess(rate_signal=rate_signal).generate_spiketrain()

    # spiketrain = NonStationaryGammaProcess(rate_signal=rate_signal,
    #                                        shape_factor=2).generate_spiketrain()

    bst = BinnedSpikeTrain(spiketrain, bin_size=binsize)
    X_train[:, j] = bst.to_array()

ss = StandardScaler()

weights_ = np.linalg.lstsq(confound_train, X_train, rcond=None)[0]
X_corr = X_train - confound_train.dot(weights_)

# predict X_train from the confound
reg = LinearRegression().fit(confound_train, X_train)
# if we use just the normal dot product it does not center
X_corr_sk = X_train - confound_train.dot(reg.coef_)
# if we use the predict method it centers
X_corr_sk = X_train - reg.predict(confound_train)


rfreg = RandomForestRegressor(n_estimators=500).fit(confound_train, X_train)
X_corr_rf = X_train - rfreg.predict(confound_train)



f, ax = plt.subplots(4, 1, figsize=[8, 8], sharey=False, sharex=True)
for i in range(n_confounds):
    ax[0].plot(np.arange(n_samples), confound_train[:, i],
               label='confound {}'.format(i+1))

for i in range(n_features):
    ax[1].plot(np.arange(n_samples), X_train[:, i],
               label='feature {},confound {}'.format(i+1, i+1))

for i in range(n_features):
    ax[2].plot(np.arange(n_samples), X_corr_sk[:, i],
               label='feature {} deconf.'.format(i+1, i+1))

for i in range(n_features) :
    ax[3].plot(np.arange(n_samples), X_corr_rf[:, i],
               label='feature {} deconf.'.format(i + 1, i + 1))
for axx in ax:
    axx.axhline(0, c='grey', ls=':')
    #axx.legend()
ax[0].set_title('$C_{train}$')
ax[1].set_title('$X_{train}$')
ax[2].set_title('$X_{train, corr}$ - linear regression')
ax[3].set_title('$X_{train, corr}$ - random forest')
sns.despine()
plt.tight_layout()

plot_name = 'nonlinear_confound.png'
f.savefig(os.path.join(plots_folder, plot_name), dpi=400)

