from scipy.stats import skewnorm, gamma
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns

n_samples = 500

C_0 = np.random.normal(loc=2, scale=1, size=n_samples)
X_0 = 0
w_cy = 2
w_xy = 0  # all information in goes via the confound
w_xc = 2


def f(x):
    return x


def g(x):
    return x

def h(x):
    a = 0
    b = 0
    d = 4
    N = gamma.rvs(a=1, loc=0, scale=1, size=x.shape[0])
    #N = np.random.normal(loc=0, scale=1, size=x.shape[0])
    return a * x + N * (b + d * x**2)


fig, ax = plt.subplots(1, 1, figsize=[4, 4])
N1 = gamma.rvs(a=0.3, loc=0, scale=1, size=1000)
N2 = gamma.rvs(a=1, loc=0, scale=1, size=1000)

sns.kdeplot(N1, c='red', ax=ax)
sns.kdeplot(N2, c='green', ax=ax)


# generate simulation
y = np.random.choice([0, 1], size=n_samples, p=[0.5, 0.5])
C = C_0 + w_cy * f(y)
X = X_0 + w_xy * g(y) + w_xc * h(C)


# plot data
fig, ax = plt.subplots(1, 3, figsize=[12, 4])

sns.kdeplot(X[y==0], c='red', ax=ax[0])
sns.kdeplot(X[y==1], c='green', ax=ax[0])

sns.kdeplot(C[y==0], c='red', ax=ax[1])
sns.kdeplot(C[y==1], c='green', ax=ax[1])

ax[2].scatter(C[y==0], X[y==0], c='red', label='y=0', s=4, linewidth=0)
ax[2].scatter(C[y==1], X[y==1], c='green', label='y=1', s=4, linewidth=0)
ax[2].legend()
#ax[2].axis('equal')
ax[2].axvline(0, ls=':', c='grey')
ax[2].axhline(0, ls=':', c='grey')

ax[0].set_xlabel('$X$')
ax[1].set_xlabel('$C$')
ax[2].set_xlabel('$C$')
ax[2].set_ylabel('$X$')

sns.despine()
plt.tight_layout()



fig, ax = plt.subplots(1, 1, figsize=[4, 4])
ax.scatter(C[y==0], X[y==0], c='red', label='y=0', s=20, linewidth=0,
           marker='o')
ax.scatter(C[y==1], X[y==1], c='green', label='y=1', s=20, linewidth=0,
           marker='o')
ax.legend(frameon=False)
#ax.axis('equal')
ax.axvline(0, ls=':', c='grey')
ax.axhline(0, ls=':', c='grey')
ax.set_xlabel('$C$')
ax.set_ylabel('$X$')
sns.despine()
plt.tight_layout()

