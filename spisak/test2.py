import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestClassifier
from scipy.stats import pearsonr, t
from mlconfound.stats import partial_confound_test, full_confound_test
from sklearn.model_selection import cross_val_predict


n_samples = 100

wyc = 2

y = np.repeat([0, 1], n_samples//2)

C = wyc * y + np.random.normal(loc=0, scale=0.1, size=n_samples)

X = C + np.random.uniform(0, 3)
X = X.reshape(-1, 1)

indx = np.random.permutation(np.arange(X.shape[0]))
C =

y_pred = cross_val_predict(RandomForestClassifier(n_estimators=100), X, y)

partial = partial_confound_test(y, y_pred, C,
                         num_perms=1000,
                         cat_y=True,
                         cat_yhat=True,
                         cat_c=False,
                         mcmc_steps=50,
                         cond_dist_method='gam',
                         return_null_dist=False,
                         random_state=None, progress=True,
                         n_jobs=-1)

full = full_confound_test(y, y_pred, C,
                         num_perms=1000,
                         cat_y=True,
                         cat_yhat=True,
                         cat_c=False,
                         mcmc_steps=50,
                         cond_dist_method='gam',
                         return_null_dist=False,
                         random_state=None, progress=True,
                         n_jobs=-1)

print('\n\n')

if partial.p < 0.05:
    print('Partial confounder test -> significant (p={:.4f})'.format(partial.p))
    print('\ny_pred partially driven by c')
else:
    print('Partial confounder test -> not significant (p={:.4f})'.format(partial.p))


if full.p < 0.05:
    print('Full confounder test -> significant (p={:.4f})'.format(full.p))
    print('\ny_pred not fully driven by c')
else:
    print('Full confounder test -> not significant (p={:.4f})'.format(full.p))
