import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from scipy.stats import pearsonr, t
from mlconfound.stats import full_confound_test

n_samples = 200
n_simulations = 1000
confound_weight = 0.1  # 0.2, 0.3

p_vals = []
sig = []
for i in range(n_simulations) :

    # generate confound
    C = np.random.uniform(0, 100, size=n_samples)

    # generate input as a function of the confound
    input_noise = np.random.normal(loc=0, scale=1, size=n_samples)
    X = 10 + 5 * C + input_noise  *(10+C*confound_weight)

    # generate outcome as a function of confound
    outcome_noise = np.random.normal(loc=0, scale=5, size=n_samples)
    y = 100 + 0.2 * C + outcome_noise

    X = X[:, np.newaxis]
    C = C[:, np.newaxis]

    # split data in train and test sets
    n_samples_train = int(n_samples / 2)

    X_train = X[0 :n_samples_train, :]
    X_test = X[n_samples_train :, :]

    C_train = C[0 :n_samples_train]
    C_test = C[n_samples_train :]

    y_train = y[0 :n_samples_train]
    y_test = y[n_samples_train :]

    # compute ML predictions

    y_pred = LinearRegression().fit(X_train, y_train).predict(X_test)

    if False:
        # DINGA
        p = LinearRegression().fit(X_train, y_train).predict(X_test)

        # pearson_r, pearson_p_val = pearsonr(p, y_test)

        # regress out confounds from outcome and predictions
        p_reg = p - LinearRegression().fit(C_test, p).predict(C_test)
        y_reg = y_test - LinearRegression().fit(C_test, y_test).predict(C_test)

        # compute partial correlation
        pcor = pearsonr(p_reg, y_reg)[0]

        # compute the empirical null distribution of pcor values
        pcors = []
        for perm in range(n_permutations):
            y_test_shuf = np.random.permutation(y_test)
            # regress out confounds from outcome and predictions
            p_reg = p - LinearRegression().fit(C_test, p).predict(C_test)
            y_reg = y_test_shuf - LinearRegression().fit(C_test, y_test_shuf).predict(C_test)

            # compute partial correlation
            pcor = pearsonr(p_reg, y_reg)[0]
            pcors.append(pcor)

        critical_val = np.quantile(pcors, 0.95)

        if pcor >= critical_val:
            sig.append(True)
        else:
            sig.append(False)

    out = full_confound_test(y_test, y_pred, C_test[:, 0],
                              num_perms=1000,
                              cat_y=False,
                            cat_yhat=False,
                            cat_c=False,
                            mcmc_steps=50,
                            cond_dist_method='gam',
                            return_null_dist=False,
                            random_state=None, progress=True,
                            n_jobs=-1)

    p_vals.append(out.p)

p_vals = np.array(p_vals)
false_positive_rate = 100 * (p_vals < 0.05).sum() / n_simulations
print(false_positive_rate)
    # SPISAK


