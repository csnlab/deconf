import os
import pickle
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
from constants import *
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from utils import *
from decoding_utils import decode, decode_alt, NotEnoughTrialsError, decode_alt_norepeats
from plotting_style import *
from scipy.stats import mannwhitneyu, bootstrap
import pickle
from scipy.stats import norm, gamma, combine_pvalues

"""
Decoding visual orientation and auditory frequency on ephys data.
"""

settings_name = 'mar2_reproduce_tune'

# DATA PARS
data_settings_name = 'aug22' #'aug22'#'true+conf_effect' #aug22
ephys_dataset = 'modid'
area_spikes_dataset = 'V1'
experiments = ['audiofreq_in_big_change']

# DECODING PARS
mode = 'reproduce' # 'reproduce', 'deconf', 'test'
linear_regression_regularization = False
tune_regularization_predictability = True
lr_penalty = 'l2'
Cs = [0.0001, 0.001, 0.01, 0.1, 1]
tune_hyperpars_decoder = False

# RF parameters
n_estimators = 50
max_depth = 3
min_samples_split = int(2)
min_samples_leaf = int(7)
max_features = 'sqrt'

run_spisak_full_test = False
run_spisak_partial_test = False
num_perms_spisak = 2500
distribution_match_binsize = None

# confound_modes = ['top_1_components', 'top_1_shuffled',
#                   'top_5_components', 'top_5_shuffled',
#                   'top_10_components', 'top_10_shuffled',
#                   'top_15_components', 'top_15_shuffled']

confound_modes = ['top_30_components',
                  'top_30_shuffled']

if mode == 'test':
    deconf_methods = ['raw',
        #'decode_from_confound',
        # 'distribution_match',
        #'linear_confound_regression',
        #'nonlinear_confound_regression',
        #'predictability_lin',
        'predictability_nonlin']
    decoders = ['logistic_regression', 'SVC']

    n_permutations = 0
    decode_all_time_bins = False
    time_bin_index = 3
    n_kfold_splits = 3
    experiments = ['audiofreq_in_big_change']
    session_ids = {'audiofreq_in_big_change': ['2018-08-13_14-31-39', '2018-08-14_14-30-15',
                                               '2018-08-15_14-13-50', '2020-01-16_11-18-35',
                                               '2021-04-20_18-27-54', '2021-04-29_15-55-12',
                                               '2019-03-12_11-28-33', '2019-03-13_13-01-19',
                                               '2019-03-08_14-39-42', '2019-03-12_15-41-39',
                                               '2019-03-13_15-32-33', '2019-04-11_11-12-47',
                                               '2019-12-13_12-46-26', '2019-12-13_09-59-24']}

elif mode == 'deconf':
    deconf_methods = ['raw',
                      'decode_from_confound',
                      #'distribution_match',
                      'linear_confound_regression',
                      'nonlinear_confound_regression',
                      'predictability_lin',
                      'predictability_nonlin',
                      'predictability_nonlin_ensemble']
    decoders = ['logistic_regression', 'SVC']
    n_permutations = 100
    decode_all_time_bins = True
    time_bin_index = None
    n_kfold_splits = 5
    experiments = ['audiofreq_in_big_change']
    # session_ids ={'audiofreq_in_big_change':  ['2019-03-08_14-39-42',
    #                                            '2020-01-16_11-18-35',
    #                                            '2019-12-13_12-46-26',
    #                                            '2019-12-13_09-59-24']}
    # session_ids = {'audiofreq_in_big_change':  ['2019-03-13_15-32-33',
    #                                             '2019-12-13_12-46-26']}

    session_ids ={'audiofreq_in_big_change':  ['2019-03-13_15-32-33',
                                               '2019-12-13_12-46-26',
                                               '2019-03-08_14-39-42',
                                               '2020-01-16_11-18-35',
                                               '2019-12-13_09-59-24']}

elif mode == 'deconf_TEST':
    deconf_methods = ['predictability_nonlin_ensemble']
    decoders = ['logistic_regression', 'SVC']
    n_permutations = 100
    decode_all_time_bins = True
    time_bin_index = None
    n_kfold_splits = 5
    experiments = ['audiofreq_in_big_change']
    # session_ids ={'audiofreq_in_big_change':  ['2019-03-08_14-39-42',
    #                                            '2020-01-16_11-18-35',
    #                                            '2019-12-13_12-46-26',
    #                                            '2019-12-13_09-59-24']}
    # session_ids = {'audiofreq_in_big_change':  ['2019-03-13_15-32-33',
    #                                             '2019-12-13_12-46-26']}

    session_ids = {'audiofreq_in_big_change': ['2019-03-08_14-39-42']}

elif mode == 'reproduce':

    deconf_methods = ['raw',
                      'decode_from_confound',
                      #'distribution_match',
                      #'linear_confound_regression',
                      #'nonlinear_confound_regression',
                      #'predictability_lin',
                      #'predictability_nonlin'
                      ]

    decoders = ['logistic_regression']
    n_permutations = 0
    decode_all_time_bins = False
    time_bin_index = 3
    n_kfold_splits = 5
    session_ids = None



elif mode == 'reproduce_selected':

    deconf_methods = ['raw',
        'decode_from_confound',
        # 'distribution_match',
        'linear_confound_regression',
        #'nonlinear_confound_regression',
        'predictability_lin',
        'predictability_nonlin']

    decoders = ['logistic_regression', 'SVC']
    n_permutations = 50
    decode_all_time_bins = False
    time_bin_index = 3
    n_kfold_splits = 3
    session_ids = None
    experiments = ['audiofreq_in_big_change']
    session_ids = {'audiofreq_in_big_change': ['2018-08-13_14-31-39', '2018-08-14_14-30-15',
                                               '2018-08-15_14-13-50', '2020-01-16_11-18-35',
                                               '2021-04-20_18-27-54', '2021-04-29_15-55-12',
                                               '2019-03-12_11-28-33', '2019-03-13_13-01-19',
                                               '2019-03-08_14-39-42', '2019-03-12_15-41-39',
                                               '2019-03-13_15-32-33', '2019-04-11_11-12-47',
                                               '2019-12-13_12-46-26', '2019-12-13_09-59-24']}

if len(experiments) > 1:
    raise ValueError
    print('Files will overwrite')

for experiment in experiments:

    data = load_decoding_data(settings_name=data_settings_name,
                              experiment_name=experiment,
                              dataset=ephys_dataset,
                              area_spikes=area_spikes_dataset)

    sessions = data['pars']['sessions']

    if session_ids is None:
        session_ids_iter = sessions['session_id'].__array__()
    elif isinstance(session_ids, dict):
        session_ids_iter = session_ids[experiment]
    else:
        session_ids_iter = session_ids

    for session_id in session_ids_iter:

        for confound_mode in confound_modes:

            df = pd.DataFrame(columns=['animal_id',
                                       'session_id',
                                       'dataset',
                                       'area_spikes',
                                       'n_neurons',
                                       'n_trials',
                                       'perc_y=1',
                                       'experiment',
                                       'decoder',
                                       'method',
                                       'time',
                                       'time_bin',
                                       't0',
                                       't1',
                                       'permuted',
                                       'permutation',
                                       'mean_score',
                                       'full_p',
                                       'full_sig',
                                       'partial_p',
                                       'partial_sig'])

            row = sessions[sessions['session_id'] == session_id].iloc[0, :]

            # for i, row in sessions.iterrows() :
            # get metadata
            animal_id = row['animal_id']
            area_spikes = row['area']

            if ephys_dataset == 'modid':
                data_key = session_id
            elif ephys_dataset == 'julien':
                data_key = '{}_{}'.format(session_id, area_spikes)

            area_spikes = data['data'][data_key]['area_spikes']
            try:
                sub_dataset = data['data'][data_key]['dataset']
            except KeyError:
                sub_dataset = 'none'
            target_name = data['data'][data_key]['target_name']
            n_trials = data['data'][data_key]['trial_df'].shape[0]

            print(session_id, n_trials, sub_dataset, confound_mode)

            # get time parameters
            time_bin_times = data['data'][data_key]['time_bin_times']
            if ephys_dataset == 'modid':
                time_bins = data['data'][data_key]['time_bins']
            elif ephys_dataset == 'julien':
                time_bins = data['data'][data_key]['time_bin_edges']

            n_time_bins_per_trial = len(time_bin_times)

            if decode_all_time_bins:
                time_bin_indexes = np.arange(n_time_bins_per_trial)
            else:
                time_bin_indexes = [time_bin_index]
            # --- Decode from spikes ---

            for time_bin_indx in time_bin_indexes:
                print('--- time bin {} of {}'.format(time_bin_indx, len(time_bin_indexes)))
                time = time_bin_times[time_bin_indx]
                t0, t1 = time_bins[time_bin_indx]

                # get decoding data
                X = data['data'][data_key]['binned_spikes'][time_bin_indx]
                C = data['data'][data_key]['binned_movement'][time_bin_indx]
                y = data['data'][data_key]['target']

                if ephys_dataset == 'julien':
                    C = np.where(np.isnan(C), np.nanmean(C, axis=0), C)

                if confound_mode == 'random':
                    #C = np.random.default_rng(seed=92).normal(loc=0, scale=2, size=[C.shape[0], C.shape[1]])
                    raise ValueError
                    #C = np.random.normal(loc=0, scale=1, size=[C.shape[0], C.shape[1]])
                    #C = np.random.random(size=[C.shape[0], C.shape[1]])
                    #C = np.random.normal(loc=0, scale=2, size=[C.shape[0], 3])

                elif confound_mode.split('_')[0] == 'top':
                    n_components = int(confound_mode.split('_')[1])
                    if confound_mode.split('_')[2] == 'components':
                        shuffle_components = False
                    elif confound_mode.split('_')[2] == 'shuffled':
                        shuffle_components = True
                    else:
                        raise ValueError

                    if shuffle_components:
                        if n_components == 1:
                            C = np.random.default_rng(seed=0).permutation(C[:, 0]).reshape(-1, 1)
                        else:
                            # C = C[:, 0:n_components]
                            # for col in range(C.shape[1]):
                            #     C[:, col] = np.random.default_rng(seed=col+2).permutation(C[:, col])
                            indx = np.random.default_rng(seed=3).permutation(C.shape[0])
                            C = C[indx, 0:n_components]

                    else:
                        if n_components == 1:
                            C = C[:, 0].reshape(-1, 1)
                        else:
                            C = C[:, 0:n_components]

                    #C = C[:, 0].reshape(-1, 1)


                elif confound_mode == 'full':
                    pass
                else:
                    raise ValueError

                perc_y1 = 100 * (y[y==1].shape[0] / y.shape[0])

                for deconf_method in deconf_methods:
                    print('---- method: {}'.format(deconf_method))

                    if np.isin(deconf_method, predictability_methods):
                        decoders_to_iterate = ['none']
                    else:
                        decoders_to_iterate = decoders

                    for decoder in decoders_to_iterate:

                        for nperm in range(n_permutations+1):

                            if nperm == 0:
                                C_dec = np.copy(C)
                                y_dec = np.copy(y)
                                permuted = False
                            elif nperm > 0:
                                random_index = np.random.default_rng(seed=nperm).permutation(np.arange(X.shape[0]))
                                # only if method is predictability we permute C together with y
                                if np.isin(deconf_method, predictability_methods):
                                    C_dec = C[random_index, :]
                                else:
                                    C_dec = np.copy(C)

                                y_dec = y[random_index]
                                permuted = True

                            # avoid running spisak tests on shuffled data
                            if permuted or np.isin(deconf_method, predictability_methods):
                                spisak_full = False
                                spisak_partial = False
                            else:
                                spisak_full = run_spisak_full_test
                                spisak_partial = run_spisak_partial_test

                            dec_out = decode_alt_norepeats(X=X, y=y_dec, C=C_dec,
                                                 deconf_method=deconf_method,
                                                 decoder=decoder,
                                                 standardize_features=True,
                                                 standardize_confound=True,
                                                 linear_regression_regularization=linear_regression_regularization,
                                                 distribution_match_binsize=distribution_match_binsize,
                                                 n_kfold_splits=n_kfold_splits,
                                                 tune_hyperpars_decoder=tune_hyperpars_decoder,
                                                 lr_penalty=lr_penalty,
                                                 run_spisak_full_test=spisak_full,
                                                 run_spisak_partial_test=spisak_partial,
                                                 num_perms_spisak=num_perms_spisak,
                                                 tune_regularization_predictability=tune_regularization_predictability,
                                                 Cs=Cs,
                                                 n_estimators=n_estimators, max_depth=max_depth,
                                                 min_samples_split=min_samples_split, min_samples_leaf=min_samples_leaf)

                            mean_score = dec_out['mean_score']
                            full_p = dec_out['full_p']
                            full_sig = dec_out['full_sig']
                            partial_p = dec_out['partial_p']
                            partial_sig = dec_out['partial_sig']

                            row = [animal_id,
                                   session_id,
                                   sub_dataset,
                                   area_spikes,
                                   X.shape[1],
                                   n_trials,
                                   perc_y1,
                                   experiment,
                                   decoder,
                                   deconf_method,
                                   time,
                                   time_bin_indx,
                                   t0,
                                   t1,
                                   permuted,
                                   nperm,
                                   mean_score,
                                   full_p,
                                   full_sig,
                                   partial_p,
                                   partial_sig]

                            df.loc[df.shape[0], :] = row


            #df['time'] = [t.item() for t in df['time']]

            output_folder = os.path.join(DATA_PATH, 'simulation_results',
                                         'SIMULATION_{}'.format(settings_name))

            if not os.path.isdir(output_folder):
                os.makedirs(output_folder)

            df['mean_score'] = pd.to_numeric(df['mean_score'])

            pars = {'settings_name' : settings_name,
                    'ephys_dataset' : ephys_dataset,
                    'area_spikes_dataset' : area_spikes_dataset,
                    'mode' : mode,
                    'session_ids' : session_ids,
                    'n_permutations' : n_permutations,
                    'decoders' : decoders,
                    'experiments' : experiments,
                    'deconf_methods' : deconf_methods,
                    'distribution_match_binsize' : distribution_match_binsize,
                    'n_kfold_splits' : n_kfold_splits,
                    'run_spisak_full_test' : run_spisak_full_test,
                    'run_spisak_partial_test' : run_spisak_partial_test,
                    'num_perms_spisak' : num_perms_spisak,
                    'n_time_bins_per_trial' : n_time_bins_per_trial,
                    'time_bin_times' : time_bin_times,
                    'time_bins' : time_bins,
                    'confound_mode' : confound_mode,
                    'tune_hyperpars_decoder' : tune_hyperpars_decoder,
                    'tune_regularization_predictability' : tune_regularization_predictability}

            out = {'pars' : pars,
                   'df' : df}

            output_full_path = os.path.join(output_folder, 'results_{}_{}_{}.pkl'.format(settings_name, confound_mode,
                                                                                         session_id))
            print('Saving simulation to {}'.format(output_full_path))
            pickle.dump(out, open(output_full_path, 'wb'))





if False:
    # --- PLOT SCATTER -------------------------------------------------------------

    df = df.drop(['time_bin', 't0', 't1'], axis='columns')


    methods_linestyles = {'raw' : '-',
                          'linear_confound_regression' : '--',
                          'nonlinear_confound_regression' : '-.',
                          'distribution_match' : ':'}


    f, ax = plt.subplots(1, 1, figsize=[4, 4])


    for deconf_method in ['confound'] + deconf_methods:

        score = df[df['method'] == deconf_method]['score'].__array__()

        if deconf_method == 'confound':
            c = 'grey'
            ls = '-'
        else:
            c = dataset_palette[dataset]
            ls = methods_linestyles[deconf_method]

        ax.plot(time_bin_times, score, c=c,
                ls=ls,
                label=deconf_method)

    if experiment == 'audiofreq_in_big_change':
        ax.set_ylabel('Accuracy of frequency decoding')

    elif experiment == 'visualori_in_big_change':
        ax.set_ylabel('Accuracy of orientation decoding')

    ax.set_xlabel('Time from stimulus onset [s]')
    ax.axvline(0, c='grey', ls='--')
    ax.axhline(0.5, c='grey', ls=':')
    ax.set_ylim([0, 1.01])
    ticks = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
    labels = [0, '', 0.2, '', 0.4, '', 0.6, '', 0.8, '', 1.0]

    ax.set_yticks(ticks)
    ax.set_yticklabels(labels, fontsize=8)
    ax.legend()
    sns.despine()
    plt.tight_layout()


    plot_name = 'decoding_ephys_{}_{}_{}.{}'.format(experiment, settings_name,
                                                session_id, plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=400,
              bbox_inches="tight")

