import os
import glob
import pickle
from constants import *
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from plotting_style import *
from scipy.stats import norm, gamma, combine_pvalues
from sklearn.preprocessing import minmax_scale
from matplotlib.lines import Line2D

settings_name = 'mar2_reproduce_tune'
#settings_name = 'feb26_visualori'

confound_mode = 'top_30_components'
plot_format = 'png'
plot_scatter = False
plot_session_over_time = False
plot_session_over_time_split = True
plot_session_over_time_by_method = False
plot_permutations_over_time = False
plot_violins = False
plot_stats = False
#plot_methods = ['predictability_nonlin_ensemble']

plot_methods   = ['raw',
                  'decode_from_confound',
                  # 'distribution_match',
                  'linear_confound_regression',
                  'nonlinear_confound_regression',
                  'predictability_lin',
                  'predictability_nonlin_ensemble']

method_palette['linear_confound_regression'] = sns.xkcd_rgb['light green']

# TODO you could also do a significance over time! Just percentage significant for method for time point

response_time_point = 0.2

alpha_level_permutations = 0.01
alpha_level_partial_test = 0.05

plots_folder = os.path.join(DATA_PATH, 'plots', 'ephys',
                            'DECODE_SETTINGS_{}'.format(settings_name), confound_mode)

if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)

output_folder = os.path.join(DATA_PATH, 'simulation_results',
                             'SIMULATION_{}'.format(settings_name))

files = glob.glob('{}/*'.format(output_folder))
dfs = []
session_ids = {}
for output_full_path in files:
    if confound_mode in output_full_path:
        res = pickle.load(open(output_full_path, 'rb'))
        dfs.append(res['df'])
        session_id = res['df']['session_id'].unique()
        experiment = res['df']['experiment'].unique()
        assert len(session_id)==1
        assert len(experiment)==1
        try:
            session_ids[experiment[0]].append(session_id[0])
        except KeyError:
            session_ids[experiment[0]] = []
            session_ids[experiment[0]].append(session_id[0])
        pars = res['pars']
df = pd.concat(dfs)

output_full_path = os.path.join(output_folder, 'results_{}_{}.pkl'.format(settings_name, confound_mode))


experiments = pars['experiments']
decoders = pars['decoders']
deconf_methods = [d for d in pars['deconf_methods'] if np.isin(d, plot_methods)]


#deconf_methods = [d for d in deconf_methods if d != 'predictability_lin']

n_time_bins_per_trial = pars['n_time_bins_per_trial']
time_bin_times = pars['time_bin_times']
n_permutations = pars['n_permutations']
run_spisak_partial_test = pars['run_spisak_partial_test']
time_bins = pars['time_bins']
time_bin_times = pars['time_bin_times']

response_time_bin = time_bins[time_bin_times==response_time_point, :][0]


df['scatter_size'] = minmax_scale(df['n_neurons'], feature_range=(20, 150))

if False:
    ds = df[(df['decoder'] == 'logistic_regression')
            & (df['method'] == 'raw')
            & (df['experiment'] == experiments[0])
            & (df['time'] == 0.2)
            & (df['permuted'] == False)]
    dss = ds.sort_values(by=['mean_score', 'n_trials'])[['session_id', 'mean_score', 'n_trials']]

    ds[(ds['mean_score'] >= 0.8) & (ds['n_trials'] >= 50)]['session_id'].__array__()
    ds[(ds['mean_score'] >= 0.6) & (ds['n_trials'] >= 50)]['session_id'].__array__()


    ds_cf = df[(df['decoder'] == 'logistic_regression')
            & (df['method'] == 'decode_from_confound')
            & (df['experiment'] == experiments[0])
            & (df['time'] == 0.2)
            & (df['permuted'] == False)]
    ds_raw = df[(df['decoder'] == 'logistic_regression')
            & (df['method'] == 'raw')
            & (df['experiment'] == experiments[0])
            & (df['time'] == 0.2)
            & (df['permuted'] == False)]
    ds = ds_cf.rename({'mean_score' : 'mean_score_cf'},axis=1)
    ds['mean_score_raw'] = ds_raw['mean_score'].__array__()

    dss = ds.sort_values(by=['mean_score_raw', 'n_trials'])[['session_id', 'mean_score_raw', 'mean_score_cf', 'n_trials']]
    dss[(dss['mean_score_raw'] >= 0.6) & (dss['mean_score_cf'] >= 0.6) &
        (dss['n_trials'] >= 45)]['session_id'].__array__()


    # session_ids = ['2018-08-10_10-14-01', '2018-08-14_14-30-15',
    #                '2020-01-16_11-18-35', '2019-03-12_11-28-33',
    #                '2019-03-08_14-39-42', '2019-03-12_15-41-39',
    #                '2019-03-13_15-32-33', '2019-12-13_12-46-26',
    #                '2019-12-13_09-59-24']

acc_methods = [m for m in deconf_methods if ~np.isin(m, predictability_methods)]
pred_methods = [m for m in deconf_methods if np.isin(m, predictability_methods)]
subsets = [acc_methods, pred_methods]
metrics = ['accuracy', 'predictability']


if plot_stats:
    raise ValueError
    # TODO no selection of time bins
    dp = pd.DataFrame(columns=['experiment',
                               'session_id',
                               'decoder',
                               'method',
                               'time',
                               'pvalue',
                               'mean_score',
                               'is_sig'])

    for experiment in experiments:
        try:
            sessions_iter = session_ids[experiment]
        except IndexError:
            sessions_iter = session_ids
        for session_id in sessions_iter:

            for subset, metric in zip(subsets, metrics):
                print(metric)
                if metric == 'accuracy':
                    fig_width = 3
                    decoders_plot = decoders
                elif metric == 'predictability':
                    fig_width = 1.8
                    decoders_plot = ['none']

                for decoder in decoders_plot:

                    for kk, deconf_method in enumerate(subset):
                        df_sel = df[(df['experiment'] == experiment) &
                                    (df['session_id'] == session_id) &
                                    (df['decoder'] == decoder) &
                                    (df['method'] == deconf_method) &
                                    (df['time'] == response_time_point)]

                        # print(kk, deconf_method)

                        obs = df_sel[df_sel['permuted'] == False]
                        perms = df_sel[(df_sel['permuted'] == True)]

                        obs_val = obs['mean_score'].iloc[0]
                        perms_val = perms['mean_score'].__array__()
                        p_val = (perms_val > obs_val).sum() / (n_permutations + 1)
                        partial_p = obs['partial_p'].iloc[0]
                        is_sig = p_val <= alpha_level_permutations

                        dp.loc[dp.shape[0], :] = [experiment, session_id,
                                                   decoder, deconf_method, response_time_point,
                                                   p_val, obs_val, is_sig]

    dsig=dp[dp['method']=='predictability_nonlin_ensemble']

    dps = dp[(dp['decoder'] == 'logistic_regression')
            & (dp['method'] == 'raw')
            & (dp['experiment'] == 'audiofreq_in_big_change')
            & (dp['time'] == 0.2)].sort_values(by='mean_score')
    selected_sessions = dps['session_id'][dps['is_sig']].__array__()

    ds[(ds['mean_score'] >= 0.6) & (ds['n_trials'] >= 30)]['session_id'].__array__()

    for experiment in experiments:
        for decoder in decoders:
            decoders_to_plot = [decoder, 'none']
            dplot = pd.DataFrame(columns=['method', 'perc_sig'])
            for method in deconf_methods:
                de = dp[(np.isin(dp['decoder'], decoders_to_plot)) &
                        (dp['experiment'] == experiment) &
                        (dp['method'] == method)]
                n_sig = de['is_sig'].astype(bool).sum()
                n_tot = de.shape[0]
                perc_sig = 100 * n_sig / n_tot
                dplot.loc[dplot.shape[0], :] = [method, perc_sig]

            f, ax = plt.subplots(1, 1, figsize=[2.5, 3])
            sns.barplot(data=dplot, x='method', y='perc_sig', palette=method_palette,
                        ax=ax)

            ax.set_xticklabels([method_labels[d._text] for d in ax.get_xticklabels()], rotation=90)
            ax.set_xlabel('')
            ax.set_ylabel('% significant')
            ax.set_ylim([0, 100])
            sns.despine()
            plt.tight_layout()

            plot_name = '{}_{}_barplots.{}'.format(experiment, decoder, plot_format)
            f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                      bbox_inches="tight")



    for experiment in experiments:
        for decoder in decoders:
            decoders_to_plot = [decoder, 'none']
            dplot = pd.DataFrame(columns=['method', 'overlap_with_raw'])

            draw = dp[(np.isin(dp['decoder'], decoders_to_plot)) &
                                (dp['experiment'] == experiment) &
                                (dp['method'] == 'raw')]
            sig_sessions_raw = draw[draw['is_sig']]['session_id']

            dconf = dp[(np.isin(dp['decoder'], decoders_to_plot)) &
                                (dp['experiment'] == experiment) &
                                (dp['method'] == 'decode_from_confound')]
            sig_sessions_deconf = dconf[dconf['is_sig']]['session_id']

            for method in deconf_methods:
                de = dp[(np.isin(dp['decoder'], decoders_to_plot)) &
                        (dp['experiment'] == experiment) &
                        (dp['method'] == method)]

                if method == 'raw' or method == 'decode_from_confound':
                    pass
                else:
                    sig_sessions_method = de[de['is_sig']]['session_id']
                    intrs = set(sig_sessions_raw).intersection(sig_sessions_deconf).intersection(set(sig_sessions_method))
                    overlap = 100 * len(intrs) / len(sig_sessions_raw)
                    dplot.loc[dplot.shape[0], :] = [method, overlap]

            f, ax = plt.subplots(1, 1, figsize=[2, 3])
            sns.barplot(data=dplot, x='method', y='overlap_with_raw', palette=method_palette,
                        ax=ax)
            ax.set_xticklabels([method_labels[d._text] for d in ax.get_xticklabels()], rotation=90)
            ax.set_xlabel('')
            ax.set_ylabel('% overlap')
            ax.set_ylim([0, 100])
            sns.despine()
            plt.tight_layout()

            plot_name = '{}_{}_overlap_with_raw_and_decfromconf_barplots.{}'.format(experiment, decoder, plot_format)
            f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                      bbox_inches="tight")

if plot_scatter:
    deconf_methods_iter = [d for d in deconf_methods if d != 'decode_from_confound']
    for experiment in experiments:
        for decoder in decoders:
            for j, deconf_method in enumerate(deconf_methods_iter):

                if np.isin(deconf_method, predictability_methods):
                    decsel = 'none'
                else:
                    decsel = decoder

                sel1 = df[(df['time'] == response_time_point) &
                          (df['experiment'] == experiment) &
                          (df['decoder'] == decsel) &
                          (df['method'] == deconf_method) &
                          (df['permuted'] == False)]

                if decsel == 'none':
                    decsel = 'logistic_regression'
                sel2 = df[(df['time'] == response_time_point) &
                          (df['experiment'] == experiment) &
                          (df['decoder'] == decsel) &
                          (df['method'] == 'decode_from_confound') &
                          (df['permuted'] == False)]

                np.testing.assert_array_equal(sel1['session_id'].__array__(),
                                               sel2['session_id'].__array__())

                vals1 = sel1['mean_score'].__array__()
                vals2 = sel2['mean_score'].__array__()
                scatter_size = sel1['scatter_size'].__array__()

                if pars['dataset'] == 'julien':
                    colors = np.array([areas_palette[d] for d in sel1['area_spikes'].__array__()])

                elif pars['dataset'] == 'modid':
                    colors = np.array([dataset_palette[d] for d in sel1['dataset'].__array__()])


                f, ax = plt.subplots(1, 1, figsize=[3, 3])

                if np.isin(deconf_method, predictability_methods):
                    axis_min, axis_max = -1, 1
                    ax.plot([-axis_min, axis_max], [-axis_min, axis_max], c='grey', ls=':')
                else:
                    axis_min, axis_max = 0.0, 1.05
                    ax.plot([0, 2], [0, 2], c='grey', ls=':')


                if run_spisak_partial_test:
                    sp_mask = sel1['partial_sig'].__array__()
                    ax.scatter(vals1[sp_mask], vals2[sp_mask], s=scatter_size[sp_mask],
                               c=colors[sp_mask], edgecolor='w', linewidth=0)

                    ax.scatter(vals1[~sp_mask], vals2[~sp_mask], s=scatter_size[sp_mask],
                               c=colors[sp_mask], edgecolor='w', linewidth=0, alpha=0.5)

                else:
                    ax.scatter(vals1, vals2, s=scatter_size,
                               c=colors, edgecolor='w', linewidth=0)

                if experiment == 'audiofreq_in_big_change':
                    ax.set_xlabel('Accuracy of frequency decoding\nfrom V1 spikes\n({})'.format(method_labels[deconf_method]))
                    ax.set_ylabel('Accuracy of frequency decoding\nfrom video data')

                elif experiment == 'visualori_in_big_change':
                    ax.set_xlabel('Accuracy of orientation decoding\nfrom V1 spikes\n({})'.format(method_labels[deconf_method]))
                    ax.set_ylabel('Accuracy of orientation decoding\nfrom video data')

                ax.set_xlim([axis_min, axis_max])
                ax.set_ylim([axis_min, axis_max])
                ax.axhline(0.5, c='grey', ls=':')
                ax.axvline(0.5, c='grey', ls=':')
                ticks = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
                labels = [0, '', 0.2, '', 0.4, '', 0.6, '', 0.8, '', 1.0]

                ax.set_xticks(ticks)
                ax.set_yticks(ticks)
                ax.set_xticklabels(labels, fontsize=8)
                ax.set_yticklabels(labels, fontsize=8)
                #ax.set_title(method)

                #ax.get_legend().remove()
                sns.despine()
                plt.tight_layout()

                plot_name = 'decoding_ephys_{}_{}_{}_{}.{}'.format(deconf_method, decoder,
                                                                experiment, settings_name,
                                                          plot_format)
                f.savefig(os.path.join(plots_folder, plot_name), dpi=400,
                          bbox_inches="tight")

if plot_permutations_over_time:
    for experiment in experiments:
        for session_id in session_ids[experiment]:
            for j, deconf_method in enumerate(plot_methods):

                if np.isin(deconf_method, predictability_methods):
                    decoders_iter = ['none']
                    metric = 'predictability'
                else:
                    decoders_iter = decoders
                    metric = 'accuracy'

                for decoder in decoders_iter:

                    f, ax = plt.subplots(1, 1, figsize=[3, 2.2])

                    top = []
                    bottom = []
                    true = []

                    for time_bin_indx in np.arange(n_time_bins_per_trial):
                        time_point = time_bin_times[time_bin_indx]

                        if np.isin(deconf_method, predictability_methods):
                            decsel = 'none'
                        else:
                            decsel = decoder

                        obs = df[(df['session_id'] == session_id) &
                                  (df['time'] == time_point) &
                                  (df['experiment'] == experiment) &
                                  (df['decoder'] == decsel) &
                                  (df['method'] == deconf_method) &
                                  (df['permuted'] == False)]['mean_score'].__array__()
                        assert obs.shape[0] == 1
                        true.append(obs[0])

                        vals = df[(df['session_id'] == session_id) &
                                  (df['time'] == time_point) &
                                  (df['experiment'] == experiment) &
                                  (df['decoder'] == decsel) &
                                  (df['method'] == deconf_method) &
                                  (df['permuted'] == True)]['mean_score'].__array__()
                        top.append(np.quantile(vals, 0.9))
                        bottom.append(np.quantile(vals, 0.1))

                    ax.plot(time_bin_times, true, c=method_palette[deconf_method])
                    ax.scatter(time_bin_times, true, s=30, c=method_palette[deconf_method],
                               edgecolor='w', label=method_labels[deconf_method], zorder=10)

                    ax.fill_between(time_bin_times, bottom, top,
                                    color=method_palette[deconf_method], alpha=0.3, linewidth=0, zorder=-2)

                    if n_permutations > 0:
                        dp = pd.DataFrame(columns=['time', 'method', 'pvalue'])
                        for k, time_point in enumerate(time_bin_times):
                            dfsel = df[(df['session_id'] == session_id) &
                                       (df['time'] == time_point) &
                                       (df['experiment'] == experiment) &
                                       (df['decoder'] == decoder) &
                                       (df['method'] == deconf_method)]

                            obs = dfsel[dfsel['permuted'] == False]['mean_score'].__array__()[0]
                            perms = dfsel[dfsel['permuted'] == True]['mean_score'].__array__()
                            p_val = (perms >= obs).sum() / (n_permutations + 1)

                            dp.loc[dp.shape[0], :] = [time_point,
                                                      deconf_method,
                                                      p_val]


                        left_edges = np.array(time_bin_times) - np.hstack(([0], np.diff(time_bin_times) / 2))
                        right_edges = np.array(time_bin_times) + np.hstack((np.diff(time_bin_times) / 2, [0]))

                        if metric == 'accuracy':
                            y = 1.2 - (j + 1) * 0.02
                        else:
                            y = 0.9 - (j + 1) * 0.02
                        dpsel = dp[dp['method'] == deconf_method]
                        for ii, (rowix, row) in enumerate(dpsel.iterrows()):
                            if row['pvalue'] < alpha_level_permutations:
                                ax.plot([left_edges[ii], right_edges[ii]], [y, y],
                                        c=method_palette[deconf_method], linewidth=3)

                    ax.set_xticks(time_bin_times)
                    ax.set_xticklabels(time_bin_times)
                    ax.set_xlabel('Time from stim. onset [s]')
                    ax.set_ylabel('Decoding accuracy')
                    ax.set_ylabel('Predictability')
                    ax.set_ylabel(metric_labels[metric])
                    if metric == 'accuracy':
                        ax.set_ylim([0.4, 1.2])
                        ax.set_yticks([0.25, 0.5, 0.75, 1])
                        ax.axhline(0.5, c='grey', ls=':')
                    else:
                        ax.set_ylim([-0.5, 0.7])
                        ax.set_yticks([-0.5, 0, 0.5, 1])
                        ax.axhline(0.0, c='grey', ls=':')

                    #ax1.legend(frameon=False, loc='lower left')
                    #ax2.legend(frameon=False, loc='lower right')
                    ax.axvspan(response_time_bin[0], response_time_bin[1], color=sns.xkcd_rgb['light blue'],
                                 zorder=-20, linewidth=0)
                    ax.axvline(0, c=sns.xkcd_rgb['dark grey'], ls='--', zorder=-20)
                    sns.despine()
                    plt.tight_layout()

                    plot_name = '{}_{}_singlesess_singlemethod_with_perms_{}_{}.{}'.format(experiment, session_id, decoder, deconf_method, plot_format)
                    f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                              bbox_inches="tight")

if plot_session_over_time:
    for experiment in experiments:
        for session_id in session_ids[experiment]:
            for decoder in decoders:

                f, ax1 = plt.subplots(1, 1, figsize=[3.5, 3])
                ax2 = ax1.twinx()  # instantiate a second Axes that shares the same x-axis

                for j, deconf_method in enumerate(deconf_methods):

                    x = []

                    for time_bin_indx in np.arange(n_time_bins_per_trial):
                        time_point = time_bin_times[time_bin_indx]



                        if np.isin(deconf_method, predictability_methods):
                            decsel = 'none'
                        else:
                            decsel = decoder

                        vals = df[(df['session_id'] == session_id) &
                                  (df['time'] == time_point) &
                                  (df['experiment'] == experiment) &
                                  (df['decoder'] == decsel) &
                                  (df['method'] == deconf_method) &
                                  (df['permuted'] == False)]['mean_score'].__array__()
                        center = vals.mean()
                        x.append(center)

                    if np.isin(deconf_method, predictability_methods):
                        ax = ax2
                    else:
                        ax = ax1

                    ax.plot(time_bin_times, x, c=method_palette[deconf_method])
                    ax.scatter(time_bin_times, x, s=30, c=method_palette[deconf_method],
                               edgecolor='w', label=method_labels[deconf_method], zorder=10)

                    if n_permutations > 0:
                        dp = pd.DataFrame(columns=['time', 'method', 'pvalue'])
                        for j, deconf_method in enumerate(deconf_methods):

                            for k, time_point in enumerate(time_bin_times):


                                if np.isin(deconf_method, predictability_methods):
                                    decsel = 'none'
                                else:
                                    decsel = decoder

                                dfsel = df[(df['session_id'] == session_id) &
                                           (df['time'] == time_point) &
                                           (df['experiment'] == experiment) &
                                           (df['decoder'] == decsel) &
                                           (df['method'] == deconf_method)]

                                obs = dfsel[dfsel['permuted'] == False]['mean_score'].__array__()[0]
                                perms = dfsel[dfsel['permuted'] == True]['mean_score'].__array__()
                                p_val = (perms >= obs).sum() / (n_permutations + 1)

                                dp.loc[dp.shape[0], :] = [time_point,
                                                          deconf_method,
                                                          p_val]

                        left_edges = np.array(time_bin_times) - np.hstack(([0], np.diff(time_bin_times) / 2))
                        right_edges = np.array(time_bin_times) + np.hstack((np.diff(time_bin_times) / 2, [0]))

                        for j, deconf_method in enumerate(deconf_methods):
                            y = 1.2 - (j + 1) * 0.02
                            dpsel = dp[dp['method'] == deconf_method]
                            for ii, (rowix, row) in enumerate(dpsel.iterrows()):
                                if row['pvalue'] < alpha_level_permutations:
                                    ax1.plot([left_edges[ii], right_edges[ii]], [y, y],
                                             c=method_palette[deconf_method], linewidth=3)

                ax1.set_xticks(time_bin_times)
                ax1.set_xticklabels(time_bin_times)
                ax1.set_xlabel('Time from stim. onset [s]')
                ax1.set_ylabel('Decoding accuracy')
                ax2.set_ylabel('Predictability')
                ax1.set_ylim([0.15, 1.2])
                ax1.set_yticks([0.25, 0.5, 0.75, 1])
                ax2.set_ylim([-0.5, 1])
                ax2.set_yticks([-0.5, 0, 0.5, 1])
                ax1.axhline(0.5, c='grey', ls=':')
                #ax1.legend(frameon=False, loc='lower left')
                #ax2.legend(frameon=False, loc='lower right')
                ax1.axvspan(response_time_bin[0], response_time_bin[1], color=sns.xkcd_rgb['light blue'],
                             zorder=-20, linewidth=0)
                ax1.axvline(0, c=sns.xkcd_rgb['dark grey'], ls='--', zorder=-20)
                sns.despine(right=False)
                plt.tight_layout()

                plot_name = '{}_{}_{}_singlesess_overtime.{}'.format(experiment, session_id, decoder, plot_format)
                f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                          bbox_inches="tight")

    f, ax = plt.subplots(1, 1, figsize=[2, 2])
    legend_elements = [plt.Line2D([0], [0], marker='o', color=method_palette[m],
                                  label=method_labels[m], lw=2,
                              markerfacecolor=method_palette[m],
                                  markeredgecolor='w',
                                  markersize=10) for m in deconf_methods]
    ax.legend(handles=legend_elements, loc='center', frameon=False,
              ncol=2)
    sns.despine(left=True, bottom=True)
    ax.axis('off')
    plt.show()
    plot_name = 'singlesess_overtime_legend.{}'.format(plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
              bbox_inches="tight")

if plot_session_over_time_by_method:
    for experiment in experiments:
        for decoder in decoders:
            for j, deconf_method in enumerate(deconf_methods):
                if np.isin(deconf_method, predictability_methods):
                    decsel = 'none'
                else:
                    decsel = decoder

                f, ax1 = plt.subplots(1, 1, figsize=[3.5, 3])
                ax2 = ax1.twinx()  # instantiate a second Axes that shares the same x-axis

                for session_id in session_ids[experiment]:

                    x = []
                    for time_bin_indx in np.arange(n_time_bins_per_trial):
                        time_point = time_bin_times[time_bin_indx]
                        vals = df[(df['session_id'] == session_id) &
                                  (df['time'] == time_point) &
                                  (df['experiment'] == experiment) &
                                  (df['decoder'] == decsel) &
                                  (df['method'] == deconf_method) &
                                  (df['permuted'] == False)]['mean_score'].__array__()
                        center = vals.mean()
                        x.append(center)

                    if np.isin(deconf_method, predictability_methods):
                        ax = ax2
                    else:
                        ax = ax1
                    ax.plot(time_bin_times, x, c=method_palette[deconf_method])
                    ax.scatter(time_bin_times, x, s=30, c=method_palette[deconf_method],
                               edgecolor='w', label=method_labels[deconf_method], zorder=10)

                ax1.set_xticks(time_bin_times)
                ax1.set_xticklabels(time_bin_times)
                ax1.set_xlabel('Time from stim. onset [s]')
                ax1.set_ylabel('Decoding accuracy')
                ax2.set_ylabel('Predictability')
                ax1.set_ylim([0.15, 1.2])
                ax1.set_yticks([0.25, 0.5, 0.75, 1])
                ax2.set_ylim([-0.5, 1])
                ax2.set_yticks([-0.5, 0, 0.5, 1])
                ax1.axhline(0.5, c='grey', ls=':')
                #ax1.legend(frameon=False, loc='lower left')
                #ax2.legend(frameon=False, loc='lower right')
                ax1.axvspan(response_time_bin[0], response_time_bin[1], color=sns.xkcd_rgb['light blue'],
                             zorder=-20, linewidth=0)
                ax1.axvline(0, c=sns.xkcd_rgb['dark grey'], ls='--', zorder=-20)
                sns.despine(right=False)
                plt.tight_layout()

                plot_name = 'bymethod_{}_{}_{}_singlesess_overtime.{}'.format(experiment, deconf_method, decoder, plot_format)
                f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                          bbox_inches="tight")

if plot_violins:
    for experiment in experiments:
        for session_id in session_ids[experiment]:

            for subset, metric in zip(subsets, metrics):
                print(metric)
                if metric == 'accuracy':
                    fig_width = 2.5
                    decoders_plot = decoders
                elif metric == 'predictability':
                    fig_width = 2
                    decoders_plot = ['none']

                for decoder in decoders_plot:

                    df_sel = df[(df['experiment'] == experiment) &
                                (df['session_id'] == session_id) &
                                (df['decoder'] == decoder) &
                                (df['time'] == response_time_point)]
                    perms = df_sel[(df_sel['permuted'] == True)]
                    print(df_sel.shape[0])

                    f, ax = plt.subplots(1, 1, figsize=[fig_width, 3])

                    if len(perms) > 1:
                        sns.violinplot(data=perms,
                                       ax=ax, x='method',
                                       y='mean_score', order=subset,
                                       color=sns.xkcd_rgb['light grey'],
                                       inner='quartile', linewidth=1)

                    for kk, deconf_method in enumerate(subset):
                        df_sel = df[(df['experiment'] == experiment) &
                                    (df['session_id'] == session_id) &
                                    (df['decoder'] == decoder) &
                                    (df['method'] == deconf_method) &
                                    (df['time'] == response_time_point)]
                        print(df_sel.shape[0])

                        # print(kk, deconf_method)

                        obs = df_sel[df_sel['permuted'] == False]
                        perms = df_sel[(df_sel['permuted'] == True)]

                        obs_val = obs['mean_score'].iloc[0]
                        print(obs_val)
                        perms_val = perms['mean_score'].__array__()
                        p_val = (perms_val > obs_val).sum() / (n_permutations + 1)
                        partial_p = obs['partial_p'].iloc[0]

                        if p_val < alpha_level_permutations:
                            ax.scatter(kk, obs_val, marker='o', s=60, c='r', zorder=10,
                                       linewidth=0, edgecolor='r')
                        else:
                            ax.scatter(kk, obs_val, marker='o', s=60, zorder=10,
                                       linewidth=2, edgecolor='r', facecolors='none')

                        if partial_p < alpha_level_partial_test:
                            ax.scatter(kk, 0.97, marker='*', s=40, c='blue', zorder=10)

                    if metric == 'predictability':
                        ax.set_ylim([-0.6, 0.6])
                        ax.set_yticks([-0.5, 0, 0.5])
                        ax.yaxis.tick_right()
                        ax.yaxis.set_label_position("right")
                        ax.axhline(0, ls=':', c='k', zorder=-5)
                        ax.set_ylabel('Predictability')
                        sns.despine(right=False, left=True, ax=ax)
                        ax.tick_params(axis='y', right=True)


                    elif metric == 'accuracy':
                        ax.set_ylim([0.2, 1])
                        ax.axhline(0.5, ls=':', c='k', zorder=-5)
                        ax.axvline(1.5, c='k', zorder=-5, linewidth=0.75)
                        ax.set_ylabel('Accuracy score')
                        sns.despine(right=True, left=False, ax=ax)
                        ax.set_yticks([0.5, 0.75, 1])

                    ax.set_xlabel('')
                    ax.set_xticklabels([method_labels[d] for d in subset], rotation=90)
                    plt.tight_layout()

                    plot_name = '{}_{}_{}_{}_singlesess_violins.{}'.format(experiment, session_id, decoder, metric, plot_format)
                    f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                              bbox_inches="tight")

if plot_session_over_time_split:
    for experiment in experiments:
        for session_id in session_ids[experiment]:
            for metric, deconf_methods in zip(['accuracy', 'predictability'], [acc_methods, pred_methods]):
                if metric == 'predictability':
                    decoders_iter = ['none']
                else:
                    decoders_iter = decoders
                for decoder in decoders_iter:

                    f, ax = plt.subplots(1, 1, figsize=[3, 2.2])

                    for j, deconf_method in enumerate(deconf_methods):

                        x = []
                        for time_bin_indx in np.arange(n_time_bins_per_trial):
                            time_point = time_bin_times[time_bin_indx]
                            vals = df[(df['session_id'] == session_id) &
                                      (df['time'] == time_point) &
                                      (df['experiment'] == experiment) &
                                      (df['decoder'] == decoder) &
                                      (df['method'] == deconf_method) &
                                      (df['permuted'] == False)]['mean_score'].__array__()
                            center = vals.mean()
                            x.append(center)

                        ax.plot(time_bin_times, x, c=method_palette[deconf_method])
                        ax.scatter(time_bin_times, x, s=30, c=method_palette[deconf_method],
                                   edgecolor='w', label=method_labels[deconf_method], zorder=10)

                        if n_permutations > 0:
                            dp = pd.DataFrame(columns=['time', 'method', 'pvalue'])
                            for j, deconf_method in enumerate(deconf_methods):
                                for k, time_point in enumerate(time_bin_times):
                                    dfsel = df[(df['session_id'] == session_id) &
                                               (df['time'] == time_point) &
                                               (df['experiment'] == experiment) &
                                               (df['decoder'] == decoder) &
                                               (df['method'] == deconf_method)]

                                    obs = dfsel[dfsel['permuted'] == False]['mean_score'].__array__()[0]
                                    perms = dfsel[dfsel['permuted'] == True]['mean_score'].__array__()
                                    p_val = (perms >= obs).sum() / (n_permutations + 1)

                                    dp.loc[dp.shape[0], :] = [time_point,
                                                              deconf_method,
                                                              p_val]

                            left_edges = np.array(time_bin_times) - np.hstack(([0], np.diff(time_bin_times) / 2))
                            right_edges = np.array(time_bin_times) + np.hstack((np.diff(time_bin_times) / 2, [0]))

                            for j, deconf_method in enumerate(deconf_methods):
                                if metric == 'accuracy':
                                    y = 1 - (j + 1) * 0.02
                                else:
                                    y = 0.7 - (j + 1) * 0.04
                                dpsel = dp[dp['method'] == deconf_method]
                                for ii, (rowix, row) in enumerate(dpsel.iterrows()):
                                    if row['pvalue'] < alpha_level_permutations:
                                        ax.plot([left_edges[ii], right_edges[ii]], [y, y],
                                                 c=method_palette[deconf_method], linewidth=3)

                    ax.set_xticks(time_bin_times)
                    ax.set_xticklabels(time_bin_times)
                    ax.set_xlabel('Time from stim. onset [s]')
                    ax.set_ylabel(metric_labels[metric])
                    if metric == 'accuracy':
                        ax.set_ylim([0.3, 1])
                        ax.set_yticks([0.5, 0.75, 1])
                        ax.axhline(0.5, c='grey', ls=':')
                    else:
                        ax.set_ylim([-0.6, 0.7])
                        ax.set_yticks([-0.5, 0, 0.5])
                        ax.axhline(0.0, c='grey', ls=':')

                    #ax1.legend(frameon=False, loc='lower left')
                    #ax2.legend(frameon=False, loc='lower right')
                    ax.axvspan(response_time_bin[0], response_time_bin[1], color=sns.xkcd_rgb['light blue'],
                                 zorder=-20, linewidth=0)
                    ax.axvline(0, c=sns.xkcd_rgb['dark grey'], ls='--', zorder=-20)
                    sns.despine(right=True)
                    plt.tight_layout()

                    plot_name = '{}_{}_{}_singlesess_overtime_{}.{}'.format(experiment, session_id, decoder, metric, plot_format)
                    f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                              bbox_inches="tight")



    f, ax = plt.subplots(1, 1, figsize=[2, 2])
    legend_elements = [plt.Line2D([0], [0], marker='o', color=method_palette[m],
                                  label=method_labels[m], lw=2,
                              markerfacecolor=method_palette[m],
                                  markeredgecolor='w',
                                  markersize=10) for m in deconf_methods]
    ax.legend(handles=legend_elements, loc='center', frameon=False,
              ncol=2)
    sns.despine(left=True, bottom=True)
    ax.axis('off')
    plt.show()
    plot_name = 'singlesess_overtime_legend.{}'.format(plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
              bbox_inches="tight")




f, ax = plt.subplots(1, 1, figsize=[2, 2])
legend_elements = [plt.Line2D([0], [0], marker='o', color='red',
                              label='$p<{}$'.format(alpha_level_permutations), lw=0,
                          markerfacecolor='red', markersize=10, markeredgewidth=2),
                   plt.Line2D([0], [0], marker='o', color='red',
                              label="Not significant, $p$$\geq$${}$".format(alpha_level_permutations), lw=0,
                              markerfacecolor='white', markersize=10, markeredgewidth=2)]
ax.legend(handles=legend_elements, loc='center', frameon=False,
          ncol=1)
sns.despine(left=True, bottom=True)
ax.axis('off')
plt.show()
plot_name = 'significant_legend.{}'.format(plot_format)
f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
          bbox_inches="tight")



