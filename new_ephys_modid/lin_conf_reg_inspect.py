import os
import pickle

import numpy as np
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
from constants import *
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from utils import *
from decoding_utils import decode, NotEnoughTrialsError
from plotting_style import *
from scipy.stats import mannwhitneyu, bootstrap
import pickle
from scipy.stats import norm, gamma, combine_pvalues
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import Ridge, RidgeCV
from sklearn.ensemble import RandomForestRegressor

experiment = 'audiofreq_in_big_change'
data_settings_name = 'aug22'
session_id = '2018-08-15_14-13-50'
data = load_decoding_data(settings_name=data_settings_name,
                          experiment_name=experiment)
time_bin_indx=3
# get decoding data
X = data['data'][session_id]['binned_spikes'][time_bin_indx]
C = data['data'][session_id]['binned_movement'][time_bin_indx]
y = data['data'][session_id]['target']

#
# X = np.load(os.path.join(DATA_PATH, 'X.npy'))
# C = np.load(os.path.join(DATA_PATH, 'C.npy'))
# y = np.load(os.path.join(DATA_PATH, 'y.npy'))

total_n_trials = X.shape[0]

kfold = StratifiedKFold(n_splits=3, shuffle=True,
                        random_state=92)
iterable = kfold.split(X, y)

for fold, (training_ind, testing_ind) in enumerate(iterable):

    # decoder = SGDClassifier(random_state=92)
    X_train = X[training_ind, :]
    X_test = X[testing_ind, :]
    y_train = y[training_ind]
    y_test = y[testing_ind]
    C_train = C[training_ind, :]
    C_test = C[testing_ind, :]

    # TODO scale before or after conf regression?
    if True:
        ss = StandardScaler()
        X_train = ss.fit_transform(X_train)
        X_test = ss.transform(X_test)

    if True:
        ss = StandardScaler()
        C_train = ss.fit_transform(C_train)
        C_test = ss.transform(C_test)

    # C_train  = np.random.normal(size=C_train.shape)
    # C_test  = np.random.normal(size=C_test.shape)

    reg = LinearRegression().fit(C_train, X_train)
    #reg = RidgeCV(alphas=[0.1, 1, 10], cv=None, scoring=None, store_cv_values=False).fit(C_train, X_train)
    reg = RandomForestRegressor(n_estimators=100, max_depth=2).fit(C_train, X_train)


    X_train = X_train - reg.predict(C_train)
    X_test = X_test - reg.predict(C_test)

    print('\nerror on train set:')
    print(np.square(X_train).sum())
    print('\nerror on test set:')

    print(np.square(X_test).sum())

