import os
import pickle

import numpy as np
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
from constants import *
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from utils import *
from decoding_utils import decode, NotEnoughTrialsError
from plotting_style import *
from scipy.stats import mannwhitneyu, bootstrap
import pickle
from scipy.stats import norm, gamma, combine_pvalues
from sklearn.linear_model import LinearRegression
from sklearn.linear_model import Ridge


"""
Decoding visual orientation and auditory frequency on modid data, comparing 
decoding from video PCAs and from neural data. 

several options for deconfounding:
'raw': neural data is not touched
'linear_confound_regression': linear confound regression on the neural data:
'nonlinear_confound_regression': non-linear confound regression on the neural data:
'confound_isolating_cv': applies confound isolating cross-validation
"""

settings_name = 'jan29_test'
data_settings_name = 'aug22'#'true+conf_effect'

experiments = ['audiofreq_in_big_change',
               'visualori_in_big_change']# 'visualori_in_big_change']#['audiofreq_in_big_change']#['visualori', 'audiofreq']
mode = 'test' # 'reproduce', 'deconf', 'test'


if mode == 'test':
    deconf_methods = [#'raw',
                      #'decode_from_confound',
                      #'distribution_match',
                      'linear_confound_regression',
                      'nonlinear_confound_regression',
                      #'predictability_lin',
                      #'predictability_nonlin'
                      ]
    decoders = ['SVC']

    n_permutations = 0
    decode_all_time_bins = True
    time_bin_index = None
    run_spisak_full_test = False
    run_spisak_partial_test = False
    num_perms_spisak = 2500
    n_kfold_splits = 3
    distribution_match_binsize = None
    min_trials_per_segment = 30
    experiments = ['audiofreq_in_big_change',
                   'visualori_in_big_change']
    session_ids = {'audiofreq_in_big_change' : ['2018-08-15_14-13-50'],
                   'visualori_in_big_change': ['2020-01-16_11-18-35']}


if mode == 'deconf':
    deconf_methods = ['raw',
                      'decode_from_confound',
                      #'distribution_match',
                      'linear_confound_regression',
                      #'nonlinear_confound_regression',
                      'predictability_lin',
                      'predictability_nonlin']
    decoders = ['logistic_regression', 'SVC']

    n_permutations = 50
    decode_all_time_bins = False
    time_bin_index = 3
    run_spisak_full_test = False
    run_spisak_partial_test = False
    num_perms_spisak = 2500
    n_kfold_splits = 3
    distribution_match_binsize = None
    min_trials_per_segment = 30
    session_ids = {'audiofreq_in_big_change' : ['2018-08-15_14-13-50',
                                                '2020-01-16_11-18-35',
                                                '2021-04-20_18-27-54'],
                    'visualori_in_big_change' : ['2020-01-16_11-18-35',
                                                '2021-04-22_13-45-10',
                                                '2020-01-23_16-24-17']}

if mode == 'reproduce':

    deconf_methods = ['raw',
                      'decode_from_confound']

    decoders = ['logistic_regression', 'SVC']
    n_permutations = 0
    decode_all_time_bins = False
    time_bin_index = 3
    run_spisak_full_test = False
    run_spisak_partial_test = False
    num_perms_spisak = 2500
    n_kfold_splits = 3
    distribution_match_binsize = None
    min_trials_per_segment = 30
    session_ids = None



df = pd.DataFrame(columns=['animal_id',
                           'session_id',
                           'dataset',
                           'area_spikes',
                           'n_neurons',
                           'experiment',
                           'decoder',
                           'method',
                           'time',
                           'time_bin',
                           't0',
                           't1',
                           'permuted',
                           'permutation',
                           'mean_score',
                           'full_p',
                           'full_sig',
                           'partial_p',
                           'partial_sig'])

for experiment in experiments:

    data = load_decoding_data(settings_name=data_settings_name,
                              experiment_name=experiment)

    sessions = data['pars']['sessions']

    if session_ids is None:
        session_ids_iter = sessions['session_id'].__array__()
    elif isinstance(session_ids, dict):
        session_ids_iter = session_ids[experiment]
    else:
        session_ids_iter = session_ids

    for session_id in session_ids_iter:

        row = sessions[sessions['session_id'] == session_id].iloc[0, :]

        # for i, row in sessions.iterrows() :
        # get metadata
        animal_id = row['animal_id']
        area_spikes = data['data'][session_id]['area_spikes']
        dataset = data['data'][session_id]['dataset']
        target_name = data['data'][session_id]['target_name']
        n_trials = data['data'][session_id]['trial_df'].shape[0]

        print(session_id, n_trials, dataset)

        # get time parameters
        time_bin_times = data['data'][session_id]['time_bin_times']

        time_bins = data['data'][session_id]['time_bins']
        n_time_bins_per_trial = len(time_bin_times)

        if decode_all_time_bins:
            time_bin_indexes = np.arange(n_time_bins_per_trial)
        else:
            time_bin_indexes = [time_bin_index]
        # --- Decode from spikes ---

        for time_bin_indx in time_bin_indexes:
            time = time_bin_times[time_bin_indx]
            t0, t1 = time_bins[time_bin_indx]

            # get decoding data
            X = data['data'][session_id]['binned_spikes'][time_bin_indx]
            C = data['data'][session_id]['binned_movement'][time_bin_indx][:, 0:1]
            y = data['data'][session_id]['target']

            np.save(os.path.join(DATA_PATH, 'X'), X)
            np.save(os.path.join(DATA_PATH, 'C'), C)
            np.save(os.path.join(DATA_PATH, 'y'), y)


            for deconf_method in deconf_methods:

                if np.isin(deconf_method, predictability_methods):
                    decoders_to_iterate = ['none']
                else:
                    decoders_to_iterate = decoders

                for decoder in decoders_to_iterate:

                    nperm = 0
                    while nperm <= n_permutations:

                        if nperm == 0:
                            C_dec = np.copy(C)
                            y_dec = np.copy(y)
                            permuted = False
                        elif nperm > 0:
                            random_index = np.random.permutation(np.arange(X.shape[0]))
                            # only if method is predictability we permute C together with y
                            if np.isin(deconf_method, predictability_methods):
                                C_dec = C[random_index, :]
                            else:
                                C_dec = np.copy(C)

                            y_dec = y[random_index]
                            permuted = True

                        # avoid running spisak tests on shuffled data
                        if permuted or np.isin(deconf_method, predictability_methods):
                            spisak_full = False
                            spisak_partial = False
                        else:
                            spisak_full = run_spisak_full_test
                            spisak_partial = run_spisak_partial_test


                        total_n_trials = X.shape[0]

                        kfold = StratifiedKFold(n_splits=n_kfold_splits, shuffle=True,
                                                random_state=92)
                        iterable = kfold.split(X, y)

                        for fold, (training_ind, testing_ind) in enumerate(iterable):

                            # decoder = SGDClassifier(random_state=92)
                            X_train = X[training_ind, :]
                            X_test = X[testing_ind, :]
                            y_train = y[training_ind]
                            y_test = y[testing_ind]
                            C_train = C[training_ind, :]
                            C_test = C[testing_ind, :]

                            # TODO scale before or after conf regression?
                            if False:
                                ss = StandardScaler()
                                X_train = ss.fit_transform(X_train)
                                X_test = ss.transform(X_test)

                            if False:
                                ss = StandardScaler()
                                C_train = ss.fit_transform(C_train)
                                C_test = ss.transform(C_test)

                            #C_train  = np.random.normal(size=C_train.shape)
                            #C_test  = np.random.normal(size=C_test.shape)

                        nperm += 1

                        try:

                            dec_out = decode(X=X, y=y_dec, C=C_dec,
                                             deconf_method=deconf_method,
                                             decoder=decoder,
                                             standardize_features=True,
                                             standardize_confound=True,
                                             posthoc_counterbalancing_binsize=distribution_match_binsize,
                                             n_kfold_splits=n_kfold_splits,
                                             run_spisak_full_test=spisak_full,
                                             run_spisak_partial_test=spisak_partial,
                                             num_perms_spisak=num_perms_spisak,
                                             predictability_tune_regularization=True)
                            nperm = nperm + 1

                        except NotEnoughTrialsError:
                            print('\nnperm={} Not enough trials \n'.format(nperm))
                            if nperm == 0:
                                raise ValueError
                                # if there are not enough trials to decode
                                # the original session, skip
                            continue
                                # if there are not enough trials to decode the resampled
                                # version, we try drawing another one

                        mean_score = dec_out['mean_score']
                        full_p = dec_out['full_p']
                        full_sig = dec_out['full_sig']
                        partial_p = dec_out['partial_p']
                        partial_sig = dec_out['partial_sig']

                        row = [animal_id,
                               session_id,
                               dataset,
                               area_spikes,
                               X.shape[1],
                               experiment,
                               decoder,
                               deconf_method,
                               time,
                               time_bin_indx,
                               t0,
                               t1,
                               permuted,
                               nperm,
                               mean_score,
                               full_p,
                               full_sig,
                               partial_p,
                               partial_sig]

                        df.loc[df.shape[0], :] = row


#df['time'] = [t.item() for t in df['time']]

output_folder = os.path.join(DATA_PATH, 'simulation_results',
                             'SIMULATION_{}'.format(settings_name))

if not os.path.isdir(output_folder):
    os.makedirs(output_folder)

df['mean_score'] = pd.to_numeric(df['mean_score'])

pars = {'settings_name' : settings_name,
        'session_ids' : session_ids,
        'n_permutations' : n_permutations,
        'decoders' : decoders,
        'experiments' : experiments,
        'deconf_methods' : deconf_methods,
        'distribution_match_binsize' : distribution_match_binsize,
        'n_kfold_splits' : n_kfold_splits,
        'run_spisak_full_test' : run_spisak_full_test,
        'run_spisak_partial_test' : run_spisak_partial_test,
        'num_perms_spisak' : num_perms_spisak,
        'n_time_bins_per_trial' : n_time_bins_per_trial,
        'time_bin_times' : time_bin_times,
        'time_bins' : time_bins}

out = {'pars' : pars,
       'df' : df}

output_full_path = os.path.join(output_folder, 'results.pkl'.format(settings_name))
print('Saving simulation to {}'.format(output_full_path))
pickle.dump(out, open(output_full_path, 'wb'))





if False:
    # --- PLOT SCATTER -------------------------------------------------------------

    df = df.drop(['time_bin', 't0', 't1'], axis='columns')


    methods_linestyles = {'raw' : '-',
                          'linear_confound_regression' : '--',
                          'nonlinear_confound_regression' : '-.',
                          'distribution_match' : ':'}


    f, ax = plt.subplots(1, 1, figsize=[4, 4])


    for deconf_method in ['confound'] + deconf_methods:

        score = df[df['method'] == deconf_method]['score'].__array__()

        if deconf_method == 'confound':
            c = 'grey'
            ls = '-'
        else:
            c = dataset_palette[dataset]
            ls = methods_linestyles[deconf_method]

        ax.plot(time_bin_times, score, c=c,
                ls=ls,
                label=deconf_method)

    if experiment == 'audiofreq_in_big_change':
        ax.set_ylabel('Accuracy of frequency decoding')

    elif experiment == 'visualori_in_big_change':
        ax.set_ylabel('Accuracy of orientation decoding')

    ax.set_xlabel('Time from stimulus onset [s]')
    ax.axvline(0, c='grey', ls='--')
    ax.axhline(0.5, c='grey', ls=':')
    ax.set_ylim([0, 1.01])
    ticks = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
    labels = [0, '', 0.2, '', 0.4, '', 0.6, '', 0.8, '', 1.0]

    ax.set_yticks(ticks)
    ax.set_yticklabels(labels, fontsize=8)
    ax.legend()
    sns.despine()
    plt.tight_layout()


    plot_name = 'decoding_ephys_{}_{}_{}.{}'.format(experiment, settings_name,
                                                session_id, plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=400,
              bbox_inches="tight")

