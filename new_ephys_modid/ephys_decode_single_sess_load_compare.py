import os
import pickle
from constants import *
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd
import seaborn as sns
from plotting_style import *
from scipy.stats import norm, gamma, combine_pvalues
from sklearn.preprocessing import minmax_scale


settings_names = ['feb6_reproduce_significance', 'feb6_reproduce_significance_tuned']
confound_mode = 'top_10_components'
plot_format = 'png'
plot_scatter = False
plot_session_over_time = False
plot_violins = False
plot_stats = True

response_time_point = 0.2

alpha_level_permutations = 0.05
alpha_level_partial_test = 0.05


# plots_folder = os.path.join(DATA_PATH, 'plots', 'ephys',
#                             'DECODE_SETTINGS'.format(settings_name))
#
# if not os.path.isdir(plots_folder) :
#     os.makedirs(plots_folder)

dp = pd.DataFrame(columns=['settings',
                           'experiment',
                           'session_id',
                           'decoder',
                           'method',
                           'time',
                           'pvalue',
                           'mean_score',
                           'is_sig'])

dc = []
for settings_name in settings_names:
    output_folder = os.path.join(DATA_PATH, 'simulation_results',
                                 'SIMULATION_{}'.format(settings_name))
    output_full_path = os.path.join(output_folder, 'results_{}_{}.pkl'.format(settings_name, confound_mode))
    res = pickle.load(open(output_full_path, 'rb'))

    df = res['df']
    df['mean_score'] = pd.to_numeric(df['mean_score'])

    pars = res['pars']

    experiments = pars['experiments']
    decoders = pars['decoders']
    deconf_methods = pars['deconf_methods']


    deconf_methods = [d for d in deconf_methods if d != 'predictability_lin']

    n_time_bins_per_trial = pars['n_time_bins_per_trial']
    time_bin_times = pars['time_bin_times']
    n_permutations = pars['n_permutations']
    run_spisak_partial_test = pars['run_spisak_partial_test']
    time_bins = pars['time_bins']
    time_bin_times = pars['time_bin_times']

    response_time_bin = time_bins[time_bin_times==response_time_point, :][0]

    session_ids = pars['session_ids']
    if session_ids is None:
        session_ids = df['session_id'].unique()


    df['scatter_size'] = minmax_scale(df['n_neurons'], feature_range=(20, 150))

    ds = df[(df['decoder'] == 'logistic_regression')
            & (df['method'] == 'raw')
            & (df['experiment'] == 'audiofreq_in_big_change')
            & (df['time'] == 0.2)
            & (df['permuted'] == False)]
    dss = ds.sort_values(by=['mean_score', 'n_trials'])[['session_id', 'mean_score', 'n_trials']]
    dc.append(dss)

    if pars['n_permutations'] > 1:
        acc_methods = [m for m in deconf_methods if ~np.isin(m, predictability_methods)]
        pred_methods = [m for m in deconf_methods if np.isin(m, predictability_methods)]
        subsets = [acc_methods, pred_methods]
        metrics = ['accuracy', 'predictability']

        for experiment in experiments:
            try:
                sessions_iter = session_ids[experiment]
            except IndexError:
                sessions_iter = session_ids
            for session_id in sessions_iter:

                for subset, metric in zip(subsets, metrics):
                    if metric == 'accuracy':
                        fig_width = 3
                        decoders_plot = decoders
                    elif metric == 'predictability':
                        fig_width = 1.8
                        decoders_plot = ['none']

                    for decoder in decoders_plot:

                        for kk, deconf_method in enumerate(subset):
                            df_sel = df[(df['experiment'] == experiment) &
                                        (df['session_id'] == session_id) &
                                        (df['decoder'] == decoder) &
                                        (df['method'] == deconf_method) &
                                        (df['time'] == response_time_point)]
                            print(df_sel.shape[0])

                            # print(kk, deconf_method)

                            obs = df_sel[df_sel['permuted'] == False]
                            perms = df_sel[(df_sel['permuted'] == True)]

                            obs_val = obs['mean_score'].iloc[0]
                            perms_val = perms['mean_score'].__array__()
                            p_val = (perms_val > obs_val).sum() / (n_permutations + 1)
                            partial_p = obs['partial_p'].iloc[0]
                            is_sig = p_val <= alpha_level_permutations

                            dp.loc[dp.shape[0], :] = [settings_name, experiment, session_id,
                                                       decoder, deconf_method, response_time_point,
                                                       p_val, obs_val, is_sig]

    dsig=dp[dp['method']=='predictability_nonlin']




dm = dc[0].merge(right=dc[1], on='session_id', how='left')

dm['improvement'] = dm['mean_score_y'] >= dm['mean_score_x']
dm['improvement_amount'] = 100*dm['mean_score_y'] - 100*dm['mean_score_x']


perc_improved = 100* dm['improvement'].sum()/dm.shape[0]
print('% improved = {}'.format(perc_improved))

print('Total improvement: {}'.format(dm['improvement_amount'].sum()))



for settings_name in settings_names:
    for experiment in experiments:
        for decoder in decoders:
            decoders_to_plot = [decoder, 'none']
            dplot = pd.DataFrame(columns=['method', 'perc_sig'])
            for method in deconf_methods:
                de = dp[(np.isin(dp['decoder'], decoders_to_plot)) &
                        (dp['experiment'] == experiment) &
                        (dp['method'] == method) &
                        (dp['settings'] == settings_name)]
                n_sig = de['is_sig'].astype(bool).sum()
                n_tot = de.shape[0]
                print(n_tot)
                perc_sig = 100 * n_sig / n_tot
                dplot.loc[dplot.shape[0], :] = [method, perc_sig]

            print('Settings: {} - DECODER: {}'.format(settings_name, decoder))
            print(dplot)


