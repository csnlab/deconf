import os
import pickle
from constants import *
import numpy as np
import pandas as pd
from constants import *
from sklearn.model_selection import StratifiedKFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, recall_score
from sklearn.metrics import balanced_accuracy_score
import pickle
import quantities as pq
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.preprocessing import StandardScaler
from utils import *
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from confound_prediction.deconfounding import *

settings_name = 'may22'

data_settings_name = 'may22'#'true+conf_effect'
experiment = 'visualori_in_big_change'

data = load_decoding_data(settings_name=data_settings_name,
                          experiment_name=experiment)

sessions = data['pars']['sessions']


# --- decoding pars ---
decoder_name = 'SGD'
n_splits = 3
n_repeats = 3
score_name = 'accuracy'


# --- DECODING ----------------------------------------------------------------

df = pd.DataFrame(columns=['animal_id', 'session_id', 'dataset',
                           'area_spikes', 'experiment', 'mode',
                           'decoder', 'time', 'time_bin', 't0', 't1',
                           'repeat', 'score', 'n_neurons'])

for mode in ['neural', 'confound']:

    for i, row in sessions.iterrows() :

        animal_id = row['animal_id']
        session_id = row['session_id']
        time_bin_times = data['data'][session_id]['time_bin_times']
        time_bins = data['data'][session_id]['time_bins']
        n_time_bins_per_trial = len(time_bin_times)

        print('\n\n--- DECODING --- {:02g} of {:02g}'
              '\nAnimal ID: {}'
              '\nSession ID: {}'
              '\nMode: {}\n\n'.format(i, sessions.shape[0], animal_id, session_id, mode))


        for time_bin in range(n_time_bins_per_trial) :

            time = time_bin_times[time_bin]
            t0, t1 = time_bins[time_bin]

            if mode == 'neural':
                X = data['data'][session_id]['binned_spikes'][time_bin]
                C = data['data'][session_id]['binned_movement'][time_bin]
                C = C.sum(axis=1)

            elif mode == 'confound':
                X = data['data'][session_id]['binned_movement'][time_bin]

            y = data['data'][session_id]['target']

            print('\n\nTime bin {} of {}'.format(time_bin + 1, n_time_bins_per_trial))


            for repeat in range(n_repeats) :

                X = np.random.randint(0, 100, size=[30, 4])
                y = np.random.randint(0, 2, size=30)
                z = np.random.normal(0, 2, size=30)

                confound_isolating_cv(X, y, C, random_seed=None,
                                      min_sample_size=None,
                                      cv_folds=3, n_remove=4)


                print('- Repeat {}/{}'.format(repeat, n_repeats))

                kfold = StratifiedKFold(n_splits=n_splits, shuffle=True,
                                        random_state=repeat)
                kfold_scores = []
                y_test_all, y_pred_all = [], []

                for fold, (training_ind, testing_ind) in enumerate(kfold.split(X, y)) :

                    if decoder_name == 'random_forest' :
                        decoder = RandomForestClassifier(n_estimators=n_estimators)
                    elif decoder_name == 'SGD' :
                        decoder = SGDClassifier()
                    else :
                        raise NotImplementedError

                    X_train = X[training_ind, :]
                    X_test = X[testing_ind, :]
                    y_train = y[training_ind]
                    y_test = y[testing_ind]

                    if mode == 'neural':
                        C_train = C[training_ind, :]
                        C_test = C[testing_ind, :]

                        # regress out the confound
                        #reg = RandomForestRegressor(n_estimators=500).fit(C_train, X_train)
                        reg = LinearRegression().fit(C_train, X_train)
                        X_train = X_train - reg.predict(C_train)
                        X_test = X_test - reg.predict(C_test)

                    ss = StandardScaler()
                    X_train = ss.fit_transform(X_train)
                    X_test = ss.transform(X_test)

                    # ss = StandardScaler()
                    # C_train = ss.fit_transform(C_train)
                    # C_test = ss.transform(C_test)

                    decoder.fit(X_train, y_train)
                    y_pred = decoder.predict(X_test)
                    score = balanced_accuracy_score(y_test, y_pred)
                    kfold_scores.append(score)
                    y_test_all.append(y_test)
                    y_pred_all.append(y_pred)

                y_test_all = np.hstack(y_test_all)
                y_pred_all = np.hstack(y_pred_all)

                mean_score = np.mean(kfold_scores)

                row = [animal_id, session_id, dataset, area_spikes,
                       experiment_name, mode,
                       decoder_name, time, time_bin,
                       t0, t1, repeat, mean_score,
                       X.shape[1]]

                df.loc[df.shape[0], :] = row

numeric_cols = ['score']

for col in numeric_cols :
    df[col] = pd.to_numeric(df[col])




df_backup = df.copy()
# --- PLOT SCATTER -------------------------------------------------------------
import matplotlib.pyplot as plt
from sklearn.preprocessing import minmax_scale
import seaborn as sns

plot_format = 'png'
plots_folder = os.path.join(DATA_PATH, 'plots', 'modid')
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)


time_range = [0, 0.3]

df = df[(df['t0'] >= time_range[0]) & (df['t1'] <= time_range[1])]

df = df.groupby([i for i in df.columns if ~np.isin(i, ['repeat', 'score',
                                                       'time', 'time_bin', 't0', 't1'])]).mean().reset_index()

dz = pd.merge(df[df['mode'] == 'neural'],
              df[df['mode'] == 'confound'][['session_id', 'score', 'experiment']],
              on=['session_id', 'experiment'])

dz = dz.rename({'score_x' : 'score_neural', 'score_y' : 'score_confound'}, axis=1)

dz['size'] = minmax_scale(dz['n_neurons'], feature_range=(20, 150))
all_scores = np.hstack([dz['score_neural'].__array__(), dz['score_confound'].__array__()])
scmin = all_scores.min()
scmax = all_scores.max()

axis_min, axis_max = 0.35, 1
assert axis_min < scmin

f, ax = plt.subplots(1, 1, figsize=[3, 3])

for dataset in dataset_palette.keys():
    dxx = dz[(dz['experiment'] == experiment_name) & (dz['dataset'] == dataset)]
    ax.scatter(dxx['score_neural'], dxx['score_confound'], s=dxx['size'],
               c=dataset_palette[dataset], edgecolor='w')

if experiment_name == 'audiofreq_in_big_change':
    ax.set_xlabel('Accuracy of frequency decoding\nfrom V1 spikes')
    ax.set_ylabel('Accuracy of frequency decoding\nfrom video data')

elif experiment_name == 'visualori_in_big_change':
    ax.set_xlabel('Accuracy of orientation decoding\nfrom V1 spikes')
    ax.set_ylabel('Accuracy of orientation decoding\nfrom video data')

ax.plot([0, 2], [0, 2], c='grey', ls=':')
ax.set_xlim([axis_min, 1])
ax.set_ylim([axis_min, 1])
ax.axhline(0.5, c='grey', ls=':')
ax.axvline(0.5, c='grey', ls=':')
ax.set_xticks([0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0])
ax.set_yticks([0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0])

#ax.get_legend().remove()
sns.despine()
plt.tight_layout()


plot_name = 'decoding_scatter_linconfreg_{}_{}.{}'.format(experiment_name, settings_name, plot_format)
plt.savefig(os.path.join(plots_folder, plot_name), dpi=400, bbox_inches="tight")

