import numpy as np
import matplotlib.pyplot as plt
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from scipy.stats import pearsonr, t

import pandas as pd
import seaborn as sns

def get_confounded_data_regression(n_samples, confound_weight):

    #input_noise = np.random.normal(loc=0, scale=1, size=n_samples)
    #outcome_noise = np.random.normal(loc=0, scale=5, size=n_samples)

    input_noise = np.array([0.1, 0.2, 0.3, 0.1, 0.2, 0.3, 0.1, 0.2, 0.3, 0.1])
    outcome_noise = np.array([0.1, 0.2, 0.3, 0.1, 0.2, 0.3, 0.1, 0.2, 0.3, 0.1])

    confound = np.linspace(start=0, stop=100, num=n_samples)
    x = input_noise * (10 + confound * confound_weight) + confound * 5
    y = 100 + outcome_noise + confound * 0.2

    df = pd.DataFrame()
    df['x'] = x
    df['y'] = y
    df['confound'] = confound
    return df


df_low = get_confounded_data_regression(n_samples=1000, confound_weight=0.1)
df_med = get_confounded_data_regression(n_samples=1000, confound_weight=0.2)
df_high = get_confounded_data_regression(n_samples=1000, confound_weight=0.8)


f, ax = plt.subplots(1, 1)
ax.plot(df_low['x'], label='x')
ax.plot(df_low['y'], label='y')
ax.plot(df_low['confound'], label='C')
ax.legend()
plt.tight_layout()
sns.despine()


f, axes = plt.subplots(1, 3, figsize=[12, 4], sharex=True, sharey=True)
sns.scatterplot(data=df_low, x='confound', y='x', ax=axes[0])
sns.scatterplot(data=df_med, x='confound', y='x', ax=axes[1])
sns.scatterplot(data=df_high, x='confound', y='x', ax=axes[2])
sns.despine()
plt.tight_layout()

f, axes = plt.subplots(1, 3, figsize=[12, 4], sharex=True, sharey=True)
sns.scatterplot(data=df_low, x='confound', y='y', ax=axes[0])
sns.scatterplot(data=df_med, x='confound', y='y', ax=axes[1])
sns.scatterplot(data=df_high, x='confound', y='y', ax=axes[2])
sns.despine()
plt.tight_layout()

f, axes = plt.subplots(1, 3, figsize=[12, 4], sharex=True, sharey=True)
sns.scatterplot(data=df_low, x='x', y='y', ax=axes[0])
sns.scatterplot(data=df_med, x='x', y='y', ax=axes[1])
sns.scatterplot(data=df_high, x='x', y='y', ax=axes[2])
sns.despine()
plt.tight_layout()


# --- just to check the code ---
n_train = 10
n_test = 10

confound_weight = 0.2
df_train = get_confounded_data_regression(n_samples=n_train,
                                          confound_weight=confound_weight)
df_test = get_confounded_data_regression(n_samples=n_test,
                                          confound_weight=confound_weight)

C_train = df_train['confound'].__array__().reshape(-1, 1)
X_train = df_train['x'].__array__()
y_train = df_train['y'].__array__()

C_test = df_test['confound'].__array__().reshape(-1, 1)
X_test = df_test['x'].__array__()
y_test = df_test['y'].__array__()

lm = LinearRegression().fit(X=C_train, y=X_train)

X_train_adjusted = X_train - lm.predict(C_train)
X_test_adjusted = X_test - lm.predict(C_test)


from sklearn.svm import SVR
from pingouin import partial_corr

svm = LinearRegression().fit(y=y_train, X=X_train_adjusted.reshape(-1, 1))
predictions = svm.predict(X_test_adjusted.reshape(-1, 1))

results_input_adj = pearsonr(predictions, y_test)
results_input_adj.statistic
results_input_adj.pvalue

df = pd.DataFrame()
df['predictions'] = predictions
df['y_test'] = y_test
df['C_test'] = C_test
results_output_adj = partial_corr(data=df, x='predictions',
                                  y='y_test', covar='C_test')


