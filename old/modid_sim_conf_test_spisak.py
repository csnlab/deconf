import os
import pickle
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
from constants import *
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from mlconfound.stats import full_confound_test, partial_confound_test
from sklearn.model_selection import cross_val_predict


settings_name = 'test_spisak_1'
experiments = ['audiofreq', '']#['visualori', 'audiofreq']

for experiment in experiments:

    output_file_name = 'modid_sim_{}_{}.pkl'.format(settings_name, experiment)
    output_folder = os.path.join(DATA_PATH, 'sim_data', 'modid')
    output_full_path = os.path.join(output_folder, output_file_name)
    sim = pickle.load(open(output_full_path, 'rb'))

    plot_format = 'png'
    plots_folder = os.path.join(DATA_PATH, 'plots', 'modid_sim', settings_name)
    if not os.path.isdir(plots_folder) :
        os.makedirs(plots_folder)

    time_bin_times = sim['data']['time_bin_times']
    n_time_bins_per_trial = len(time_bin_times)
    target = sim['data']['target_name']

    n_splits = 3
    decoder_name = 'SGD'

    df = pd.DataFrame(columns=['experiment', 'features',
                               'decoder', 'time', 'score', 'n_neurons',
                               'full_p', 'full_sig', 'partial_p',
                               'partial_sig'])

    for features in ['spikes', 'confound'] :

        for time_bin in range(n_time_bins_per_trial) :

            time = time_bin_times[time_bin]

            X = sim['data'][features][time_bin]

            if features == 'spikes':
                C = sim['data']['confound'][time_bin][:, 0]

            y = sim['data']['target']

            # kfold = StratifiedKFold(n_splits=n_splits, shuffle=True,
            #                         random_state=92)
            #
            # kfold_scores = []
            # y_test_all, y_pred_all = [], []
            #
            # for fold, (training_ind, testing_ind) in enumerate(
            #         kfold.split(X, y)) :
            #
            #     if decoder_name == 'SGD' :
            #         decoder = SGDClassifier(random_state=92)
            #     else :
            #         raise NotImplementedError
            #
            #     X_train = X[training_ind, :]
            #     X_test = X[testing_ind, :]
            #     y_train = y[training_ind]
            #     y_test = y[testing_ind]
            #
            #     ss = StandardScaler()
            #     X_train = ss.fit_transform(X_train)
            #     X_test = ss.transform(X_test)
            #
            #     decoder.fit(X_train, y_train)
            #     y_pred = decoder.predict(X_test)
            #     score = balanced_accuracy_score(y_test, y_pred)
            #     kfold_scores.append(score)
            #     y_test_all.append(y_test)
            #     y_pred_all.append(y_pred)
            #
            # y_test_all = np.hstack(y_test_all)
            # y_pred_all = np.hstack(y_pred_all)
            #
            # mean_score = np.mean(kfold_scores)

            # indx = np.random.permutation(np.arange(y.shape[0]))
            # X = X[indx, :]
            # y = y.__array__()[indx]
            # C = C[indx]

            y_test_all = y.__array__()
            y_pred_all = cross_val_predict(SGDClassifier(random_state=92), X, y)
            mean_score = balanced_accuracy_score(y_test_all, y_pred_all)

            if features == 'spikes':
                full_test = full_confound_test(y_test_all, y_pred_all, C,
                                               num_perms=1000,
                                               cat_y=True,
                                               cat_yhat=True,
                                               cat_c=False,
                                               mcmc_steps=50,
                                               cond_dist_method='gam',
                                               return_null_dist=False,
                                               random_state=92, progress=True,
                                               n_jobs=-1)
                full_p = full_test.p
                full_sig = full_p < 0.05

                partial_test = partial_confound_test(y_test_all, y_pred_all, C,
                                               num_perms=1000,
                                               cat_y=True,
                                               cat_yhat=True,
                                               cat_c=False,
                                               mcmc_steps=50,
                                               cond_dist_method='gam',
                                               return_null_dist=False,
                                               random_state=92, progress=True,
                                               n_jobs=-1)
                partial_p = partial_test.p
                partial_sig = partial_p < 0.05

            else:
                full_p = np.nan
                full_sig = False
                partial_p = np.nan
                partial_sig = False

            row = [experiment, features,
                   decoder_name, time, mean_score, X.shape[1], full_p,
                   full_sig, partial_p, partial_sig]

            df.loc[df.shape[0], :] = row

    df['time'] = [t.item() for t in df['time']]

    features_palette = {'spikes' : sns.xkcd_rgb['purple'],
                        'confound' : sns.xkcd_rgb['grey']}


    f, ax = plt.subplots(1, 1, figsize=[4, 4], sharex=True, sharey=True)

    for features in ['spikes', 'confound']:
        dx = df[df['features'] == features]
        ax.plot(dx['time'], dx['score'], c=features_palette[features],
                label='decoding from {}'.format(features))
        ax.scatter(dx['time'], dx['score'], c=features_palette[features])

    # sns.lineplot(data=df, x='time', y='score', hue='features', ax=ax,
    #              palette=features_palette, markers=True, style='experiment')
    ax.set_xlabel('Time [s]')
    ax.set_ylabel('Decoding accuracy {}'.format(target))
    ax.axhline(0.5, ls=':', c='grey')

    dxx = df[(df['features'] == 'spikes') & (df['full_sig'])]
    ax.scatter(dxx['time'], np.repeat(0.42, dxx.shape[0]),  marker='o',
              c=features_palette['spikes'], zorder=10, alpha=1, s=40,
               label='full test p<0.05')

    dxx = df[(df['features'] == 'spikes') & (df['partial_sig'])]
    ax.scatter(dxx['time'], np.repeat(0.45, dxx.shape[0]), marker='s',
               c=features_palette['spikes'], zorder=10, alpha=1, s=40,
               label='partial test p<0.05')
    ax.legend()

    ax.set_ylim([0.38, 1.01])
    sns.despine()
    plt.tight_layout()


    plot_name = 'decoding_spisak_{}.{}'.format(experiment, plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=400, bbox_inches="tight")

