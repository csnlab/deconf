import os
import pickle
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
from constants import *
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from sklearn.linear_model import LinearRegression
from confound_prediction.deconfounding import *
import itertools

settings_name = 'may18_ok'
experiments = ['audiofreq', 'visualori']

for experiment in experiments :

    output_file_name = 'modid_sim_{}_{}.pkl'.format(settings_name, experiment)
    output_folder = os.path.join(DATA_PATH, 'sim_data', 'modid')
    output_full_path = os.path.join(output_folder, output_file_name)
    sim = pickle.load(open(output_full_path, 'rb'))

    plot_format = 'png'
    plots_folder = os.path.join(DATA_PATH, 'plots', 'modid_sim', settings_name)
    if not os.path.isdir(plots_folder) :
        os.makedirs(plots_folder)

    time_bin_times = sim['data']['time_bin_times']
    n_time_bins_per_trial = len(time_bin_times)
    target = sim['data']['target_name']

    n_splits = 3
    decoder_name = 'SGD'


    df = pd.DataFrame(columns=['experiment', 'features',
                               'decoder', 'time', 'score', 'n_neurons'])

    for features in ['spikes'] :

        for time_bin in range(n_time_bins_per_trial) :

            time = time_bin_times[time_bin]

            X = sim['data'][features][time_bin]

            if features == 'spikes':
                C = sim['data']['confound'][time_bin][:, 0]

            y = sim['data']['target'].__array__()

            print('\n\nTime bin {} of {}'.format(time_bin + 1, n_time_bins_per_trial))

            x_test, x_train, y_test, y_train, ids_test, ids_train = confound_isolating_cv(X, y, C,
                                                                    random_seed=92,
                                                      min_sample_size=None,
                                                      cv_folds=n_splits, n_remove=None)

            set(ids_test[0]).intersection(ids_test[1])

            kfold_scores = []
            y_test_all, y_pred_all = [], []

            for fold, (training_ind, testing_ind) in enumerate(zip(ids_train, ids_test)) :

                if decoder_name == 'SGD' :
                    decoder = SGDClassifier(random_state=92)
                else :
                    raise NotImplementedError

                X_train = X[training_ind, :]
                X_test = X[testing_ind, :]
                y_train = y[training_ind]
                y_test = y[testing_ind]

                ss = StandardScaler()
                X_train = ss.fit_transform(X_train)
                X_test = ss.transform(X_test)

                decoder.fit(X_train, y_train)
                y_pred = decoder.predict(X_test)
                score = balanced_accuracy_score(y_test, y_pred)
                kfold_scores.append(score)
                y_test_all.append(y_test)
                y_pred_all.append(y_pred)

            y_test_all = np.hstack(y_test_all)
            y_pred_all = np.hstack(y_pred_all)

            mean_score = np.mean(kfold_scores)

            row = [experiment, features,
                   decoder_name, time, mean_score, X.shape[1]]

            df.loc[df.shape[0], :] = row

    df['time'] = [t.item() for t in df['time']]

    features_palette = {'spikes' : sns.xkcd_rgb['purple'],
                        'confound' : sns.xkcd_rgb['grey']}

    f, ax = plt.subplots(1, 1, figsize=[4, 4], sharex=True, sharey=True)
    sns.lineplot(data=df, x='time', y='score', hue='features', ax=ax,
                 palette=features_palette)
    ax.set_xlabel('Time [s]')
    ax.set_ylabel('Decoding accuracy {}'.format(target))
    ax.axhline(0.5, ls=':', c='grey')
    ax.set_ylim([0.4, 1])
    sns.despine()
    plt.tight_layout()

    plot_name = 'decoding_confisolcv_{}.{}'.format(experiment, plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=400, bbox_inches="tight")

