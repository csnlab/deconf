import os
from MLencoding import MLencoding
from simulate.sim_utils import *

plot_format = 'png'
plots_folder = os.path.join(DATA_PATH, 'plots', 'poisson_glm')
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)


X, C, T = make_place_fields_with_stimulus()

binsize_in_ms = 100
max_time = 1000

predictor_matrix = np.hstack([C/C.max(), T[:, np.newaxis]])
predictor_matrix_onlyconf = np.hstack([C/C.max(), np.zeros(shape=[C.shape[0], 1])])
predictor_matrix_onlytarg = np.hstack([np.zeros(shape=[C.shape[0], 1]), T[:, np.newaxis]])

# ENCODING PARAMETERS

encoding_model = 'random_forest'#'glm'#'random_forest'
n_cv = 3
encoder_params = {'n_estimators' : 100}
cov_history = True
spike_history = False
max_time = 2000
n_filters = 5
n_every=1 # predict all the time bins



encoder = MLencoding(tunemodel=encoding_model,
                     cov_history=cov_history,
                     window=binsize_in_ms,
                     max_time=max_time,
                     n_filters=n_filters)

if encoding_model == 'random_forest' :
    encoder.set_params(encoder_params)

spikes_binned = X[:, 0]

# Y_hat_all, PR2s_all_list = encoder.fit_cv(predictor_matrix, spikes_binned, n_cv=n_cv,
#                                           verbose=0, continuous_folds=True)
#
# full_model_score = np.mean(PR2s_all_list)
#
# print('--> PR2={:.2f}'.format(full_model_score))
#

encoder.fit(predictor_matrix, spikes_binned)
rate_pred_full = encoder.predict(predictor_matrix)

rate_pred_onlyconf = encoder.predict(predictor_matrix_onlyconf)
rate_pred_onlytarg = encoder.predict(predictor_matrix_onlytarg)


# equivalent to if we had just written np.exp(design_mat_offse

f, ax = plt.subplots(3, 1, figsize=[10, 7], sharex=True)

ax[0].plot(C[:, 0]/C.max(), label='Confound', color='red')
ax[0].plot(T, label='Target', color='green')
ax[0].legend(loc='upper right')

ax[1].plot(spikes_binned, label="Binned spikes", c='k', alpha=0.8, lw=0.8)
ax[1].plot(rate_pred_full, label="RF Prediction (full model)", c='orange')
ax[1].legend(loc='upper right')

ax[2].plot(spikes_binned, label="Binned spikes", c='k', alpha=0.8, lw=0.8)
ax[2].plot(rate_pred_onlyconf, label="RF Prediction (only confound)", c='red')
ax[2].plot(rate_pred_onlytarg, label="RF Prediction (only target)", c='green')
ax[2].legend(loc='upper right')

ax[1].set_ylabel('spike counts')
plt.tight_layout()
sns.despine()

plot_name = 'RF_encoder.{}'.format(plot_format)
f.savefig(os.path.join(plots_folder, plot_name), dpi=400)

