from simulate.sim_utils import *
from scipy.linalg import hankel
import statsmodels.api as sm


X, C = make_place_fields()

stim = C[:, 0]
ntfilt = 50

num_time_bins = stim.size

padded_stim = np.hstack((np.zeros(ntfilt-1), stim))   # pad early bins of stimulus with zero
design_mat = hankel(padded_stim[:-ntfilt+1], stim[-ntfilt:])

# (You can check for you like that this gives the same matrix as the one created above!)

# plt.clf()
# plt.figure(figsize=[12,8])
# plt.imshow(design_mat, aspect='auto', interpolation='nearest')
# plt.xlabel('lags before spike time')
# plt.ylabel('time bin of response')
# plt.colorbar()
# plt.show()

design_mat_offset = np.hstack((np.ones((num_time_bins,1)), design_mat))     # just add a column of ones
spikes_binned = X[:, 0]

### This is super-easy if we rely on built-in GLM fitting code
glm_poisson_exp = sm.GLM(endog=spikes_binned, exog=design_mat_offset,
                         family=sm.families.Poisson())

pGLM_results = glm_poisson_exp.fit(max_iter=100, tol=1e-6, tol_criterion='params')


# pGLM_const = glm_poisson_exp[-1].fit_['beta0'] # constant ("dc term)")
pGLM_const = pGLM_results.params[0]
pGLM_filt = pGLM_results.params[1:] # stimulus filter

# The 'GLM' function can fit a GLM for us. Here we have specified that
# we want the noise model to be Poisson. The default setting for the link
# function (the inverse of the nonlinearity) is 'log', so default
# nonlinearity is 'exp').

### Compute predicted spike rate on training data
rate_pred_pGLM = np.exp(pGLM_const + design_mat @ pGLM_filt)
# equivalent to if we had just written np.exp(design_mat_offse

fig, ax = plt.subplots(1, 1)

ax.plot(spikes_binned, label="Binned spikes", c='k')
ax.plot(rate_pred_pGLM, label="GLM Predicted rate", c='orange')
ax.set_ylabel('spike count / bin')
ax.set_xlabel('Samples')
ax.legend(loc='upper right')
plt.tight_layout()
