from simulate.sim_utils import *
from scipy.linalg import hankel
import statsmodels.api as sm


X, C, T = make_place_fields_with_stimulus()

conf = C[:, 0] / C.max()


ntfilt = 50

num_time_bins = conf.size

padded_conf = np.hstack((np.zeros(ntfilt - 1), conf))   # pad early bins of stimulus with zero
design_mat_conf = hankel(padded_conf[:-ntfilt + 1], conf[-ntfilt :])
design_mat_conf = design_mat_conf / design_mat_conf.max()


padded_targ = np.hstack((np.zeros(ntfilt - 1), T))   # pad early bins of stimulus with zero
design_mat_targ = hankel(padded_targ[:-ntfilt + 1], T[-ntfilt :])


#design_mat = design_mat_conf
#design_mat = design_mat_targ
design_mat = np.hstack([design_mat_conf, design_mat_targ])
design_mat_only_conf = np.hstack([design_mat_conf, 0*design_mat_targ])
design_mat_only_targ = np.hstack([0*design_mat_conf, design_mat_targ])

# (You can check for you like that this gives the same matrix as the one created above!)
#
# plt.clf()
# plt.figure(figsize=[12,8])
# plt.imshow(design_mat, aspect='auto', interpolation='nearest')
# plt.xlabel('lags before spike time')
# plt.ylabel('time bin of response')
# plt.colorbar()
# plt.show()

design_mat_offset = np.hstack((np.ones((num_time_bins,1)), design_mat))     # just add a column of ones


X_deconf = []
for feat in range(X.shape[1]):
    spikes_binned = X[:, feat]

    glm_poisson_exp = sm.GLM(endog=spikes_binned, exog=design_mat_offset,
                             family=sm.families.Poisson())

    # u, s, vt = np.linalg.svd(glm_poisson_exp.exog, 0)
    # print(s)

    #pGLM_results = glm_poisson_exp.fit(max_iter=100, tol=1e-6, tol_criterion='params')

    pGLM_results = glm_poisson_exp.fit_regularized(method='elastic_net', alpha=0.01)

    # pGLM_const = glm_poisson_exp[-1].fit_['beta0'] # constant ("dc term)")
    pGLM_const = pGLM_results.params[0]
    pGLM_filt = pGLM_results.params[1:] # stimulus filter

    ### Compute predicted spike rate on training data
    rate_pred_pGLM_full = np.exp(pGLM_const + design_mat @ pGLM_filt)
    rate_pred_pGLM_conf = np.exp(pGLM_const + design_mat_only_conf @ pGLM_filt)
    rate_pred_pGLM_targ = np.exp(pGLM_const + design_mat_only_targ @ pGLM_filt)

    X_deconf.append(rate_pred_pGLM_targ)



X_deconf = np.hstack([x[:, np.newaxis] for x in X_deconf])


n_features = X_deconf.shape[1]
f, ax = plt.subplots(n_features+2, 1, figsize=[10, 8], sharex=True)
ax[0].plot(C[:, 0], label='Confound')
ax[1].plot(T, label='Target')
ax[0].legend()
ax[1].legend()
for i in range(n_features):
    ax[i+2].plot(X_deconf[:, i])
for axx in ax:
    axx.set_yticks([])
    axx.set_yticklabels([])
sns.despine()
plt.tight_layout()


plot_name = 'features.{}'.format(plot_format)
f.savefig(os.path.join(plots_folder, plot_name), dpi=400)



