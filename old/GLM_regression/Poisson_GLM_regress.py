from simulate.sim_utils import *
from scipy.linalg import hankel
import statsmodels.api as sm


plot_format = 'png'
plots_folder = os.path.join(DATA_PATH, 'plots', 'poisson_glm')
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)


X, C, T = make_place_fields_with_stimulus()

conf = C[:, 0] / C.max()

ntfilt = 10

num_time_bins = conf.size

padded_conf = np.hstack((np.zeros(ntfilt - 1), conf))   # pad early bins of stimulus with zero
design_mat_conf = hankel(padded_conf[:-ntfilt + 1], conf[-ntfilt :])
design_mat_conf = design_mat_conf / design_mat_conf.max()


padded_targ = np.hstack((np.zeros(ntfilt - 1), T))   # pad early bins of stimulus with zero
design_mat_targ = hankel(padded_targ[:-ntfilt + 1], T[-ntfilt :])


#design_mat = design_mat_conf
#design_mat = design_mat_targ
design_mat = np.hstack([design_mat_conf, design_mat_targ])
design_mat_only_conf = np.hstack([design_mat_conf, 0*design_mat_targ])
design_mat_only_targ = np.hstack([0*design_mat_conf, design_mat_targ])

# (You can check for you like that this gives the same matrix as the one created above!)
#
# plt.clf()
# plt.figure(figsize=[12,8])
# plt.imshow(design_mat, aspect='auto', interpolation='nearest')
# plt.xlabel('lags before spike time')
# plt.ylabel('time bin of response')
# plt.colorbar()
# plt.show()


design_mat_offset = np.hstack((np.ones((num_time_bins,1)), design_mat))     # just add a column of ones


spikes_binned = X[:, 0]

### This is super-easy if we rely on built-in GLM fitting code
glm_poisson_exp = sm.GLM(endog=spikes_binned, exog=design_mat_offset,
                         family=sm.families.Poisson())

#pGLM_results = glm_poisson_exp.fit(max_iter=100, tol=1e-6, tol_criterion='params')

pGLM_results = glm_poisson_exp.fit_regularized(method='elastic_net', alpha=0.01)

# pGLM_const = glm_poisson_exp[-1].fit_['beta0'] # constant ("dc term)")
pGLM_const = pGLM_results.params[0]
pGLM_filt = pGLM_results.params[1:] # stimulus filter

# The 'GLM' function can fit a GLM for us. Here we have specified that
# we want the noise model to be Poisson. The default setting for the link
# function (the inverse of the nonlinearity) is 'log', so default
# nonlinearity is 'exp').

### Compute predicted spike rate on training data

rate_pred_pGLM_full = np.exp(pGLM_const + design_mat @ pGLM_filt)

rate_pred_pGLM_conf = np.exp(pGLM_const + design_mat_only_conf @ pGLM_filt)
rate_pred_pGLM_targ = np.exp(pGLM_const + design_mat_only_targ @ pGLM_filt)

# equivalent to if we had just written np.exp(design_mat_offse

f, ax = plt.subplots(3, 1, figsize=[10, 7], sharex=True)

ax[0].plot(C[:, 0]/C.max(), label='Confound', color='red')
ax[0].plot(T, label='Target', color='green')
ax[0].legend(loc='upper right')

ax[1].plot(spikes_binned, label="Binned spikes", c='k', alpha=0.8, lw=0.8)
ax[1].plot(rate_pred_pGLM_full, label="GLM Prediction (full model)", c='orange')
ax[1].legend(loc='upper right')

ax[2].plot(spikes_binned, label="Binned spikes", c='k', alpha=0.8, lw=0.8)
ax[2].plot(rate_pred_pGLM_conf, label="GLM Prediction (only confound)", c='red')
ax[2].plot(rate_pred_pGLM_targ, label="GLM Prediction (only target)", c='green')
ax[2].legend(loc='upper right')

ax[1].set_ylabel('spike counts')
plt.tight_layout()
sns.despine()

plot_name = 'Poisson_GLM.{}'.format(plot_format)
f.savefig(os.path.join(plots_folder, plot_name), dpi=400)

