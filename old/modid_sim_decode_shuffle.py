import os
import pickle
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
from constants import *
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from mlconfound.stats import full_confound_test, partial_confound_test
from sklearn.model_selection import cross_val_predict
from confound_prediction.deconfounding import confound_isolating_cv
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import log_loss

"""
OUTDATED: now combined in modid_sim_decode_predictability.py
"""

settings_name = 'sep13'

simulation_settings_name = 'true+conf_effect'#'true+conf_effect'
experiments = ['audiofreq', 'visualori']#['visualori', 'audiofreq']

n_splits = 3
methods = ['shuffle'] #None
run_spisak_tests = False
num_perms_spisak = 5000
save_plots = True

plot_format = 'png'
plots_folder = os.path.join(DATA_PATH, 'plots', 'modid',
                            'DATA_SETTINGS_{}'.format(data_settings_name),
                            'DECODE_SETTINGS_{}'.format(settings_name))

if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)


def log2_likelihood(true, predictions) :
    y_true = true
    y_pred = predictions[:, 1]
    terms = y_true * np.log2(y_pred) + (1 - y_true) * np.log2(1 - y_pred)
    ll = np.sum(terms) / len(y_true)
    return ll


for method in methods:

    for experiment in experiments:

        output_file_name = 'modid_sim_{}_{}.pkl'.format(simulation_settings_name, experiment)
        output_folder = os.path.join(DATA_PATH, 'sim_data', 'modid')
        output_full_path = os.path.join(output_folder, output_file_name)
        sim = pickle.load(open(output_full_path, 'rb'))

        time_bin_times = sim['data']['time_bin_times']
        n_time_bins_per_trial = len(time_bin_times)
        target_name = sim['data']['target_name']



        df = pd.DataFrame(columns=['experiment', 'features',
                                   'time', 'score', 'n_neurons',
                                   'full_p', 'full_sig', 'partial_p',
                                   'partial_sig'])

        df_shift = pd.DataFrame(columns=['experiment', 'features',
                                   'time', 'score', 'shift', 'n_neurons',])

        # # --- Decode from confounds ---
        # for time_bin in range(n_time_bins_per_trial) :
        #
        #     time = time_bin_times[time_bin]
        #     X = sim['data']['confound'][time_bin]
        #     y = sim['data']['target']
        #
        #     kfold = StratifiedKFold(n_splits=n_splits, shuffle=True,
        #                             random_state=92)
        #
        #     kfold_scores = []
        #
        #     for fold, (training_ind, testing_ind) in enumerate(kfold.split(X, y)) :
        #
        #         decoder = SGDClassifier(random_state=92)
        #
        #         X_train = X[training_ind, :]
        #         X_test = X[testing_ind, :]
        #         y_train = y[training_ind]
        #         y_test = y[testing_ind]
        #
        #         ss = StandardScaler()
        #         X_train = ss.fit_transform(X_train)
        #         X_test = ss.transform(X_test)
        #
        #         decoder.fit(X_train, y_train)
        #         y_pred = decoder.predict(X_test)
        #         score = balanced_accuracy_score(y_test, y_pred)
        #         kfold_scores.append(score)
        #
        #     mean_score = np.mean(kfold_scores)
        #     full_p = np.nan
        #     full_sig = False
        #     partial_p = np.nan
        #     partial_sig = False
        #
        #     row = [experiment, 'confound', time, mean_score, X.shape[1], full_p,
        #            full_sig, partial_p, partial_sig]
        #
        #     df.loc[df.shape[0], :] = row


        # TODO could randomize the trial order so we are more likely to cut
        # balanced segments

        n_shuffles = 200

        # --- Decode from spikes ---
        for time_bin in range(n_time_bins_per_trial):

            time = time_bin_times[time_bin]
            X_full = sim['data']['spikes'][time_bin]
            C_full = sim['data']['confound'][time_bin]
            y_full = sim['data']['target'].__array__()

            N_shifts = 9
            T = X_full.shape[0]
            center_point = T // 2

            for s in range(n_shuffles+1):
                #print(s)
                if s == 0:
                    X = X_full
                else:
                    X = np.random.permutation(X_full)

                C = C_full
                y = y_full

                # --- predictability with behavioral predictors ---
                kfold = StratifiedKFold(n_splits=n_splits, shuffle=True,
                                        random_state=92)
                iterable = kfold.split(X, y)

                kfold_scores = []
                y_test_all, y_pred_full_all, y_pred_naive_all, c_all = [], [], [], []

                for fold, (training_ind, testing_ind) in enumerate(iterable) :

                    #decoder = SGDClassifier(random_state=92)
                    #decoder = RandomForestClassifier(n_estimators=300, random_state=92)

                    X_train = X[training_ind, :]
                    X_test = X[testing_ind, :]
                    y_train = y[training_ind]
                    y_test = y[testing_ind]
                    C_train = C[training_ind, :]
                    C_test = C[testing_ind, :]

                    ss = StandardScaler()
                    X_train = ss.fit_transform(X_train)
                    X_test = ss.transform(X_test)

                    ss = StandardScaler()
                    C_train = ss.fit_transform(C_train)
                    C_test = ss.transform(C_test)

                    full_model = LogisticRegression(penalty='l1',
                                                    solver='liblinear')
                    features_full_train = np.hstack([X_train, C_train])
                    features_full_test = np.hstack([X_test, C_test])
                    full_model.fit(features_full_train, y_train)
                    y_pred_full = full_model.predict_proba(features_full_test)

                    naive_model = LogisticRegression(penalty='l1',
                                                    solver='liblinear')
                    features_naive_train = C_train
                    features_naive_test = C_test
                    naive_model.fit(features_naive_train, y_train)
                    y_pred_naive = naive_model.predict_proba(features_naive_test)

                    #score = balanced_accuracy_score(y_test, y_pred)
                    #kfold_scores.append(score)
                    y_test_all.append(y_test)
                    y_pred_full_all.append(y_pred_full)
                    y_pred_naive_all.append(y_pred_naive)
                    c_all.append(C_test[:, 0])

                target = np.hstack(y_test_all)
                predictions_full = np.vstack(y_pred_full_all)
                predictions_naive = np.vstack(y_pred_naive_all)
                confound = np.hstack(c_all)

                log_likelihood_full = -log_loss(target, predictions_full)
                log_likelihood_naive = -log_loss(target, predictions_naive)
                llratio = log_likelihood_full - log_likelihood_naive
                predictability = llratio
                mean_score = predictability
                #print(mean_score)

                if s == 0:
                    full_p = np.nan
                    full_sig = False
                    partial_p = np.nan
                    partial_sig = False

                    row = [experiment, 'spikes',
                           time, mean_score, X.shape[1], full_p,
                           full_sig, partial_p, partial_sig]

                    df.loc[df.shape[0], :] = row

                else:
                    row = [experiment, 'spikes',
                           time, mean_score, s, X.shape[1]]

                    df_shift.loc[df_shift.shape[0], :] = row


        df['time'] = [t.item() for t in df['time']]

        df_shift['time'] = [t.item() for t in df_shift['time']]

        df['q1'] = np.nan
        df['q2'] = np.nan
        df['significant'] = np.nan

        for i, time in enumerate(df_shift['time'].unique()):

            shuffles_scores = df_shift[df_shift['time'] == time]['score']
            q1, q2 = shuffles_scores.quantile(q=[0.025, 0.975])
            df.loc[df['time'] == time, 'q1'] = q1
            df.loc[df['time'] == time, 'q2'] = q2
            obs_score = df.loc[df['time'] == time, 'score']
            n_higher = (shuffles_scores > obs_score.array[0]).sum()
            p_val = n_higher / len(shuffles_scores)
            df.loc[df['time'] == time, 'significant'] = p_val < 0.05

            #print(obs_score.iloc[0], q2, obs_score.iloc[0]>q2, p_val < 0.05)



        features_palette = {'spikes' : sns.xkcd_rgb['purple'],
                            'confound' : sns.xkcd_rgb['grey']}

        #MAX = df['score'].max()+0.06

        f, ax = plt.subplots(1, 1, figsize=[5, 5], sharex=True, sharey=True)

        for features in ['spikes']: #'confound']:
            dx = df[df['features'] == features]
            ax.plot(dx['time'], dx['score'], c=features_palette[features],
                    label='decoding from {}'.format(features))
            ax.scatter(dx['time'], dx['score'], c=features_palette[features])

        # sns.lineplot(data=df, x='time', y='score', hue='features', ax=ax,
        #              palette=features_palette, markers=True, style='experiment')
        ax.set_xlabel('Time [s]')
        ax.set_ylabel('Decoding accuracy {}'.format(target_name))
        ax.axhline(0.0, ls=':', c='grey')
        ax.set_title('deconf method: {}'.format(method))

        ax.fill_between(df['time'], df['q1'], df['q2'], color='grey',
                        zorder=-10, alpha=0.5, label='Null distribution',
                        linewidth=0)

        dxx = df[(df['features'] == 'spikes') & (df['significant'])]
        ax.scatter(dxx['time'], np.repeat(-0.06, dxx.shape[0]),  marker='X',
                  c=features_palette['spikes'], zorder=10, alpha=1, s=40,
                   label='Significant p<0.05')

        # dxx = df[(df['features'] == 'spikes') & (df['full_sig'])]
        # ax.scatter(dxx['time'], np.repeat(0.33, dxx.shape[0]),  marker='o',
        #           c=features_palette['spikes'], zorder=10, alpha=1, s=40,
        #            label='full test p<0.05')
        #
        # dxx = df[(df['features'] == 'spikes') & (df['partial_sig'])]
        # ax.scatter(dxx['time'], np.repeat(0.36, dxx.shape[0]), marker='s',
        #            c=features_palette['spikes'], zorder=10, alpha=1, s=40,
        #            label='partial test p<0.05')
        ax.legend()

        ax.set_ylim([-0.08, ax.get_ylim()[1]])
        sns.despine()
        plt.tight_layout()

        if save_plots:
            plot_name = 'decoding_{}_{}_{}.{}'.format(method, experiment, settings_name, plot_format)
            f.savefig(os.path.join(plots_folder, plot_name), dpi=400, bbox_inches="tight")

