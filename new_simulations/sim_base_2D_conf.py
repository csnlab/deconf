import os
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from decoding_utils import decode
import pandas as pd
from sklearn.model_selection import StratifiedKFold
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from sklearn.linear_model import LinearRegression
from plotting_style import *
from constants import *
from scipy.stats import alpha, gamma, norm
from scipy.stats import skewnorm
"""
Simplest version of the simulation.

1 dataset is generated. 
"""

settings_name = 'dec13_setup'

# parameters
n_samples = 100
simulation_type = 'two_confounds_continuous'
plot_confound_regression = False


decoder = 'logistic_regression'
distribution_match_binsize = 0.3
n_kfold_splits = 3
deconf_methods = ['raw', 'distribution_match', 'linear_confound_regression',
                   'nonlinear_confound_regression', 'predictability_lin',
                  'predictability_nonlin']
run_spisak_full_test = False
run_spisak_partial_test = False
num_perms_spisak = 1000



# set up paths for figures
plot_format = 'png'
plots_folder = os.path.join(DATA_PATH, 'plots', 'simulations',
                            'SIMULATION_{}'.format(simulation_type),
                            'DECODE_SETTINGS_{}'.format(settings_name))

if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)


if simulation_type == 'two_confounds_discrete':
    C_0 = 0
    X_0 = np.random.normal(loc=0, scale=0.6, size=n_samples)

    w_cy = 1
    w_xy = 0
    w_xc = 1

    # def f(x):
    #     p = 0.5
    #     v = np.random.choice([0, 1], p=[p, 1 - p])
    #     if v == 0:
    #         ret_x = x
    #     else:
    #         ret_x = 1-x
    #     return ret_x

    def f(x):
        p = 0.8
        out1, out2 = [], []
        for sample in x:
            if sample == 0:
                out1.append(np.random.choice([0, 1], p=[p, 1 - p]))
                out2.append(np.random.choice([0, 1], p=[p, 1 - p]))
            elif sample == 1:
                out1.append(np.random.choice([0, 1], p=[1 - p, p]))
                out2.append(np.random.choice([0, 1], p=[1 - p, p]))

        return np.vstack([out1, out2]).T

    def g(x):
        return x

    def h(x):
        return x[:, 0] + x[:, 1]

if simulation_type == 'two_confounds_continuous':
    C_01 = np.random.normal(loc=0, scale=0.6, size=n_samples).reshape(-1, 1)
    C_02 = np.random.normal(loc=0, scale=0.6, size=n_samples).reshape(-1, 1)
    C_0 = np.hstack([C_01, C_02])
    X_0 = np.random.normal(loc=0, scale=0.6, size=n_samples)

    w_cy = 1
    w_xy = 0
    w_xc = 1

    # def f(x):
    #     p = 0.5
    #     v = np.random.choice([0, 1], p=[p, 1 - p])
    #     if v == 0:
    #         ret_x = x
    #     else:
    #         ret_x = 1-x
    #     return ret_x

    def f(x):
        return np.vstack([x, x]).T

    def g(x):
        return x

    def h(x):
        return x[:, 0] + x[:, 1]



# generate simulation
y = np.random.choice([0, 1], size=n_samples, p=[0.5, 0.5])
C = C_0 + w_cy * f(y)
X = X_0 + w_xy * g(y) + w_xc * h(C)


# plot data
if simulation_type == 'two_confounds_discrete':
    fig, ax = plt.subplots(1, 3, figsize=[12, 4])
    sns.kdeplot(X[y == 0], c='red', ax=ax[0], label='$y=0$')
    sns.kdeplot(X[y == 1], c='green', ax=ax[0], label='$y=1$')
    sns.kdeplot(X[C[:, 0]==0], c='red', ax=ax[1], label='$C_0=0$')
    sns.kdeplot(X[C[:, 0]==1], c='green', ax=ax[1], label='$C_0=1$')
    sns.kdeplot(X[C[:, 1]==0], c='red', ax=ax[2], label='$C_1=0$')
    sns.kdeplot(X[C[:, 1]==1], c='green', ax=ax[2], label='$C_1=1$')
    ax[0].legend()
    ax[1].legend()
    ax[2].legend()
    ax[0].set_xlabel('$X$')
    ax[1].set_xlabel('$X$')
    ax[2].set_xlabel('$X$')
    sns.despine()
    plt.tight_layout()

else:
    fig, ax = plt.subplots(1, 4, figsize=[12, 4])

    sns.kdeplot(X[y==0], c='red', ax=ax[0])
    sns.kdeplot(X[y==1], c='green', ax=ax[0])

    sns.kdeplot(C[y == 0, 0], c='red', ax=ax[1])
    sns.kdeplot(C[y == 1, 0], c='green', ax=ax[1])
    for ik, axx in enumerate([ax[2], ax[3]]):
        axx.scatter(C[y==0, ik], X[y==0], c='red', label='y=0', s=4, linewidth=0)
        axx.scatter(C[y==1, ik], X[y==1], c='green', label='y=1', s=4, linewidth=0)
        axx.legend()
        axx.axis('equal')
        axx.axvline(0, ls=':', c='grey')
        axx.axhline(0, ls=':', c='grey')

    ax[0].set_xlabel('$X$')
    ax[1].set_xlabel('$C$')
    ax[2].set_xlabel('$C_1$')
    ax[2].set_ylabel('$X$')
    ax[3].set_xlabel('$C_2$')
    ax[3].set_ylabel('$X$')

    sns.despine()
    plt.tight_layout()


if False:
    fig, ax = plt.subplots(1, 4, figsize=[12, 4])

    sns.kdeplot(X[y==0], c='red', ax=ax[0])
    sns.kdeplot(X[y==1], c='green', ax=ax[0])

    sns.kdeplot(C[y==0, 0], c='red', ax=ax[1])
    sns.kdeplot(C[y==1, 0], c='green', ax=ax[1])

    for ik, axx in enumerate([ax[2], ax[3]]):
        axx.scatter(C[y==0, ik], X[y==0], c='red', label='y=0', s=4, linewidth=0)
        axx.scatter(C[y==1, ik], X[y==1], c='green', label='y=1', s=4, linewidth=0)
        axx.legend()
        axx.axis('equal')
        axx.axvline(0, ls=':', c='grey')
        axx.axhline(0, ls=':', c='grey')

    ax[0].set_xlabel('$X$')
    ax[1].set_xlabel('$C$')
    ax[2].set_xlabel('$C_1$')
    ax[2].set_ylabel('$X$')
    ax[3].set_xlabel('$C_2$')
    ax[3].set_ylabel('$X$')

    sns.despine()
    plt.tight_layout()

    plot_name = 'data.{}'.format(plot_format)
    fig.savefig(os.path.join(plots_folder, plot_name), dpi=250,
              bbox_inches="tight")


    fig, ax = plt.subplots(1, 1, figsize=[4, 4])
    ax.scatter(C[y==0], X[y==0], c='red', label='y=0', s=20, linewidth=0,
               marker='o')
    ax.scatter(C[y==1], X[y==1], c='green', label='y=1', s=20, linewidth=0,
               marker='o')
    ax.legend(frameon=False)
    #ax.axis('equal')
    ax.axvline(0, ls=':', c='grey')
    ax.axhline(0, ls=':', c='grey')
    ax.set_xlabel('$C$')
    ax.set_ylabel('$X$')
    sns.despine()
    plt.tight_layout()

    plot_name = 'data_single.{}'.format(plot_format)
    fig.savefig(os.path.join(plots_folder, plot_name), dpi=250,
              bbox_inches="tight")



if plot_confound_regression:
    kfold = StratifiedKFold(n_splits=3, shuffle=True,
                            random_state=92)

    X_deconf_lin, X_deconf_nonlin, y_shuf, c_plot = [], [], [], []

    for fold, (training_ind, testing_ind) in enumerate(kfold.split(X, y)):

        # decoder = SGDClassifier(random_state=92)
        X_train = X[training_ind].reshape(-1, 1)
        X_test = X[testing_ind].reshape(-1, 1)
        C_train = C[training_ind, :]
        C_test = C[testing_ind, :]
        y_shuf.append(y[testing_ind])

        # TODO scale before or after conf regression?
        ss = StandardScaler()
        X_train = ss.fit_transform(X_train)
        X_test = ss.transform(X_test)

        ss = StandardScaler()
        C_train = ss.fit_transform(C_train)
        C_test = ss.transform(C_test)
        c_plot.append(C_test)

        reg_lin = LinearRegression().fit(C_train, X_train)
        X_deconf_lin.append(X_test - reg_lin.predict(C_test))

        reg_nonlin = RandomForestRegressor(n_estimators=200).fit(C_train, X_train)
        X_deconf_nonlin.append(X_test - reg_nonlin.predict(C_test).reshape(-1, 1))

    X_l = np.vstack(X_deconf_lin)
    X_nl = np.vstack(X_deconf_nonlin)
    yplot = np.hstack(y_shuf)
    cplot = np.vstack(c_plot)

    fig, ax = plt.subplots(2, 4, figsize=[12, 8])

    for i, Xplot in enumerate([X_l, X_nl]):
        sns.kdeplot(Xplot[yplot==0, 0], c='red', ax=ax[i, 0])
        sns.kdeplot(Xplot[yplot==1, 0], c='green', ax=ax[i, 0])

        sns.kdeplot(cplot[yplot==0, 0], c='red', ax=ax[i, 1])
        sns.kdeplot(cplot[yplot==1, 0], c='green', ax=ax[i, 1])

        ax[i, 2].scatter(cplot[yplot==0, 0], Xplot[yplot==0, 0], c='red', label='y=0', s=4, linewidth=0)
        ax[i, 2].scatter(cplot[yplot==1, 0], Xplot[yplot==1, 0], c='green', label='y=1', s=4, linewidth=0)

        ax[i, 3].scatter(cplot[yplot==0, 1], Xplot[yplot==0, 0], c='red', label='y=0', s=4, linewidth=0)
        ax[i, 3].scatter(cplot[yplot==1, 1], Xplot[yplot==1, 0], c='green', label='y=1', s=4, linewidth=0)

        ax[i, 0].set_xlabel('$X$')
        ax[i, 1].set_xlabel('$C$')
        ax[i, 2].set_xlabel('$C$')
        ax[i, 2].set_ylabel('$X$')

    sns.despine()
    plt.tight_layout()

    plot_name = 'confound_regression.{}'.format(plot_format)
    fig.savefig(os.path.join(plots_folder, plot_name), dpi=250,
              bbox_inches="tight")




# TODO wool significant test is one directional

df = pd.DataFrame(columns=['method', 'mean_score',
                           'full_p', 'full_sig', 'partial_p', 'partial_sig'])

for deconf_method in deconf_methods:

    dec_out = decode(X=X.reshape(-1, 1), y=y, C=C,
                     deconf_method=deconf_method,
                     decoder=decoder,
                     posthoc_counterbalancing_binsize=distribution_match_binsize,
                     n_kfold_splits=n_kfold_splits,
                     run_spisak_full_test=run_spisak_full_test,
                     run_spisak_partial_test=run_spisak_partial_test,
                     num_perms_spisak=num_perms_spisak)

    mean_score = dec_out['mean_score']
    full_p = dec_out['full_p']
    full_sig = dec_out['full_sig']
    partial_p = dec_out['partial_p']
    partial_sig = dec_out['partial_sig']

    df.loc[df.shape[0], :] = [deconf_method,
                              dec_out['mean_score'], dec_out['full_p'],
                              dec_out['full_sig'], dec_out['partial_p'],
                              dec_out['partial_sig']]

print(df)



f, ax = plt.subplots(1, 1, figsize=[4, 4])

# plot bars of methods scored by accuracy
labels1 = df[~np.isin(df['method'], ['predictability_lin', 'predictability_nonlin'])]['method'].__array__()
positions = np.arange(len(labels1))
vals = df[~np.isin(df['method'], ['predictability_lin', 'predictability_nonlin'])]['mean_score'].__array__()
#sns.barplot(data=df, x='method', y='mean_score', ax=ax)
ax.bar(positions, vals, color =sns.xkcd_rgb['light blue'])

# plot bars of methods scored by predictability
labels2 = df[np.isin(df['method'], ['predictability_lin', 'predictability_nonlin'])]['method'].__array__()
positions = np.arange(len(labels1), len(labels1)+len(labels2))
vals = df[np.isin(df['method'], ['predictability_lin', 'predictability_nonlin'])]['mean_score'].__array__()
ax2 = ax.twinx()  # instantiate a second Axes that shares the same x-axis
ax2.bar(positions, vals, color =sns.xkcd_rgb['light green'])


for i in range(len(df['partial_sig'])):
    if df['partial_sig'].iloc[i]:
        ax.scatter(i, 0.95, marker='D', s=40, c=sns.xkcd_rgb['dark blue'],
                   label='Partial conf. test\np<0.05')

# plot horizontal lines
xlim = ax.get_xlim()
ax.plot([0-2, len(labels1)-0.5], [0.5, 0.5],
        c=sns.xkcd_rgb['dark grey'], ls=':')
ax2.plot([positions-0.5, positions+2], [0, 0],
        c=sns.xkcd_rgb['dark grey'], ls=':')
ax.set_xlim(xlim)

# set labels
ax.set_ylim([0, 1])
ax2.set_ylim([-0.3, 0.3])
ax.set_yticks([0, 0.25, 0.5, 0.75, 1])
ax.set_ylabel('Accuracy score')
ax2.set_ylabel('Predictability')
labels = list(labels1) + list(labels2)
ax.set_xticks(np.arange(len(labels)))
ax.set_xticklabels([method_labels[l] for l in labels],
                   rotation=45)

sns.despine(right=False)
plt.tight_layout()

plot_name = 'methods_barplot.{}'.format(plot_format)
f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
          bbox_inches="tight")

