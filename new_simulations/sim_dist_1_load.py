import os
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from decoding_utils import decode
import pandas as pd
from plotting_style import *
from constants import *
from scipy.stats import mannwhitneyu, bootstrap
import pickle
from scipy.stats import norm, gamma, combine_pvalues

"""

"""

settings_name = 'dec18_figure_2'
settings_name = 'dec17_figure_3_test'
settings_name = 'feb25_100'
settings_name = 'mar5_test6_n_samples'

# plots to generate
plot_lineplot = True
plot_violins = False
plot_data = False
plot_legends = False

# plot options
save_plots = True
use_first_repeat_only_for_the_violins = True
plot_only_first_repeat = True
plot_decode_from_confound_in_lineplot = False

alpha_level_permutations = 0.05
alpha_level_partial_test = 0.05
alpha_level_permutations_aggregate = 0.05


plot_format = 'png'

plots_folder = os.path.join(DATA_PATH, 'plots', 'simulations', 'DECODE_SETTINGS_{}'.format(settings_name))
if not os.path.isdir(plots_folder):
    os.makedirs(plots_folder)

if settings_name == 'dec17_figure_3_test':
    output_folder = os.path.join(DATA_PATH, 'simulation_results', 'SIMULATION_{}'.format(settings_name))
    results_full_path = os.path.join(output_folder, 'results.pkl')
    res = pickle.load(open(results_full_path, 'rb'))
    df = res['df']
    pars = res['pars']

else:
    simulation_types = ['linear',
                        'nonlinear_2',
                        'conf_noise_2',
                        'joint_XOR',
                        'joint_AND']

    simulation_types = ['linear_true',
                        'joint_XOR_true']

    #simulation_types = ['two_confounds_continuous']

    dfs = []
    pars_list = []
    data = {}
    for simulation_type in simulation_types:
        output_folder = os.path.join(DATA_PATH, 'simulation_results', 'SIMULATION_{}'.format(settings_name))
        results_full_path = os.path.join(output_folder, 'results_{}_{}.pkl'.format(settings_name, simulation_type))
        res = pickle.load(open(results_full_path, 'rb'))
        dfs.append(res['df'])
        pars_list.append(res['pars'])
        data[simulation_type] = res['data']
    df = pd.concat(dfs)
    pars = pars_list[0]



decoders = pars['decoders']
#simulation_types = pars['simulation_types']
deconf_methods = pars['deconf_methods']
#deconf_methods = [d for d in deconf_methods if d != 'distribution_match']
if ~ plot_decode_from_confound_in_lineplot:
    deconf_methods_lineplot = [d for d in deconf_methods if d != 'decode_from_confound']
n_repeats = pars['n_repeats']
n_permutations = pars['n_permutations']
parameter_to_vary = pars['parameter_to_vary']
parameter_values = pars['parameter_values']


if plot_lineplot:

    for simulation_type in simulation_types:

        for decoder in decoders:
            f, ax1 = plt.subplots(1, 1, figsize=[4, 2.7])
            ax2 = ax1.twinx()  # instantiate a second Axes that shares the same x-axis

            for j, deconf_method in enumerate(deconf_methods_lineplot):
                if deconf_method == 'decode_from_confound':
                    continue
                x = []
                err = []
                for k, parameter_value in enumerate(parameter_values):

                    if np.isin(deconf_method, predictability_methods):
                        decsel = 'none'
                    else:
                        decsel = decoder

                    vals = df[(df['simulation_type'] == simulation_type) &
                              (df['parameter_value'] == parameter_value) &
                              (df['decoder'] == decsel) &
                              (df['method'] == deconf_method) &
                              (df['permuted'] == False)]['mean_score'].__array__()
                    center = vals.mean()
                    x.append(center)

                    if n_repeats > 1:
                        bootstrap_vals = (vals.__array__().reshape(-1, 1).astype(float),)
                        bres = bootstrap(bootstrap_vals,
                                         axis=0,
                                         statistic=np.mean,
                                         n_resamples=100,
                                         confidence_level=0.95,
                                         method='basic')
                        error = np.array([bres.confidence_interval.low[0], bres.confidence_interval.high[0]])
                        err.append(error)

                if np.isin(deconf_method, predictability_methods):
                    ax = ax2
                else:
                    ax = ax1

                ax.plot(parameter_values, x, c=method_palette[deconf_method], zorder=-2)
                ax.scatter(parameter_values, x, s=30, c=method_palette[deconf_method],
                           edgecolor='w', label=method_labels[deconf_method], zorder=-1)
                if n_repeats > 1:
                    err = np.vstack(err)
                    ax.fill_between(parameter_values, err[:, 0], err[:, 1], alpha=0.5,
                                    color=method_palette[deconf_method], linewidth=0)

            if n_permutations > 0:
                dp = pd.DataFrame(columns=['parameter_value', 'method', 'pvalue'])
                for j, deconf_method in enumerate(deconf_methods_lineplot):
                    x = []
                    err = []
                    for k, parameter_value in enumerate(parameter_values):
                        pvals = []
                        for nrep in range(n_repeats):

                            if np.isin(deconf_method, predictability_methods):
                                decsel = 'none'
                            else:
                                decsel = decoder

                            dfsel = df[(df['simulation_type'] == simulation_type) &
                                      (df['parameter_value'] == parameter_value) &
                                      (df['decoder'] == decsel) &
                                      (df['method'] == deconf_method) &
                                      (df['repeat'] == nrep)]

                            obs = dfsel[dfsel['permuted'] == False]['mean_score'].__array__()[0]
                            perms = dfsel[dfsel['permuted'] == True]['mean_score'].__array__()
                            p_val = (perms > obs).sum() / (n_permutations + 1)
                            pvals.append(p_val)

                        # this works even if you have just one repeat/pvalue
                        res = combine_pvalues(pvals, method='pearson')

                        dp.loc[dp.shape[0], :] = [parameter_value,
                                                  deconf_method,
                                                  res.pvalue]

                left_edges = np.array(parameter_values) - np.hstack(([0], np.diff(parameter_values) / 2))
                right_edges = np.array(parameter_values) + np.hstack((np.diff(parameter_values) / 2, [0]))

                for j, deconf_method in enumerate(deconf_methods):
                    y = 1.1 - (j + 1) * 0.02
                    dpsel = dp[dp['method'] == deconf_method]
                    for ii, (rowix, row) in enumerate(dpsel.iterrows()):
                        if row['pvalue'] < alpha_level_permutations_aggregate:
                            ax1.plot([left_edges[ii], right_edges[ii]], [y, y],
                                     c=method_palette[deconf_method], linewidth=3)

            ax1.set_xticks(parameter_values)
            ax1.set_xticklabels(parameter_values)
            ax1.set_xlabel(parameter_labels[parameter_to_vary])
            ax1.set_ylabel('Decoding accuracy')
            ax2.set_ylabel('Predictability')
            ax1.set_ylim([0.3, 1.1])
            ax1.set_yticks([0.5, 0.75, 1.0])
            ax2.set_ylim([-0.2, 0.6])
            ax2.set_yticks([-0.2, 0, 0.2, 0.4, 0.6])
            ax1.axhline(0.5, c='grey', ls=':')
            #ax1.legend(frameon=False, loc='lower left')
            #ax2.legend(frameon=False, loc='lower right')
            sns.despine(right=False)
            plt.tight_layout()

            if save_plots:
                plot_name = 'methods_inspection_{}_{}_{}.{}'.format(simulation_type, parameter_to_vary, decoder, plot_format)
                f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                          bbox_inches="tight")

# TODO in the violin you can aggregate multiple repeats
# or only plot the first repeat (the one for which we save the data plot)
# combining p-values if n_repeats>1

acc_methods = [m for m in deconf_methods if ~np.isin(m, predictability_methods)]
pred_methods = [m for m in deconf_methods if np.isin(m, predictability_methods)]
subsets = [acc_methods, pred_methods]
metrics = ['accuracy', 'predictability']

if plot_violins:

    for simulation_type in simulation_types:

        for k, parameter_value in enumerate(parameter_values):


            for subset, metric in zip(subsets, metrics):
                print(metric)
                if metric == 'accuracy':
                    fig_width = 2.5
                    if simulation_type == 'two_confounds_continuous':
                        fig_width = 2.8
                    decoders_plot = decoders
                elif metric == 'predictability':
                    fig_width = 2
                    if simulation_type == 'two_confounds_continuous':
                        fig_width = 1.7
                    decoders_plot = ['none']

                for decoder in decoders_plot:

                    f, ax = plt.subplots(1, 1, figsize=[fig_width, 3])

                    df_sel = df[(df['simulation_type'] == simulation_type) &
                                (df['parameter_value'] == parameter_value) &
                                (df['decoder'] == decoder)]
                    perms = df_sel[(df_sel['permuted'] == True)]

                    if len(perms) > 1:
                        sns.violinplot(data=perms,
                                       ax=ax, x='method',
                                       y='mean_score', order=subset,
                                       color=sns.xkcd_rgb['light grey'],
                                       inner='quartile', linewidth=1)

                    for kk, deconf_method in enumerate(subset):
                        df_sel = df[(df['simulation_type'] == simulation_type) &
                                    (df['parameter_value'] == parameter_value) &
                                    (df['decoder'] == decoder) &
                                    (df['method'] == deconf_method)]
                        #print(kk, deconf_method)

                        obs = df_sel[df_sel['permuted'] == False]
                        perms = df_sel[(df_sel['permuted'] == True)]

                        if use_first_repeat_only_for_the_violins:
                            obs_val = obs[obs['repeat'] == 0]['mean_score'].__array__()[0]
                            perms = perms[perms['repeat'] == 0]
                            p_val = (perms['mean_score'].__array__() > obs_val).sum() / (n_permutations + 1)
                            partial_p = obs['partial_p'].iloc[0]

                        else:
                            if obs.shape[0] > 1:
                                # if I have multiple repeats of the same simulations, the
                                # observed value I plot is the mean, the p-values are aggregated,
                                # and the permutations are all grouped together. The latter may not be ideal.
                                assert n_repeats > 1

                                pvals = []
                                for nrep in range(n_repeats):
                                    obs_rep = obs[obs['repeat'] == nrep]['mean_score'].__array__()[0]
                                    perms_rep = perms[perms['repeat'] == nrep]['mean_score'].__array__()
                                    p_val = (perms_rep > obs_rep).sum() / (n_permutations + 1)
                                    pvals.append(p_val)
                                res = combine_pvalues(pvals, method='pearson')
                                p_val = res.pvalue
                                obs_val = np.mean(obs['mean_score'])
                                partial_p =combine_pvalues(obs['partial_p'], method='pearson')

                            else:
                                # if I have only one repeat of the dataset, no problem
                                obs_val = np.mean(obs['mean_score'])
                                p_val = (perms['mean_score'].__array__() > obs_val).sum() / (n_permutations+1)
                                partial_p = obs['partial_p'].iloc[0]

                        print(p_val, partial_p)

                        if p_val < alpha_level_permutations:
                            ax.scatter(kk, obs_val, marker='o', s=60, c='r', zorder=10,
                                       linewidth=0, edgecolor='r')
                        else:
                            ax.scatter(kk, obs_val, marker='o', s=60, zorder=10,
                                       linewidth=2, edgecolor='r', facecolors='none')

                        if partial_p < alpha_level_partial_test:
                            ax.scatter(kk, 0.97, marker='*', s=40, c='blue', zorder=10)

                    if metric == 'predictability':
                        ax.set_ylim([-0.3, 0.5])
                        ax.set_yticks([-0.6, -0.4, -0.2, 0, 0.2, 0.4, 0.6])
                        ax.yaxis.tick_right()
                        ax.yaxis.set_label_position("right")
                        ax.axhline(0, ls=':', c='k', zorder=-5)
                        ax.set_ylabel('Predictability')
                        sns.despine(right=False, left=True, ax=ax)
                        ax.tick_params(axis='y', right=True)


                    elif metric == 'accuracy':
                        ax.set_ylim([0.25, 1.02])
                        ax.axhline(0.5, ls=':', c='k', zorder=-5)
                        ax.axvline(1.5, c='k', zorder=-5, linewidth=0.75)
                        ax.set_ylabel('Accuracy score')
                        sns.despine(right=True, left=False, ax=ax)
                        ax.set_yticks([0.25, 0.5, 0.75, 1])

                    ax.set_xlabel('')
                    ax.set_xticklabels([method_labels[d] for d in subset], rotation=90)
                    plt.tight_layout()

                    if save_plots:
                        plot_name = 'methods_violins_{}_{}_{}_p{}.{}'.format(simulation_type, decoder,metric, k, plot_format)
                        f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                                  bbox_inches="tight")


# TODO, for some reason, if you plot from here, the plot is slightly different from plot_data_transformations.py
# TODO, so plotting from sim_dist_1_compute.py
if plot_data:

    for sim_ix, simulation_type in enumerate(simulation_types):

        for k, parameter_value in enumerate(parameter_values):

            if plot_only_first_repeat:
                data_key = '{}={}'.format(parameter_to_vary, parameter_value)
                X = data[simulation_type][data_key][0]['X']
                y = data[simulation_type][data_key][0]['y']
                C = data[simulation_type][data_key][0]['C']
            else:
                raise ValueError

            # plot data simple for 1d X we consider both 1d and 2d C,
            # for 2d X we assume 1d C
            if C.shape[1] > 1:
                fig, ax = plt.subplots(1, 2, figsize=[5, 3], sharex=True, sharey=True)
                for iii, axx in enumerate(ax):
                    axx.scatter(C[y == 0, iii], X[y == 0, 0], c='red', label='y=0', s=20, linewidth=0,
                                marker='o')
                    axx.scatter(C[y == 1, iii], X[y == 1, 0], c='green', label='y=1', s=20, linewidth=0,
                                marker='o')
                    axx.legend(frameon=False, loc='upper left')
                    # ax.axis('equal')
                    axx.axvline(0, ls=':', c='grey')
                    axx.axhline(0, ls=':', c='grey')
                ax[0].set_xlabel('$C_1$')
                ax[0].set_ylabel('$X$')
                ax[1].set_xlabel('$C_2$')
                sns.despine()
                plt.tight_layout()

                plot_name = 'data_single_{}_p{}.{}'.format(simulation_type, k, plot_format)
                fig.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                            bbox_inches="tight")
            else:
                if X.shape[1] == 1:
                    x1, x2 = C[y == 0], C[y == 1]
                    y1, y2 = X[y == 0, 0], X[y == 1, 0]
                    xlab, ylab = '$C$', '$X$'
                else:
                    x1, x2 = X[y == 0, 0], X[y == 1, 0]
                    y1, y2 = X[y == 0, 1], X[y == 1, 1]
                    xlab, ylab = '$X_1$', '$X_2$'

                fig, ax = plt.subplots(1, 1, figsize=[3, 3])
                ax.scatter(x1, y1, c='red', label='y=0', s=20, linewidth=0,
                           marker='o')
                ax.scatter(x2, y2, c='green', label='y=1', s=20, linewidth=0,
                           marker='o')
                ax.legend(frameon=False, loc='best').remove()
                # ax.axis('equal')
                ax.axvline(0, ls=':', c='grey')
                ax.axhline(0, ls=':', c='grey')
                ax.set_xlabel(xlab)
                ax.set_ylabel(ylab)
                sns.despine()
                plt.tight_layout()

                if save_plots:
                    plot_name = 'data_single_{}_p{}.{}'.format(simulation_type, k, plot_format)
                    fig.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                                bbox_inches="tight")


           # plot data 1d conf, 1 or 2D X
            # if C.shape[1] == 1:
            #     if X.shape[1] == 1:
            #         fig, ax = plt.subplots(1, 3, figsize=[12, 4])
            #
            #         sns.kdeplot(X[y == 0], color='red', ax=ax[0], label='y=0')
            #         sns.kdeplot(X[y == 1], color='green', ax=ax[0], label='y=1')
            #
            #         sns.kdeplot(C[y == 0], color='red', ax=ax[1], label='y=0')
            #         sns.kdeplot(C[y == 1], color='green', ax=ax[1], label='y=1')
            #         ax[0].legend()
            #         ax[1].legend()
            #
            #         ax[2].scatter(C[y == 0], X[y == 0], c='red', label='y=0', s=4, linewidth=0)
            #         ax[2].scatter(C[y == 1], X[y == 1], c='green', label='y=1', s=4, linewidth=0)
            #         ax[2].legend()
            #         ax[2].axis('equal')
            #         ax[2].axvline(0, ls=':', c='grey')
            #         ax[2].axhline(0, ls=':', c='grey')
            #
            #         ax[0].set_xlabel('$X$')
            #         ax[1].set_xlabel('$C$')
            #         ax[2].set_xlabel('$C$')
            #         ax[2].set_ylabel('$X$')
            #
            #         sns.despine()
            #         plt.tight_layout()
            #
            #         plot_name = 'data_{}_p{}.{}'.format(simulation_type, k, plot_format)
            #         fig.savefig(os.path.join(plots_folder, plot_name), dpi=250,
            #                     bbox_inches="tight")
            #     else:
            #         fig, ax = plt.subplots(1, 4, figsize=[12, 4])
            #
            #         sns.kdeplot(X[y == 0, 0], c='red', ax=ax[0])
            #         sns.kdeplot(X[y == 1, 0], c='green', ax=ax[0])
            #
            #         sns.kdeplot(X[y == 0, 1], c='red', ax=ax[1])
            #         sns.kdeplot(X[y == 1, 1], c='green', ax=ax[1])
            #
            #         sns.kdeplot(C[y == 0], c='red', ax=ax[2])
            #         sns.kdeplot(C[y == 1], c='green', ax=ax[2])
            #
            #         ax[3].scatter(X[y == 0, 0], X[y == 0, 1], c='red', label='y=0', s=4, linewidth=0)
            #         ax[3].scatter(X[y == 1, 0], X[y == 1, 1], c='green', label='y=1', s=4, linewidth=0)
            #         ax[3].legend()
            #         ax[3].axis('equal')
            #         ax[3].axvline(0, ls=':', c='grey')
            #         ax[3].axhline(0, ls=':', c='grey')
            #
            #         ax[0].set_xlabel('$X_1$')
            #         ax[1].set_xlabel('$X_2$')
            #         ax[2].set_xlabel('$C$')
            #         ax[3].set_ylabel('$X_1$')
            #         ax[3].set_ylabel('$X_2$')
            #
            #         sns.despine()
            #         plt.tight_layout()
            #
            #         plot_name = 'data_{}_p{}.{}'.format(simulation_type, k, plot_format)
            #         fig.savefig(os.path.join(plots_folder, plot_name), dpi=250,
            #                     bbox_inches="tight")


if plot_legends:
    f, ax = plt.subplots(1, 1, figsize=[2, 2])
    legend_elements = [plt.Line2D([0], [0], marker='o', label='$y=0$', lw=0,   markerfacecolor='red', markeredgecolor='w', markersize=8),
                       plt.Line2D([0], [0], marker='o', label='$y=1$', lw=0,   markerfacecolor='green', markeredgecolor='w', markersize=8)]
    ax.legend(handles=legend_elements, loc='center', frameon=False,
              ncol=1)
    sns.despine(left=True, bottom=True)
    ax.axis('off')
    plt.show()
    plot_name = 'legend_scatter.{}'.format(plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
              bbox_inches="tight")


    f, ax = plt.subplots(1, 1, figsize=[2, 2])
    legend_elements = [plt.Line2D([0], [0], marker='o', color=method_palette[m],
                                  label=method_labels[m], lw=2.5,
                              markerfacecolor=method_palette[m],
                                  markeredgecolor='w',
                                  markersize=8) for m in deconf_methods_lineplot]
    ax.legend(handles=legend_elements, loc='center', frameon=False,
              ncol=1)
    sns.despine(left=True, bottom=True)
    ax.axis('off')
    plt.show()
    plot_name = 'legend_overtime.{}'.format(plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
              bbox_inches="tight")

