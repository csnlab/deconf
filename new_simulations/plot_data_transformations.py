import os
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from decoding_utils import decode, decode_alt_norepeats
import pandas as pd
from plotting_style import *
from constants import *
from sklearn.model_selection import StratifiedKFold
from sklearn.preprocessing import StandardScaler
from scipy.stats import mannwhitneyu, bootstrap
import pickle
from scipy.stats import norm, gamma, combine_pvalues
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from sklearn.linear_model import LinearRegression
from sklearn.svm import SVR
from sklearn.multioutput import MultiOutputRegressor
from decoding_utils import posthoc_counterbalancing, confound_isolating_cv
"""
Version of the simulation in which we generate the dataset
and permute the y labels N times 

Works for simulations of 1D and 2D features
"""



settings_name = 'mar3'

figure = 'test'
simulation_types = ['linear',
                    'nonlinear_2',
                    'conf_noise_2',
                    'joint_XOR',
                    'joint_AND']

#simulation_types = ['two_confounds_continuous']
n_samples = 200

plot_confound_regression = True
plot_posthoc_counterbalancing = False
plot_confound_isolating_cv = False

confcv_min_sample_size = 40
confcv_n_remove = 1
posthoc_counterbalancing_binsize = None
posthoc_counterbalancing_n_bins = 30
posthoc_counterbalancing_already_binned = False
n_kfold_splits = 3
set_seed = True
linear_regression_regularization = False
signal_strength = 1
plot_format = 'png'
coherence_p = 0.8
p = coherence_p
parameter_to_vary = 'none'

plots_folder = os.path.join(DATA_PATH, 'plots', 'data_transformation_plots',
                            '{}'.format(settings_name))

if not os.path.isdir(plots_folder):
    os.makedirs(plots_folder)

df = pd.DataFrame(columns=['simulation_type',
                           'parameter_to_vary',
                           'parameter_value',
                           'repeat',
                           'permuted',
                           'permutation',
                           'decoder',
                           'method',
                           'mean_score',
                           'full_p',
                           'full_sig',
                           'partial_p',
                           'partial_sig'])

for sim_ix, simulation_type in enumerate(simulation_types):

    print('Simulation: {} ({:01d} of {:01d})'.format(simulation_type, sim_ix+1, len(simulation_types)))

    # by setting the seed here, I ensure that regardless of how many repeats
    # I run or which simulations, I get the same simulations (and permutations)
    # in this way, if I change the parameter, the permutations are the same
    # upon which different parameter values act
    if set_seed:
        np.random.seed(0)
    print(np.random.get_state()[1][0])
    a = np.random.normal(0, 2)
    print(a)
    if simulation_type == 'linear':
        # confound noise term
        C_0 = np.random.normal(loc=0, scale=1, size=n_samples)
        # confound weight
        w_cy = 2.5


        # function with which y enters c
        def f(x):
            return x


        # feature noise term
        X_0 = np.random.normal(loc=0, scale=1, size=n_samples)
        w_xy = 0
        w_xc = 1


        # function with which y enters X
        def g(x):
            return x


        # function with which c enters X
        def h(x):
            return x

    elif simulation_type == 'conf_noise':
        C_0 = np.random.normal(loc=0, scale=1, size=n_samples)
        X_0 = np.random.normal(loc=0, scale=1, size=n_samples)

        w_cy = 3
        w_xy = 0  # all information in goes via the confound
        w_xc = 2


        def f(x):
            return x


        def g(x):
            return x


        def h(x):
            a = 1
            b = 0.5
            d = 0.8
            N = np.random.normal(loc=0, scale=1, size=x.shape[0])
            return a * x + N * (b + d * x)

    elif simulation_type == 'conf_noise_2':
        C_0 = np.random.gumbel(loc=2, scale=1, size=n_samples)
        C_0 = np.random.uniform(0, 4, size=n_samples)
        X_0 = 0
        w_cy = 3
        w_xy = 0  # all information in goes via the confound
        w_xc = 1


        def f(x):
            return x


        def g(x):
            return x


        def h(x):
            # a = 0
            # b = 0
            # d = 1
            # N = gamma.rvs(a=1, loc=0, scale=1, size=x.shape[0])
            # # N = np.random.normal(loc=0, scale=1, size=x.shape[0])
            # return a * x + N * (b + d * x ** 2)
            N = gamma.rvs(a=1, loc=0, scale=1, size=x.shape[0])
            # N = np.random.normal(loc=0, scale=1, size=x.shape[0])
            return N * (x ** 2)

    elif simulation_type == 'nonlinear':
        C_0 = np.random.normal(loc=0, scale=1, size=n_samples)
        X_0 = np.random.normal(loc=0, scale=0.5, size=n_samples)

        w_cy = 3
        w_xy = 0  # all information in goes via the confound
        w_xc = 2


        def f(x):
            return x


        def g(x):
            return x


        def h(x):
            return np.arctan(12 * x - 4)

    elif simulation_type == 'nonlinear_2':
        C_0 = np.random.normal(loc=0, scale=0.3, size=n_samples)
        X_0 = np.random.normal(loc=0, scale=0.3, size=n_samples)

        w_cy = 1
        w_xy = 0
        w_xc = 1


        def f(x):
            return x


        def g(x):
            return x


        def h(x):
            # noisy_x = np.random.normal(x, scale=0.5)
            return norm.pdf(x, loc=0, scale=0.2)

    elif simulation_type == 'joint_XOR':
        C_0 = np.random.normal(loc=0, scale=1, size=n_samples)
        X_0 = 0

        w_cy = 2
        w_xy = 0  # all information in goes via the confound
        w_xc = 1


        def f(x):
            # TODO could be a degraded version of Y
            return x


        def g(x):
            return np.hstack([x.reshape(-1, 1), x.reshape(-1, 1)])


        def h(x):
            samples = []
            for val in x:
                if val >= 1:
                    cov = [[1, 0.9], [0.9, 1]]

                elif val < 1:
                    cov = [[1, -0.9], [-0.9, 1]]

                sample = np.random.multivariate_normal([0, 0], cov, 1)
                samples.append(sample)
            return np.vstack(samples)

    elif simulation_type == 'joint_AND':
        C_0 = np.random.normal(loc=0, scale=0.5, size=n_samples)
        X_0 = 0
        w_cy = 2
        w_xy = 0  # all information in goes via the confound
        w_xc = 1


        def f(x):
            # TODO could be a degraded version of Y
            return x


        def g(x):
            return np.hstack([x.reshape(-1, 1), x.reshape(-1, 1)])


        def h(x):
            samples = []
            for val in x:
                cov = [[0.5, 0], [0, 0.5]]
                if val >= 1:
                    mu = [5, 5]
                    sample = np.random.multivariate_normal(mu, cov, 1)

                elif val < 1:
                    mu = np.random.choice([[0, 5], [5, 0]], p=[0.5, 0.5])
                    sample = np.random.multivariate_normal(mu, cov, 1)

                samples.append(sample)
            return np.vstack(samples)


        def h(x):
            samples = []
            for val in x:
                cov = [[0.5, 0], [0, 0.5]]
                if val >= 1:
                    mu = [5, 5]
                elif val < 1:
                    mu = np.array([[0, 5], [5, 0]])[np.random.randint(2), :]
                sample = np.random.multivariate_normal(mu, cov, 1)
                samples.append(sample)
            return np.vstack(samples)

    elif simulation_type == 'linear_true':
        C_0 = np.random.normal(loc=0, scale=0.6, size=n_samples)
        X_0 = np.random.normal(loc=0, scale=0.6, size=n_samples)

        w_cy = 1

        if parameter_to_vary == 'signal_strength':
            w_xy = parameter_values[k]
            print('Setting {} to {}'.format(parameter_to_vary, parameter_values[k]))
        else:
            w_xy = signal_strength

        w_xc = 1


        def f(x):
            ret_x = []
            for sample in x:
                if sample == 0:
                    ret_x.append(np.random.choice([0, 1], p=[p, 1 - p]))
                elif sample == 1:
                    ret_x.append(np.random.choice([0, 1], p=[1 - p, p]))
            return np.array(ret_x)


        def g(x):
            return x


        def h(x):
            return x

    elif simulation_type == 'nonlinear_true':
        C_0 = np.random.normal(loc=0, scale=0.6, size=n_samples)
        X_0 = np.random.normal(loc=0, scale=0.6, size=n_samples)

        w_cy = 1

        if parameter_to_vary == 'signal_strength':
            w_xy = parameter_values[k]
            print('Setting {} to {}'.format(parameter_to_vary, parameter_values[k]))
        else:
            w_xy = signal_strength

        w_xc = 1


        def f(x):
            ret_x = []
            for sample in x:
                if sample == 0:
                    ret_x.append(np.random.choice([0, 1], p=[p, 1 - p]))
                elif sample == 1:
                    ret_x.append(np.random.choice([0, 1], p=[1 - p, p]))
            return np.array(ret_x)


        def g(x):
            return x


        def h(x):
            return x

    elif simulation_type == 'nonlinear_true_2D':
        X_0 = np.random.multivariate_normal([0, 0], [[1, 0], [0, 1]], size=n_samples)
        C_0 = np.random.normal(loc=0, scale=0.5, size=n_samples)

        w_cy = 1
        if parameter_to_vary == 'signal_strength':
            w_xy = parameter_values[k]
            print('Setting {} to {}'.format(parameter_to_vary, parameter_values[k]))
        else:
            w_xy = signal_strength
        w_xc = 0.5


        def f(x):
            ret_x = []
            for sample in x:
                if sample == 0:
                    ret_x.append(np.random.choice([0, 1], p=[p, 1 - p]))
                elif sample == 1:
                    ret_x.append(np.random.choice([0, 1], p=[1 - p, p]))
            return np.array(ret_x)


        def g(x):
            samples = []
            for val in x:
                if val == 1:
                    x1 = np.random.uniform(low=-5, high=5)
                    x2 = 3 - x1 ** 2
                elif val == 0:
                    x1 = np.random.uniform(low=-1, high=1)
                    x2 = -3 - x1 ** 2
                sample = [x1, x2]
                samples.append(sample)
            return np.vstack(samples)


        def h(x):
            return np.hstack([x.reshape(-1, 1), x.reshape(-1, 1)])

    elif simulation_type == 'conf_noise_true':
        C_0 = np.random.normal(loc=0, scale=1, size=n_samples)
        X_0 = np.random.normal(loc=0, scale=1, size=n_samples)

        w_cy = 1

        if parameter_to_vary == 'signal_strength':
            w_xy = parameter_values[k]
            print('Setting {} to {}'.format(parameter_to_vary, parameter_values[k]))
        else:
            w_xy = signal_strength

        w_xc = 3


        def f(x):
            ret_x = []
            for sample in x:
                if sample == 0:
                    ret_x.append(np.random.choice([0, 1], p=[p, 1 - p]))
                elif sample == 1:
                    ret_x.append(np.random.choice([0, 1], p=[1 - p, p]))
            return np.array(ret_x)


        # y influences linearly + noise
        def g(x):
            # a = 0
            # b = 0.5
            # d = 2
            a = 0
            b = 0
            d = 4
            N = np.random.normal(loc=0, scale=1, size=x.shape[0])
            return a * x + N * (b + d * x)


        def g(x):
            a = 0
            b = 0
            d = 3
            N = np.random.normal(loc=0, scale=1, size=x.shape[0])
            N = gamma.rvs(a=1, loc=0, scale=1, size=x.shape[0])
            return a * x + N * (b + d * x)


        def h(x):
            return x

    elif simulation_type == 'joint_XOR_true':
        C_0 = np.random.normal(loc=0, scale=1, size=n_samples)
        # needs a bit of noise otherwise of w_xy = 0 we get a straight line
        X_0 = np.random.multivariate_normal([0, 0], [[0.1, 0], [0, 0.1]], size=n_samples)
        # X_0 = 0

        w_cy = 1

        if parameter_to_vary == 'signal_strength':
            w_xy = parameter_values[k]
            print('Setting {} to {}'.format(parameter_to_vary, parameter_values[k]))
        else:
            w_xy = signal_strength

        w_xc = 1


        # TODO how to map 1D C to 2D X without changing the correlation structure?

        def f(x):
            ret_x = []
            for sample in x:
                if sample == 0:
                    ret_x.append(np.random.choice([0, 1], p=[p, 1 - p]))
                elif sample == 1:
                    ret_x.append(np.random.choice([0, 1], p=[1 - p, p]))
            return np.array(ret_x)


        def g(x):
            samples = []
            for val in x:
                if val == 1:
                    cov = [[1, 0.9], [0.9, 1]]

                elif val == 0:
                    cov = [[1, -0.9], [-0.9, 1]]
                sample = np.random.multivariate_normal([0, 0], cov, 1)
                samples.append(sample)
            return np.vstack(samples)


        def h(x):
            return np.hstack([x.reshape(-1, 1), x.reshape(-1, 1)])

    elif simulation_type == 'two_confounds_continuous':
        C_01 = np.random.normal(loc=0, scale=0.5, size=n_samples).reshape(-1, 1)
        C_02 = np.random.normal(loc=0, scale=0.5, size=n_samples).reshape(-1, 1)
        C_0 = np.hstack([C_01, C_02])
        X_0 = np.random.normal(loc=0, scale=0.5, size=n_samples)

        w_cy = 1
        w_xy = 0
        w_xc = 1

        def f(x):
            return np.vstack([x, x]).T

        def g(x):
            return x

        def h(x):
            return x[:, 0] + x[:, 1]

    else:
        raise ValueError


    # generate simulation
    y = np.random.choice([0, 1], size=n_samples, p=[0.5, 0.5])
    C = C_0 + w_cy * f(y)
    X = X_0 + w_xy * g(y) + w_xc * h(C)
    # generate simulation

    print(X[3])

    try:
        X.shape[1]
    except IndexError:
        X = X.reshape(-1, 1)

    try:
        C.shape[1]
    except IndexError:
        C = C.reshape(-1, 1)

    if plot_confound_regression:
        kfold = StratifiedKFold(n_splits=n_kfold_splits, shuffle=True,
                                random_state=92)

        X_original, X_deconf_lin, X_deconf_nonlin, y_shuf, c_plot = [], [], [], [], []

        for fold, (training_ind, testing_ind) in enumerate(kfold.split(X, y)):
            # decoder = SGDClassifier(random_state=92)
            pass
            X_train = X[training_ind, :]
            X_test = X[testing_ind, :]
            C_train = C[training_ind, :]
            C_test = C[testing_ind, :]
            y_shuf.append(y[testing_ind])

            # TODO scale before or after conf regression?
            ss = StandardScaler()
            X_train = ss.fit_transform(X_train)
            X_test = ss.transform(X_test)

            ss = StandardScaler()
            C_train = ss.fit_transform(C_train)
            C_test = ss.transform(C_test)
            c_plot.append(C_test)

            reg_lin = LinearRegression().fit(C_train, X_train)
            X_deconf_lin.append(X_test - reg_lin.predict(C_test))

            # reg_nonlin = RandomForestRegressor(n_estimators=50).fit(C_train, X_train)
            #
            # if X.shape[1] == 1:
            #     Xdn_fold = X_test - reg_nonlin.predict(C_test).reshape(-1, 1)
            # elif X.shape[1] > 1:
            #     Xdn_fold = X_test - reg_nonlin.predict(C_test)

            reg_nonlin = MultiOutputRegressor(SVR(C=1, kernel='rbf')).fit(C_train, X_train)
            #reg_nonlin = RandomForestRegressor(n_estimators=50).fit(C_train, X_train)

            if X.shape[1] == 1:
                Xdn_fold = X_test - reg_nonlin.predict(C_test).reshape(-1, 1)
            elif X.shape[1] > 1:
                Xdn_fold = X_test - reg_nonlin.predict(C_test)

            # Xdn_fold = np.zeros_like(X_test)
            # for col in range(X_train.shape[1]):
            #     reg_nonlin =RandomForestRegressor(n_estimators=100).fit(C_train, X_train[:, col])
            #     Xdn_fold[:, col] = reg_nonlin.predict(C_test)
            #
            X_deconf_nonlin.append(Xdn_fold)
            X_original.append(X_test)

        X_nodeconf = np.vstack(X_original)
        X_l = np.vstack(X_deconf_lin)
        X_nl = np.vstack(X_deconf_nonlin)
        yplot = np.hstack(y_shuf)
        cplot = np.vstack(c_plot)

        if C.shape[1] > 1:
            fig, ax = plt.subplots(1, 2, figsize=[5, 3], sharex=True, sharey=True)
            for iii, axx in enumerate(ax):
                axx.scatter(C[y == 0, iii], X_l[y == 0, 0], c='red', label='y=0', s=20, linewidth=0,
                            marker='o')
                axx.scatter(C[y == 1, iii], X_l[y == 1, 0], c='green', label='y=1', s=20, linewidth=0,
                            marker='o')
                axx.legend(frameon=False, loc='upper left')
                # ax.axis('equal')
                axx.axvline(0, ls=':', c='grey')
                axx.axhline(0, ls=':', c='grey')
                axx.legend().remove()
            ax[0].set_xlabel('$C_1$')
            ax[0].set_ylabel('$X$')
            ax[1].set_xlabel('$C_2$')

            sns.despine()
            plt.tight_layout()

            plot_name = 'data_single_{}_linear_confound_regression.{}'.format(simulation_type, plot_format)
            fig.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                        bbox_inches="tight")
        else:
            if X.shape[1] == 1:
                fig, ax = plt.subplots(1, 3, figsize=[8, 2.6], sharey=True, sharex=True)
                for iix, (axx, XX) in enumerate(zip(ax, [X_nodeconf, X_l, X_nl])):
                    axx.scatter(cplot[yplot == 0, 0], XX[yplot == 0, 0], c='red', label='y=0', s=20, linewidth=0,
                                marker='o')
                    axx.scatter(cplot[yplot == 1, 0], XX[yplot == 1, 0], c='green', label='y=1', s=20, linewidth=0,
                                marker='o')
                    axx.legend(frameon=True, loc='best')
                    axx.axvline(0, ls=':', c='grey')
                    axx.axhline(0, ls=':', c='grey')
                    axx.set_xlabel('$C$')
                    axx.set_ylabel('$X$')
                    axx.legend().remove()
                #ax[0].axis('equal')
                ax[0].set_title('Simulated data')
                ax[1].set_title('Linear CR')
                ax[2].set_title('Nonlinear CR')
                sns.despine()
                plt.tight_layout()

                plot_name = 'confound_regression_{}.{}'.format(simulation_type, plot_format)
                fig.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                            bbox_inches="tight")

            elif X.shape[1] > 1:
                fig, ax = plt.subplots(1, 3, figsize=[8, 2.6], sharey=True, sharex=True)
                for iix, (axx, XX) in enumerate(zip(ax, [X_nodeconf, X_l, X_nl])):
                    axx.scatter(XX[yplot == 0, 0], XX[yplot == 0, 1], c='red', label='y=0', s=20, linewidth=0,
                                marker='o')
                    axx.scatter(XX[yplot == 1, 0], XX[yplot == 1, 1], c='green', label='y=1', s=20, linewidth=0,
                                marker='o')
                    axx.legend(frameon=True, loc='best')
                    axx.axvline(0, ls=':', c='grey')
                    axx.axhline(0, ls=':', c='grey')
                    axx.set_xlabel('$X_1$')
                    axx.set_ylabel('$X_2$')
                    axx.legend().remove()
                #ax[0].axis('equal')
                ax[0].set_title('Simulated data')
                ax[1].set_title('Linear CR')
                ax[2].set_title('Nonlinear CR')
                sns.despine()
                plt.tight_layout()

                plot_name = 'confound_regression_{}.{}'.format(simulation_type, plot_format)
                fig.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                            bbox_inches="tight")

    elif plot_posthoc_counterbalancing:

        fig, ax = plt.subplots(1, 2, figsize=[5, 3], sharex=True, sharey=True)
        for iii, axx in enumerate(ax):
            axx.scatter(C[y == 0, iii], X[y == 0, 0], c='red', label='y=0', s=20, linewidth=0,
                        marker='o')
            axx.scatter(C[y == 1, iii], X[y == 1, 0], c='green', label='y=1', s=20, linewidth=0,
                        marker='o')
            axx.legend(frameon=False, loc='upper left')
            # ax.axis('equal')
            axx.axvline(0, ls=':', c='grey')
            axx.axhline(0, ls=':', c='grey')
            axx.legend().remove()
        ax[0].set_xlabel('$C_1$')
        ax[0].set_ylabel('$X$')
        ax[1].set_xlabel('$C_2$')

        sns.despine()
        plt.tight_layout()

        plot_name = 'data_single_{}.{}'.format(simulation_type, plot_format)
        fig.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                    bbox_inches="tight")


        X, y, C = posthoc_counterbalancing(X, y, C, posthoc_counterbalancing_binsize,
                                           n_bins=posthoc_counterbalancing_n_bins,
                                           already_binned=posthoc_counterbalancing_already_binned,
                                           seed=1)
        total_n_trials = X.shape[0]
        print('N trials after counterbalancing: {}'.format(X.shape[0]))


        fig, ax = plt.subplots(1, 2, figsize=[5, 3], sharex=True, sharey=True)
        for iii, axx in enumerate(ax):
            axx.scatter(C[y == 0, iii], X[y == 0, 0], c='red', label='y=0', s=20, linewidth=0,
                        marker='o')
            axx.scatter(C[y == 1, iii], X[y == 1, 0], c='green', label='y=1', s=20, linewidth=0,
                        marker='o')
            axx.legend(frameon=False, loc='upper left')
            # ax.axis('equal')
            axx.axvline(0, ls=':', c='grey')
            axx.axhline(0, ls=':', c='grey')
            axx.legend().remove()
        ax[0].set_xlabel('$C_1$')
        ax[0].set_ylabel('$X$')
        ax[1].set_xlabel('$C_2$')

        sns.despine()
        plt.tight_layout()

        plot_name = 'data_single_{}_posthoc_counterbalancing.{}'.format(simulation_type, plot_format)
        fig.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                    bbox_inches="tight")


    elif plot_confound_isolating_cv:

        fig, ax = plt.subplots(1, 2, figsize=[5, 3], sharex=True, sharey=True)
        for iii, axx in enumerate(ax):
            axx.scatter(C[y == 0, iii], X[y == 0, 0], c='red', label='y=0', s=20, linewidth=0,
                        marker='o')
            axx.scatter(C[y == 1, iii], X[y == 1, 0], c='green', label='y=1', s=20, linewidth=0,
                        marker='o')
            axx.legend(frameon=False, loc='upper left')
            # ax.axis('equal')
            axx.axvline(0, ls=':', c='grey')
            axx.axhline(0, ls=':', c='grey')
            axx.legend().remove()
        ax[0].set_xlabel('$C_1$')
        ax[0].set_ylabel('$X$')
        ax[1].set_xlabel('$C_2$')

        sns.despine()
        plt.tight_layout()

        plot_name = 'data_single_{}.{}'.format(simulation_type, plot_format)
        fig.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                    bbox_inches="tight")


        _, _, _, _, ids_test, ids_train = confound_isolating_cv(X, y, C[:, 0],
                                                                    random_seed=None,
                                                                    min_sample_size=confcv_min_sample_size,
                                                                    cv_folds=n_kfold_splits,
                                                                    n_remove=confcv_n_remove)

        for foldix, (testix, trainix) in enumerate(zip(ids_test, ids_train)):
            C_fold = C[testix, :]
            y_fold = y[testix]
            X_fold = X[testix, :]

            fig, ax = plt.subplots(1, 2, figsize=[5, 3], sharex=True, sharey=True)
            for iii, axx in enumerate(ax):
                axx.scatter(C_fold[y_fold == 0, iii], X_fold[y_fold == 0, 0], c='red', label='y=0', s=20, linewidth=0,
                            marker='o')
                axx.scatter(C_fold[y_fold == 1, iii], X_fold[y_fold == 1, 0], c='green', label='y=1', s=20, linewidth=0,
                            marker='o')
                axx.legend(frameon=False, loc='upper left')
                # ax.axis('equal')
                axx.axvline(0, ls=':', c='grey')
                axx.axhline(0, ls=':', c='grey')
                axx.legend().remove()
            ax[0].set_xlabel('$C_1$')
            ax[0].set_ylabel('$X$')
            ax[1].set_xlabel('$C_2$')

            sns.despine()
            plt.tight_layout()

            plot_name = 'data_single_confound_isolating_cv_fold_{}.{}'.format(foldix, plot_format)
            fig.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                        bbox_inches="tight")

        total_n_trials = np.unique(np.hstack([ids_test, ids_train])).shape[0]