import os
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from decoding_utils import decode
import pandas as pd
from sklearn.model_selection import StratifiedKFold
from sklearn.preprocessing import StandardScaler
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from sklearn.linear_model import LinearRegression
from plotting_style import *
from constants import *
from scipy.stats import alpha, gamma, norm
from scipy.stats import skewnorm
"""
Simplest version of the simulation.

1 dataset is generated. 
"""

settings_name = 'dec17_design_fig3'


# parameters
n_samples = 200
simulation_type = 'nonlinear_2'
plot_confound_regression = True

p = 1

decoder = 'logistic_regression'
distribution_match_binsize = 0.3
n_kfold_splits = 8
deconf_methods = ['raw', #'distribution_match', #'confound_isolating_cv',
                  'linear_confound_regression',
                   'nonlinear_confound_regression', 'predictability_lin',
                  'predictability_nonlin']
run_spisak_full_test = False
run_spisak_partial_test = False
num_perms_spisak = 1000



# set up paths for figures
plot_format = 'png'
plots_folder = os.path.join(DATA_PATH, 'plots', 'simulations',
                            'DECODE_SETTINGS_{}'.format(settings_name))

if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)



if simulation_type == 'simple_conf':
    # confound noise term
    C_0 = np.random.normal(loc=0, scale=1, size=n_samples)
    # confound weight
    w_cy = 2
    # function with which y enters c
    def f(x):
        return x
    # feature noise term
    X_0 = np.random.normal(loc=0, scale=1, size=n_samples)
    w_xy = 0
    w_xc = 1
    # function with which y enters X
    def g(x):
        return x
    # function with which c enters X
    def h(x):
        return x

if simulation_type == 'conf_noise':
    C_0 = np.random.normal(loc=0, scale=1, size=n_samples)
    X_0 = np.random.normal(loc=0, scale=1, size=n_samples)

    w_cy = 3
    w_xy = 0 # all information in goes via the confound
    w_xc = 2

    def f(x):
        return x

    def g(x):
        return x

    def h(x):
        a = 1
        b = 0.5
        d = 0.8
        N = np.random.normal(loc=0, scale=1, size=x.shape[0])
        return a * x + N * (b + d * x)

    # TODO try sinh arcsinh transofmr np.sinh(d * np.arcsinh(x) - e)

if simulation_type == 'nonlinear':
    C_0 = np.random.normal(loc=0, scale=1, size=n_samples)
    X_0 = np.random.normal(loc=0, scale=0.5, size=n_samples)

    w_cy = 3
    w_xy = 0 # all information in goes via the confound
    w_xc = 2

    def f(x):
        return x

    def g(x):
        return x

    def h(x):
        return np.arctan(12*x-4)

if simulation_type == 'conf_noise_2':

    # def h(x):
    #     # N = np.random.normal(loc=0, scale=1, size=x.shape[0])
    #     # #out = np.arctan(12 * x - 4) + np.sinh(1 * np.arcsinh(N) + x)
    #     # out = np.sinh(1 * np.arcsinh(N) + 1.2 * x)
    #     a = 0
    #     b = 0
    #     d = 1
    #     delta = 1
    #     epsilon = 0
    #     N = np.random.normal(loc=0, scale=1, size=x.shape[0])
    #     N_scaled = a * x + N * (b + d * x)
    #     return np.sinh(delta * np.arcsinh(N_scaled) - epsilon)

    C_0 = np.random.normal(loc=2, scale=1, size=n_samples)
    X_0 = 0
    w_cy = 3
    w_xy = 0  # all information in goes via the confound
    w_xc = 2

    def f(x):
        return x


    def g(x):
        return x

    def h(x):
        a = 0
        b = 0
        d = 2
        N = gamma.rvs(a=1, loc=0, scale=1, size=x.shape[0])
        # N = np.random.normal(loc=0, scale=1, size=x.shape[0])
        return a * x + N * (b + d * x ** 2)

if simulation_type == 'linear_true':
    # confound noise term
    C_0 = np.random.normal(loc=0, scale=1, size=n_samples)
    X_0 = np.random.normal(loc=0, scale=1, size=n_samples)

    w_cy = 1
    w_xy = 1
    w_xc = 0

    def f(x):
        return x

    def g(x):
        return x

    def h(x):
        return x

if simulation_type == 'nonlinear_2':
    # confound noise term
    C_0 = np.random.normal(loc=0, scale=0.3, size=n_samples)
    X_0 = np.random.normal(loc=0, scale=0.3, size=n_samples)

    w_cy = 1
    w_xy = 0
    w_xc = 1

    def f(x):
        return x

    def g(x):
        return x

    def h(x):
        #noisy_x = np.random.normal(x, scale=0.5)
        return norm.pdf(x, loc=0, scale=0.25)

if simulation_type == 'linear_true_degraded':
    C_0 = np.random.normal(loc=0, scale=0.6, size=n_samples)
    X_0 = np.random.normal(loc=0, scale=0.6, size=n_samples)

    w_cy = 1
    w_xy = 1
    w_xc = 1

    # def f(x):
    #     p = 0.5
    #     v = np.random.choice([0, 1], p=[p, 1 - p])
    #     if v == 0:
    #         ret_x = x
    #     else:
    #         ret_x = 1-x
    #     return ret_x

    def f(x):
        p = 0.5
        ret_x = []
        for sample in x:
            if sample == 0:
                ret_x.append(np.random.choice([0, 1], p=[p, 1 - p]))
            elif sample == 1:
                ret_x.append(np.random.choice([0, 1], p=[1 - p, p]))
        return np.array(ret_x)

    def g(x):
        return x

    def h(x):
        return x

if simulation_type == 'joint_AND':
    C_0 = np.random.normal(loc=0, scale=0.5, size=n_samples)
    X_0 = 0
    w_cy = 2
    w_xy = 0  # all information in goes via the confound
    w_xc = 1


    def f(x):
        # TODO could be a degraded version of Y
        return x


    def g(x):
        return np.hstack([x.reshape(-1, 1), x.reshape(-1, 1)])


    def h(x):
        samples = []
        for val in x:
            if val >= 1:
                cov = [[0.5, 0], [0, 0.5]]
                sample = np.random.multivariate_normal([0, 0], cov, 1) + 5

            elif val < 1:
                cov = [[0.5, 0], [0, 0.5]]
                fir = np.random.choice(a=[0, 1], size=1)[0]
                loc = np.array([0, 0])
                loc[fir] = 5
                sample = np.random.multivariate_normal([0, 0], cov, 1) + loc

            samples.append(sample)
        return np.vstack(samples)

if simulation_type == 'joint_XOR':
    C_0 = np.random.normal(loc=0, scale=1, size=n_samples)
    X_0 = 0

    w_cy = 2
    w_xy = 0  # all information in goes via the confound
    w_xc = 1


    def f(x):
        # TODO could be a degraded version of Y
        return x


    def g(x):
        return np.hstack([x.reshape(-1, 1), x.reshape(-1, 1)])


    def h(x):
        samples = []
        for val in x:
            if val >= 1:
                cov = [[1, 0.9], [0.9, 1]]

            elif val < 1:
                cov = [[1, -0.9], [-0.9, 1]]

            sample = np.random.multivariate_normal([0, 0], cov, 1)
            samples.append(sample)
        return np.vstack(samples)

elif simulation_type == 'joint_XOR_true':
    C_0 = np.random.normal(loc=0, scale=1, size=n_samples)
    X_0 = np.random.multivariate_normal([0, 0], [[0.1, 0], [0, 0.1]], size=n_samples)
    X_0 = 0

    w_cy = 1
    w_xy = 1
    w_xc = 1

    # TODO how to map 1D C to 2D X without changing the correlation structure?

    def f(x):
        return x

    def g(x):
        samples = []
        for val in x:
            if val >= 1:
                cov = [[1, 0.9], [0.9, 1]]

            elif val < 1:
                cov = [[1, -0.9], [-0.9, 1]]
            sample = np.random.multivariate_normal([0, 0], cov, 1)
            samples.append(sample)
        return np.vstack(samples)

    def h(x):
        return np.hstack([x.reshape(-1, 1), x.reshape(-1, 1)])


elif simulation_type == 'linear_true':
    C_0 = np.random.normal(loc=0, scale=0.6, size=n_samples)
    X_0 = np.random.normal(loc=0, scale=0.6, size=n_samples)

    w_cy = 1

    if parameter_to_vary == 'signal_strength':
        w_xy = parameter_values[k]
        print('Setting {} to {}'.format(parameter_to_vary, parameter_values[k]))
    else:
        w_xy = 1

    w_xc = 1


    def f(x):
        ret_x = []
        for sample in x:
            if sample == 0:
                ret_x.append(np.random.choice([0, 1], p=[p, 1 - p]))
            elif sample == 1:
                ret_x.append(np.random.choice([0, 1], p=[1 - p, p]))
        return np.array(ret_x)


    def g(x):
        return x


    def h(x):
        return x

elif simulation_type == 'nonlinear_true':
    C_0 = np.random.normal(loc=0, scale=0.6, size=n_samples)
    X_0 = np.random.normal(loc=0, scale=0.6, size=n_samples)

    w_cy = 1
    w_xy = 1
    w_xc = 1


    def f(x):
        ret_x = []
        for sample in x:
            if sample == 0:
                ret_x.append(np.random.choice([0, 1], p=[p, 1 - p]))
            elif sample == 1:
                ret_x.append(np.random.choice([0, 1], p=[1 - p, p]))
        return np.array(ret_x)


    def g(x):
        return x


    def h(x):
        return x

elif simulation_type == 'conf_noise_true':
    C_0 = np.random.normal(loc=0, scale=1, size=n_samples)
    X_0 = np.random.normal(loc=0, scale=1, size=n_samples)

    w_cy = 1
    w_xy = 1
    w_xc = 1

    def f(x):
        print(p)
        ret_x = []
        for sample in x:
            if sample == 0:
                ret_x.append(np.random.choice([0, 1], p=[p, 1 - p]))
            elif sample == 1:
                ret_x.append(np.random.choice([0, 1], p=[1 - p, p]))
        return np.array(ret_x)


    # y influences linearly + noise
    def g(x):
        a = 0
        b = 0.5
        d = 2
        N = np.random.normal(loc=0, scale=1, size=x.shape[0])
        return a * x + N * (b + d * x)

    def h(x):
        return x

elif simulation_type == 'conf_noise_true_2':
    C_0 = np.random.normal(loc=0, scale=1, size=n_samples)
    X_0 = np.random.normal(loc=0, scale=1, size=n_samples)

    w_cy = 2
    w_xy = 1
    w_xc = 1

    def f(x):
        return x

    # def f(x):
    #     print(p)
    #     ret_x = []
    #     for sample in x:
    #         if sample == 0:
    #             ret_x.append(np.random.choice([0, 1], p=[p, 1 - p]))
    #         elif sample == 1:
    #             ret_x.append(np.random.choice([0, 1], p=[1 - p, p]))
    #     return np.array(ret_x)

    # y influences linearly + noise
    def g(x):
        a = 0
        b = 0
        d = 2
        N = gamma.rvs(a=1, loc=0, scale=1, size=x.shape[0])
        # N = np.random.normal(loc=0, scale=1, size=x.shape[0])
        return a * x + N * (b + d * x ** 2)

    def h(x):
        return x

elif simulation_type == 'two_confounds_continuous':
    C_01 = np.random.normal(loc=0, scale=0.6, size=n_samples).reshape(-1, 1)
    C_02 = np.random.normal(loc=0, scale=0.6, size=n_samples).reshape(-1, 1)
    C_0 = np.hstack([C_01, C_02])
    X_0 = np.random.normal(loc=0, scale=0.6, size=n_samples)

    w_cy = 1
    w_xy = 0
    w_xc = 1


    def f(x):
        return np.vstack([x, x]).T


    def g(x):
        return x


    def h(x):
        return x[:, 0] + x[:, 1]

# generate simulation
y = np.random.choice([0, 1], size=n_samples, p=[0.5, 0.5])
C = C_0 + w_cy * f(y)
X = X_0 + w_xy * g(y) + w_xc * h(C)

try:
    X.shape[1]
except IndexError:
    X = X.reshape(-1, 1)

try:
    C.shape[1]
except IndexError:
    C = C.reshape(-1, 1)



if X.shape[1] == 1:
    # plot data
    fig, ax = plt.subplots(1, 3, figsize=[12, 4])

    sns.kdeplot(X[y==0], c='red', ax=ax[0])
    sns.kdeplot(X[y==1], c='green', ax=ax[0])

    sns.kdeplot(C[y==0], c='red', ax=ax[1])
    sns.kdeplot(C[y==1], c='green', ax=ax[1])

    ax[2].scatter(C[y==0], X[y==0], c='red', label='y=0', s=4, linewidth=0)
    ax[2].scatter(C[y==1], X[y==1], c='green', label='y=1', s=4, linewidth=0)
    ax[2].legend()
    #ax[2].axis('equal')
    ax[2].axvline(0, ls=':', c='grey')
    ax[2].axhline(0, ls=':', c='grey')

    ax[0].set_xlabel('$X$')
    ax[1].set_xlabel('$C$')
    ax[2].set_xlabel('$C$')
    ax[2].set_ylabel('$X$')

    sns.despine()
    plt.tight_layout()

    plot_name = 'data_{}.{}'.format(simulation_type, plot_format)
    fig.savefig(os.path.join(plots_folder, plot_name), dpi=250,
              bbox_inches="tight")


    fig, ax = plt.subplots(1, 1, figsize=[4, 4])
    ax.scatter(C[y==0], X[y==0], c='red', label='y=0', s=20, linewidth=0,
               marker='o')
    ax.scatter(C[y==1], X[y==1], c='green', label='y=1', s=20, linewidth=0,
               marker='o')
    ax.legend(frameon=False)
    #ax.axis('equal')
    ax.axvline(0, ls=':', c='grey')
    ax.axhline(0, ls=':', c='grey')
    ax.set_xlabel('$C$')
    ax.set_ylabel('$X$')
    sns.despine()
    plt.tight_layout()

    plot_name = 'data_single_{}.{}'.format(simulation_type, plot_format)
    fig.savefig(os.path.join(plots_folder, plot_name), dpi=250,
              bbox_inches="tight")


    fig, ax = plt.subplots(1, 1, figsize=[4, 4])
    ax.scatter(C[y==0], X[y==0], c='red', label='y=0', s=20, linewidth=0,
               marker='o')
    ax.scatter(C[y==1], X[y==1], c='green', label='y=1', s=20, linewidth=0,
               marker='o')
    ax.legend(frameon=False)
    #ax.axis('equal')
    ax.axvline(0, ls=':', c='grey')
    ax.axhline(0, ls=':', c='grey')
    ax.set_xlabel('$C$')
    ax.set_ylabel('$X$')
    sns.despine()
    plt.tight_layout()

    plot_name = 'data_single_{}.{}'.format(simulation_type, plot_format)
    fig.savefig(os.path.join(plots_folder, plot_name), dpi=250,
              bbox_inches="tight")

else:
    fig, ax = plt.subplots(1, 1, figsize=[4, 4])
    ax.scatter(X[y==0, 0], X[y==0, 1], c='red', label='y=0', s=20, linewidth=0,
               marker='o')
    ax.scatter(X[y==1, 0], X[y==1, 1], c='green', label='y=1', s=20, linewidth=0,
               marker='o')
    ax.legend(frameon=False)
    #ax.axis('equal')
    ax.axvline(0, ls=':', c='grey')
    ax.axhline(0, ls=':', c='grey')
    ax.set_xlabel('$X_1$')
    ax.set_ylabel('$X_2$')
    sns.despine()
    plt.tight_layout()

    plot_name = 'data_single_{}.{}'.format(simulation_type, plot_format)
    fig.savefig(os.path.join(plots_folder, plot_name), dpi=250,
              bbox_inches="tight")


if plot_confound_regression:
    if X.shape[1] == 1:
        kfold = StratifiedKFold(n_splits=3, shuffle=True,
                                random_state=92)

        X_deconf_lin, X_deconf_nonlin, y_shuf, c_plot = [], [], [], []

        for fold, (training_ind, testing_ind) in enumerate(kfold.split(X, y)):

            # decoder = SGDClassifier(random_state=92)
            X_train = X[training_ind].reshape(-1, 1)
            X_test = X[testing_ind].reshape(-1, 1)
            C_train = C[training_ind].reshape(-1, 1)
            C_test = C[testing_ind].reshape(-1, 1)
            y_shuf.append(y[testing_ind])

            # TODO scale before or after conf regression?
            ss = StandardScaler()
            X_train = ss.fit_transform(X_train)
            X_test = ss.transform(X_test)

            ss = StandardScaler()
            C_train = ss.fit_transform(C_train)
            C_test = ss.transform(C_test)
            c_plot.append(C_test)

            reg_lin = LinearRegression().fit(C_train, X_train)
            X_deconf_lin.append(X_test - reg_lin.predict(C_test))

            reg_nonlin = RandomForestRegressor(n_estimators=200).fit(C_train, X_train)
            X_deconf_nonlin.append(X_test - reg_nonlin.predict(C_test).reshape(-1, 1))

        X_l = np.vstack(X_deconf_lin)
        X_nl = np.vstack(X_deconf_nonlin)
        yplot = np.hstack(y_shuf)
        cplot = np.vstack(c_plot)

        fig, ax = plt.subplots(2, 3, figsize=[12, 8])

        for i, Xplot in enumerate([X_l, X_nl]):
            sns.kdeplot(Xplot[yplot==0, 0], c='red', ax=ax[i, 0])
            sns.kdeplot(Xplot[yplot==1, 0], c='green', ax=ax[i, 0])

            sns.kdeplot(cplot[yplot==0, 0], c='red', ax=ax[i, 1])
            sns.kdeplot(cplot[yplot==1, 0], c='green', ax=ax[i, 1])

            ax[i, 2].scatter(cplot[yplot==0, 0], Xplot[yplot==0, 0], c='red', label='y=0', s=4, linewidth=0)
            ax[i, 2].scatter(cplot[yplot==1, 0], Xplot[yplot==1, 0], c='green', label='y=1', s=4, linewidth=0)

            ax[i, 0].set_xlabel('$X$')
            ax[i, 1].set_xlabel('$C$')
            ax[i, 2].set_xlabel('$C$')
            ax[i, 2].set_ylabel('$X$')

        sns.despine()
        plt.tight_layout()

        plot_name = 'confound_regression_{}.{}'.format(simulation_type, plot_format)
        fig.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                  bbox_inches="tight")


    else:
        kfold = StratifiedKFold(n_splits=3, shuffle=True,
                                random_state=92)

        X_deconf_lin, X_deconf_nonlin, y_shuf, c_plot = [], [], [], []

        for fold, (training_ind, testing_ind) in enumerate(kfold.split(X, y)):
            # decoder = SGDClassifier(random_state=92)
            X_train = X[training_ind, :]
            X_test = X[testing_ind, :]
            C_train = C[training_ind].reshape(-1, 1)
            C_test = C[testing_ind].reshape(-1, 1)
            y_shuf.append(y[testing_ind])

            # TODO scale before or after conf regression?
            ss = StandardScaler()
            X_train = ss.fit_transform(X_train)
            X_test = ss.transform(X_test)

            ss = StandardScaler()
            C_train = ss.fit_transform(C_train)
            C_test = ss.transform(C_test)
            c_plot.append(C_test)

            reg_lin = LinearRegression().fit(C_train, X_train)
            X_deconf_lin.append(X_test - reg_lin.predict(C_test))

            reg_nonlin = RandomForestRegressor(n_estimators=200).fit(C_train, X_train)
            X_deconf_nonlin.append(X_test - reg_nonlin.predict(C_test))

        X_l = np.vstack(X_deconf_lin)
        X_nl = np.vstack(X_deconf_nonlin)
        yplot = np.hstack(y_shuf)
        cplot = np.vstack(c_plot)

        fig, ax = plt.subplots(1, 3, figsize=[6, 3])
        for iix, (axx, XX) in enumerate(zip(ax, [X ,X_l, X_nl])):
            axx.scatter(XX[y == 0, 0], XX[y == 0, 1], c='red', label='y=0', s=20, linewidth=0,
                       marker='o')
            axx.scatter(XX[y == 1, 0], XX[y == 1, 1], c='green', label='y=1', s=20, linewidth=0,
                       marker='o')
            axx.legend(frameon=True, loc='best')
            axx.axis('equal')
            axx.axvline(0, ls=':', c='grey')
            axx.axhline(0, ls=':', c='grey')
            axx.set_xlabel('$X_1$')
            axx.set_ylabel('$X_2$')
        sns.despine()
        plt.tight_layout()

        feature_indx = 1
        fig, ax = plt.subplots(2, 3, figsize=[12, 8])

        for i, Xplot in enumerate([X_l, X_nl]):
            sns.kdeplot(Xplot[yplot == 0, feature_indx], c='red', ax=ax[i, 0])
            sns.kdeplot(Xplot[yplot == 1, feature_indx], c='green', ax=ax[i, 0])

            sns.kdeplot(cplot[yplot == 0, 0], c='red', ax=ax[i, 1])
            sns.kdeplot(cplot[yplot == 1, 0], c='green', ax=ax[i, 1])

            ax[i, 2].scatter(cplot[yplot == 0, 0], Xplot[yplot == 0, feature_indx], c='red', label='y=0', s=4, linewidth=0)
            ax[i, 2].scatter(cplot[yplot == 1, 0], Xplot[yplot == 1, feature_indx], c='green', label='y=1', s=4, linewidth=0)

            ax[i, 0].set_xlabel('$X_{}$'.format(feature_indx+1))
            ax[i, 1].set_xlabel('$C$')
            ax[i, 2].set_xlabel('$C$')
            ax[i, 2].set_ylabel('$X_{}$'.format(feature_indx+1))

        sns.despine()
        plt.tight_layout()

        plot_name = 'confound_regression_{}.{}'.format(simulation_type, plot_format)
        fig.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                    bbox_inches="tight")

# TODO wool significant test is one directional

df = pd.DataFrame(columns=['method', 'mean_score',
                           'full_p', 'full_sig', 'partial_p', 'partial_sig'])

for deconf_method in deconf_methods:

    dec_out = decode(X=X, y=y, C=C,
                     deconf_method=deconf_method,
                     decoder=decoder,
                     posthoc_counterbalancing_binsize=distribution_match_binsize,
                     n_kfold_splits=n_kfold_splits,
                     run_spisak_full_test=run_spisak_full_test,
                     run_spisak_partial_test=run_spisak_partial_test,
                     num_perms_spisak=num_perms_spisak)

    mean_score = dec_out['mean_score']
    full_p = dec_out['full_p']
    full_sig = dec_out['full_sig']
    partial_p = dec_out['partial_p']
    partial_sig = dec_out['partial_sig']

    df.loc[df.shape[0], :] = [deconf_method,
                              dec_out['mean_score'], dec_out['full_p'],
                              dec_out['full_sig'], dec_out['partial_p'],
                              dec_out['partial_sig']]

print(df)



f, ax = plt.subplots(1, 1, figsize=[4, 4])

# plot bars of methods scored by accuracy
labels1 = df[~np.isin(df['method'], predictability_methods)]['method'].__array__()
positions = np.arange(len(labels1))
vals = df[~np.isin(df['method'], predictability_methods)]['mean_score'].__array__()
#sns.barplot(data=df, x='method', y='mean_score', ax=ax)
ax.bar(positions, vals, color =sns.xkcd_rgb['light blue'])

# plot bars of methods scored by predictability
labels2 = df[np.isin(df['method'], predictability_methods)]['method'].__array__()
positions = np.arange(len(labels1), len(labels1)+len(labels2))
vals = df[np.isin(df['method'], predictability_methods)]['mean_score'].__array__()
ax2 = ax.twinx()  # instantiate a second Axes that shares the same x-axis
ax2.bar(positions, vals, color =sns.xkcd_rgb['light green'])


for i in range(len(df['partial_sig'])):
    if df['partial_sig'].iloc[i]:
        ax.scatter(i, 0.95, marker='D', s=40, c=sns.xkcd_rgb['dark blue'],
                   label='Partial conf. test\np<0.05')

# plot horizontal lines
xlim = ax.get_xlim()
ax.plot([0-2, len(labels1)-0.5], [0.5, 0.5],
        c=sns.xkcd_rgb['dark grey'], ls=':')
ax2.plot([positions-0.5, positions+2], [0, 0],
        c=sns.xkcd_rgb['dark grey'], ls=':')
ax.set_xlim(xlim)

# set labels
ax.set_ylim([0, 1])
ax2.set_ylim([-0.3, 0.3])
ax.set_yticks([0, 0.25, 0.5, 0.75, 1])
ax.set_ylabel('Accuracy score')
ax2.set_ylabel('Predictability')
labels = list(labels1) + list(labels2)
ax.set_xticks(np.arange(len(labels)))
ax.set_xticklabels([method_labels[l] for l in labels],
                   rotation=90)

sns.despine(right=False)
plt.tight_layout()

plot_name = 'methods_barplot_{}.{}'.format(simulation_type, plot_format)
f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
          bbox_inches="tight")

