import os
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from decoding_utils import decode, decode_alt_norepeats
import pandas as pd
from plotting_style import *
from constants import *
from scipy.stats import mannwhitneyu, bootstrap
import pickle
from scipy.stats import norm, gamma, combine_pvalues

"""
Version of the simulation in which we generate the dataset
and permute the y labels N times 

Works for simulations of 1D and 2D features
"""

# TODO wool significant test is one directional

# TODO in the original run, probably I had distribution_match_binsize=0.3
# TODO should be set in automatic way in the final version


settings_name = 'dec18_figure_2'
settings_name = 'feb25'
settings_name = 'mar5_test6_n_samples'

figure = 'figure3_nsamples'
plot_data = True
set_seed = True
plot_format = 'png'


deconf_methods = ['raw',
                  'decode_from_confound',
                  'linear_confound_regression',
                  'nonlinear_confound_regression',
                  'predictability_lin',
                  'predictability_nonlin']

# Predictability parameters
predictability_tune_regularization = True
LR_predictability_C_list = [0.0001, 0.001, 0.01, 0.1, 1]
SVC_predictability_C_list = [0.0001, 0.001, 0.01, 0.1, 1]
LR_predictability_penalty = 'l1'
LR_predictability_solver = 'saga'
LR_predictability_C = 1
LR_predictability_max_iter = 8000
SVC_predictability_C = 100
#tune_C_predictability_nfolds
#max_iter = 7000, tune_C_predictability_nfolds = 3

tune_C_predictability_nfolds = 3

# CR parameters
linear_regression_regularization = False

# counterbalancing parameters
posthoc_counterbalancing_binsize = None
posthoc_counterbalancing_n_bins = None

# decode parameters
n_kfold_splits = 5
tune_hyperpars_decoder = False
LR_decode_C_list = [0.01, 0.1, 1, 10]
SVC_decode_C_list = [0.001, 0.01, 0.1, 1]
decoders = ['logistic_regression', 'SVC']
LR_decode_C=1
SVC_decode_C=1

# spisak parameters
run_spisak_full_test = False
run_spisak_partial_test = False
num_perms_spisak = 5000

if figure == 'figure2':
    n_repeats = 1
    n_permutations = 100
    simulation_types = ['linear',
                        'nonlinear_2',
                        'conf_noise_2',
                        'joint_XOR',
                        'joint_AND']
    n_samples = 200
    coherence_p = None
    signal_strength = None
    parameter_to_vary = 'none'
    parameter_values = ['none']

if figure == 'figure3_signalstrength':

    n_repeats = 1
    n_permutations = 100

    simulation_types = ['linear_true',
                        'joint_XOR_true']

    n_samples = 200
    coherence_p = 0.8
    # option 1: keep coherence fixed, vary signal strength
    parameter_to_vary = 'signal_strength'
    parameter_values = [0, 0.2, 0.4, 0.6, 0.8, 1]
    signal_strength = parameter_values
    # option 2: keep signal strength fixed, vary coherence
    # parameter_to_vary = 'coherence_p'
    # parameter_values = [0, 0.2, 0.4, 0.6, 0.8, 1]


    # signal_strength = 1
    # parameter_to_vary = 'none'
    # parameter_values = ['none']
elif figure == 'figure3_nsamples':

    n_repeats = 1
    n_permutations = 100

    simulation_types = ['linear_true',
                        'joint_XOR_true']

    coherence_p = 0.8
    signal_strength = 0.8
    parameter_to_vary = 'n_samples'
    parameter_values = [30, 40, 50, 75, 100, 200]
    n_samples = parameter_values


elif figure == '2D_conf':

    n_repeats = 1
    n_permutations = 100
    n_samples = 200
    n_kfold_splits = 3

    simulation_types = ['two_confounds_continuous']

    deconf_methods = ['raw',
                      'decode_from_confound',
                      'distribution_match',
                      'confound_isolating_cv',
                      'linear_confound_regression',
                      'predictability_lin']

    confcv_min_sample_size = 40
    confcv_n_remove = 1
    posthoc_counterbalancing_binsize = None
    posthoc_counterbalancing_n_bins = 30
    coherence_p = None
    signal_strength = None
    parameter_to_vary = 'none'
    parameter_values = ['none']

if figure == 'test':
    n_repeats = 1
    n_permutations = 100
    simulation_types = ['nonlinear_true_2D']
    n_samples = 200
    coherence_p = 0.8
    signal_strength = 1
    parameter_to_vary = 'none'
    parameter_values = ['none']


output_folder = os.path.join(DATA_PATH, 'simulation_results',
                             'SIMULATION_{}'.format(settings_name))

plots_folder = os.path.join(DATA_PATH, 'plots', 'simulations',
                            'DECODE_SETTINGS_{}'.format(settings_name))

if not os.path.isdir(plots_folder):
    os.makedirs(plots_folder)

if not os.path.isdir(output_folder):
    os.makedirs(output_folder)


df = pd.DataFrame(columns=['simulation_type',
                           'parameter_to_vary',
                           'parameter_value',
                           'repeat',
                           'permuted',
                           'permutation',
                           'decoder',
                           'method',
                           'mean_score',
                           'full_p',
                           'full_sig',
                           'partial_p',
                           'partial_sig'])

for sim_ix, simulation_type in enumerate(simulation_types):

    print('\nSimulation: {} ({:01d} of {:01d})\n'.format(simulation_type, sim_ix+1, len(simulation_types)))

    # set up paths for figures
    # iterate over values of the parameter to vary

    data_keys = ['{}={}'.format(parameter_to_vary, v) for v in parameter_values]
    data = {k : {n : {} for n in range(n_repeats)} for k in data_keys}

    for k, parameter_value in enumerate(parameter_values):

        for nrep in range(n_repeats):

            # by setting the seed here, I ensure that regardless of how many repeats
            # I run or which simulations, I get the same simulations (and permutations)
            # in this way, if I change the parameter, the permutations are the same
            # upon which different parameter values act
            if set_seed:
                np.random.seed(nrep)

            if parameter_to_vary == 'n_samples':
                n_samples = parameter_value
                print('Setting {} to {}'.format(parameter_to_vary, parameter_values[k]))

            if parameter_to_vary == 'coherence_p':
                p = parameter_values[k]
                print('Setting {} to {}'.format(parameter_to_vary, parameter_values[k]))
            else:
                p = coherence_p

            a = np.random.normal(0, 2)
            print(a)

            if simulation_type == 'linear':
                # confound noise term
                C_0 = np.random.normal(loc=0, scale=1, size=n_samples)
                # confound weight
                w_cy = 2.5
                # function with which y enters c
                def f(x):
                    return x
                # feature noise term
                X_0 = np.random.normal(loc=0, scale=1, size=n_samples)
                w_xy = 0
                w_xc = 1
                # function with which y enters X
                def g(x):
                    return x
                # function with which c enters X
                def h(x):
                    return x

            elif simulation_type == 'conf_noise':
                C_0 = np.random.normal(loc=0, scale=1, size=n_samples)
                X_0 = np.random.normal(loc=0, scale=1, size=n_samples)

                w_cy = 3
                w_xy = 0 # all information in goes via the confound
                w_xc = 2

                def f(x):
                    return x

                def g(x):
                    return x

                def h(x):
                    a = 1
                    b = 0.5
                    d = 0.8
                    N = np.random.normal(loc=0, scale=1, size=x.shape[0])
                    return a * x + N * (b + d * x)

            elif simulation_type == 'conf_noise_2':
                C_0 = np.random.gumbel(loc=2, scale=1, size=n_samples)
                C_0 = np.random.uniform(0, 4, size=n_samples)
                X_0 = 0
                w_cy = 3
                w_xy = 0  # all information in goes via the confound
                w_xc = 1

                def f(x):
                    return x


                def g(x):
                    return x


                def h(x):
                    # a = 0
                    # b = 0
                    # d = 1
                    # N = gamma.rvs(a=1, loc=0, scale=1, size=x.shape[0])
                    # # N = np.random.normal(loc=0, scale=1, size=x.shape[0])
                    # return a * x + N * (b + d * x ** 2)
                    N = gamma.rvs(a=1, loc=0, scale=1, size=x.shape[0])
                    # N = np.random.normal(loc=0, scale=1, size=x.shape[0])
                    return N * (x ** 2)

            elif simulation_type == 'nonlinear':
                C_0 = np.random.normal(loc=0, scale=1, size=n_samples)
                X_0 = np.random.normal(loc=0, scale=0.5, size=n_samples)

                w_cy = 3
                w_xy = 0  # all information in goes via the confound
                w_xc = 2

                def f(x):
                    return x

                def g(x):
                    return x

                def h(x):
                    return np.arctan(12 * x - 4)

            elif simulation_type == 'nonlinear_2':
                C_0 = np.random.normal(loc=0, scale=0.3, size=n_samples)
                X_0 = np.random.normal(loc=0, scale=0.3, size=n_samples)

                w_cy = 1
                w_xy = 0
                w_xc = 1


                def f(x):
                    return x

                def g(x):
                    return x

                def h(x):
                    # noisy_x = np.random.normal(x, scale=0.5)
                    return norm.pdf(x, loc=0, scale=0.3)

            elif simulation_type == 'joint_XOR':
                C_0 = np.random.normal(loc=0, scale=1, size=n_samples)
                X_0 = 0

                w_cy = 2
                w_xy = 0  # all information in goes via the confound
                w_xc = 1

                def f(x):
                    # TODO could be a degraded version of Y
                    return x

                def g(x):
                    return np.hstack([x.reshape(-1, 1), x.reshape(-1, 1)])

                def h(x):
                    samples = []
                    for val in x:
                        if val >= 1:
                            cov = [[1, 0.9], [0.9, 1]]

                        elif val < 1:
                            cov = [[1, -0.9], [-0.9, 1]]

                        sample = np.random.multivariate_normal([0, 0], cov, 1)
                        samples.append(sample)
                    return np.vstack(samples)

            elif simulation_type == 'joint_AND':
                C_0 = np.random.normal(loc=0, scale=0.5, size=n_samples)
                X_0 = 0
                w_cy = 2
                w_xy = 0  # all information in goes via the confound
                w_xc = 1


                def f(x):
                    # TODO could be a degraded version of Y
                    return x

                def g(x):
                    return np.hstack([x.reshape(-1, 1), x.reshape(-1, 1)])

                def h(x):
                    samples = []
                    for val in x:
                        cov = [[0.5, 0], [0, 0.5]]
                        if val >= 1:
                            mu = [5, 5]
                            sample = np.random.multivariate_normal(mu, cov, 1)

                        elif val < 1:
                            mu = np.random.choice([[0, 5], [5, 0]], p=[0.5, 0.5])
                            sample = np.random.multivariate_normal(mu, cov, 1)

                        samples.append(sample)
                    return np.vstack(samples)


                def h(x):
                    samples = []
                    for val in x:
                        cov = [[0.5, 0], [0, 0.5]]
                        if val >= 1:
                            mu = [5, 5]
                        elif val < 1:
                            mu = np.array([[0, 5], [5, 0]])[np.random.randint(2), :]
                        sample = np.random.multivariate_normal(mu, cov, 1)
                        samples.append(sample)
                    return np.vstack(samples)

            elif simulation_type == 'linear_true':
                C_0 = np.random.normal(loc=0, scale=0.6, size=n_samples)
                X_0 = np.random.normal(loc=0, scale=0.6, size=n_samples)

                w_cy = 1

                if parameter_to_vary == 'signal_strength':
                    w_xy = parameter_values[k]
                    print('Setting {} to {}'.format(parameter_to_vary, parameter_values[k]))
                else:
                    w_xy = signal_strength

                w_xc = 1

                def f(x):
                    ret_x = []
                    for sample in x:
                        if sample == 0:
                            ret_x.append(np.random.choice([0, 1], p=[0.5 + p / 2, 0.5 - p / 2]))
                        elif sample == 1:
                            ret_x.append(np.random.choice([0, 1], p=[0.5 - p / 2, 0.5 + p / 2]))
                    return np.array(ret_x)

                def g(x):
                    return x

                def h(x):
                    return x

            elif simulation_type == 'nonlinear_true':
                C_0 = np.random.normal(loc=0, scale=0.6, size=n_samples)
                X_0 = np.random.normal(loc=0, scale=0.6, size=n_samples)

                w_cy = 1

                if parameter_to_vary == 'signal_strength':
                    w_xy = parameter_values[k]
                    print('Setting {} to {}'.format(parameter_to_vary, parameter_values[k]))
                else:
                    w_xy = signal_strength

                w_xc = 1

                def f(x):
                    ret_x = []
                    for sample in x:
                        if sample == 0:
                            ret_x.append(np.random.choice([0, 1], p=[0.5 + p / 2, 0.5 - p / 2]))
                        elif sample == 1:
                            ret_x.append(np.random.choice([0, 1], p=[0.5 - p / 2, 0.5 + p / 2]))
                    return np.array(ret_x)

                def g(x):
                    return x

                def h(x):
                    return x

            elif simulation_type == 'nonlinear_true_2D':
                X_0 = np.random.multivariate_normal([0, 0], [[1, 0], [0, 1]], size=n_samples)
                C_0 = np.random.normal(loc=0, scale=0.5, size=n_samples)

                w_cy = 1
                if parameter_to_vary == 'signal_strength':
                    w_xy = parameter_values[k]
                    print('Setting {} to {}'.format(parameter_to_vary, parameter_values[k]))
                else:
                    w_xy = signal_strength
                w_xc = 0.5

                def f(x):
                    ret_x = []
                    for sample in x:
                        if sample == 0:
                            ret_x.append(np.random.choice([0, 1], p=[0.5 + p / 2, 0.5 - p / 2]))
                        elif sample == 1:
                            ret_x.append(np.random.choice([0, 1], p=[0.5 - p / 2, 0.5 + p / 2]))
                    return np.array(ret_x)

                def g(x):
                    samples = []
                    for val in x:
                        if val == 1:
                            x1 = np.random.uniform(low=-6, high=6)
                            x2 = 3 - x1 ** 2
                        elif val == 0:
                            x1 = np.random.uniform(low=-2.5, high=2.5)
                            x2 = -3 - (x1) ** 4
                        sample = [x1, x2]
                        samples.append(sample)
                    return np.vstack(samples)

                def h(x):
                    return np.hstack([x.reshape(-1, 1), x.reshape(-1, 1)])

            elif simulation_type == 'conf_noise_true':
                C_0 = np.random.normal(loc=0, scale=1, size=n_samples)
                X_0 = np.random.normal(loc=0, scale=1, size=n_samples)

                w_cy = 1

                if parameter_to_vary == 'signal_strength':
                    w_xy = parameter_values[k]
                    print('Setting {} to {}'.format(parameter_to_vary, parameter_values[k]))
                else:
                    w_xy = signal_strength

                w_xc = 3

                def f(x):
                    ret_x = []
                    for sample in x:
                        if sample == 0:
                            ret_x.append(np.random.choice([0, 1], p=[0.5 + p / 2, 0.5 - p / 2]))
                        elif sample == 1:
                            ret_x.append(np.random.choice([0, 1], p=[0.5 - p / 2, 0.5 + p / 2]))
                    return np.array(ret_x)

                # y influences linearly + noise
                def g(x):
                    # a = 0
                    # b = 0.5
                    # d = 2
                    a = 0
                    b = 0
                    d = 4
                    N = np.random.normal(loc=0, scale=1, size=x.shape[0])
                    return a * x + N * (b + d * x)


                def g(x):
                    a = 0
                    b = 0
                    d = 3
                    N = np.random.normal(loc=0, scale=1, size=x.shape[0])
                    N = gamma.rvs(a=1, loc=0, scale=1, size=x.shape[0])
                    return a * x + N * (b + d * x)


                def h(x):
                    return x

            elif simulation_type == 'joint_XOR_true':
                C_0 = np.random.normal(loc=0, scale=1, size=n_samples)
                # needs a bit of noise otherwise of w_xy = 0 we get a straight line
                X_0 = np.random.multivariate_normal([0, 0], [[0.1, 0], [0, 0.1]], size=n_samples)
                #X_0 = 0

                w_cy = 1

                if parameter_to_vary == 'signal_strength':
                    w_xy = parameter_values[k]
                    print('Setting {} to {}'.format(parameter_to_vary, parameter_values[k]))
                else:
                    w_xy = signal_strength

                w_xc = 1
                # TODO how to map 1D C to 2D X without changing the correlation structure?

                def f(x):
                    ret_x = []
                    for sample in x:
                        if sample == 0:
                            ret_x.append(np.random.choice([0, 1], p=[0.5 + p / 2, 0.5 - p / 2]))
                        elif sample == 1:
                            ret_x.append(np.random.choice([0, 1], p=[0.5 - p / 2, 0.5 + p / 2]))
                    return np.array(ret_x)

                def g(x):
                    samples = []
                    for val in x:
                        if val == 1:
                            cov = [[1, 0.9], [0.9, 1]]

                        elif val == 0:
                            cov = [[1, -0.9], [-0.9, 1]]
                        sample = np.random.multivariate_normal([0, 0], cov, 1)
                        samples.append(sample)
                    return np.vstack(samples)


                def h(x):
                    return np.hstack([x.reshape(-1, 1), x.reshape(-1, 1)])

            elif simulation_type == 'two_confounds_continuous':
                C_01 = np.random.normal(loc=0, scale=0.5, size=n_samples).reshape(-1, 1)
                C_02 = np.random.normal(loc=0, scale=0.5, size=n_samples).reshape(-1, 1)
                C_0 = np.hstack([C_01, C_02])
                X_0 = np.random.normal(loc=0, scale=0.5, size=n_samples)

                w_cy = 1
                w_xy = 0
                w_xc = 1

                def f(x):
                    return np.vstack([x, x]).T


                def g(x):
                    return x


                def h(x):
                    return x[:, 0] + x[:, 1]


            else:
                raise ValueError

            # generate simulation
            y = np.random.choice([0, 1], size=n_samples, p=[0.5, 0.5])
            C = C_0 + w_cy * f(y)
            X = X_0 + w_xy * g(y) + w_xc * h(C)

            print(X[3])

            try:
                X.shape[1]
            except IndexError:
                X = X.reshape(-1, 1)

            try:
                C.shape[1]
            except IndexError:
                C = C.reshape(-1, 1)

            # if simulation_type == 'linear':
            #     print(X[3, :])
            # if simulation_type == 'nonlinear_2':
            #     print(C[39, :])
            # if simulation_type == 'conf_noise_2':
            #     print(X[49, :])

            # generate permutations
            # permute C and y in the same way, determine later if C permutations
            # are necessary depending on the deconf method used
            # TODO shifting not implemented
            C_permuted_list, y_permuted_list = [], []
            for pix in range(n_permutations + 1):
                # print(s)
                if pix == 0:
                    C_permuted = np.copy(C)
                    y_permuted = np.copy(y)
                else:
                    random_index = np.random.permutation(np.arange(X.shape[0]))
                    C_permuted = C[random_index, :]
                    y_permuted = y[random_index]
                C_permuted_list.append(C_permuted)
                y_permuted_list.append(y_permuted)

            data_key = '{}={}'.format(parameter_to_vary, parameter_value)
            data[data_key][nrep]['X'] = X
            data[data_key][nrep]['y'] = y
            data[data_key][nrep]['C'] = C
            # TODO saving permutations is probably not necessary
            #data[data_key][nrep]['y_permuted_list'] = y_permuted_list
            #data[data_key][nrep]['C_permuted_list'] = C_permuted_list


            if plot_data and nrep == 0:

                # plot data simple for 1d X we consider both 1d and 2d C,
                # for 2d X we assume 1d C
                if X.shape[1] == 1:
                    if C.shape[1] > 1:
                        fig, ax = plt.subplots(1, 2, figsize=[5, 3], sharex=True, sharey=True)
                        for iii, axx in enumerate(ax):
                            axx.scatter(C[y == 0, iii], X[y == 0, 0], c='red', label='y=0', s=20, linewidth=0,
                                       marker='o')
                            axx.scatter(C[y == 1, iii], X[y == 1, 0], c='green', label='y=1', s=20, linewidth=0,
                                       marker='o')
                            axx.legend(frameon=False, loc='upper left')
                        # ax.axis('equal')
                            axx.axvline(0, ls=':', c='grey')
                            axx.axhline(0, ls=':', c='grey')
                            axx.legend().remove()
                        ax[0].set_xlabel('$C_1$')
                        ax[0].set_ylabel('$X$')
                        ax[1].set_xlabel('$C_2$')

                        sns.despine()
                        plt.tight_layout()

                        plot_name = 'data_single_{}_p{}.{}'.format(simulation_type, k, plot_format)
                        fig.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                                    bbox_inches="tight")
                    else:
                        fig, ax = plt.subplots(1, 1, figsize=[3, 3])
                        ax.scatter(C[y==0], X[y==0, 0], c='red', label='y=0', s=20, linewidth=0,
                                   marker='o')
                        ax.scatter(C[y==1], X[y==1, 0], c='green', label='y=1', s=20, linewidth=0,
                                   marker='o')
                        ax.legend(frameon=False, loc='best')
                        #ax.axis('equal')
                        ax.legend().remove()
                        ax.axvline(0, ls=':', c='grey')
                        ax.axhline(0, ls=':', c='grey')
                        ax.set_xlabel('$C$')
                        ax.set_ylabel('$X$')
                        sns.despine()
                        plt.tight_layout()

                        plot_name = 'data_single_{}_p{}.{}'.format(simulation_type, k, plot_format)
                        fig.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                                  bbox_inches="tight")

                else:
                    fig, ax = plt.subplots(1, 1, figsize=[3, 3])
                    ax.scatter(X[y==0, 0], X[y==0, 1], c='red', label='y=0', s=20, linewidth=0,
                               marker='o')
                    ax.scatter(X[y==1, 0], X[y==1, 1], c='green', label='y=1', s=20, linewidth=0,
                               marker='o')
                    ax.legend(frameon=True, loc='best')
                    #ax.axis('equal')
                    ax.legend().remove()
                    ax.axvline(0, ls=':', c='grey')
                    ax.axhline(0, ls=':', c='grey')
                    ax.set_xlabel('$X_1$')
                    ax.set_ylabel('$X_2$')
                    sns.despine()
                    plt.tight_layout()

                    plot_name = 'data_single_{}_p{}.{}'.format(simulation_type, k, plot_format)
                    fig.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                              bbox_inches="tight")


            for method_idx, deconf_method in enumerate(deconf_methods):


                if np.isin(deconf_method, predictability_methods):
                    decoders_to_iterate = ['none']
                else:
                    decoders_to_iterate = decoders

                for decoder in decoders_to_iterate:
                    print('Decoding with {}, deconf method: {}'.format(decoder, deconf_method))

                    for nperm in range(n_permutations + 1):
                        if nperm == 0:
                            permuted = False
                        elif nperm > 0:
                            permuted = True

                        y_dec = y_permuted_list[nperm]

                        # only if method is predictability we permute C together with y
                        if np.isin(deconf_method, predictability_methods):
                            C_dec = C_permuted_list[nperm]
                        else:
                            C_dec = np.copy(C)

                        # avoid running spisak tests on shuffled data
                        if permuted or np.isin(deconf_method, predictability_methods):
                            spisak_full = False
                            spisak_partial = False
                        else:
                            spisak_full = run_spisak_full_test
                            spisak_partial = run_spisak_partial_test

                        # print('Decoding with {}, deconf method: {}'.format(decoder, deconf_method))
                        # print('--- repeat {:02d} of {:02d} --- permutation {:02d} of {:02d}'.format(nrep, n_repeats, nperm, n_permutations))

                        dec_out = decode(X=X, y=y_dec, C=C_dec,
                                         deconf_method=deconf_method,
                                         decoder=decoder,
                                         posthoc_counterbalancing_binsize=posthoc_counterbalancing_binsize,
                                         posthoc_counterbalancing_n_bins=posthoc_counterbalancing_n_bins,
                                         standardize_features=True,
                                         standardize_confound=True,
                                         n_kfold_splits=n_kfold_splits,
                                         tune_hyperpars_decoder=tune_hyperpars_decoder,
                                         run_spisak_full_test=spisak_full,
                                         run_spisak_partial_test=spisak_partial,
                                         num_perms_spisak=num_perms_spisak,
                                         predictability_tune_regularization=predictability_tune_regularization,
                                         LR_predictability_C_list=LR_predictability_C_list,
                                         SVC_predictability_C_list=SVC_predictability_C_list,
                                         SVC_predictability_C=SVC_predictability_C,
                                         LR_predictability_penalty=LR_predictability_penalty,
                                         LR_predictability_C=LR_predictability_C,
                                         LR_predictability_solver=LR_predictability_solver,
                                         tune_C_predictability_nfolds=tune_C_predictability_nfolds,
                                         LR_predictability_max_iter=LR_predictability_max_iter,
                                         LR_decode_C_list=LR_decode_C_list,
                                         SVC_decode_C_list=SVC_decode_C_list,
                                         LR_decode_C=LR_decode_C,
                                         SVC_decode_C=SVC_decode_C)
                                         # n_estimators=n_estimators, max_depth=max_depth,
                                         # min_samples_split=min_samples_split,
                                         # min_samples_leaf=min_samples_leaf)


                        mean_score = dec_out['mean_score']
                        kfold_scores = dec_out['kfold_scores']
                        full_p = dec_out['full_p']
                        full_sig = dec_out['full_sig']
                        partial_p = dec_out['partial_p']
                        partial_sig = dec_out['partial_sig']

                        df.loc[df.shape[0], :] = [simulation_type,
                                                  parameter_to_vary,
                                                  parameter_value,
                                                  nrep,
                                                  permuted,
                                                  nperm,
                                                  decoder,
                                                  deconf_method,
                                                  dec_out['mean_score'],
                                                  dec_out['full_p'],
                                                  dec_out['full_sig'],
                                                  dec_out['partial_p'],
                                                  dec_out['partial_sig']]

            df['mean_score'] = pd.to_numeric(df['mean_score'])
            # plot bars of methods scored by accuracy



    pars = {'settings_name' : settings_name,

            'n_permutations' : n_permutations,
            'n_repeats' : n_repeats,
            'n_samples' : n_samples,
            'set_seed' : set_seed,
            'simulation_types' : simulation_types,
            'decoders' : decoders,
            'deconf_methods' : deconf_methods,
            'posthoc_counterbalancing_binsize' : posthoc_counterbalancing_binsize,
            'posthoc_counterbalancing_n_bins' : posthoc_counterbalancing_n_bins,
            'n_kfold_splits' : n_kfold_splits,
            'run_spisak_full_test' : run_spisak_full_test,
            'run_spisak_partial_test' : run_spisak_partial_test,
            'num_perms_spisak' : num_perms_spisak,
            'coherence_p' : coherence_p,
            'signal_strength' : signal_strength,
            'parameter_to_vary' : parameter_to_vary,
            'parameter_values' : parameter_values}

    out = {'pars' : pars,
           'df' : df,
           'data' : data}

    output_full_path = os.path.join(output_folder, 'results_{}_{}.pkl'.format(settings_name, simulation_type))
    print('Saving simulation to {}'.format(output_full_path))
    pickle.dump(out, open(output_full_path, 'wb'))



