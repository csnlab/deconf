import os
import pickle
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
from constants import *
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from mlconfound.stats import full_confound_test, partial_confound_test
from sklearn.model_selection import cross_val_predict
from confound_prediction.deconfounding import confound_isolating_cv
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import minmax_scale
from utils import *
from decoding_utils import decode

settings_name = 'aug19'

data_settings_name = 'may24'  # 'true+conf_effect'

experiments = ['choice_V', 'choice_T']

methods = ['raw', 'linear_confound_regression',
           'confound_isolating_cv', 'distribution_match']

# methods = ['raw']

time_bin = 1


plot_format = 'png'
plots_folder = os.path.join(DATA_PATH, 'plots', 'modid',
                            'DATA_SETTINGS_{}'.format(data_settings_name),
                            'DECODE_SETTINGS_{}'.format(settings_name))
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)

for method in methods :

    for experiment in experiments :

        data = load_decoding_data(settings_name=data_settings_name,
                                  experiment_name=experiment,
                                  area_spikes='all',
                                  dataset='julien')

        sessions = data['pars']['sessions']

        df = pd.DataFrame(columns=['animal_id', 'session_id',
                                   'area_spikes', 'n_neurons',
                                   'experiment', 'features',
                                   'time', 'time_bin',
                                   'score',
                                   'full_p', 'full_sig', 'partial_p',
                                   'partial_sig'])

        pos = []
        for i, row in sessions.iterrows() :
            # get metadata
            animal_id = row['animal_id']
            session_id = row['session_id']
            area_spikes = row['area']
            target_name = row['target_name']

            key = '{}_{}'.format(session_id, area_spikes)

            C = data['data'][key]['binned_movement'][time_bin]
            C = np.where(np.isnan(C), np.nanmean(C, axis=0), C)
            pos.append(C)

        p = np.vstack(pos)


        out = pd.qcut(p[:, 0], q=10, labels=None)
        bins_X = out.categories

        out = pd.qcut(p[:, 1], q=10, labels=None)
        bins_Y = out.categories


        out_X = pd.cut(C[:, 0], bins=bins_X, labels=False)
