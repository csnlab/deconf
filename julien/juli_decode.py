import os
import pickle
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
from constants import *
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from mlconfound.stats import full_confound_test, partial_confound_test
from sklearn.model_selection import cross_val_predict
from confound_prediction.deconfounding import confound_isolating_cv
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from sklearn.linear_model import LinearRegression
from sklearn.preprocessing import minmax_scale
from utils import *
from decoding_utils import decode

settings_name = 'aug21b'

data_settings_name = 'aug21_samplestart' #'may24'#'true+conf_effect'

experiments = ['choice_V', 'choice_T',
               'object_T', 'object_V']


methods = ['raw', 'linear_confound_regression',
           'distribution_match']

methods = ['nonlinear_confound_regression']

bin_position_data_dict = {'raw' : False,
                          'linear_confound_regression' : False,
                          'nonlinear_confound_regression' : False,
                          'distribution_match' : True,
                          'confound_isolating_cv' : True,
                          'wool_deconf' : False}
n_pos_bins = 4

min_trials = 40

#methods = ['raw']

time_bin = 0

plot = True
distribution_match_binsize = 0.3
n_kfold_splits = 3
run_spisak_tests = False
num_perms_spisak = 5000
n_shifts = 20
n_shuffles = 200
surrogate_method = 'shift'
tune_C = True
Cs = 10
tune_C_nfolds = 3
max_iter = 5000
C_reg = 0.3



plot_format = 'png'
plots_folder = os.path.join(DATA_PATH, 'plots', 'julien',
                            'DATA_SETTINGS_{}'.format(data_settings_name),
                            'DECODE_SETTINGS_{}'.format(settings_name))
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)


for method in methods:

    for experiment in experiments:

        data = load_decoding_data(settings_name=data_settings_name,
                                  experiment_name=experiment,
                                  area_spikes='all',
                                  dataset='julien')

        sessions = data['pars']['sessions']

        df = pd.DataFrame(columns=['animal_id', 'session_id',
                                   'area_spikes', 'n_neurons',
                                   'experiment', 'features',
                                   'time', 'time_bin',
                                   'score',
                                   'full_p', 'full_sig', 'partial_p', 'partial_sig'])

        bin_position_data = bin_position_data_dict[method]
        if bin_position_data:
            position_data_list = []
            for i, row in sessions.iterrows() :
                # get metadata
                session_id = row['session_id']
                area_spikes = row['area']
                key = '{}_{}'.format(session_id, area_spikes)
                C = data['data'][key]['binned_movement'][time_bin]
                C = np.where(np.isnan(C), np.nanmean(C, axis=0), C)
                position_data_list.append(C)

            position_data = np.vstack(position_data_list)

            out_X = pd.qcut(position_data[:, 0], q=n_pos_bins, labels=None)
            bins_X = out_X.categories

            out_Y = pd.qcut(position_data[:, 1], q=n_pos_bins, labels=None)
            bins_Y = out_Y.categories


        for i, row in sessions.iterrows() :
            # get metadata
            animal_id = row['animal_id']
            session_id = row['session_id']
            area_spikes = row['area']
            target_name =  row['target_name']


            key = '{}_{}'.format(session_id, area_spikes)

            # get time parameters
            time_bin_times = data['data'][key]['time_bin_times']
            time_bins = data['data'][key]['time_bins']
            n_time_bins_per_trial = len(time_bin_times)

            # select one time bin
            time = time_bin_times[time_bin]
            #t0, t1 = time_bins[time_bin]


            # --- decode from confound ---

            # get decoding data
            X = data['data'][key]['binned_movement'][time_bin]

            # TODO fill nan
            X = np.where(np.isnan(X), np.nanmean(X, axis=0), X)

            y = data['data'][key]['target']

            kfold = StratifiedKFold(n_splits=n_kfold_splits, shuffle=True,
                                    random_state=92)

            kfold_scores = []

            for fold, (training_ind, testing_ind) in enumerate(kfold.split(X, y)) :

                decoder = SGDClassifier(loss='log_loss', random_state=92)
                #decoder = RandomForestClassifier(n_estimators=300, random_state=92)

                X_train = X[training_ind, :]
                X_test = X[testing_ind, :]
                y_train = y[training_ind]
                y_test = y[testing_ind]

                ss = StandardScaler()
                X_train = ss.fit_transform(X_train)
                X_test = ss.transform(X_test)

                decoder.fit(X_train, y_train)
                y_pred = decoder.predict(X_test)


                score = balanced_accuracy_score(y_test, y_pred)
                kfold_scores.append(score)

            mean_score = np.mean(kfold_scores)
            full_p = np.nan
            full_sig = False
            partial_p = np.nan
            partial_sig = False

            df.loc[df.shape[0], :] = [animal_id, session_id, area_spikes, X.shape[1],
                   experiment, 'confound', time, time_bin, mean_score,
                   full_p, full_sig, partial_p, partial_sig]


            # --- Decode from spikes ---

            # get decoding data
            X = data['data'][key]['binned_spikes'][time_bin]
            C = data['data'][key]['binned_movement'][time_bin]
            C = np.where(np.isnan(C), np.nanmean(C, axis=0), C)

            if bin_position_data:
                binned_X = pd.cut(C[:, 0], bins=bins_X, labels=False).codes
                binned_Y = pd.cut(C[:, 1], bins=bins_Y, labels=False).codes
                C = pd.Categorical(zip(binned_X, binned_Y)).codes.reshape(-1, 1)


            #C = np.sum(C, axis=1)
            y = data['data'][key]['target']

            try:
                dec_out = decode(X=X, y=y, C=C,
                                 deconf_method=method,
                                 posthoc_counterbalancing_binsize=distribution_match_binsize,
                                 posthoc_counterbalancing_already_binned=bin_position_data,
                                 min_trials=min_trials,
                                 n_kfold_splits=n_kfold_splits,
                                 run_spisak_tests=run_spisak_tests,
                                 num_perms_spisak=num_perms_spisak,
                                 surrogate_method=surrogate_method,
                                 n_shifts=n_shifts,
                                 n_shuffles=n_shuffles,
                                 predictability_tune_regularization=tune_C,
                                 LR_predictability_C_list=Cs,
                                 tune_C_predictability_nfolds=tune_C_nfolds,
                                 LR_predictability_max_iter=max_iter)

            except np.linalg.LinAlgError:
                continue

            except Exception as error:
                print(repr(error))
                continue
            #
            # if method == 'confound_isolating_cv':
            #
            #     C = np.sum(C, axis=1).reshape(-1, 1)
            #
            #     try:
            #         _, _, _, _, ids_test, ids_train = confound_isolating_cv(X, y, C[:, 0],
            #                                                   random_seed=92,
            #                                                   min_sample_size=None,
            #                                                   cv_folds=n_splits,
            #                                                   n_remove=None)
            #
            #         iterable = zip(ids_train, ids_test)
            #     except np.linalg.LinAlgError:
            #         continue
            #
            # else:
            #
            #     kfold = StratifiedKFold(n_splits=n_splits, shuffle=True,
            #                             random_state=92)
            #     iterable = kfold.split(X, y)
            #
            #
            # kfold_scores = []
            # y_test_all, y_pred_all, c_all = [], [], []
            #
            # for fold, (training_ind, testing_ind) in enumerate(iterable) :
            #
            #     decoder = SGDClassifier(random_state=92)
            #     #decoder = RandomForestClassifier(n_estimators=300, random_state=92)
            #     X_train = X[training_ind, :]
            #     X_test = X[testing_ind, :]
            #     y_train = y[training_ind]
            #     y_test = y[testing_ind]
            #     C_train = C[training_ind, :]
            #     C_test = C[testing_ind, :]
            #
            #     if method == 'linear_confound_regression' :
            #         reg = LinearRegression().fit(C_train, X_train)
            #         X_train = X_train - reg.predict(C_train)
            #         X_test = X_test - reg.predict(C_test)
            #
            #     elif method == 'nonlinear_confound_regression':
            #         reg = RandomForestRegressor(n_estimators=500,
            #                                     random_state=92).fit(C_train, X_train)
            #         X_train = X_train - reg.predict(C_train)
            #         X_test = X_test - reg.predict(C_test)
            #
            #     ss = StandardScaler()
            #     X_train = ss.fit_transform(X_train)
            #     X_test = ss.transform(X_test)
            #
            #     ss = StandardScaler()
            #     C_train = ss.fit_transform(C_train)
            #     C_test = ss.transform(C_test)
            #
            #     decoder.fit(X_train, y_train)
            #     y_pred = decoder.predict(X_test)
            #     score = balanced_accuracy_score(y_test, y_pred)
            #     kfold_scores.append(score)
            #     y_test_all.append(y_test)
            #     y_pred_all.append(y_pred)
            #     c_all.append(C_test[:, 0])
            #
            # target = np.hstack(y_test_all)
            # predictions = np.hstack(y_pred_all)
            # confound = np.hstack(c_all)
            # mean_score = np.mean(kfold_scores)
            #
            # if run_spisak_tests:
            #     try:
            #         full_test = full_confound_test(target, predictions, confound,
            #                                        num_perms=num_perms_spisak,
            #                                        cat_y=True,
            #                                        cat_yhat=True,
            #                                        cat_c=False,
            #                                        mcmc_steps=50,
            #                                        cond_dist_method='gam',
            #                                        return_null_dist=False,
            #                                        random_state=92, progress=True,
            #                                        n_jobs=-1)
            #         full_p = full_test.p
            #         full_sig = full_p < 0.05
            #     except ValueError:
            #         full_p = np.nan
            #         full_sig = False
            #
            #     partial_test = partial_confound_test(target, predictions, confound,
            #                                    num_perms=num_perms_spisak,
            #                                    cat_y=True,
            #                                    cat_yhat=True,
            #                                    cat_c=False,
            #                                    mcmc_steps=50,
            #                                    cond_dist_method='gam',
            #                                    return_null_dist=False,
            #                                    random_state=92, progress=True,
            #                                    n_jobs=-1)
            #     partial_p = partial_test.p
            #     partial_sig = partial_p < 0.05
            #
            # else:
            #     full_p = np.nan
            #     full_sig = False
            #     partial_p = np.nan
            #     partial_sig = False

            mean_score = dec_out['mean_score']
            full_p = dec_out['full_p']
            full_sig = dec_out['full_sig']
            partial_p = dec_out['partial_p']
            partial_sig = dec_out['partial_sig']

            df.loc[df.shape[0], :] = [animal_id, session_id, area_spikes, X.shape[1],
                   experiment, 'spikes', time, time_bin, mean_score,
                   full_p, full_sig, partial_p, partial_sig]


        df['time'] = [t.item() for t in df['time']]

        features_palette = {'spikes' : sns.xkcd_rgb['purple'],
                            'confound' : sns.xkcd_rgb['grey']}

        if method == 'wool_deconf':
            df.loc[:, 'score'] += 0.5

        # --- PLOT SCATTER -------------------------------------------------------------

        df = df.drop(['time_bin'], axis='columns')

        dz = pd.merge(df[df['features'] == 'spikes'],
                      df[df['features'] == 'confound'][['session_id', 'score', 'experiment']],
                      on=['session_id', 'experiment'])

        dz = dz.rename({'score_x' : 'score_spikes', 'score_y' : 'score_confound'}, axis=1)

        dz['size'] = minmax_scale(dz['n_neurons'], feature_range=(20, 150))
        all_scores = np.hstack([dz['score_spikes'].__array__(), dz['score_confound'].__array__()])
        scmin = all_scores.min()
        scmax = all_scores.max()

        axis_min, axis_max = 0, 1
        #assert axis_min <= scmin

        f, ax = plt.subplots(1, 1, figsize=[3, 3])

        for area_spikes in areas_palette.keys():
            dxx = dz[(dz['experiment'] == experiment) & (dz['area_spikes'] == area_spikes)]

            if run_spisak_tests:
                dx1 = dxx[dxx['partial_sig'].astype(bool)]
                dx2 = dxx[~dxx['partial_sig'].astype(bool)]

                ax.scatter(dx1['score_spikes'], dx1['score_confound'], s=dx1['size'],
                           c=areas_palette[area_spikes], edgecolor='w', linewidth=0)

                ax.scatter(dx2['score_spikes'], dx2['score_confound'], s=dx2['size'],
                           c=areas_palette[area_spikes], edgecolor='w', linewidth=0, alpha=0.5)
            else:
                ax.scatter(dxx['score_spikes'], dxx['score_confound'],
                           s=dxx['size'],
                           c=areas_palette[area_spikes], edgecolor='w', linewidth=1)

        if experiment == 'choice_T':
            ax.set_xlabel('Accuracy of choice dec. (dark)\nfrom spikes')
            ax.set_ylabel('Accuracy of choice dec. (dark)\nfrom position data')

        elif experiment == 'choice_V':
            ax.set_xlabel('Accuracy of choice dec. (light)\nfrom spikes')
            ax.set_ylabel('Accuracy of choice dec. (light)\nfrom position data')

        elif experiment == 'object_T':
            ax.set_xlabel('Accuracy of object dec. (dark)\nfrom spikes')
            ax.set_ylabel('Accuracy of object dec. (dark)\nfrom position data')

        elif experiment == 'object_V':
            ax.set_xlabel('Accuracy of object dec. (light)\nfrom spikes')
            ax.set_ylabel('Accuracy of object dec. (light)\nfrom position data')

        ax.plot([0, 2], [0, 2], c='grey', ls=':')
        if np.isin(method, ['wool_deconf', 'wool_nodeconf']):
            pass
        else:
            ax.set_xlim([axis_min, axis_max])
            ax.set_ylim([axis_min, axis_max])

            ax.set_xticks(np.arange(0, 1.01, 0.25))
            ax.set_yticks(np.arange(0, 1.01, 0.25))
        ax.axhline(0.5, c='grey', ls=':')
        ax.axvline(0.5, c='grey', ls=':')
        ax.set_title(method)

        #ax.get_legend().remove()
        sns.despine()
        plt.tight_layout()

        plot_name = 'decoding_ephys_{}_{}_{}.{}'.format(method, experiment, settings_name,
                                                  plot_format)
        f.savefig(os.path.join(plots_folder, plot_name), dpi=400,
                  bbox_inches="tight")

