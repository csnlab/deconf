import os
import pickle
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
from constants import *
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from mlconfound.stats import full_confound_test, partial_confound_test
from sklearn.model_selection import cross_val_predict
from confound_prediction.deconfounding import confound_isolating_cv
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from sklearn.linear_model import LinearRegression
import random
from sklearn.preprocessing import minmax_scale
from sklearn.preprocessing import scale
from decoding_utils import decode

"""
third version of the simulated modid data. 

now we generate datasets with one time point and one neuron
we vary a parameter of choice across a grid of values and 
plot the decoding accuracy of the different methods across the 
values of the parameter

confound_amount:
    the confound is generated as a copy of the stimulus, to which
    we add gaussian noise. the confound is then added to the firing
    rate after minmax scaling it between -X and X, where X is the amount. 
    in that way, for low X, the confound is associated statistically with the
    target, but it does not add predictive power to the firing rate, 
    whereas for high X it should improve the predictability
confound_noise:
    the confound is generated as a copy of the stimulus, to which
    we add gaussian noise. the confound is then not added to the firing
    rate so it does not
    not affect the decoding accuracy from the stimulus
confound_coherence: DEPRECATED: let's focus on the case where the confound
is a continuous variable
    the confound is generated as a copy of the stimulus. then, 
    for each value of the confound, with probability
    100-coherence we pick a new value (randomly either 0 or 1). 
    the confound is not added to the firing rate, 
    so it does not affect the decoding accuracy from the stimulus
trial_number:
    fix a noise/coherence confound, then vary the trial number

"""

settings_name = 'feb27'
plot = True
distribution_match_binsize = 0.3
n_kfold_splits = 3
methods = ['wool_deconf', 'distribution_match', 'linear_confound_regression', 'raw']
run_spisak_tests = False
num_perms_spisak = 5000
n_shifts = 20
n_shuffles = 200
surrogate_method = 'shift'


# pick the parameter to vary
# confound_noise, confound_coherence, trial_number
sim_parameter_names = ['confound_amount', 'confound_noise', 'trial_number']

# n of datasets generated for each parameter setting
n_trials = 200
n_repeats = 50


plot_format = 'png'
plots_folder = os.path.join(DATA_PATH, 'plots', 'modid_sim_v3', '{}'.format(settings_name))
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)


# TODO now confound noise and confound coherence are pretty much
# the same thing...


for sim_parameter_name in sim_parameter_names:
    # --- MODULATE CONFOUND NOISE --------------------------------------------------

    if sim_parameter_name == 'confound_amount':

        sim_values = np.arange(0.1, 2.5, 0.2)

        auditory_frequency = np.repeat([0, 1], n_trials // 2)
        X_all = []
        C_all = []
        y_all = []
        sim_parameter = []

        for rep in range(n_repeats):
            # for every repeat generate a confound and the auditory response
            confound_noise = np.random.normal(0, 0.5, n_trials)
            confound = auditory_frequency.__array__().copy() + confound_noise

            auditory_noise = np.random.uniform(0, 2, n_trials)
            auditory_firing_rate = auditory_frequency + auditory_noise

            for i, par_value in enumerate(sim_values):
                # then scale it
                confound_firing_rate = minmax_scale(confound, feature_range=(-par_value, par_value))
                firing_rate = auditory_firing_rate + confound_firing_rate

                # f, ax = plt.subplots(1, 2)
                # ax[0].plot(confound, label='only audio', c='red')
                #
                # ax[1].plot(auditory_firing_rate, label='only audio', c='red')
                # ax[1].plot(auditory_firing_rate+confound_firing_rate, label='audio+conf', c='green')
                # plt.legend()

                X_all.append(firing_rate.reshape(-1, 1))
                C_all.append(confound.reshape(-1, 1))
                y_all.append(auditory_frequency)
                sim_parameter.append(par_value)

        sim_parameter = np.hstack(sim_parameter)


    elif sim_parameter_name == 'confound_noise':

        vals = [0.25, 0.5, 0.75, 1, 1.5, 2, 3]
        sim_parameter = np.repeat(vals, n_repeats)

        auditory_frequency = np.repeat([0, 1], n_trials // 2)
        X_all = []
        C_all = []
        y_all = []

        for i, par_value in enumerate(sim_parameter):

            confound_noise = np.random.normal(0, par_value, n_trials)
            confound = auditory_frequency.__array__().copy() + confound_noise

            auditory_noise = np.random.uniform(0, 2, n_trials)
            auditory_firing_rate = auditory_frequency + auditory_noise
            confound_firing_rate = minmax_scale(confound, feature_range=(0, 0.1))

            firing_rate = auditory_firing_rate #+ confound_firing_rate

            # f, ax = plt.subplots(1, 1)
            # ax.plot(auditory_firing_rate, label='only audio', c='red')
            # ax.plot(auditory_firing_rate+confound_firing_rate, label='audio+conf', c='green')
            # plt.legend()

            X_all.append(firing_rate.reshape(-1, 1))
            C_all.append(confound.reshape(-1, 1))
            y_all.append(auditory_frequency)


    # --- MODULATE CONFOUND COHERENCE ----------------------------------------------

    elif sim_parameter_name == 'confound_coherence':
        # deprecated because it considers a binary confound

        raise ValueError


        # sim_parameter = np.repeat(np.arange(0, 101, 20), n_repeats)
        #
        # auditory_frequency = np.repeat([0, 1], n_trials // 2)
        # X_all = []
        # C_all = []
        # y_all = []
        #
        # for i, par_value in enumerate(sim_parameter):
        #
        #     confound = auditory_frequency.__array__().copy()
        #
        #     for index, val in enumerate(confound) :
        #         if random.randrange(0, 100) <= (100 - par_value):
        #             confound[index] = np.random.choice([0, 1])
        #
        #     auditory_noise = np.random.uniform(0, 2, n_trials)
        #     auditory_firing_rate = auditory_frequency + auditory_noise
        #     confound_firing_rate = minmax_scale(confound, feature_range=(0, 0.1))
        #     firing_rate = auditory_firing_rate #+ confound_firing_rate
        #
        #     X_all.append(firing_rate.reshape(-1, 1))
        #     C_all.append(confound.reshape(-1, 1))
        #     y_all.append(auditory_frequency)


    # --- MODULATE TRIAL NUMBER ----------------------------------------------------

    elif sim_parameter_name == 'trial_number':

        confound_amount = 1
        confound_sigma = 0.5
        #sim_parameter = (30 * np.logspace(0, 6, num=6, base=2, endpoint=False)).astype(int)
        sim_parameter = np.repeat([64, 96, 128, 160, 192, 256, 512, 1024], n_repeats)
        X_all = []
        C_all = []
        y_all = []

        for i, par_value in enumerate(sim_parameter):

            auditory_frequency = np.repeat([0, 1], par_value // 2)

            confound_noise = np.random.normal(0, confound_sigma, par_value)
            confound = auditory_frequency.__array__().copy() + confound_noise

            auditory_noise = np.random.uniform(0, 2, par_value)
            auditory_firing_rate = auditory_frequency + auditory_noise

            # then scale it
            confound_firing_rate = minmax_scale(confound, feature_range=(-confound_amount, confound_amount))
            firing_rate = auditory_firing_rate + confound_firing_rate

            # f, ax = plt.subplots(1, 2)
            # ax[0].plot(confound, label='only audio', c='red')
            #
            # ax[1].plot(auditory_firing_rate, label='only audio', c='red')
            # ax[1].plot(auditory_firing_rate+confound_firing_rate, label='audio+conf', c='green')
            # plt.legend()

            X_all.append(firing_rate.reshape(-1, 1))
            C_all.append(confound.reshape(-1, 1))
            y_all.append(auditory_frequency)
    else:
        raise ValueError



    df = pd.DataFrame(columns=['sim_parameter_name','sim_parameter', 'method', 'mean_score',
                               'full_p', 'full_sig', 'partial_p', 'partial_sig'])

    for method in methods:
        print(method)

        for i, par_value in enumerate(sim_parameter):

            X = X_all[i]
            C = C_all[i]
            y = y_all[i]


            dec_out = decode(X=X, y=y, C=C,
                             deconf_method=method,
                             posthoc_counterbalancing_binsize=distribution_match_binsize,
                             n_kfold_splits=n_kfold_splits,
                             run_spisak_tests=run_spisak_tests,
                             num_perms_spisak=num_perms_spisak,
                             surrogate_method=surrogate_method,
                             n_shifts=n_shifts,
                             n_shuffles=n_shuffles)

            mean_score = dec_out['mean_score']
            full_p = dec_out['full_p']
            full_sig = dec_out['full_sig']
            partial_p = dec_out['partial_p']
            partial_sig = dec_out['partial_sig']


            df.loc[df.shape[0], :] = [sim_parameter_name, par_value, method,
                                      dec_out['mean_score'], dec_out['full_p'],
                                      dec_out['full_sig'], dec_out['partial_p'],
                                      dec_out['partial_sig']]


    df.loc[df['method'] == 'wool_deconf', 'mean_score'] +=0.5

    f, ax = plt.subplots(1, 1, figsize=[6, 6])
    sns.lineplot(data=df, x='sim_parameter', y='mean_score',
                 hue='method', ax=ax, ci='sd', markers=True, style='method')
    ax.set_ylabel('Decoding score')
    ax.set_xlabel(sim_parameter_name)
    ax.axhline(0.5, ls='--', c='grey')
    ax.set_ylim([0.4, 1])
    sns.despine()
    plt.tight_layout()

    plot_name = 'dec_sim_v3_{}_{}.{}'.format(settings_name, sim_parameter_name,
                                                   plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=400, bbox_inches="tight")
