import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns


n_samples = 600



confound_weight = 0.6
C = np.repeat([0, 1], int(n_samples/2))

means = [0, 0]
# stdevs = sqrt(5),sqrt(2)
# corr = .3 / (sqrt(5)*sqrt(2) = .134
cov1 = [[1, 0.9], [0.9, 1]]
cov2 = [[1, -0.9], [-0.9, 1]]

X1 = np.random.multivariate_normal(means, cov1, int(n_samples/2))
X2 = np.random.multivariate_normal(means, cov2, int(n_samples/2))
X = np.vstack([X1, X2])

y1 = np.random.choice(a=[0, 1], size=int(n_samples/2), p=[1-confound_weight, confound_weight])
y2 = np.ones(int(n_samples/2)).astype(int)
y = np.hstack([y1, y2])

# indx = np.random.permutation(np.arange(y.shape[0]))
# X = X[indx, :]
# C = C[indx]
# y = y[indx]


f, ax = plt.subplots(1, 3, figsize=[12, 4])
sns.kdeplot(X[y==0, 0], c='red',ax=ax[0])
sns.kdeplot(X[y==1, 0], c='blue', ax=ax[0])

sns.kdeplot(X[y==0, 1], c='red',ax=ax[1])
sns.kdeplot(X[y==1, 1], c='blue', ax=ax[1])

ax[2].scatter(x=X[C==0, 0], y=X[C==0, 1], c='red',s=4, alpha=0.5, linewidth=0)
ax[2].scatter(x=X[C==1, 0], y=X[C==1, 1], c='blue',s=4, alpha=0.5, linewidth=0)

sns.kdeplot(x=X[C==0, 0], y=X[C==0, 1], c='red', ax=ax[2])
sns.kdeplot(x=X[C==1, 0], y=X[C==1, 1], c='blue', ax=ax[2])

ax[0].set_title('Variable 1')
ax[1].set_title('Variable 2')
ax[2].set_xlabel('Var 1')
ax[2].set_ylabel('Var 2')

plt.tight_layout()
sns.despine()