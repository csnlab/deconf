import os
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
from decoding_utils import decode
import pandas as pd
from plotting_style import *
from constants import *
from scipy.stats import mannwhitneyu, bootstrap
import pickle
from sklearn.model_selection import StratifiedKFold
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from utils import log2_likelihood
from sklearn.metrics import balanced_accuracy_score
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis
from sklearn.svm import SVC
"""

"""

settings_name = 'dec11'

# parameters
n_permutations = 3
n_samples = 1000
plot_data = False

simulation_types = ['linear', 'conf_noise', 'nonlinear',
                    'joint_XOR', 'joint_AND', 'linear_true_degraded', 'joint_XOR_true']

model = 'nonlinear'

n_kfold_splits = 3

run_spisak_full_test = False
run_spisak_partial_test = False
num_perms_spisak = 1000
n_shuffles = 100
surrogate_method = 'shuffle'
n_shifts = 20

standardize_confound = True
tune_C = False

lr_penalty='l1'
lr_solver='saga'
C_reg=0.2

df = pd.DataFrame(columns=['simulation_type',
                           'permuted',
                           'predictability',
                           'accuracy_full',
                           'accuracy_naive'])

for simulation_type in simulation_types:

    if simulation_type == 'linear':
        # confound noise term
        C_0 = np.random.normal(loc=0, scale=1, size=n_samples)
        # confound weight
        w_cy = 2.5
        # function with which y enters c
        def f(x):
            return x
        # feature noise term
        X_0 = np.random.normal(loc=0, scale=1, size=n_samples)
        w_xy = 0
        w_xc = 1
        # function with which y enters X
        def g(x):
            return x
        # function with which c enters X
        def h(x):
            return x

    elif simulation_type == 'conf_noise':
        C_0 = np.random.normal(loc=0, scale=1, size=n_samples)
        X_0 = np.random.normal(loc=0, scale=1, size=n_samples)

        w_cy = 3
        w_xy = 0 # all information in goes via the confound
        w_xc = 2

        def f(x):
            return x

        def g(x):
            return x

        def h(x):
            a = 1
            b = 0.5
            d = 0.8
            N = np.random.normal(loc=0, scale=1, size=x.shape[0])
            return a * x + N * (b + d * x)

    elif simulation_type == 'conf_noise_2':
        C_0 = np.random.normal(loc=0, scale=1, size=n_samples)
        X_0 = 0
        w_cy = 2
        w_xy = 0  # all information in goes via the confound
        w_xc = 2


        def f(x):
            return x


        def g(x):
            return x


        def h(x):
            N = np.random.normal(loc=0, scale=1, size=x.shape[0])
            # out = np.arctan(12 * x - 4) + np.sinh(1 * np.arcsinh(N) + x)
            out = x + np.sinh(1 * np.arcsinh(N) + 1.2 * x)
            return out

    elif simulation_type == 'nonlinear':
        C_0 = np.random.normal(loc=0, scale=1, size=n_samples)
        X_0 = np.random.normal(loc=0, scale=0.5, size=n_samples)

        w_cy = 3
        w_xy = 0  # all information in goes via the confound
        w_xc = 2

        def f(x):
            return x

        def g(x):
            return x

        def h(x):
            return np.arctan(12 * x - 4)

    elif simulation_type == 'linear_true':
        # confound noise term
        C_0 = np.random.normal(loc=0, scale=1, size=n_samples)
        X_0 = np.random.normal(loc=0, scale=1, size=n_samples)

        # confound weight
        w_cy = 1
        w_xy = 1
        w_xc = 1
        # function with which y enters c
        def f(x):
            return x
        # feature noise term

        # function with which y enters X
        def g(x):
            return x
        # function with which c enters X
        def h(x):
            return x

    elif simulation_type == 'joint_XOR':
        C_0 = np.random.normal(loc=0, scale=1, size=n_samples)
        X_0 = 0

        w_cy = 2
        w_xy = 0  # all information in goes via the confound
        w_xc = 1


        def f(x):
            # TODO could be a degraded version of Y
            return x


        def g(x):
            return np.hstack([x.reshape(-1, 1), x.reshape(-1, 1)])


        def h(x):
            samples = []
            for val in x:
                if val >= 1:
                    cov = [[1, 0.9], [0.9, 1]]

                elif val < 1:
                    cov = [[1, -0.9], [-0.9, 1]]

                sample = np.random.multivariate_normal([0, 0], cov, 1)
                samples.append(sample)
            return np.vstack(samples)

    elif simulation_type == 'joint_AND':
        C_0 = np.random.normal(loc=0, scale=0.5, size=n_samples)
        X_0 = 0
        w_cy = 2
        w_xy = 0  # all information in goes via the confound
        w_xc = 1


        def f(x):
            # TODO could be a degraded version of Y
            return x


        def g(x):
            return np.hstack([x.reshape(-1, 1), x.reshape(-1, 1)])

        def h(x):
            samples = []
            for val in x:
                if val >= 1:
                    cov = [[0.5, 0], [0, 0.5]]
                    sample = np.random.multivariate_normal([0, 0], cov, 1) + 5

                elif val < 1:
                    cov = [[0.5, 0], [0, 0.5]]
                    fir = np.random.choice(a=[0, 1], size=1)[0]
                    loc = np.array([0, 0])
                    loc[fir] = 5
                    sample = np.random.multivariate_normal([0, 0], cov, 1) + loc

                samples.append(sample)
            return np.vstack(samples)

    elif simulation_type == 'joint_XOR_true':
        C_0 = np.random.normal(loc=0, scale=1, size=n_samples)
        X_0 = 0

        w_cy = 0
        w_xy = 1
        w_xc = 1
        # TODO how to map 1D C to 2D X without changing the correlation structure?

        def f(x):
            # TODO could be a degraded version of Y
            return x

        def g(x):
            samples = []
            for val in x:
                if val >= 1:
                    cov = [[1, 0.9], [0.9, 1]]

                elif val < 1:
                    cov = [[1, -0.9], [-0.9, 1]]
                sample = np.random.multivariate_normal([0, 0], cov, 1)
                samples.append(sample)

            return np.vstack(samples)


        def h(x):
            return np.hstack([x.reshape(-1, 1), x.reshape(-1, 1)])

    elif simulation_type == 'linear_true_degraded':
        C_0 = np.random.normal(loc=0, scale=0.6, size=n_samples)
        X_0 = np.random.normal(loc=0, scale=0.6, size=n_samples)

        w_cy = 1
        w_xy = 1
        w_xc = 1

        def f(x):
            p = 0.5
            ret_x = []
            for sample in x:
                if sample == 0:
                    ret_x.append(np.random.choice([0, 1], p=[p, 1 - p]))
                elif sample == 1:
                    ret_x.append(np.random.choice([0, 1], p=[1 - p, p]))
            return np.array(ret_x)

        def g(x):
            return x

        def h(x):
            return x

    elif simulation_type == 'conf_noise_true':
        C_0 = np.random.normal(loc=0, scale=1, size=n_samples)
        X_0 = np.random.normal(loc=0, scale=1, size=n_samples)

        w_cy = 3
        w_xy = 2 # all information in goes via the confound
        w_xc = 2

        def f(x):
            return x

        def g(x):
            a = 1
            b = 0.5
            d = 0.8
            N = np.random.normal(loc=0, scale=1, size=x.shape[0])
            return a * x + N * (b + d * x)

        def h(x):
            return x

    else:
        raise ValueError

    # generatez simulation
    y = np.random.choice([0, 1], size=n_samples, p=[0.5, 0.5])
    C = C_0 + w_cy * f(y)
    X = X_0 + w_xy * g(y) + w_xc * h(C)

    # generate permutations
    y_permuted = []
    for j in range(n_permutations + 1):
        if j == 0:
            y_permuted.append(np.copy(y))
        else:
            y_permuted.append(np.random.permutation(y))

    try:
        X.shape[1]
    except IndexError:
        X = X.reshape(-1, 1)

    C = C.reshape(-1, 1)

    T = X.shape[0]
    center_point = T // 2

    index_list, X_list, C_list, y_list = [], [], [], []

    if surrogate_method == 'shift':
        for s in range(-n_shifts, n_shifts + 1):
            # print(s)
            # shif y and C, not X
            X_shift = X[n_shifts:T - n_shifts, :]
            C_shift = C[n_shifts + s:T - n_shifts + s, :]
            y_shift = y[n_shifts + s:T - n_shifts + s, :]

            X_list.append(X_shift)
            C_list.append(C_shift)
            y_list.append(y_shift)
            index_list.append(s)

    elif surrogate_method == 'shuffle' :
        # TODO shuffle C and y together
        for s in range(n_shuffles + 1):
            # print(s)
            random_index = np.random.permutation(np.arange(X.shape[0]))
            if s == 0:
                C_shuffled = np.copy(C)
                y_shuffled = np.copy(y)
            else:
                C_shuffled = C[random_index, :]
                y_shuffled = y[random_index]
            X_list.append(X)
            C_list.append(C_shuffled)
            y_list.append(y_shuffled)
            index_list.append(s)

    surrogate_scores = []



    for s, X_surr, C_surr, y_surr in zip(index_list, X_list, C_list,
                                         y_list):

        # --- predictability with behavioral predictors ---
        kfold = StratifiedKFold(n_splits=n_kfold_splits, shuffle=True,
                                random_state=92)
        iterable = kfold.split(X_surr, y_surr)


        kfold_accuracy_full, kfold_accuracy_naive = [], []
        llratios = []

        for fold, (training_ind, testing_ind) in enumerate(iterable):

            X_train = X_surr[training_ind, :]
            X_test = X_surr[testing_ind, :]
            y_train = y_surr[training_ind]
            y_test = y_surr[testing_ind]
            C_train = C_surr[training_ind, :]
            C_test = C_surr[testing_ind, :]

            ss = StandardScaler()
            X_train = ss.fit_transform(X_train)
            X_test = ss.transform(X_test)

            if standardize_confound:
                ss = StandardScaler()
                C_train = ss.fit_transform(C_train)
                C_test = ss.transform(C_test)

            # C_train = C_train * SCALE_C
            # C_test = C_test * SCALE_C
            # X_train = X_train * 0.1
            # X_test = X_test * 0.1
            features_full_train = np.hstack([X_train, C_train])
            features_full_test = np.hstack([X_test, C_test])

            features_naive_train = C_train
            features_naive_test = C_test

            if model == 'linear':
                if tune_C:
                    full_model = LogisticRegressionCV(penalty=lr_penalty,
                                                      solver=lr_solver,
                                                      Cs=Cs,
                                                      max_iter=max_iter,
                                                      cv=tune_C_nfolds)
                    # naive_model = LogisticRegressionCV(penalty=lr_penalty,
                    #                                    solver=lr_solver,
                    #                                    Cs=Cs,
                    #                                    max_iter=max_iter,
                    #                                    cv=tune_C_nfolds)
                    # TODO naive model not regularized!
                    naive_model = LogisticRegression(penalty=None,
                                                     solver=lr_solver)

                else:
                    full_model = LogisticRegression(penalty=lr_penalty,
                                                    solver=lr_solver,
                                                    C=C_reg)
                    # naive_model = LogisticRegression(penalty=lr_penalty,
                    #                                  solver=lr_solver,
                    #                                  C=C_reg)
                    # TODO naive model not regularized!
                    naive_model = LogisticRegression(penalty=None,
                                                     solver=lr_solver)

            elif model == 'nonlinear':
                # full_model = QuadraticDiscriminantAnalysis()
                # naive_model = QuadraticDiscriminantAnalysis()

                full_model = SVC(probability=True)
                naive_model = SVC(probability=True)

                # full_model = RandomForestClassifier(n_estimators=200)
                # naive_model = RandomForestClassifier(n_estimators=200)


            full_model.fit(features_full_train, y_train)
            naive_model.fit(features_naive_train, y_train)

            y_pred_proba_full = full_model.predict_proba(features_full_test)
            y_pred_proba_naive = naive_model.predict_proba(features_naive_test)

            fold_accuracy_full = balanced_accuracy_score(y_test, full_model.predict(features_full_test))
            fold_accuracy_naive = balanced_accuracy_score(y_test,  naive_model.predict(features_naive_test))

            kfold_accuracy_full.append(fold_accuracy_full)
            kfold_accuracy_naive.append(fold_accuracy_naive)

            log_likelihood_full = log2_likelihood(y_test, y_pred_proba_full)
            log_likelihood_naive = log2_likelihood(y_test, y_pred_proba_naive)

            llratio = log_likelihood_full - log_likelihood_naive
            llratios.append(llratio)

        accuracy_full = np.mean(kfold_accuracy_full)
        accuracy_naive = np.mean(kfold_accuracy_naive)
        predictability = np.mean(llratios)

        if s == 0:
            obs_score = predictability
            permuted = False
        else:
            surrogate_scores.append(predictability)
            permuted = True



        df.loc[df.shape[0], :] = [simulation_type,
                                  permuted,
                                  predictability,
                                  accuracy_full,
                                  accuracy_naive]


    if False:
        # TODO compute significance
        wool_q1, wool_q2 = pd.Series(surrogate_scores).quantile(q=[0.025, 0.975])
        surrogate_scores = np.array(surrogate_scores)
        n_higher = (surrogate_scores > obs_score).sum()

        if surrogate_method == 'shift':
            wool_significant = n_higher == 0
        elif surrogate_method == 'shuffle':
            p_val = n_higher / (len(surrogate_scores)+1)
            wool_significant = p_val < 0.05

        mean_score = obs_score


df['predictability'] = pd.to_numeric(df['predictability'])

f, ax = plt.subplots(1, 1, figsize=[4, 4])

sns.violinplot(data=df[df['permuted']==True],
               ax=ax,
               x='simulation_type',
               y='predictability',
               color=sns.xkcd_rgb['light grey'],
               inner='quartile', linewidth=1)

for i, simulation_type in enumerate(df['simulation_type'].unique()):
    obs = df[(df['permuted'] == False) & (df['simulation_type'] == simulation_type)]['predictability'].iloc[0]
    ax.scatter(i, obs, c='red', marker='*', s=30)
ax.set_xticklabels(ax.get_xticklabels(), rotation=90)
ax.set_xlabel('')
#ax.set_ylim([-0.4, 0.4])
sns.despine()
plt.tight_layout()