import os
import pickle
from sklearn.model_selection import StratifiedKFold, KFold
from sklearn.metrics import balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
from constants import *
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from mlconfound.stats import full_confound_test, partial_confound_test
from sklearn.model_selection import cross_val_predict
from confound_prediction.deconfounding import confound_isolating_cv
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from sklearn.linear_model import LinearRegression
from utils import load_decoding_data
from sklearn.preprocessing import minmax_scale
from utils import *
from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from sklearn.metrics import log_loss
from sklearn.metrics import accuracy_score

"""
decoding of the modid dataset using the wool method.

This script is not necessary: wool method can be run
from ephys_decode_single_sess.py, but it can be useful to have separate for
debugging or development.
"""



settings_name = 'aug22'

data_settings_name = 'may22'#'true+conf_effect'

experiments = ['audiofreq_in_big_change',
               'visualori_in_big_change']# 'visualori_in_big_change']#['audiofreq_in_big_change']#['visualori', 'audiofreq']

n_splits = 3

methods = ['wool_deconf']

time_bin = 0

min_trials_per_segment = 20

lr_penalty = 'l1' #'l2'
lr_solver = 'saga'#'lbfgs'

tune_C = True
Cs = 10
tune_C_nfolds = 3
max_iter = 5000
C_reg = 0.3

standardize_confound = True
n_shifts = 20
n_shuffles = 200
surrogate_methods = ['shuffle', 'shift']


plot_format = 'png'
plots_folder = os.path.join(DATA_PATH, 'plots', 'modid',
                            'DATA_SETTINGS_{}'.format(data_settings_name),
                            'DECODE_SETTINGS_{}'.format(settings_name))
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)


for experiment in experiments:

    for surrogate_method in surrogate_methods:

        dfs = {}

        for method in methods:

            print(experiment, surrogate_method, method)
            data = load_decoding_data(settings_name=data_settings_name,
                                      experiment_name=experiment)
            sessions = data['pars']['sessions']

            df = pd.DataFrame(columns=['animal_id', 'session_id', 'dataset', 'area_spikes',
                                       'experiment', 'features',
                                       'time', 'score', 'n_neurons',
                                       'full_p', 'full_sig', 'partial_p',
                                       'partial_sig'])

            df_null = pd.DataFrame(columns=['animal_id', 'session_id', 'dataset', 'area_spikes',
                                        'experiment', 'features',
                                       'time', 'score', 'shift', 'n_neurons', ])


            for i, row in sessions.iterrows() :

                animal_id = row['animal_id']
                session_id = row['session_id']
                area_spikes = data['data'][session_id]['area_spikes']
                dataset = data['data'][session_id]['dataset']
                target_name =  data['data'][session_id]['target_name']
                tf = data['data'][session_id]['trial_df']
                n_trials = tf.shape[0]

                if n_trials < (2*n_shifts+min_trials_per_segment) and surrogate_method == 'shift':
                    print('Skipping session {}, not enough trials'.format(session_id))
                    continue
                # get time parameters
                time_bin_times = data['data'][session_id]['time_bin_times']
                time_bins = data['data'][session_id]['time_bins']
                n_time_bins_per_trial = len(time_bin_times)

                # select one time bin
                time = time_bin_times[time_bin]
                t0, t1 = time_bins[time_bin]

                # --- Decode from spikes ---
                for time_bin in range(n_time_bins_per_trial):

                    time = time_bin_times[time_bin]
                    X_full = data['data'][session_id]['binned_spikes'][time_bin]
                    C_full = data['data'][session_id]['binned_movement'][time_bin]
                    y_full = data['data'][session_id]['target'].__array__()

                    T = X_full.shape[0]
                    center_point = T // 2

                    index_list, X_list, C_list, y_list = [], [], [], []

                    if surrogate_method == 'shift':
                        for s in range(-n_shifts, n_shifts + 1):
                            #print(s)
                            X_shift = X_full[n_shifts + s :T - n_shifts + s, :]
                            C_shift = C_full[n_shifts :T - n_shifts, :]
                            y_shift = y_full[n_shifts :T - n_shifts]

                            X_list.append(X_shift)
                            C_list.append(C_shift)
                            y_list.append(y_shift)
                            index_list.append(s)

                    elif surrogate_method == 'shuffle':
                        for s in range(n_shuffles + 1) :
                            # print(s)
                            if s == 0 :
                                X_shuffle = X_full
                            else :
                                X_shuffle = np.random.permutation(X_full)

                            X_list.append(X_shuffle)
                            C_list.append(C_full)
                            y_list.append(y_full)
                            index_list.append(s)

                    for s, X, C, y in zip(index_list, X_list, C_list, y_list) :

                        # --- predictability with behavioral predictors ---
                        kfold = StratifiedKFold(n_splits=n_splits, shuffle=True,
                                                random_state=92)

                        # kfold = KFold(n_splits=n_splits, shuffle=True,
                        #                         random_state=92)

                        iterable = kfold.split(X, y)

                        # TODO qua ci sono troppe cose, sfoltire
                        kfold_accuracy_full, kfold_accuracy_naive = [], []
                        y_test_all, y_pred_proba_full_list, y_pred_proba_naive_list, c_all = [], [], [], []
                        log_likelihood_full_list = []
                        log_likelihood_naive_list = []
                        llratios = []

                        for fold, (training_ind, testing_ind) in enumerate(
                                iterable) :

                            # decoder = SGDClassifier(random_state=92)
                            # decoder = RandomForestClassifier(n_estimators=300, random_state=92)

                            X_train = X[training_ind, :]
                            X_test = X[testing_ind, :]
                            y_train = y[training_ind]
                            y_test = y[testing_ind]
                            C_train = C[training_ind, :]
                            C_test = C[testing_ind, :]

                            ss = StandardScaler()
                            X_train = ss.fit_transform(X_train)
                            X_test = ss.transform(X_test)

                            if standardize_confound:
                                ss = StandardScaler()
                                C_train = ss.fit_transform(C_train)
                                C_test = ss.transform(C_test)

                            if method == 'wool_deconf' :
                                # C_train = C_train * SCALE_C
                                # C_test = C_test * SCALE_C
                                # X_train = X_train * 0.1
                                # X_test = X_test * 0.1
                                features_full_train = np.hstack([X_train, C_train])
                                features_full_test = np.hstack([X_test, C_test])

                                features_naive_train = C_train
                                features_naive_test = C_test

                            elif method == 'wool_nodeconf' :
                                features_full_train = X_train
                                features_full_test = X_test

                                features_naive_train = np.ones_like(C_train)
                                features_naive_test = np.ones_like(C_test)

                            if tune_C :
                                full_model = LogisticRegressionCV(
                                    penalty=lr_penalty,
                                    solver=lr_solver,
                                    Cs=Cs,
                                    max_iter=max_iter,
                                    cv=tune_C_nfolds)
                                # naive_model = LogisticRegressionCV(penalty=lr_penalty,
                                #                                    solver=lr_solver,
                                #                                    Cs=Cs,
                                #                                    max_iter=max_iter,
                                #                                    cv=tune_C_nfolds)
                                # TODO naive model not regularized!
                                naive_model = LogisticRegression(penalty=None,
                                                                 solver=lr_solver)


                            else:
                                full_model = LogisticRegression(
                                                    penalty=lr_penalty,
                                                    solver=lr_solver,
                                                    C=C_reg)
                                # naive_model = LogisticRegression(penalty=lr_penalty,
                                #                                  solver=lr_solver,
                                #                                  C=C_reg)
                                # TODO naive model not regularized!
                                naive_model = LogisticRegression(penalty=None,
                                                                 solver=lr_solver)

                            full_model.fit(features_full_train, y_train)
                            y_pred_proba_full = full_model.predict_proba(features_full_test)

                            naive_model.fit(features_naive_train, y_train)
                            y_pred_proba_naive = naive_model.predict_proba(features_naive_test)

                            y_pred_full = full_model.predict(features_full_test)
                            y_pred_naive = naive_model.predict(features_naive_test)
                            fold_accuracy_full = balanced_accuracy_score(y_test, y_pred_full)
                            fold_accuracy_naive = balanced_accuracy_score(y_test, y_pred_naive)

                            y_test_all.append(y_test)
                            y_pred_proba_full_list.append(y_pred_proba_full)
                            y_pred_proba_naive_list.append(y_pred_proba_naive)
                            c_all.append(C_test[:, 0])

                            kfold_accuracy_full.append(fold_accuracy_full)
                            kfold_accuracy_naive.append(fold_accuracy_naive)

                            log_likelihood_full = log2_likelihood(y_test, y_pred_proba_full)
                            log_likelihood_naive = log2_likelihood(y_test, y_pred_proba_naive)
                            log_likelihood_full_list.append(log_likelihood_full)
                            log_likelihood_naive_list.append(log_likelihood_naive)
                            llratio = log_likelihood_full - log_likelihood_naive
                            llratios.append(llratio)

                        target = np.hstack(y_test_all)
                        predictions_full = np.vstack(y_pred_proba_full_list)
                        predictions_naive = np.vstack(y_pred_proba_naive_list)
                        confound = np.hstack(c_all)

                        log_likelihood_full = np.mean(log_likelihood_full_list)
                        log_likelihood_naive = np.mean(log_likelihood_naive)

                        accuracy_full = np.mean(kfold_accuracy_full)
                        accuracy_naive = np.mean(kfold_accuracy_naive)

                        predictability = np.mean(llratios)
                        mean_score = predictability

                        if s == 0:
                            full_p = np.nan
                            full_sig = False
                            partial_p = np.nan
                            partial_sig = False

                            row = [animal_id, session_id, dataset, area_spikes,
                                   experiment, 'spikes',
                                   time, mean_score, X.shape[1], full_p,
                                   full_sig, partial_p, partial_sig]

                            df.loc[df.shape[0], :] = row

                        else:
                            row = [animal_id, session_id, dataset, area_spikes,
                                   experiment, 'spikes_deconf',
                                   time, mean_score, s, X.shape[1]]

                            df_null.loc[df_null.shape[0], :] = row


            df['time'] = [t.item() for t in df['time']]
            df_null['time'] = [t.item() for t in df_null['time']]

            # we make sure we have only one time point so we can compute
            # the significance per session
            # (we would have to do it per time and per session otherwise)
            assert df['time'].unique().__len__() == 1
            assert df_null['time'].unique().__len__() == 1

            df['q1'] = np.nan
            df['q2'] = np.nan
            df['significant'] = np.nan


            for session_id in df_null['session_id'].unique():

                surrogate_scores = df_null[df_null['session_id'] == session_id]['score']
                q1, q2 = surrogate_scores.quantile(q=[0.025, 0.975])
                df.loc[df['session_id'] == session_id, 'q1'] = q1
                df.loc[df['session_id'] == session_id, 'q2'] = q2
                obs_score = df.loc[df['session_id'] == session_id, 'score']
                n_higher = (surrogate_scores > obs_score.array[0]).sum()

                if surrogate_method == 'shift':
                    df.loc[df['session_id'] == session_id, 'significant'] = n_higher == 0
                elif surrogate_method == 'shuffle':
                    # TODO this is an approximate test right?
                    p_val = n_higher / len(surrogate_scores)
                    df.loc[df['session_id'] == session_id, 'significant'] = p_val < 0.05

                sig_wool = n_higher == 0 # conservative test
                p_val = n_higher / n_shifts # equivalent to m<=a(N+1), not to m<=a(2N+1)
                sig_pval = p_val < 0.05
                assert sig_wool == sig_pval

            dfs[method] = df


            #MAX = df['score'].max()+0.06

        # --- PLOT SCATTER -------------------------------------------------------------

        dz = pd.merge(dfs['wool_deconf'],
                      dfs['wool_nodeconf'][['session_id', 'score', 'experiment']],
                      on=['session_id', 'experiment'])

        dz = dz.rename({'score_x' : 'score_deconf', 'score_y' : 'score_nodeconf'},
                       axis=1)

        dz['size'] = minmax_scale(dz['n_neurons'], feature_range=(20, 150))
        all_scores = np.hstack(
            [dz['score_deconf'].__array__(), dz['score_nodeconf'].__array__()])
        scmin = all_scores.min()
        scmax = all_scores.max()

        #axis_min, axis_max = 0.0, 1.01
        #assert axis_min <= scmin
        significance_hue = True

        f, ax = plt.subplots(1, 1, figsize=[3, 3])

        for dataset in dataset_palette.keys() :
            dxx = dz[(dz['experiment'] == experiment) & (dz['dataset'] == dataset)]

            if significance_hue:
                dx1 = dxx[dxx['significant'].astype(bool)]
                dx2 = dxx[~dxx['partial_sig'].astype(bool)]

                ax.scatter(dx1['score_deconf'], dx1['score_nodeconf'], s=dx1['size'],
                           c=dataset_palette[dataset], edgecolor='w', linewidth=0)

                ax.scatter(dx2['score_deconf'], dx2['score_nodeconf'], s=dx2['size'],
                           c=dataset_palette[dataset], edgecolor='w', linewidth=0,
                           alpha=0.5)
            else :
                ax.scatter(dxx['score_deconf'], dxx['score_nodeconf'],
                           s=dxx['size'],
                           c=dataset_palette[dataset], edgecolor='w', linewidth=1)

        if experiment == 'audiofreq_in_big_change' :
            ax.set_xlabel('Predictability of frequency\n(deconfounded)')
            ax.set_ylabel('Predictability of frequency')

        elif experiment == 'visualori_in_big_change' :
            ax.set_xlabel('Predictability of orientation\n(deconfounded)')
            ax.set_ylabel('Predictability of orientation')

        ax.plot([-1, 2.3], [-1, 2.3], c='grey', ls=':')
        # ax.set_xlim([-1, 3])
        # ax.set_ylim([-1, 3])
        ax.axhline(0, c='grey', ls=':')
        ax.axvline(0, c='grey', ls=':')
        # ax.set_xticks([0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0])
        # ax.set_yticks([0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0])

        # ax.get_legend().remove()
        sns.despine()
        plt.tight_layout()

        plot_name = 'decoding_ephys_{}_{}_{}_{}.{}'.format(method, surrogate_method,experiment,
                                                        settings_name,
                                                        plot_format)
        f.savefig(os.path.join(plots_folder, plot_name), dpi=400,
                  bbox_inches="tight")

