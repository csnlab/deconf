import os
import pickle
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
from constants import *
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from mlconfound.stats import full_confound_test, partial_confound_test
from sklearn.model_selection import cross_val_predict
from confound_prediction.deconfounding import confound_isolating_cv
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from sklearn.linear_model import LinearRegression
from utils import load_decoding_data
from sklearn.preprocessing import minmax_scale
from utils import *
from decoding_utils import decode

"""
Decoding visual orientation and auditory frequency on modid data, comparing 
decoding from video PCAs and from neural data. 

several options for deconfounding:
'raw': neural data is not touched
'linear_confound_regression': linear confound regression on the neural data:
'nonlinear_confound_regression': non-linear confound regression on the neural data:
'confound_isolating_cv': applies confound isolating cross-validation

'wool_deconf' and 'wool_nodeconf' currently not implemented 
(needs some small adaptations), for now use 
modide_decode_predictability.py
"""


settings_name = 'aug19'

data_settings_name = 'may22'#'true+conf_effect'

experiments = ['audiofreq_in_big_change',
               'visualori_in_big_change']# 'visualori_in_big_change']#['audiofreq_in_big_change']#['visualori', 'audiofreq']

methods = ['raw', 'linear_confound_regression', 'distribution_match',
           'confound_isolating_cv'] #None

methods = ['nonlinear_confound_regression']

run_spisak_tests = False
num_perms_spisak = 2500

distribution_match_binsize = None

n_shifts = 10
n_shuffles = 200
surrogate_method = 'shift'

tune_C = True
Cs = 10
tune_C_nfolds = 3
max_iter = 5000
C_reg = 0.3

min_trials_per_segment = 20

# decode data from a single time bin, not over time
time_bin_indx = 0

n_kfold_splits = 3

plot_format = 'png'
plots_folder = os.path.join(DATA_PATH, 'plots', 'modid',
                            'DATA_SETTINGS_{}'.format(data_settings_name),
                            'DECODE_SETTINGS_{}'.format(settings_name))
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)


for method in methods:

    for experiment in experiments:

        data = load_decoding_data(settings_name=data_settings_name,
                                  experiment_name=experiment)

        sessions = data['pars']['sessions']

        df = pd.DataFrame(columns=['animal_id', 'session_id', 'dataset',
                                   'area_spikes', 'n_neurons',
                                   'experiment', 'features',
                                   'time', 'time_bin', 't0', 't1',
                                   'score',
                                   'full_p', 'full_sig', 'partial_p', 'partial_sig'])


        for i, row in sessions.iterrows() :
            # get metadata
            animal_id = row['animal_id']
            session_id = row['session_id']
            area_spikes = data['data'][session_id]['area_spikes']
            dataset = data['data'][session_id]['dataset']
            target_name =  data['data'][session_id]['target_name']
            n_trials = data['data'][session_id]['trial_df'].shape[0]

            # get time parameters
            time_bin_times = data['data'][session_id]['time_bin_times']
            time_bins = data['data'][session_id]['time_bins']
            n_time_bins_per_trial = len(time_bin_times)

            # select one time bin
            time = time_bin_times[time_bin_indx]
            t0, t1 = time_bins[time_bin_indx]

            # --- decode from confound ---

            # get decoding data
            X = data['data'][session_id]['binned_movement'][time_bin_indx]
            y = data['data'][session_id]['target']

            kfold = StratifiedKFold(n_splits=n_kfold_splits, shuffle=True,
                                    random_state=92)

            kfold_scores = []

            for fold, (training_ind, testing_ind) in enumerate(kfold.split(X, y)) :

                decoder = SGDClassifier(loss='log_loss', random_state=92)
                #decoder = RandomForestClassifier(n_estimators=300, random_state=92)

                X_train = X[training_ind, :]
                X_test = X[testing_ind, :]
                y_train = y[training_ind]
                y_test = y[testing_ind]

                ss = StandardScaler()
                X_train = ss.fit_transform(X_train)
                X_test = ss.transform(X_test)

                decoder.fit(X_train, y_train)
                y_pred = decoder.predict(X_test)

                score = balanced_accuracy_score(y_test, y_pred)
                kfold_scores.append(score)

            mean_score = np.mean(kfold_scores)
            full_p = np.nan
            full_sig = False
            partial_p = np.nan
            partial_sig = False

            row = [animal_id, session_id, dataset, area_spikes, X.shape[1],
                   experiment, 'confound', time, time_bin_indx, t0, t1, mean_score,
                   full_p, full_sig, partial_p, partial_sig]

            df.loc[df.shape[0], :] = row


            # --- Decode from spikes ---

            # get decoding data
            X = data['data'][session_id]['binned_spikes'][time_bin_indx]
            C = data['data'][session_id]['binned_movement'][time_bin_indx]
            #C = np.sum(C, axis=1)
            y = data['data'][session_id]['target']

            if method == 'wool_deconf':
                if n_trials < (2 * n_shifts + min_trials_per_segment) and surrogate_method == 'shift' :
                    print(
                        'Skipping session {}, not enough trials'.format(session_id))
                    continue

            try:
                dec_out = decode(X=X, y=y, C=C,
                                 deconf_method=method,
                                 posthoc_counterbalancing_binsize=distribution_match_binsize,
                                 n_kfold_splits=n_kfold_splits,
                                 run_spisak_tests=run_spisak_tests,
                                 num_perms_spisak=num_perms_spisak,
                                 surrogate_method=surrogate_method,
                                 n_shifts=n_shifts,
                                 n_shuffles=n_shuffles,
                                 predictability_tune_regularization=tune_C,
                                 LR_predictability_C_list=Cs,
                                 tune_C_predictability_nfolds=tune_C_nfolds,
                                 LR_predictability_max_iter=max_iter)

            except np.linalg.LinAlgError:
                continue

            mean_score = dec_out['mean_score']
            full_p = dec_out['full_p']
            full_sig = dec_out['full_sig']
            partial_p = dec_out['partial_p']
            partial_sig = dec_out['partial_sig']
            wool_significant = dec_out['wool_significant']

            # # determine splits using either a normal stratified cross-validation
            # # or with the confound isolating cross-validation method
            # if method == 'confound_isolating_cv':
            #
            #     C = np.sum(C, axis=1).reshape(-1, 1)
            #
            #     try:
            #         _, _, _, _, ids_test, ids_train = confound_isolating_cv(X, y, C[:, 0],
            #                                                                 random_seed=92,
            #                                                                 min_sample_size=None,
            #                                                                 cv_folds=n_cv_splits,
            #                                                                 n_remove=None)
            #
            #         iterable = zip(ids_train, ids_test)
            #     except np.linalg.LinAlgError:
            #         continue
            #
            # else:
            #
            #     kfold = StratifiedKFold(n_splits=n_cv_splits, shuffle=True,
            #                             random_state=92)
            #     iterable = kfold.split(X, y)
            #
            #
            # kfold_scores = []
            # y_test_all, y_pred_all, c_all = [], [], []
            #
            # for fold, (training_ind, testing_ind) in enumerate(iterable) :
            #
            #     decoder = SGDClassifier(random_state=92)
            #     #decoder = RandomForestClassifier(n_estimators=300, random_state=92)
            #     X_train = X[training_ind, :]
            #     X_test = X[testing_ind, :]
            #     y_train = y[training_ind]
            #     y_test = y[testing_ind]
            #     C_train = C[training_ind, :]
            #     C_test = C[testing_ind, :]
            #
            #     # TODO scale before or after conf. regression?
            #     ss = StandardScaler()
            #     X_train = ss.fit_transform(X_train)
            #     X_test = ss.transform(X_test)
            #
            #     ss = StandardScaler()
            #     C_train = ss.fit_transform(C_train)
            #     C_test = ss.transform(C_test)
            #
            #     if method == 'linear_confound_regression' :
            #         reg = LinearRegression().fit(C_train, X_train)
            #         X_train = X_train - reg.predict(C_train)
            #         X_test = X_test - reg.predict(C_test)
            #
            #     elif method == 'nonlinear_confound_regression':
            #         reg = RandomForestRegressor(n_estimators=500,
            #                                     random_state=92).fit(C_train, X_train)
            #         X_train = X_train - reg.predict(C_train)
            #         X_test = X_test - reg.predict(C_test)
            #
            #     decoder.fit(X_train, y_train)
            #     y_pred = decoder.predict(X_test)
            #     score = balanced_accuracy_score(y_test, y_pred)
            #     kfold_scores.append(score)
            #     y_test_all.append(y_test)
            #     y_pred_all.append(y_pred)
            #     c_all.append(C_test[:, 0])
            #
            # target = np.hstack(y_test_all)
            # predictions = np.hstack(y_pred_all)
            # confound = np.hstack(c_all)
            # mean_score = np.mean(kfold_scores)
            #
            # if run_spisak_tests:
            #     try:
            #         full_test = full_confound_test(target, predictions, confound,
            #                                        num_perms=num_perms_spisak,
            #                                        cat_y=True,
            #                                        cat_yhat=True,
            #                                        cat_c=False,
            #                                        mcmc_steps=50,
            #                                        cond_dist_method='gam',
            #                                        return_null_dist=False,
            #                                        random_state=92, progress=True,
            #                                        n_jobs=-1)
            #         full_p = full_test.p
            #         full_sig = full_p < 0.05
            #     except ValueError:
            #         full_p = np.nan
            #         full_sig = False
            #
            #     partial_test = partial_confound_test(target, predictions, confound,
            #                                    num_perms=num_perms_spisak,
            #                                    cat_y=True,
            #                                    cat_yhat=True,
            #                                    cat_c=False,
            #                                    mcmc_steps=50,
            #                                    cond_dist_method='gam',
            #                                    return_null_dist=False,
            #                                    random_state=92, progress=True,
            #                                    n_jobs=-1)
            #     partial_p = partial_test.p
            #     partial_sig = partial_p < 0.05
            #
            # else:
            #     full_p = np.nan
            #     full_sig = False
            #     partial_p = np.nan
            #     partial_sig = False

            row = [animal_id, session_id, dataset, area_spikes, X.shape[1],
                   experiment, 'spikes', time, time_bin_indx, t0, t1, mean_score,
                   full_p, full_sig, partial_p, partial_sig]

            df.loc[df.shape[0], :] = row


        df['time'] = [t.item() for t in df['time']]

        features_palette = {'spikes' : sns.xkcd_rgb['purple'],
                            'confound' : sns.xkcd_rgb['grey']}


        # --- PLOT SCATTER -------------------------------------------------------------

        df = df.drop(['time_bin', 't0', 't1'], axis='columns')

        dz = pd.merge(df[df['features'] == 'spikes'],
                      df[df['features'] == 'confound'][['session_id', 'score', 'experiment']],
                      on=['session_id', 'experiment'])

        dz = dz.rename({'score_x' : 'score_spikes', 'score_y' : 'score_confound'}, axis=1)

        dz['size'] = minmax_scale(dz['n_neurons'], feature_range=(20, 150))
        all_scores = np.hstack([dz['score_spikes'].__array__(), dz['score_confound'].__array__()])
        scmin = all_scores.min()
        scmax = all_scores.max()

        f, ax = plt.subplots(1, 1, figsize=[3, 3])


        if method == 'wool_deconf':
            axis_min, axis_max = scmin-0.1, scmax+0.1
            ax.plot([-axis_min, axis_max], [-axis_min, axis_max], c='grey', ls=':')
        else:
            axis_min, axis_max = 0.0, 1.01
            ax.plot([0, 2], [0, 2], c='grey', ls=':')

        assert axis_min <= scmin


        for dataset in dataset_palette.keys():
            dxx = dz[(dz['experiment'] == experiment) & (dz['dataset'] == dataset)]

            if run_spisak_tests:
                dx1 = dxx[dxx['partial_sig'].astype(bool)]
                dx2 = dxx[~dxx['partial_sig'].astype(bool)]

                ax.scatter(dx1['score_spikes'], dx1['score_confound'], s=dx1['size'],
                           c=dataset_palette[dataset], edgecolor='w', linewidth=0)

                ax.scatter(dx2['score_spikes'], dx2['score_confound'], s=dx2['size'],
                           c=dataset_palette[dataset], edgecolor='w', linewidth=0, alpha=0.5)
            else:
                ax.scatter(dxx['score_spikes'], dxx['score_confound'],
                           s=dxx['size'],
                           c=dataset_palette[dataset], edgecolor='w', linewidth=1)

        if experiment == 'audiofreq_in_big_change':
            ax.set_xlabel('Accuracy of frequency decoding\nfrom V1 spikes')
            ax.set_ylabel('Accuracy of frequency decoding\nfrom video data')

        elif experiment == 'visualori_in_big_change':
            ax.set_xlabel('Accuracy of orientation decoding\nfrom V1 spikes')
            ax.set_ylabel('Accuracy of orientation decoding\nfrom video data')

        ax.set_xlim([axis_min, axis_max])
        ax.set_ylim([axis_min, axis_max])
        ax.axhline(0.5, c='grey', ls=':')
        ax.axvline(0.5, c='grey', ls=':')
        ticks = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
        labels = [0, '', 0.2, '', 0.4, '', 0.6, '', 0.8, '', 1.0]

        ax.set_xticks(ticks)
        ax.set_yticks(ticks)
        ax.set_xticklabels(labels, fontsize=8)
        ax.set_yticklabels(labels, fontsize=8)
        ax.set_title(method)

        #ax.get_legend().remove()
        sns.despine()
        plt.tight_layout()

        plot_name = 'decoding_ephys_{}_{}_{}.{}'.format(method, experiment, settings_name,
                                                  plot_format)
        f.savefig(os.path.join(plots_folder, plot_name), dpi=400,
                  bbox_inches="tight")

