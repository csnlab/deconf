import os
import pickle
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
from constants import *
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from mlconfound.stats import full_confound_test, partial_confound_test
from sklearn.model_selection import cross_val_predict
from confound_prediction.deconfounding import confound_isolating_cv
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from sklearn.linear_model import LinearRegression
from utils import load_decoding_data
from sklearn.preprocessing import minmax_scale
from utils import *
from decoding_utils import decode

"""
Decoding visual orientation and auditory frequency on modid data, comparing 
decoding from video PCAs and from neural data. 

several options for deconfounding:
'raw': neural data is not touched
'linear_confound_regression': linear confound regression on the neural data:
'nonlinear_confound_regression': non-linear confound regression on the neural data:
'confound_isolating_cv': applies confound isolating cross-validation

'wool_deconf' and 'wool_nodeconf' currently not implemented 
(needs some small adaptations), for now use 
modide_decode_predictability.py
"""


settings_name = 'aug19'

data_settings_name = 'aug22'#'true+conf_effect'

experiments = ['audiofreq_in_big_change',
               'visualori_in_big_change']# 'visualori_in_big_change']#['audiofreq_in_big_change']#['visualori', 'audiofreq']

methods = ['raw', 'linear_confound_regression', 'distribution_match',
           'confound_isolating_cv'] #None

methods = ['raw', 'linear_confound_regression', 'nonlinear_confound_regression',
           'distribution_match']


run_spisak_tests = False
num_perms_spisak = 2500


distribution_match_binsize = None

n_shifts = 10
n_shuffles = 200
surrogate_method = 'shift'

min_trials_per_segment = 30

# decode data from a single time bin, not over time
time_bin_indx = 0

n_kfold_splits = 5

plot_format = 'png'
plots_folder = os.path.join(DATA_PATH, 'plots', 'modid',
                            'DATA_SETTINGS_{}'.format(data_settings_name),
                            'DECODE_SETTINGS_{}'.format(settings_name))
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)


session_id = '2020-01-16_11-18-35'

for experiment in experiments:

    data = load_decoding_data(settings_name=data_settings_name,
                              experiment_name=experiment)

    sessions = data['pars']['sessions']

    df = pd.DataFrame(columns=['animal_id', 'session_id', 'dataset',
                               'area_spikes', 'n_neurons',
                               'experiment', 'features', 'method',
                               'time', 'time_bin', 't0', 't1',
                               'score',
                               'full_p', 'full_sig', 'partial_p',
                               'partial_sig'])

    row = sessions[sessions['session_id'] == session_id].iloc[0, :]

    # for i, row in sessions.iterrows() :
    # get metadata
    animal_id = row['animal_id']
    session_id = row['session_id']
    area_spikes = data['data'][session_id]['area_spikes']
    dataset = data['data'][session_id]['dataset']
    target_name = data['data'][session_id]['target_name']
    n_trials = data['data'][session_id]['trial_df'].shape[0]

    # get time parameters
    time_bin_times = data['data'][session_id]['time_bin_times']
    time_bins = data['data'][session_id]['time_bins']
    n_time_bins_per_trial = len(time_bin_times)

    # --- decode from confound ---

    for time_bin_indx in np.arange(n_time_bins_per_trial) :

        time = time_bin_times[time_bin_indx]
        t0, t1 = time_bins[time_bin_indx]


        # get decoding data
        X = data['data'][session_id]['binned_movement'][time_bin_indx]
        y = data['data'][session_id]['target']

        kfold = StratifiedKFold(n_splits=n_kfold_splits, shuffle=True,
                                random_state=92)

        kfold_scores = []

        for fold, (training_ind, testing_ind) in enumerate(kfold.split(X, y)) :
            decoder = SGDClassifier(loss='log_loss', random_state=92)
            # decoder = RandomForestClassifier(n_estimators=300, random_state=92)

            X_train = X[training_ind, :]
            X_test = X[testing_ind, :]
            y_train = y[training_ind]
            y_test = y[testing_ind]

            ss = StandardScaler()
            X_train = ss.fit_transform(X_train)
            X_test = ss.transform(X_test)

            decoder.fit(X_train, y_train)
            y_pred = decoder.predict(X_test)

            score = balanced_accuracy_score(y_test, y_pred)
            kfold_scores.append(score)

        mean_score = np.mean(kfold_scores)
        full_p = np.nan
        full_sig = False
        partial_p = np.nan
        partial_sig = False

        row = [animal_id, session_id, dataset, area_spikes, X.shape[1],
               experiment, 'confound', 'confound', time, time_bin_indx, t0, t1, mean_score,
               full_p, full_sig, partial_p, partial_sig]

        df.loc[df.shape[0], :] = row


    # --- Decode from spikes ---

    for method in methods:

        for time_bin_indx in np.arange(n_time_bins_per_trial) :
            time = time_bin_times[time_bin_indx]
            t0, t1 = time_bins[time_bin_indx]

            # get decoding data
            X = data['data'][session_id]['binned_spikes'][time_bin_indx]
            C = data['data'][session_id]['binned_movement'][time_bin_indx]
            #C = np.sum(C, axis=1)
            y = data['data'][session_id]['target']

            if method == 'wool_deconf':
                if n_trials < (2 * n_shifts + min_trials_per_segment) and surrogate_method == 'shift' :
                    print(
                        'Skipping session {}, not enough trials'.format(session_id))
                    continue

            try:
                dec_out = decode(X=X, y=y, C=C,
                                 deconf_method=method,
                                 posthoc_counterbalancing_binsize=distribution_match_binsize,
                                 n_kfold_splits=n_kfold_splits,
                                 run_spisak_tests=run_spisak_tests,
                                 num_perms_spisak=num_perms_spisak,
                                 surrogate_method=surrogate_method,
                                 n_shifts=n_shifts,
                                 n_shuffles=n_shuffles)

            except np.linalg.LinAlgError:
                continue

            mean_score = dec_out['mean_score']
            full_p = dec_out['full_p']
            full_sig = dec_out['full_sig']
            partial_p = dec_out['partial_p']
            partial_sig = dec_out['partial_sig']
            wool_significant = dec_out['wool_significant']


            row = [animal_id, session_id, dataset, area_spikes, X.shape[1],
                   experiment, 'spikes', method, time, time_bin_indx, t0, t1, mean_score,
                   full_p, full_sig, partial_p, partial_sig]

            df.loc[df.shape[0], :] = row


    df['time'] = [t.item() for t in df['time']]

    features_palette = {'spikes' : sns.xkcd_rgb['purple'],
                        'confound' : sns.xkcd_rgb['grey']}


    # --- PLOT SCATTER -------------------------------------------------------------

    df = df.drop(['time_bin', 't0', 't1'], axis='columns')


    methods_linestyles = {'raw' : '-',
                          'linear_confound_regression' : '--',
                          'nonlinear_confound_regression' : '-.',
                          'distribution_match' : ':'}


    f, ax = plt.subplots(1, 1, figsize=[4, 4])


    for method in ['confound'] + methods:

        score = df[df['method'] == method]['score'].__array__()

        if method == 'confound':
            c = 'grey'
            ls = '-'
        else:
            c = dataset_palette[dataset]
            ls = methods_linestyles[method]

        ax.plot(time_bin_times, score, c=c,
                ls=ls,
                label=method)

    if experiment == 'audiofreq_in_big_change':
        ax.set_ylabel('Accuracy of frequency decoding')

    elif experiment == 'visualori_in_big_change':
        ax.set_ylabel('Accuracy of orientation decoding')

    ax.set_xlabel('Time from stimulus onset [s]')
    ax.axvline(0, c='grey', ls='--')
    ax.axhline(0.5, c='grey', ls=':')
    ax.set_ylim([0, 1.01])
    ticks = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
    labels = [0, '', 0.2, '', 0.4, '', 0.6, '', 0.8, '', 1.0]

    ax.set_yticks(ticks)
    ax.set_yticklabels(labels, fontsize=8)
    ax.legend()
    sns.despine()
    plt.tight_layout()


    plot_name = 'decoding_ephys_{}_{}_{}.{}'.format(experiment, settings_name,
                                                session_id, plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=400,
              bbox_inches="tight")

