import os
import pickle
from constants import *
import numpy as np
import pandas as pd
from sklearn.model_selection import StratifiedKFold
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import accuracy_score, recall_score
from sklearn.metrics import balanced_accuracy_score
import pickle
import quantities as pq
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.linear_model import SGDClassifier
from sklearn.preprocessing import StandardScaler
from modid.modid_utils import *
from mlconfound.stats import full_confound_test


settings_name = 'may17'
area_spikes = 'V1'

dataset = 'ChangeDetectionConflict'
experiments = ['audiofreq_in_big_change', 'visualori_in_big_change']


# --- decoding pars ---
decoder_name = 'SGD'
n_splits = 3
n_repeats = 3
score_name = 'accuracy'


time_bin = 4

# --- DECODING ----------------------------------------------------------------

df = pd.DataFrame(columns=['animal_id', 'session_id', 'dataset',
                           'area_spikes', 'experiment',
                           'decoder', 'time', 'time_bin', 't0', 't1',
                           'score', 'p_value', 'n_neurons'])


for experiment_name in experiments:

    data = load_decoding_data(settings_name=settings_name,
                              dataset=dataset,
                              experiment_name=experiment_name,
                              area_spikes=area_spikes)

    sessions = data['pars']['sessions']

    for i, row in sessions.iterrows() :

        animal_id = row['animal_id']
        session_id = row['session_id']
        time_bin_times = data['data'][session_id]['time_bin_times']
        time_bins = data['data'][session_id]['time_bins']
        n_time_bins_per_trial = len(time_bin_times)
        tdf = data['data'][session_id]['trial_df']

        # TODO we only look at 1 time bin

        time = time_bin_times[time_bin]
        t0, t1 = time_bins[time_bin]

        X = data['data'][session_id]['binned_spikes'][time_bin]
        C = data['data'][session_id]['binned_movement'][time_bin]
        C = np.sum(C, axis=1)


        y = data['data'][session_id]['target']

        kfold = StratifiedKFold(n_splits=n_splits, shuffle=True,
                                random_state=1)
        kfold_scores = []
        y_test_all, y_pred_all = [], []

        for fold, (training_ind, testing_ind) in enumerate(kfold.split(X, y)) :

            if decoder_name == 'random_forest' :
                decoder = RandomForestClassifier(n_estimators=n_estimators)
            elif decoder_name == 'SGD' :
                decoder = SGDClassifier()
            else :
                raise NotImplementedError

            X_train = X[training_ind, :]
            X_test = X[testing_ind, :]
            y_train = y[training_ind]
            y_test = y[testing_ind]

            ss = StandardScaler()
            X_train = ss.fit_transform(X_train)
            X_test = ss.transform(X_test)

            decoder.fit(X_train, y_train)
            y_pred = decoder.predict(X_test)
            score = balanced_accuracy_score(y_test, y_pred)
            kfold_scores.append(score)
            y_test_all.append(y_test)
            y_pred_all.append(y_pred)

        y_test_all = np.hstack(y_test_all)
        y_pred_all = np.hstack(y_pred_all)

        out = full_confound_test(y_test_all, y_pred_all, C,
                                 num_perms=1000,
                                 cat_y=False,
                                 cat_yhat=False,
                                 cat_c=False,
                                 mcmc_steps=150,
                                 cond_dist_method='gam',
                                 return_null_dist=False,
                                 random_state=None, progress=True,
                                 n_jobs=-1)

        mean_score = np.mean(kfold_scores)

        row = [animal_id, session_id, dataset, area_spikes,
               experiment_name,
               decoder_name, time, time_bin,
               t0, t1, mean_score, out.p,
               X.shape[1]]

        df.loc[df.shape[0], :] = row


numeric_cols = ['score', 'p_value']

for col in numeric_cols :
    df[col] = pd.to_numeric(df[col])


print(df)

df['sig'] = [True if p<0.05 else False for p in df['p_value']]

df_backup = df.copy()
# --- PLOT SCATTER -------------------------------------------------------------
import matplotlib.pyplot as plt
from sklearn.preprocessing import minmax_scale
import seaborn as sns

plot_format = 'png'
plots_folder = os.path.join(DATA_PATH, 'plots', 'modid')
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)


f, ax = plt.subplots(1, 1, figsize=[3, 3])
