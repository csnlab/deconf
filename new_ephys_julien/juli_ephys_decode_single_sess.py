import os
import pickle
from sklearn.model_selection import StratifiedKFold
from sklearn.metrics import balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
from constants import *
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
from utils import *
from decoding_utils import decode, decode_alt_norepeats, decode_alt_repeats
from plotting_style import *
from scipy.stats import mannwhitneyu, bootstrap
import pickle
from scipy.stats import norm, gamma, combine_pvalues

"""
Decoding visual orientation and auditory frequency on ephys data.
"""

settings_name = 'mar1_V2_samplestart_sig'

# DATA PARS
data_settings_name = 'aug22_samplestart'#'feb28_retraction'#'aug22_samplestart'#'true+conf_effect' #aug22
ephys_dataset = 'julien'
area_spikes_dataset = 'all'

# DECODING PARS
mode = 'deconf' # 'reproduce', 'deconf', 'test'
linear_regression_regularization = False
tune_regularization_predictability = True
lr_penalty = 'l2'
Cs = [0.001, 0.01, 0.1, 1]

tune_hyperpars_decoder = False

# RF parameters
n_estimators = 50
max_depth = 3
min_samples_split = int(2)
min_samples_leaf = int(7)
max_features = 'sqrt'

run_spisak_full_test = False
run_spisak_partial_test = False
num_perms_spisak = 2500
distribution_match_binsize = None

confound_modes = ['top_2_components',
                  'top_2_shuffled']


if mode == 'deconf':
    deconf_methods = ['raw',
                      'decode_from_confound',
                      #'distribution_match',
                      'linear_confound_regression',
                      'nonlinear_confound_regression',
                      'predictability_lin',
                      'predictability_nonlin_ensemble'
                      ]
    decoders = ['logistic_regression']
    n_permutations = 0
    decode_all_time_bins = True
    time_bin_index = None
    decode_only_time = None
    session_ids = None
    n_kfold_splits = 5
    experiments = ['object_T', 'object_V']
    n_repeats = 5

    area = 'V2'
    area = 'HPC'


elif mode == 'reproduce':
    deconf_methods = ['raw',
                      'decode_from_confound',
                      #'distribution_match',
                      # 'linear_confound_regression',
                      #'nonlinear_confound_regression',
                      # 'predictability_lin',
                      # 'predictability_nonlin_ensemble'
                      ]
    decoders = ['logistic_regression', 'SVC']
    n_permutations = 0
    decode_all_time_bins = True
    time_bin_index = None
    decode_only_time = None
    n_kfold_splits = 5
    session_ids = None
    experiments = ['object_T', 'object_V']
    area = 'V2'


elif mode == 'significance':
    deconf_methods = ['raw',
                      'decode_from_confound',
                      #'distribution_match',
                      'linear_confound_regression',
                      'nonlinear_confound_regression',
                      # 'predictability_lin',
                      'predictability_nonlin_ensemble'
                      ]
    decoders = ['logistic_regression', 'SVC']
    n_permutations = 100
    decode_all_time_bins = True
    time_bin_index = None
    decode_only_time = None
    n_kfold_splits = 5
    session_ids = None
    experiments = ['object_T', ]
    area = 'V2'
    n_repeats = 1


    session_ids = {'object_T' : ['rat-16_181016_V2', 'rat-16_181020_V2',
                                 'rat-26_190626_V2', 'rat-26_190702_V2'],
                   'object_V' : ['rat-26_190624_V2', 'rat-26_190625_V2',
                                 'rat-26_190626_V2', 'rat-26_190702_V2']}



for experiment in experiments:



    data = load_decoding_data(settings_name=data_settings_name,
                              experiment_name=experiment,
                              dataset=ephys_dataset,
                              area_spikes=area_spikes_dataset)

    sessions = data['pars']['sessions']
    sessions['session_id'] = ['{}_{}'.format(s, a) for s, a in zip(sessions['session_id'], sessions['area'])]

    if session_ids is None:
        session_ids_iter = sessions['session_id'].__array__()
        if area is not None:
            session_ids_iter = sessions[sessions['area'] == area]['session_id'].__array__()

    elif isinstance(session_ids, dict):
        session_ids_iter = session_ids[experiment]
    else:
        session_ids_iter = session_ids



    for session_id in session_ids_iter:

        for confound_mode in confound_modes:

            df = pd.DataFrame(columns=['animal_id',
                                       'session_id',
                                       'dataset',
                                       'area_spikes',
                                       'n_neurons',
                                       'n_trials',
                                       'perc_y=1',
                                       'experiment',
                                       'decoder',
                                       'method',
                                       'time',
                                       'time_bin',
                                       't0',
                                       't1',
                                       'permuted',
                                       'permutation',
                                       'mean_score',
                                       'full_p',
                                       'full_sig',
                                       'partial_p',
                                       'partial_sig'])

            row = sessions[sessions['session_id'] == session_id].iloc[0, :]

            # for i, row in sessions.iterrows() :
            # get metadata
            animal_id = row['animal_id']
            area_spikes = row['area']

            data_key = session_id

            area_spikes = data['data'][data_key]['area_spikes']
            try:
                sub_dataset = data['data'][data_key]['dataset']
            except KeyError:
                sub_dataset = 'none'
            target_name = data['data'][data_key]['target_name']
            n_trials = data['data'][data_key]['trial_df'].shape[0]

            print(session_id, n_trials, sub_dataset, confound_mode)

            # get time parameters
            time_bin_times = data['data'][data_key]['time_bin_times']
            if ephys_dataset == 'modid':
                time_bins = data['data'][data_key]['time_bins']
            elif ephys_dataset == 'julien':
                time_bins = data['data'][data_key]['time_bin_edges']

            n_time_bins_per_trial = len(time_bin_times)

            if decode_all_time_bins:
                #time_bin_indexes = np.arange(n_time_bins_per_trial)
                time_bin_indexes = np.arange(3, n_time_bins_per_trial-4)

            else:
                time_bin_indexes = [time_bin_index]
            # --- Decode from spikes ---

            for time_bin_indx in time_bin_indexes:
                time = time_bin_times[time_bin_indx]
                print('--- time bin {} of {} (t={})'.format(time_bin_indx, len(time_bin_indexes), time))
                if decode_only_time is not None:
                    if time != decode_only_time:
                        print('--- skipping')
                        continue
                t0, t1 = time_bins[time_bin_indx]

                # get decoding data
                X = data['data'][data_key]['binned_spikes'][time_bin_indx]
                C = data['data'][data_key]['binned_movement'][time_bin_indx]
                y = data['data'][data_key]['target']

                if ephys_dataset == 'julien':
                    if np.isnan(C[:, 0]).sum() > C.shape[0]/5:
                        print('--- skipping time bin'.format(time_bin_indx, len(time_bin_indexes)))
                        continue

                if confound_mode == 'random':
                    #C = np.random.default_rng(seed=92).normal(loc=0, scale=2, size=[C.shape[0], C.shape[1]])
                    raise ValueError
                    #C = np.random.normal(loc=0, scale=1, size=[C.shape[0], C.shape[1]])
                    #C = np.random.random(size=[C.shape[0], C.shape[1]])
                    #C = np.random.normal(loc=0, scale=2, size=[C.shape[0], 3])

                elif confound_mode.split('_')[0] == 'top':
                    n_components = int(confound_mode.split('_')[1])
                    if confound_mode.split('_')[2] == 'components':
                        shuffle_components = False
                    elif confound_mode.split('_')[2] == 'shuffled':
                        shuffle_components = True
                    else:
                        raise ValueError

                    if shuffle_components:
                        if n_components == 1:
                            C = np.random.default_rng(seed=0).permutation(C[:, 0]).reshape(-1, 1)
                        else:
                            # C = C[:, 0:n_components]
                            # for col in range(C.shape[1]):
                            #     C[:, col] = np.random.default_rng(seed=col+2).permutation(C[:, col])
                            indx = np.random.default_rng(seed=3).permutation(C.shape[0])
                            C = C[indx, 0:n_components]

                    else:
                        if n_components == 1:
                            C = C[:, 0].reshape(-1, 1)
                        else:
                            C = C[:, 0:n_components]

                    #C = C[:, 0].reshape(-1, 1)


                elif confound_mode == 'full':
                    pass
                else:
                    raise ValueError

                perc_y1 = 100 * (y[y==1].shape[0] / y.shape[0])

                for deconf_method in deconf_methods:
                    print('---- method: {}'.format(deconf_method))

                    if np.isin(deconf_method, predictability_methods):
                        decoders_to_iterate = ['none']
                    else:
                        decoders_to_iterate = decoders

                    for decoder in decoders_to_iterate:

                        for nperm in range(n_permutations+1):

                            if nperm == 0:
                                C_dec = np.copy(C)
                                y_dec = np.copy(y)
                                permuted = False
                            elif nperm > 0:
                                random_index = np.random.default_rng(seed=nperm).permutation(np.arange(X.shape[0]))
                                # only if method is predictability we permute C together with y
                                if np.isin(deconf_method, predictability_methods):
                                    C_dec = C[random_index, :]
                                else:
                                    C_dec = np.copy(C)

                                y_dec = y[random_index]
                                permuted = True

                            # avoid running spisak tests on shuffled data
                            if permuted or np.isin(deconf_method, predictability_methods):
                                spisak_full = False
                                spisak_partial = False
                            else:
                                spisak_full = run_spisak_full_test
                                spisak_partial = run_spisak_partial_test
                            if n_repeats>1:
                                dec_out = decode_alt_repeats(X=X, y=y_dec, C=C_dec,
                                                     deconf_method=deconf_method,
                                                     decoder=decoder,
                                                     standardize_features=True,
                                                     standardize_confound=True,
                                                     linear_regression_regularization=linear_regression_regularization,
                                                     distribution_match_binsize=distribution_match_binsize,
                                                     n_kfold_splits=n_kfold_splits,
                                                     tune_hyperpars_decoder=tune_hyperpars_decoder,
                                                     lr_penalty=lr_penalty,
                                                     run_spisak_full_test=spisak_full,
                                                     run_spisak_partial_test=spisak_partial,
                                                     num_perms_spisak=num_perms_spisak,
                                                     tune_regularization_predictability=tune_regularization_predictability,
                                                     Cs=Cs,
                                                     n_estimators=n_estimators, max_depth=max_depth,
                                                     min_samples_split=min_samples_split,
                                                     min_samples_leaf=min_samples_leaf,
                                                     fill_C_nans=True,
                                                     n_repeats=n_repeats)

                            else:
                               dec_out = decode_alt_norepeats(X=X, y=y_dec, C=C_dec,
                                                     deconf_method=deconf_method,
                                                     decoder=decoder,
                                                     standardize_features=True,
                                                     standardize_confound=True,
                                                     linear_regression_regularization=linear_regression_regularization,
                                                     distribution_match_binsize=distribution_match_binsize,
                                                     n_kfold_splits=n_kfold_splits,
                                                     tune_hyperpars_decoder=tune_hyperpars_decoder,
                                                     lr_penalty=lr_penalty,
                                                     run_spisak_full_test=spisak_full,
                                                     run_spisak_partial_test=spisak_partial,
                                                     num_perms_spisak=num_perms_spisak,
                                                     tune_regularization_predictability=tune_regularization_predictability,
                                                     Cs=Cs,
                                                     n_estimators=n_estimators, max_depth=max_depth,
                                                     min_samples_split=min_samples_split,
                                                     min_samples_leaf=min_samples_leaf,
                                                     fill_C_nans=True)



                            mean_score = dec_out['mean_score']
                            full_p = dec_out['full_p']
                            full_sig = dec_out['full_sig']
                            partial_p = dec_out['partial_p']
                            partial_sig = dec_out['partial_sig']

                            row = [animal_id,
                                   session_id,
                                   sub_dataset,
                                   area_spikes,
                                   X.shape[1],
                                   n_trials,
                                   perc_y1,
                                   experiment,
                                   decoder,
                                   deconf_method,
                                   time,
                                   time_bin_indx,
                                   t0,
                                   t1,
                                   permuted,
                                   nperm,
                                   mean_score,
                                   full_p,
                                   full_sig,
                                   partial_p,
                                   partial_sig]

                            df.loc[df.shape[0], :] = row


            #df['time'] = [t.item() for t in df['time']]

            output_folder = os.path.join(DATA_PATH, 'ephys_results_julien',
                                         '{}'.format(settings_name))

            if not os.path.isdir(output_folder):
                os.makedirs(output_folder)

            df['mean_score'] = pd.to_numeric(df['mean_score'])

            pars = {'settings_name' : settings_name,
                    'ephys_dataset' : ephys_dataset,
                    'area_spikes_dataset' : area_spikes_dataset,
                    'mode' : mode,
                    'session_ids' : session_ids,
                    'n_permutations' : n_permutations,
                    'decoders' : decoders,
                    'experiments' : experiments,
                    'deconf_methods' : deconf_methods,
                    'decode_only_time' : decode_only_time,
                    'distribution_match_binsize' : distribution_match_binsize,
                    'n_kfold_splits' : n_kfold_splits,
                    'run_spisak_full_test' : run_spisak_full_test,
                    'run_spisak_partial_test' : run_spisak_partial_test,
                    'num_perms_spisak' : num_perms_spisak,
                    'n_time_bins_per_trial' : n_time_bins_per_trial,
                    'time_bin_times' : time_bin_times,
                    'time_bins' : time_bins,
                    'confound_mode' : confound_mode,
                    'tune_hyperpars_decoder' : tune_hyperpars_decoder,
                    'tune_regularization_predictability' : tune_regularization_predictability}

            out = {'pars' : pars,
                   'df' : df}

            output_full_path = os.path.join(output_folder, 'results_{}_{}_{}_{}.pkl'.format(settings_name, confound_mode,
                                                                                         experiment, session_id))
            print('Saving results to {}'.format(output_full_path))
            pickle.dump(out, open(output_full_path, 'wb'))





if False:
    # --- PLOT SCATTER -------------------------------------------------------------

    df = df.drop(['time_bin', 't0', 't1'], axis='columns')


    methods_linestyles = {'raw' : '-',
                          'linear_confound_regression' : '--',
                          'nonlinear_confound_regression' : '-.',
                          'distribution_match' : ':'}


    f, ax = plt.subplots(1, 1, figsize=[4, 4])


    for deconf_method in ['confound'] + deconf_methods:

        score = df[df['method'] == deconf_method]['score'].__array__()

        if deconf_method == 'confound':
            c = 'grey'
            ls = '-'
        else:
            c = dataset_palette[dataset]
            ls = methods_linestyles[deconf_method]

        ax.plot(time_bin_times, score, c=c,
                ls=ls,
                label=deconf_method)

    if experiment == 'audiofreq_in_big_change':
        ax.set_ylabel('Accuracy of frequency decoding')

    elif experiment == 'visualori_in_big_change':
        ax.set_ylabel('Accuracy of orientation decoding')

    ax.set_xlabel('Time from stimulus onset [s]')
    ax.axvline(0, c='grey', ls='--')
    ax.axhline(0.5, c='grey', ls=':')
    ax.set_ylim([0, 1.01])
    ticks = [0, 0.1, 0.2, 0.3, 0.4, 0.5, 0.6, 0.7, 0.8, 0.9, 1.0]
    labels = [0, '', 0.2, '', 0.4, '', 0.6, '', 0.8, '', 1.0]

    ax.set_yticks(ticks)
    ax.set_yticklabels(labels, fontsize=8)
    ax.legend()
    sns.despine()
    plt.tight_layout()


    plot_name = 'decoding_ephys_{}_{}_{}.{}'.format(experiment, settings_name,
                                                session_id, plot_format)
    f.savefig(os.path.join(plots_folder, plot_name), dpi=400,
              bbox_inches="tight")

