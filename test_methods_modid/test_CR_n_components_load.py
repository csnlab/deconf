import os
import glob
import pickle
from constants import *
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from utils import *
from plotting_style import *
from scipy.stats import bootstrap

settings_name = 'feb27_all_rf'

plot_single_sessions_subplots = False
plot_single_sessions_singleplots = False
plot_sessions_combined_subplots = False
plot_sessions_combined_singleplots = False
plot_sessions_combined_singleplots_v2 = True
plot_kde = True
plot_kde_comparison = True
plot_dots = True
plot_format = 'png'
plot_methods = ['linear_confound_regression_reg', 'nonlinear_confound_regression']
plots_folder = os.path.join(DATA_PATH, 'plots', 'test_CR_n_components', settings_name)
if not os.path.isdir(plots_folder):
    os.makedirs(plots_folder)

output_folder = os.path.join(DATA_PATH, 'test_CR_n_components',
                             'SIMULATION_{}'.format(settings_name))
files = glob.glob('{}/*'.format(output_folder))
dfs = []
pars = {}
for output_full_path in files:
    res = pickle.load(open(output_full_path, 'rb'))
    df = res['df']
    dm = df['deconf_method'].unique()
    assert dm.shape[0] == 1
    pars[dm[0]] = res['pars']
    dfs.append(df)
df = pd.concat(dfs)


for shuffle_components in [False, True]:
    if shuffle_components:
        extra_string = 'shuffledC'
    else:
        extra_string = 'trueC'

    rs = df[df['shuffle_confound'] == shuffle_components]
    deconf_methods = [d for d in rs['deconf_method'].unique() if d in plot_methods]
    session_ids = pars[deconf_methods[0]]['session_ids']
    n_pca_components_list = pars[deconf_methods[0]]['n_pca_components_list']
    decoders = pars[deconf_methods[0]]['decoders']


    if plot_single_sessions_subplots:
        for session_id in session_ids:
            rsel = rs[rs['session_id'] == session_id]
            f, ax = plt.subplots(1, 3, figsize=[10, 3], sharey=True)
            sns.lineplot(x='n_pca_components', y='mean_score', ax=ax[0], markers=True, marker='o',
                         hue='deconf_method', data=rsel[rsel['decoder'] == decoders[0]], palette=method_palette)
            sns.lineplot(x='n_pca_components', y='mean_score', ax=ax[1], markers=True, marker='o',
                         hue='deconf_method', data=rsel[rsel['decoder'] == decoders[1]], palette=method_palette)
            try:
                sns.lineplot(x='n_pca_components', y='mean_score', ax=ax[2], markers=True, marker='o',
                             hue='deconf_method', data=rsel[rsel['decoder'] == decoders[2]], palette=method_palette)
            except IndexError:
                pass
            ax[0].set_title('Linear decoder')
            ax[1].set_title('Nonlinear decoder 1')
            ax[2].set_title('Nonlinear decoder 2')

            sns.despine()
            ax[0].set_ylabel('Mean accuracy')
            for axx in ax:
                axx.set_xlabel('# movement PCA components')
                axx.axhline(0.5, ls=':', c='grey')
                axx.legend().remove()
                axx.set_ylim(0.3, 1)
                axx.set_xticks(n_pca_components_list)

            plot_name = 'lineplot_{}_{}.{}'.format(session_id, extra_string, plot_format)
            f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                      bbox_inches="tight")

    if plot_single_sessions_singleplots:
        for session_id in session_ids:
            for decoder in decoders:
                f, ax = plt.subplots(1, 1, figsize=[2.8, 2.2], sharey=True)

                for j, deconf_method in enumerate(deconf_methods):
                    x = []
                    err = []
                    for k, n_components in enumerate(n_pca_components_list):
                        vals = rs[(rs['n_pca_components'] == n_components) &
                                  (rs['decoder'] == decoder) &
                                  (rs['deconf_method'] == deconf_method) &
                                  (rs['session_id'] == session_id)]['mean_score'].__array__()
                        center = vals.mean()
                        print(vals.mean())

                        bootstrap_vals = (vals.__array__().reshape(-1, 1).astype(float),)
                        bres = bootstrap(bootstrap_vals,
                                         axis=0,
                                         statistic=np.mean,
                                         n_resamples=100,
                                         confidence_level=0.95,
                                         method='basic')
                        error = np.array([bres.confidence_interval.low[0], bres.confidence_interval.high[0]])
                        x.append(center)
                        err.append(error)
                    err = np.vstack(err)

                    ax.plot(n_pca_components_list, x, c=method_palette[deconf_method], zorder=-5)
                    ax.scatter(n_pca_components_list, x, s=30, c=method_palette[deconf_method],
                               edgecolor='w', label=method_labels[deconf_method])
                    ax.fill_between(n_pca_components_list, err[:, 0], err[:, 1], alpha=0.3, zorder=-10,
                                    color=method_palette[deconf_method], linewidth=0)
                sns.despine()
                ax.set_ylabel('Mean accuracy')
                ax.set_xlabel('# movement PCA components')
                # ax.set_title(decoder_labels[decoder])
                ax.axhline(0.5, ls=':', c='grey')

                ax.set_ylim(0.3, 1)
                ax.legend().remove()
                if n_pca_components_list.__len__() > 10:
                    ax.set_xticks(np.array(n_pca_components_list)[::2])
                else:
                    ax.set_xticks(n_pca_components_list)
                plt.tight_layout()

                plot_name = 'lineplot_{}_{}_{}.{}'.format(session_id, extra_string, decoder, plot_format)
                f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                          bbox_inches="tight")

    if plot_sessions_combined_subplots:
        # plot all sessions together
        f, ax = plt.subplots(1, 3, figsize=[10, 3], sharey=True)
        for axix, decoder in enumerate(decoders):
            for j, deconf_method in enumerate(deconf_methods):
                for session_id in session_ids:
                    x = []
                    for k, n_components in enumerate(n_pca_components_list):
                        vals = rs[(rs['n_pca_components'] == n_components) &
                                  (rs['deconf_method'] == deconf_method) &
                                  (rs['decoder'] == decoder) &
                                  (rs['session_id'] == session_id)]['mean_score'].__array__()

                        center = vals.mean()
                        x.append(center)

                    ax[axix].plot(n_pca_components_list, x, c=method_palette[deconf_method], zorder=-5)
                # ax.scatter(n_trials_dict[session_id], x, s=50, c=method_palette[deconf_method],
                #            edgecolor='w', label=method_labels[deconf_method])

        for axx in ax:
            axx.set_xlabel('# movement PCA components')
            axx.axhline(0.5, ls=':', c='grey')
            axx.legend().remove()
            axx.set_xticks(np.array(n_pca_components_list))
            axx.set_xlim([np.min(n_pca_components_list), np.max(n_pca_components_list)])
            axx.set_ylim(0.3, 1)

        ax[0].set_ylabel('Mean accuracy')
        ax[0].set_title('Linear decoder')
        ax[1].set_title('Nonlinear decoder 1')
        ax[2].set_title('Nonlinear decoder 2')
        sns.despine()
        # ax.set_yticks([-0.5, -0.25, 0, 0.25, 0.5, 0.75, 1])
        plt.tight_layout()

        plot_name = 'allsess_lineplot_{}.{}'.format(extra_string, plot_format)
        f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                  bbox_inches="tight")

    if plot_sessions_combined_singleplots:
        # plot all sessions together
        for decoder in decoders:
            f, ax = plt.subplots(1, 1, figsize=[3, 3], sharey=True)
            for j, deconf_method in enumerate(deconf_methods):
                for session_id in session_ids:
                    x = []
                    for k, n_components in enumerate(n_pca_components_list):
                        vals = rs[(rs['n_pca_components'] == n_components) &
                                  (rs['deconf_method'] == deconf_method) &
                                  (rs['decoder'] == decoder) &
                                  (rs['session_id'] == session_id)]['mean_score'].__array__()

                        center = vals.mean()
                        x.append(center)

                    ax.plot(n_pca_components_list, x, c=method_palette[deconf_method], zorder=-5)
                # ax.scatter(n_trials_dict[session_id], x, s=50, c=method_palette[deconf_method],
                #            edgecolor='w', label=method_labels[deconf_method])

            ax.set_xlabel('# confound features')
            ax.axhline(0.5, ls=':', c='grey')
            ax.legend().remove()
            ax.set_xticks(np.array(n_pca_components_list))
            ax.set_xlim([np.min(n_pca_components_list), np.max(n_pca_components_list)])
            ax.set_ylim(0.3, 1)

            ax.set_ylabel('Mean accuracy')
            ax.set_title(decoder_labels[decoder])
            sns.despine()
            # ax.set_yticks([-0.5, -0.25, 0, 0.25, 0.5, 0.75, 1])
            plt.tight_layout()

            plot_name = 'allsess_lineplot_{}_{}.{}'.format(extra_string, decoder, plot_format)
            f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                      bbox_inches="tight")

    if plot_sessions_combined_singleplots_v2:
        # plot all sessions together
        for decoder in decoders:
            f, ax = plt.subplots(1, 1, figsize=[3, 2.2], sharey=True)
            for j, deconf_method in enumerate(deconf_methods):
                mean_score = {i: [] for i in n_pca_components_list}
                for session_id in session_ids:
                    x = []
                    for k, n_components in enumerate(n_pca_components_list):
                        vals = rs[(rs['n_pca_components'] == n_components) &
                                  (rs['deconf_method'] == deconf_method) &
                                  (rs['decoder'] == decoder) &
                                  (rs['session_id'] == session_id)]['mean_score'].__array__()

                        center = vals.mean()
                        x.append(center)
                        mean_score[n_components].append(center)
                    ax.plot(n_pca_components_list, x, c=method_palette[deconf_method], zorder=-5,
                            lw=0.7, alpha=0.6)

                mean_score = [np.mean(mean_score[m]) for m in n_pca_components_list]
                ax.plot(n_pca_components_list, mean_score, c=method_palette[deconf_method], zorder=-4,
                        lw=3)
                # ax.scatter(n_trials_dict[session_id], x, s=50, c=method_palette[deconf_method],
                #            edgecolor='w', label=method_labels[deconf_method])

            ax.set_xlabel('# confound features')
            ax.axhline(0.5, ls=':', c='grey')
            ax.legend().remove()
            ax.set_xticks(np.array(n_pca_components_list))
            ax.set_xlim([np.min(n_pca_components_list), np.max(n_pca_components_list)])
            ax.set_ylim(0.4, 0.85)

            ax.set_ylabel('Mean accuracy')
            #ax.set_title(decoder_labels[decoder])
            sns.despine()
            # ax.set_yticks([-0.5, -0.25, 0, 0.25, 0.5, 0.75, 1])
            plt.tight_layout()

            plot_name = 'allsess_lineplot_v2_{}_{}.{}'.format(extra_string, decoder, plot_format)
            f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                      bbox_inches="tight")

    if plot_kde:
        for axix, decoder in enumerate(decoders):

            n_components = 30
            f, ax = plt.subplots(1, 1, figsize=[1.5, 2.2], sharey=True)

            dfsel = rs[(rs['n_pca_components'] == n_components) &
                       (rs['decoder'] == decoder)]

            dfsel = dfsel.groupby(['session_id', 'shuffle_confound', 'decoder',
                                   'n_pca_components', 'deconf_method']).mean().reset_index()

            for deconf_method in deconf_methods:
                dplot = dfsel[dfsel['deconf_method'] == deconf_method]
                sns.kdeplot(data=dplot, x='mean_score',
                            vertical=True, ax=ax, color=method_palette[deconf_method])

            # sns.kdeplot(data=dfsel, hue='deconf_method', x='mean_score',
            #             vertical=True, ax=ax, palette=method_palette)
            ax.legend().remove()
            sns.despine()
            ax.set_ylim(0.4, 0.85)
            ax.set_ylabel('')
            ax.axhline(0.5, ls=':', c='grey')
            plt.tight_layout()
            plot_name = 'kde_{}_{}.{}'.format(extra_string, decoder, plot_format)
            f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                      bbox_inches="tight")

rx = df.copy()
ylims2 = [0.35, 0.85]
yticks2 = [0.4, 0.5, 0.6, 0.7, 0.8]
if plot_kde_comparison:
    n_components = 30
    for decoder in decoders:
        for deconf_method in deconf_methods:
            f, ax = plt.subplots(1, 1, figsize=[1, 2.1], sharey=True)

            dfsel = rx[(rx['n_pca_components'] == n_components) &
                       (rx['deconf_method'] == deconf_method) &
                       (rx['decoder'] == decoder)]
            dfsel = dfsel.groupby(['session_id', 'shuffle_confound', 'decoder',
                                   'n_pca_components', 'deconf_method']).mean().reset_index()

            sns.kdeplot(data=dfsel[dfsel['shuffle_confound'] == True],
                        y='mean_score',
                        ax=ax, color=method_palette[deconf_method], linestyle='--')
            sns.kdeplot(data=dfsel[dfsel['shuffle_confound'] == False],
                        y='mean_score',
                        ax=ax, color=method_palette[deconf_method], linestyle='-')

            ax.legend().remove()

            sns.despine()
            ax.set_ylim(ylims2)
            ax.set_yticks(yticks2)
            ax.set_xticklabels([])
            # ax.set_title('{}\n{}'.format(method_labels[deconf_method].replace('\n', ' '), decoder_labels[decoder]))
            # ax.set_xlabel('Mean accuracy')
            ax.axhline(0.5, ls=':', c='grey')
            ax.set_ylabel('')
            plt.tight_layout()
            plot_name = 'kde_{}_{}.{}'.format(deconf_method, decoder, plot_format)
            f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                      bbox_inches="tight")

if plot_dots:
    n_components = 30
    for decoder in decoders:
        for deconf_method in deconf_methods:
            f, ax = plt.subplots(1, 1, figsize=[1.8, 2.1], sharey=True)

            dfsel = rx[(rx['n_pca_components'] == n_components) &
                       (rx['deconf_method'] == deconf_method) &
                       (rx['decoder'] == decoder)]
            dfsel = dfsel.groupby(['session_id', 'shuffle_confound', 'decoder',
                                   'n_pca_components', 'deconf_method']).mean().reset_index()
            n_sessions = dfsel['session_id'].unique().shape[0]
            print('KDE comparison computed with {} sessions'.format(n_sessions))

            y1 = data = dfsel[dfsel['shuffle_confound'] == True]['mean_score'].__array__()
            y0 = data = dfsel[dfsel['shuffle_confound'] == False]['mean_score'].__array__()
            x0 = [0 for i in range(y0.shape[0])]
            x1 = [1 for i in range(y1.shape[0])]

            from scipy.stats import mannwhitneyu

            stat, p = mannwhitneyu(y0, y1)
            print('Method {}, p-value = {}'.format(deconf_method, p))

            ax.scatter(x0, y0, s=30, c=method_palette[deconf_method],
                       edgecolor='w')
            ax.scatter(x1, y1, s=30, c='w',
                       edgecolor=method_palette[deconf_method])
            for i in range(y0.shape[0]):
                ax.plot([0, 1], [y0[i], y1[i]], ls='-', linewidth=1,
                        color=method_palette[deconf_method], zorder=-10)
            ax.legend().remove()
            sns.despine()
            ax.set_xlim([-0.1, 1.1])
            ax.set_xticks([0, 1])
            ax.set_ylim(ylims2)
            ax.set_yticks(yticks2)
            ax.set_xticklabels(['True C', 'Shuffled C'])
            # ax.set_title(method_labels[deconf_method].replace('\n', ' '))
            ax.set_ylabel('Mean accuracy')
            ax.axhline(0.5, ls=':', c='grey', zorder=-20)
            ax.text(x=0.4, y=0.4, text='p={:.4f}'.format(p), s=8)
            plt.tight_layout()
            plot_name = 'dotsss_{}_{}.{}'.format(deconf_method, decoder, plot_format)
            f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                      bbox_inches="tight")

f, ax = plt.subplots(1, 1, figsize=[2, 2])
legend_elements = [plt.Line2D([0], [0], marker='o', color=method_palette[m],
                              label=method_labels[m], lw=2,
                              markerfacecolor=method_palette[m],
                              markeredgecolor='w',
                              markersize=10) for m in deconf_methods]
ax.legend(handles=legend_elements, loc='center', frameon=False)
sns.despine(left=True, bottom=True)
ax.axis('off')
plt.show()
plot_name = 'singlesess_overtime_legend.{}'.format(plot_format)
f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
          bbox_inches="tight")

f, ax = plt.subplots(1, 1, figsize=[2, 2])
legend_elements = [plt.Line2D([0], [0], marker='o', color=method_palette[m],
                              label=method_labels[m], lw=2,
                              markerfacecolor=method_palette[m],
                              markeredgecolor='w',
                              markersize=0) for m in deconf_methods]
ax.legend(handles=legend_elements, loc='center', frameon=False)
sns.despine(left=True, bottom=True)
ax.axis('off')
plt.show()
plot_name = 'singlesess_nomarker_overtime_legend.{}'.format(plot_format)
f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
          bbox_inches="tight")
