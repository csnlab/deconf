import os
import pickle
from sklearn.model_selection import StratifiedKFold, KFold, LeaveOneOut
from sklearn.metrics import balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
from constants import *
import pandas as pd
try:
    from mlconfound.stats import full_confound_test, partial_confound_test
except ModuleNotFoundError:
    print('Spisak tests not installed')

try:
    from confound_prediction.deconfounding import confound_isolating_cv
except ModuleNotFoundError:
    print('Confound isolating cv not installed')
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from sklearn.svm import SVC, LinearSVC
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import balanced_accuracy_score, make_scorer
from sklearn.ensemble import RandomForestRegressor, RandomForestClassifier
from sklearn.linear_model import LinearRegression, RidgeCV
from sklearn.linear_model import LogisticRegression, LogisticRegressionCV
from sklearn.linear_model import RidgeClassifier
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis, QuadraticDiscriminantAnalysis
from utils import *
from plotting_style import *

"""
Test confound regression methods 

'raw': 

"""

settings_name = 'feb28_test_tune'

#session_ids = ['2019-03-08_14-39-42']
experiment = 'audiofreq_in_big_change'
#experiment = 'visualori_in_big_change'

data_settings_name = 'aug22'
ephys_dataset = 'modid'
area_spikes_dataset = 'V1'
time_bin_indx = 3
n_repeats = 10
n_pca_components = 30

deconf_methods = ['raw']
tune_hyperpars_decoder = True
decoders = ['logistic_regression', 'SVC']
n_kfold_splits = 5
standardize_features = True
standardize_confound = True

run_spisak_full_test = False
run_spisak_partial_test = True
num_perms_spisak = 1000

max_n_trials = 80
# RF parameters
# n_estimators = 50
# max_depth = 7
# min_samples_split = int(2)
# min_samples_leaf = int(5)
# max_features = 'sqrt'
# RF parameters
n_estimators = 50
max_depth = None
min_samples_split = int(2)
min_samples_leaf = int(1)
max_features = 'sqrt'

if experiment == 'audiofreq_in_big_change':
    session_ids = ['2018-08-10_10-14-01', '2018-08-14_14-30-15',
                   '2020-01-16_11-18-35', '2019-03-12_11-28-33',
                   '2019-03-08_14-39-42', '2019-03-12_15-41-39',
                   '2019-03-13_15-32-33', '2019-12-13_12-46-26',
                   '2019-12-13_09-59-24']

elif experiment == 'visualori_in_big_change':
    session_ids = ['2019-04-10_11-43-25', '2019-04-11_11-12-47',
                     '2019-03-08_14-39-42', '2018-08-15_10-09-24',
                     '2020-01-16_11-18-35', '2019-03-13_15-32-33',
                     '2019-03-12_11-28-33', '2021-04-30_15-26-10',
                     '2021-04-22_13-45-10', '2019-03-08_12-38-20',
                     '2019-04-12_09-04-51', '2019-12-13_12-46-26']

data = load_decoding_data(settings_name=data_settings_name,
                          experiment_name=experiment,
                          dataset=ephys_dataset,
                          area_spikes=area_spikes_dataset)

for deconf_method in deconf_methods:

    rs = pd.DataFrame(columns=['session_id', 'deconf_method', 'decoder',
                               'n_pca_components', 'n_trials', 'shuffle_confound',
                               'repeat', 'mean_score', 'partial_p', 'partial_sig',
                               'full_p', 'full_sig'])

    for shuffle_components in [False, True]:

        n_trials_dict = {}
        for session_id in session_ids:
            print(session_id)

            X_full = data['data'][session_id]['binned_spikes'][time_bin_indx]
            C_full = data['data'][session_id]['binned_movement'][time_bin_indx]
            y_full = data['data'][session_id]['target']
            total_n_trials = X_full.shape[0]
            n_trials_dict[session_id] = np.arange(40, max_n_trials+1, 10)

            for n_trials_draw in n_trials_dict[session_id]:
                print('# trials: {}'.format(n_trials_draw))
                if n_trials_draw >= total_n_trials:
                    print('Not enought trials, skipping')
                    continue

                for decoder in decoders:

                    for repeat in range(n_repeats):

                        indx0 = np.where(y_full == 0)[0]
                        indx1 = np.where(y_full == 1)[0]

                        n_pick = int(n_trials_draw / 2)
                        indx0_sel = np.random.default_rng(seed=repeat).choice(indx0, size=n_pick, replace=False)
                        indx1_sel = np.random.default_rng(seed=repeat).choice(indx1, size=n_pick, replace=False)
                        assert set(indx0_sel).intersection(set(indx1_sel)).__len__() == 0
                        indx_sub = np.sort(np.hstack([indx0_sel, indx1_sel]))

                        X = X_full[indx_sub, :]
                        C = C_full[indx_sub, :]
                        y = y_full[indx_sub]

                        # TODO every repeat shuffle C differently?
                        if shuffle_components:
                            if n_pca_components == 1:
                                C = np.random.default_rng(seed=0).permutation(C[:, 0]).reshape(-1, 1)
                            else:
                                #C = C[:, 0:n_pca_components]
                                # for col in range(C.shape[1]):
                                #     C[:, col] = np.random.default_rng(seed=col+2).permutation(C[:, col])
                                indx = np.random.default_rng(seed=3).permutation(C.shape[0])
                                C = C[indx, 0:n_pca_components]

                        else:
                            if n_pca_components == 1:
                                C = C[:, 0].reshape(-1, 1)
                            else:
                                C = C[:, 0:n_pca_components]

                        # run the decoding
                        kfold = StratifiedKFold(n_splits=n_kfold_splits, shuffle=True,
                                                random_state=repeat)
                        iterable = kfold.split(X, y)

                        kfold_scores = []
                        y_test_all, y_pred_all, c_all = [], [], []

                        for fold, (training_ind, testing_ind) in enumerate(iterable):

                            # decoder = SGDClassifier(random_state=92)
                            X_train = X[training_ind, :]
                            X_test = X[testing_ind, :]
                            y_train = y[training_ind]
                            y_test = y[testing_ind]
                            C_train = C[training_ind, :]
                            C_test = C[testing_ind, :]

                            # TODO scale before or after conf regression?
                            if standardize_features:
                                ss = StandardScaler()
                                X_train = ss.fit_transform(X_train)
                                X_test = ss.transform(X_test)

                            if standardize_confound:
                                ss = StandardScaler()
                                C_train = ss.fit_transform(C_train)
                                C_test = ss.transform(C_test)

                            # Apply confound regression (linear regression)
                            if deconf_method == 'linear_confound_regression':
                                # for high-dimensional confounds, avoid overfitting on the
                                # train set
                                reg = LinearRegression().fit(C_train, X_train)
                                X_train = X_train - reg.predict(C_train)
                                X_test = X_test - reg.predict(C_test)

                            elif deconf_method == 'linear_confound_regression_reg':
                                reg = RidgeCV(alphas=[0.0001, 0.001, 0.1, 1], cv=None,
                                              scoring=None, store_cv_values=False)
                                reg.fit(C_train, X_train)
                                X_train_pred = reg.predict(C_train)
                                X_train = X_train - reg.predict(C_train)
                                X_test = X_test - reg.predict(C_test)

                            # Apply confound regression (nonlinear variation)
                            elif deconf_method == 'nonlinear_confound_regression':
                                if X.shape[1] == 1:
                                    reg = RandomForestRegressor(n_estimators=n_estimators, random_state=fold,
                                                                 max_depth=max_depth,
                                                                 min_samples_split=min_samples_split,
                                                                 min_samples_leaf=min_samples_leaf,
                                                                 max_features=max_features)

                                    reg.fit(C_train, X_train.flatten())
                                    # reg = SVC().fit(C_train, X_train.flatten())
                                    X_train = X_train - reg.predict(C_train).reshape(-1, 1)
                                    X_test = X_test - reg.predict(C_test).reshape(-1, 1)
                                elif X.shape[1] > 1:
                                    #reg = RandomForestRegressor(n_estimators=50, random_state=92)
                                    reg = RandomForestRegressor(n_estimators=n_estimators, random_state=fold,
                                                                 max_depth=max_depth,
                                                                 min_samples_split=min_samples_split,
                                                                 min_samples_leaf=min_samples_leaf,
                                                                 max_features=max_features)
                                    reg.fit(C_train, X_train)
                                    X_train_pred = reg.predict(C_train)
                                    # reg = SVC().fit(C_train, X_train)
                                    X_train = X_train - reg.predict(C_train)
                                    X_test = X_test - reg.predict(C_test)

                            # DECODE
                            if decoder == 'random_forest':
                                if tune_hyperpars_decoder:
                                    raise NotImplementedError
                                model = RandomForestClassifier(n_estimators=n_estimators, random_state=fold,
                                                             max_depth=5,
                                                             min_samples_split=min_samples_split,
                                                             min_samples_leaf=5,
                                                             max_features=max_features)
                            elif decoder == 'logistic_regression':
                                if tune_hyperpars_decoder:
                                    parameters = {'C': [0.00001, 0.0001, 0.001, 0.01]}
                                    lr = LogisticRegression(penalty='l2')
                                    inner_cv = StratifiedKFold(n_splits=3, shuffle=True, random_state=fold)
                                    model = GridSearchCV(lr, parameters, refit=True, scoring=None, #make_scorer(balanced_accuracy_score)
                                                         cv=inner_cv)
                                else:
                                    model = LogisticRegression(penalty='l2', C=0.001, random_state=92)
                            elif decoder == 'linear_discriminant_analysis':
                                if tune_hyperpars_decoder:
                                    raise NotImplementedError
                                model = LinearDiscriminantAnalysis()
                            elif decoder == 'quadratic_discriminant_analysis':
                                if tune_hyperpars_decoder:
                                    raise NotImplementedError
                                model = QuadraticDiscriminantAnalysis()
                            elif decoder == 'ridge_regression':
                                if tune_hyperpars_decoder:
                                    raise NotImplementedError
                                model = RidgeClassifier()
                            elif decoder == 'SVC':
                                if tune_hyperpars_decoder:
                                    #parameters = {'C': [0.0001, 0.001, 0.01, 0.1, 1, 10]}
                                    # parameters = {'C': [0.0001, 0.001, 0.01, 0.1, 1, 10],
                                    #               'gamma': ['scale', 'auto', 0.0001, 0.001, 0.01, 0.1]}
                                    parameters = {'kernel': ['linear', 'rbf'],
                                                  'C': [0.00001, 0.0001, 0.001, 0.01, 0.1, 1, 10],
                                                  'gamma': ['scale', 'auto']}
                                    svc = SVC(probability=True)
                                    inner_cv = StratifiedKFold(n_splits=5, shuffle=True, random_state=fold)
                                    model = GridSearchCV(svc, parameters, refit=True,
                                                         scoring=None, cv=inner_cv)
                                else:
                                    model = SVC(kernel='rbf', C=0.1, random_state=92)
                            else:
                                raise ValueError

                            if deconf_method == 'decode_from_confound':
                                model.fit(C_train, y_train)
                                y_pred = model.predict(C_test)

                            else:
                                model.fit(X_train, y_train)
                                y_pred = model.predict(X_test)

                            if tune_hyperpars_decoder:
                                try:
                                    best_C = model.best_params_['C']
                                    #print('BEST C: {}'.format(best_C))
                                except KeyError:
                                    pass
                            score = balanced_accuracy_score(y_test, y_pred)
                            kfold_scores.append(score)
                            y_test_all.append(y_test)
                            y_pred_all.append(y_pred)
                            c_all.append(C_test[:, 0])

                        target = np.hstack(y_test_all)
                        predictions = np.hstack(y_pred_all)
                        confound = np.hstack(c_all)
                        mean_score = np.mean(kfold_scores)

                        if run_spisak_full_test:

                            if np.unique(confound).shape[0] == 1:
                                confound += np.random.normal(0, 0.01, confound.shape[0])

                            full_test = full_confound_test(target, predictions, confound,
                                                           num_perms=num_perms_spisak,
                                                           cat_y=True,
                                                           cat_yhat=True,
                                                           cat_c=False,
                                                           mcmc_steps=50,
                                                           cond_dist_method='gam',
                                                           return_null_dist=False,
                                                           random_state=92, progress=True,
                                                           n_jobs=1)
                            full_p = full_test.p
                            full_sig = full_p < 0.05

                        else:
                            full_p = np.nan
                            full_sig = False

                        if run_spisak_partial_test:

                            partial_test = partial_confound_test(target, predictions, confound,
                                                                 num_perms=num_perms_spisak,
                                                                 cat_y=True,
                                                                 cat_yhat=True,
                                                                 cat_c=False,
                                                                 mcmc_steps=50,
                                                                 cond_dist_method='gam',
                                                                 return_null_dist=False,
                                                                 random_state=92, progress=True,
                                                                 n_jobs=1)
                            partial_p = partial_test.p
                            partial_sig = partial_p < 0.05

                        else:
                            partial_p = np.nan
                            partial_sig = False

                        rs.loc[rs.shape[0], :] = [session_id, deconf_method, decoder, n_pca_components,
                                                  n_trials_draw, shuffle_components, repeat, mean_score,
                                                  partial_p, partial_sig, full_p, full_sig]


    rs['mean_score'] = pd.to_numeric(rs['mean_score'])

    pars = {'session_ids': session_ids,
            'data_settings_name': data_settings_name,
            'ephys_dataset': ephys_dataset,
            'area_spikes_dataset': area_spikes_dataset,
            'time_bin_indx': time_bin_indx,
            'n_repeats': n_repeats,
            'n_kfold_splits': n_kfold_splits,
            'n_pca_components': n_pca_components,
            'deconf_methods': deconf_methods,
            'decoders' : decoders,
            'n_trials_dict' : n_trials_dict,
            'standardize_features': standardize_features,
            'standardize_confound': standardize_confound,
            'n_estimators' : n_estimators,
            'max_depth' : max_depth,
            'min_samples_split' : min_samples_split,
            'min_samples_leaf' : min_samples_leaf,
            'max_features' : max_features}

    out = {'pars' : pars,
           'df' : rs}

    output_folder = os.path.join(DATA_PATH, 'test_spisak',
                                 '{}'.format(settings_name))

    if not os.path.isdir(output_folder):
        os.makedirs(output_folder)

    output_full_path = os.path.join(output_folder, 'spisak_results_{}_{}.pkl'.format(settings_name, deconf_method))
    print('Saving simulation to {}'.format(output_full_path))
    pickle.dump(out, open(output_full_path, 'wb'))







