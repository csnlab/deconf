import os
import glob
import pickle
from sklearn.model_selection import StratifiedKFold, KFold, LeaveOneOut
from sklearn.metrics import balanced_accuracy_score
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import SGDClassifier
from constants import *
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from utils import *
from plotting_style import *
from scipy.stats import mannwhitneyu, bootstrap


"""
Test the predictability for a given session with a fixed number of PCA components
but increasing the number of trials available.
"""

# TODO if we shuffle, the variance in the end will both be due to the
# shuffling of the confound and to the random subsampling, less meaningful

settings_name = 'feb26_test'

plot_single_sessions = False
plot_sessions_combined = True
plot_kde=True
n_trials_all = 60

plot_methods = ['predictability_lin',
                  'predictability_nonlin',
                  'predictability_nonlin_ensemble']
plot_format = 'png'
ylims = -0.5, 0.75
yticks = [-0.5, -0.25, 0, 0.25, 0.5, 0.75]
ylimsss = [-0.4, 0.4]
yticksss = [-0.4, 0, 0.4]
plots_folder = os.path.join(DATA_PATH, 'plots', 'test_predictability_n_trials', settings_name)
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)

output_folder = os.path.join(DATA_PATH, 'test_predictability_n_trials',
                             'SIMULATION_{}'.format(settings_name))
files = glob.glob('{}/*'.format(output_folder))
dfs = []
pars = {}
for output_full_path in files:
    res = pickle.load(open(output_full_path, 'rb'))
    df = res['df']
    dm = df['deconf_method'].unique()
    assert dm.shape[0] == 1
    pars[dm[0]] = res['pars']
    dfs.append(df)
df = pd.concat(dfs)


for shuffle_components in [False, True]:
    if shuffle_components:
        extra_string = 'shuffledC'
    else:
        extra_string = 'trueC'

    rs = df[df['shuffle_confound'] == shuffle_components]
    deconf_methods = [d for d in rs['deconf_method'].unique() if d in plot_methods]
    session_ids = pars[deconf_methods[0]]['session_ids']
    n_trials_dict = pars[deconf_methods[0]]['n_trials_dict']


    # make your own CIs
    if plot_single_sessions:
        for session_id in session_ids:

            f, ax = plt.subplots(1, 1, figsize=[2.8, 2.2], sharey=True)

            for j, deconf_method in enumerate(deconf_methods):
                x = []
                err = []
                for k, n_trials in enumerate(n_trials_dict[session_id]):

                    vals = rs[(rs['n_trials'] == n_trials) &
                              (rs['deconf_method'] == deconf_method) &
                              (rs['session_id'] == session_id)]['mean_score'].__array__()
                    center = vals.mean()
                    print(vals.mean())

                    bootstrap_vals = (vals.__array__().reshape(-1, 1).astype(float),)
                    bres = bootstrap(bootstrap_vals,
                                     axis=0,
                                     statistic=np.mean,
                                     n_resamples=100,
                                     confidence_level=0.95,
                                     method='basic')
                    error = np.array([bres.confidence_interval.low[0], bres.confidence_interval.high[0]])
                    x.append(center)
                    err.append(error)
                err = np.vstack(err)

                ax.plot(n_trials_dict[session_id], x, c=method_palette[deconf_method], zorder=-5)
                ax.scatter(n_trials_dict[session_id], x, s=30, c=method_palette[deconf_method],
                           edgecolor='w', label=method_labels[deconf_method])
                ax.fill_between(n_trials_dict[session_id], err[:, 0], err[:, 1], alpha=0.3, zorder=-10,
                                color=method_palette[deconf_method], linewidth=0)
            sns.despine()
            ax.set_ylabel('Mean predictability')
            ax.set_xlabel('# trials')
            ax.axhline(0.0, ls=':', c='grey')
            ax.legend().remove()

            if n_trials_dict[session_id].shape[0] > 6 and n_trials_dict[session_id].shape[0] < 12:
                ax.set_xticks(n_trials_dict[session_id][::2])
            elif n_trials_dict[session_id].shape[0] >= 12:
                ax.set_xticks(n_trials_dict[session_id][::6])
            else:
                ax.set_xticks(n_trials_dict[session_id])

            ax.set_ylim(ylimsss)
            ax.set_yticks(yticksss)
            plt.tight_layout()

            plot_name = 'lineplot_{}_{}.{}'.format(session_id, extra_string, plot_format)
            f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                      bbox_inches="tight")


    if plot_sessions_combined:
        # plot all sessions together
        f, ax = plt.subplots(1, 1, figsize=[3, 2.2], sharey=True)

        for j, deconf_method in enumerate(deconf_methods):
            for session_id in session_ids:
                x = []
                for k, n_trials in enumerate(n_trials_dict[session_id]):
                    vals = rs[(rs['n_trials'] == n_trials) &
                              (rs['deconf_method'] == deconf_method) &
                              (rs['session_id'] == session_id)]['mean_score'].__array__()

                    center = vals.mean()
                    x.append(center)

                ax.plot(n_trials_dict[session_id], x, c=method_palette[deconf_method], zorder=-5)
                # ax.scatter(n_trials_dict[session_id], x, s=50, c=method_palette[deconf_method],
                #            edgecolor='w', label=method_labels[deconf_method])

        sns.despine()
        ax.set_ylabel('Mean predictability')
        ax.set_xlabel('# trials')
        ax.axhline(0.0, ls=':', c='grey')
        #ax.axvline(n_trials_all, c='green', zorder=-10)
        ax.legend().remove()

        # restrict to where we have at least 2 sessions
        second_max = np.sort([n_trials_dict[s].max() for s in session_ids])[-2]
        axmin = np.sort([n_trials_dict[s].min() for s in session_ids])[0]
        ax.set_xticks(n_trials_dict[session_id][::3])
        ax.set_xlim([axmin, second_max])
        ax.set_ylim(ylims)
        ax.set_yticks(yticks)
        plt.tight_layout()

        plot_name = 'allsess_lineplot_{}.{}'.format(extra_string, plot_format)
        f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                  bbox_inches="tight")


    if plot_kde:
        f, ax = plt.subplots(1, 1, figsize=[1.5, 2.2], sharey=True)

        dfsel = rs[(rs['n_trials'] == n_trials_all)]

        dfsel = dfsel.groupby(['session_id', 'decoder', 'n_pca_components',
                               'n_trials', 'deconf_method']).mean().reset_index()
        n_sessions = dfsel['session_id'].unique().shape[0]
        print('KDE computed with {} sessions'.format(n_sessions))

        for deconf_method in deconf_methods:
            dplot = dfsel[dfsel['deconf_method'] == deconf_method]
            sns.kdeplot(data=dplot, x='mean_score',
                        vertical=True, ax=ax, color=method_palette[deconf_method])

        # sns.kdeplot(data=dfsel, hue='deconf_method', x='mean_score',
        #             vertical=True, ax=ax, palette=method_palette)

        ax.legend().remove()
        sns.despine()
        ax.set_ylim(ylims)
        ax.set_yticks(yticks)
        ax.set_ylabel('')
        ax.axhline(0.0, ls=':', c='grey')
        plt.tight_layout()
        plot_name = 'kde_{}.{}'.format(extra_string, plot_format)
        f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                  bbox_inches="tight")



f, ax = plt.subplots(1, 1, figsize=[2, 2])
legend_elements = [plt.Line2D([0], [0], marker='o', color=method_palette[m],
                              label=method_labels[m], lw=2,
                          markerfacecolor=method_palette[m],
                              markeredgecolor='w',
                              markersize=10) for m in deconf_methods]
ax.legend(handles=legend_elements, loc='center', frameon=False)
sns.despine(left=True, bottom=True)
ax.axis('off')
plt.show()
plot_name = 'singlesess_overtime_legend.{}'.format(plot_format)
f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
          bbox_inches="tight")


