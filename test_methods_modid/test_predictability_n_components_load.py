import os
import glob
import pickle
from constants import *
import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt
import numpy as np
from utils import *
from plotting_style import *
from scipy.stats import bootstrap

settings_name = 'feb26_test'

plot_single_sessions = False
plot_sessions_combined = True
plot_kde = False
plot_kde_comparison = True
plot_dots = True
shuffle_components_list = [False, True]
plot_methods = ['predictability_lin',
                  'predictability_nonlin',
                  'predictability_nonlin_ensemble']
plot_format = 'png'
ylims = -0.5, 0.75
yticks = [-0.5, -0.25, 0, 0.25, 0.5, 0.75]
ylims2 = -0.4, 0.5
yticks2 = [0, 0.5]
ylimsss = [-0.4, 0.4]
yticksss = [-0.4, 0, 0.4]
plots_folder = os.path.join(DATA_PATH, 'plots', 'test_predictability_n_components', settings_name)
if not os.path.isdir(plots_folder) :
    os.makedirs(plots_folder)


output_folder = os.path.join(DATA_PATH, 'test_predictability_n_components',
                             'SIMULATION_{}'.format(settings_name))
files = glob.glob('{}/*'.format(output_folder))
dfs = []
pars = {}
for output_full_path in files:
    res = pickle.load(open(output_full_path, 'rb'))
    df = res['df']
    dm = df['deconf_method'].unique()
    assert dm.shape[0] == 1
    pars[dm[0]] = res['pars']
    dfs.append(df)
df = pd.concat(dfs)


for shuffle_components in shuffle_components_list:
    if shuffle_components:
        extra_string = 'shuffledC'
    else:
        extra_string = 'trueC'

    rs = df[df['shuffle_confound'] == shuffle_components]
    deconf_methods = [d for d in rs['deconf_method'].unique() if d in plot_methods]
    session_ids = pars[deconf_methods[0]]['session_ids']
    n_pca_components_list = pars[deconf_methods[0]]['n_pca_components_list']


    if plot_single_sessions:
        for session_id in session_ids:

            f, ax = plt.subplots(1, 1, figsize=[2.8, 2.2], sharey=True)

            for j, deconf_method in enumerate(deconf_methods):
                x = []
                err = []
                for k, n_components in enumerate(n_pca_components_list):

                    vals = rs[(rs['n_pca_components'] == n_components) &
                              (rs['deconf_method'] == deconf_method) &
                              (rs['session_id'] == session_id)]['mean_score'].__array__()
                    center = vals.mean()
                    print(vals.mean())

                    bootstrap_vals = (vals.__array__().reshape(-1, 1).astype(float),)
                    bres = bootstrap(bootstrap_vals,
                                     axis=0,
                                     statistic=np.mean,
                                     n_resamples=100,
                                     confidence_level=0.95,
                                     method='basic')
                    error = np.array([bres.confidence_interval.low[0], bres.confidence_interval.high[0]])
                    x.append(center)
                    err.append(error)
                err = np.vstack(err)

                ax.plot(n_pca_components_list, x, c=method_palette[deconf_method], zorder=-10)
                ax.scatter(n_pca_components_list, x, s=30, c=method_palette[deconf_method],
                           edgecolor='w', label=method_labels[deconf_method])
                ax.fill_between(n_pca_components_list, err[:, 0], err[:, 1], alpha=0.3, zorder=-10,
                                color=method_palette[deconf_method], linewidth=0)
            sns.despine()
            ax.set_ylabel('Mean predictability')
            ax.set_xlabel('# confound features')
            ax.axhline(0.0, ls=':', c='grey')
            ax.set_ylim(ylimsss)
            ax.set_yticks(yticksss)
            ax.legend().remove()
            if n_pca_components_list.__len__() > 10:
                ax.set_xticks(np.array(n_pca_components_list)[::2])
            else:
                ax.set_xticks(n_pca_components_list)
            plt.tight_layout()
            plot_name = 'lineplot_{}_{}.{}'.format(session_id, extra_string, plot_format)
            f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                      bbox_inches="tight")


    if plot_sessions_combined:
        # plot all sessions together
        f, ax = plt.subplots(1, 1, figsize=[3, 2.2], sharey=True)

        for j, deconf_method in enumerate(deconf_methods):
            for session_id in session_ids:
                x = []
                for k, n_components in enumerate(n_pca_components_list):
                    vals = rs[(rs['n_pca_components'] == n_components) &
                              (rs['deconf_method'] == deconf_method) &
                              (rs['session_id'] == session_id)]['mean_score'].__array__()

                    center = vals.mean()
                    x.append(center)

                ax.plot(n_pca_components_list, x, c=method_palette[deconf_method], zorder=-5)
                # ax.scatter(n_trials_dict[session_id], x, s=50, c=method_palette[deconf_method],
                #            edgecolor='w', label=method_labels[deconf_method])

        sns.despine()
        ax.set_ylabel('Mean predictability')
        ax.set_xlabel('# confound features')
        ax.axhline(0.0, ls=':', c='grey')
        ax.legend().remove()

        ax.set_xticks(np.array(n_pca_components_list))

        ax.set_ylim(ylims)
        ax.set_yticks(yticks)
        ax.set_xlim([np.min(n_pca_components_list), np.max(n_pca_components_list)])
        plt.tight_layout()

        plot_name = 'allsess_lineplot_{}.{}'.format(extra_string, plot_format)
        f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                  bbox_inches="tight")


    if plot_kde:
        n_components = 30
        f, ax = plt.subplots(1, 1, figsize=[1.5, 2.2], sharey=True)

        dfsel = rs[(rs['n_pca_components'] == n_components)]
        dfsel = dfsel.groupby(['session_id', 'shuffle_confound', 'decoder',
                               'n_pca_components', 'deconf_method']).mean().reset_index()

        n_sessions = dfsel['session_id'].unique().shape[0]
        print('KDE computed with {} sessions'.format(n_sessions))

        for deconf_method in deconf_methods:
            dplot = dfsel[dfsel['deconf_method'] == deconf_method]
            sns.kdeplot(data=dplot, x='mean_score',
                        vertical=True, ax=ax, color=method_palette[deconf_method])
        # sns.kdeplot(data=dfsel, hue='deconf_method', x='mean_score',
        #             vertical=True, ax=ax, palette=method_palette)

        ax.legend().remove()
        sns.despine()
        ax.set_ylim(ylims)
        ax.set_yticks(yticks)
        ax.set_ylabel('')
        ax.axhline(0.0, ls=':', c='grey')
        plt.tight_layout()
        plot_name = 'kde_{}.{}'.format(extra_string, plot_format)
        f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                  bbox_inches="tight")



rx = df.copy()

if plot_kde_comparison:
    n_components = 30
    for deconf_method in deconf_methods:
        f, ax = plt.subplots(1, 1, figsize=[1, 2.1], sharey=True)

        dfsel = rx[(rx['n_pca_components'] == n_components) &
                   (rx['deconf_method'] == deconf_method)]
        dfsel = dfsel.groupby(['session_id', 'shuffle_confound','decoder',
                               'n_pca_components', 'deconf_method']).mean().reset_index()
        n_sessions = dfsel['session_id'].unique().shape[0]
        print('KDE comparison computed with {} sessions'.format(n_sessions))

        sns.kdeplot(data=dfsel[dfsel['shuffle_confound'] == True],
                    y='mean_score', ax=ax, color=method_palette[deconf_method],
                    linestyle='--')
        sns.kdeplot(data=dfsel[dfsel['shuffle_confound'] == False],
                    y='mean_score', ax=ax, color=method_palette[deconf_method],
                    linestyle='-')

        ax.legend().remove()
        sns.despine()
        ax.set_ylim(ylims2)
        ax.set_yticks(yticks2)
        ax.set_xticklabels('')
        #ax.set_yticklabels('')
        #ax.set_title(method_labels[deconf_method].replace('\n', ' '))
        ax.set_ylabel('')
        ax.axhline(0.0, ls=':', c='grey')
        plt.tight_layout()
        plot_name = 'kde_{}.{}'.format(deconf_method, plot_format)
        f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                  bbox_inches="tight")




if plot_dots:
    n_components = 30
    for deconf_method in deconf_methods:
        f, ax = plt.subplots(1, 1, figsize=[2.1, 2.1], sharey=True)

        dfsel = rx[(rx['n_pca_components'] == n_components) &
                   (rx['deconf_method'] == deconf_method)]
        dfsel = dfsel.groupby(['session_id', 'shuffle_confound','decoder',
                               'n_pca_components', 'deconf_method']).mean().reset_index()
        n_sessions = dfsel['session_id'].unique().shape[0]
        print('KDE comparison computed with {} sessions'.format(n_sessions))

        # sns.swarmplot(data=dfsel[dfsel['shuffle_confound'] == True],
        #             y='mean_score', ax=ax, color=method_palette[deconf_method],
        #             linestyle='--', x=0)
        # sns.swarmplot(data=dfsel[dfsel['shuffle_confound'] == False],
        #             y='mean_score', ax=ax, color=method_palette[deconf_method],
        #             linestyle='-', x=1)

        y1 = data = dfsel[dfsel['shuffle_confound'] == True]['mean_score'].__array__()
        y0 = data = dfsel[dfsel['shuffle_confound'] == False]['mean_score'].__array__()
        x0 = [0 for i in range(y0.shape[0])]
        x1 = [1 for i in range(y1.shape[0])]

        from scipy.stats import mannwhitneyu
        stat, p = mannwhitneyu(y0, y1)
        print('Method {}, p-value = {}'.format(deconf_method, p))

        ax.scatter(x0, y0, s=30, c=method_palette[deconf_method],
                    edgecolor='w')
        ax.scatter(x1, y1, s=30, c='w',
                    edgecolor=method_palette[deconf_method])
        for i in range(y0.shape[0]):
            ax.plot([0, 1], [y0[i], y1[i]], ls='-', linewidth=1,
                    color=method_palette[deconf_method], zorder=-20)

        ax.legend().remove()
        sns.despine()
        ax.set_xlim([-0.1, 1.1])
        ax.set_xticks([0, 1])
        ax.set_ylim(ylims2)
        ax.set_yticks(yticks2)
        ax.set_xticklabels(['True C', 'Shuffled C'])
        #ax.set_title(method_labels[deconf_method].replace('\n', ' '))
        ax.set_ylabel('Mean predictability')
        ax.axhline(0.0, ls=':', c='grey', zorder=-20)
        ax.text(x=0.25, y=-0.3, text='p={:.5f}'.format(p), s=8)
        plt.tight_layout()
        plot_name = 'dotsss_{}.{}'.format(deconf_method, plot_format)
        f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
                  bbox_inches="tight")



# f, ax = plt.subplots(1, 2)
# sns.kdeplot(d1[d1['deconf_method'] == 'predictability_nonlin_ensemble'], ax=ax[0])
# sns.kdeplot(d2[d2['shuffle_confound'] == True], ax=ax[1])


f, ax = plt.subplots(1, 1, figsize=[2, 2])
legend_elements = [plt.Line2D([0], [0], marker='o', color=method_palette[m],
                              label=method_labels[m], lw=2,
                          markerfacecolor=method_palette[m],
                              markeredgecolor='w',
                              markersize=10) for m in deconf_methods]
ax.legend(handles=legend_elements, loc='center', frameon=False)
sns.despine(left=True, bottom=True)
ax.axis('off')
plt.show()
plot_name = 'singlesess_overtime_legend.{}'.format(plot_format)
f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
          bbox_inches="tight")


f, ax = plt.subplots(1, 1, figsize=[2, 2])
legend_elements = [plt.Line2D([0], [0], color=method_palette[m],
                              label=method_labels[m], lw=2,
                              markerfacecolor=method_palette[m],
                              markeredgecolor='w',
                              markersize=0) for m in deconf_methods]
ax.legend(handles=legend_elements, loc='center', frameon=False)
sns.despine(left=True, bottom=True)
ax.axis('off')
plt.show()
plot_name = 'singlesess_nomarker_overtime_legend.{}'.format(plot_format)
f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
          bbox_inches="tight")



f, ax = plt.subplots(1, 1, figsize=[2, 2])
legend_elements = [plt.Line2D([0], [0], color='grey',  label='True confound', lw=2, ls='-', markersize=0),
                   plt.Line2D([0], [0], color='grey',  label='Shuffled confound', lw=2, ls='--', markersize=0)]
ax.legend(handles=legend_elements, loc='center', frameon=False)
sns.despine(left=True, bottom=True)
ax.axis('off')
plt.show()
plot_name = 'singlesess_shuffled_confound_legend.{}'.format(plot_format)
f.savefig(os.path.join(plots_folder, plot_name), dpi=250,
          bbox_inches="tight")



