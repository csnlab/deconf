import seaborn as sns


method_labels = {'raw' : 'Raw',
                 'decode_from_confound' : 'Decode from\nconfound',
                 'distribution_match' : 'Post-hoc\nCB',
                 'confound_isolating_cv' : 'Conf. isolating\nCV',
                 'linear_confound_regression' : 'Linear CR',
                 'linear_confound_regression_reg': 'Linear CR',
                 'nonlinear_confound_regression' : 'Nonlin. CR',
                 'predictability_lin' : 'Predictability\n(Linear)',
                 'predictability_nonlin' : 'Predictability\n(Nonlin.)',
                 'predictability_nonlin_ensemble' : 'Predictability\n(Nonlinear)',
                 'predictability_lin_nodeconf' : 'Bo',
                 'predictability_nonlin_nodeconf' : 'Bo'}

decoder_labels = {'logistic_regression' : 'Logistic reg. decoder',
                  'SVC' : 'SVM decoder',
                  'random_forest' : 'RF decoder'}

predictability_label = 'Predictability (bits)'

predictability_methods = ['predictability_lin',
                          'predictability_nonlin',
                          'predictability_nonlin_ensemble',
                          'predictability_lin_nodeconf',
                          'predictability_nonlin_nodeconf',
                          'predictability_nonlin_ensemble_nodeconf']

method_palette = {'raw' : sns.xkcd_rgb['electric blue'],
                  'decode_from_confound' : sns.xkcd_rgb['steel grey'],
                 'distribution_match' : sns.xkcd_rgb['light red'],
                 'linear_confound_regression' : sns.xkcd_rgb['light green'],
                  'linear_confound_regression_reg': sns.xkcd_rgb['light green'],
                  'nonlinear_confound_regression' : sns.xkcd_rgb['green'],
                 'predictability_lin' : sns.xkcd_rgb['light purple'],
                 'predictability_nonlin' : sns.xkcd_rgb['purple'],
                 'predictability_nonlin_ensemble' : sns.xkcd_rgb['dark purple']}


method_spisak_palette = {'raw' : sns.xkcd_rgb['ocean blue'],
                  'decode_from_confound' : sns.xkcd_rgb['steel grey'],
                 'linear_confound_regression' : sns.xkcd_rgb['teal'],
                  'linear_confound_regression_reg': sns.xkcd_rgb['light green'],
                  'nonlinear_confound_regression' : sns.xkcd_rgb['green']}


dataset_palette = {'ChangeDetectionConflict'  : sns.xkcd_rgb['darkish purple'],
                   'ChangeDetectionConflictDecor' : sns.xkcd_rgb['emerald'],
                   'VisOnlyTwolevels' : sns.xkcd_rgb['bright orange']}


parameter_labels = {'coherence_p' : 'Coherence $p$',
                    'signal_strength' : 'Signal strength $w_{Xy}$',
                    'n_samples' : '# of samples'}

metric_labels = {'accuracy' : 'Decoding accuracy',
                 'predictability' : 'Predictability'}

areas_palette = {'V2': sns.color_palette('bright')[2],
                 'BR': sns.color_palette('bright')[1],
                 'HPC': sns.color_palette('bright')[9],
                 'Hpc': sns.color_palette('bright')[9],
                 'PRH': sns.color_palette('bright')[6]}