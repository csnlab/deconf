import os
import numpy as np
import matplotlib.pyplot as plt
import seaborn as sns
import quantities as pq
from elephant.spike_train_generation import StationaryPoissonProcess
from elephant.spike_train_generation import NonStationaryPoissonProcess
from elephant.spike_train_generation import NonStationaryGammaProcess
from elephant.statistics import BinnedSpikeTrain
from elephant.statistics import mean_firing_rate
import neo
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression
from sklearn.ensemble import RandomForestRegressor
from scipy.stats import norm
from sklearn.metrics import r2_score, make_scorer
from constants import *
import math

def make_place_fields(n_trials=None, n_features=None):

    if n_features is None:
        n_features = 14 # each feature is confounded by only one confound
    if n_trials is None:
        n_trials = 6

    n_confounds = 1
    n_samples_trial = 500

    min_rate = 0
    max_rate = 50
    binsize = 100 * pq.ms
    t_start = 0 * pq.s
    t_stop = (n_samples_trial * n_trials * binsize).rescale(pq.s)
    n_samples_total = n_samples_trial * n_trials

    average_rate = 5

    # generating the confound
    C = np.zeros(shape=[n_samples_trial, n_confounds])
    C[:, 0] = np.arange(0, n_samples_trial, 1)

    confound_rates = np.zeros(shape=[n_samples_trial, n_features])
    for i in range(n_features):
        bump = norm.pdf(np.arange(-10, 10, 0.05))[150:250]
        cr = np.zeros(n_samples_trial)
        cr[i*30:i*30+100] = bump
        confound_rates[:, i] = cr

    # f, ax = plt.subplots(1, 1)
    # for i in range(n_features):
    #     ax.plot(confound_rates[:, i])
    C = np.vstack(n_trials*[C])
    confound_rates = np.vstack(n_trials*[confound_rates])


    # generating the neural data
    X = np.zeros([n_samples_trial * n_trials, n_features])

    for j in range(n_features):

        rate = np.repeat(average_rate, n_samples_trial * n_trials) + 80 * confound_rates[:, j]
        sampling_period = binsize
        rate_signal = neo.AnalogSignal(t_start=t_start, t_stop=t_stop,
                                       signal=rate*pq.Hz, sampling_period=sampling_period)
        spiketrain = NonStationaryPoissonProcess(rate_signal=rate_signal).generate_spiketrain()

        # spiketrain = NonStationaryGammaProcess(rate_signal=rate_signal,
        #                                        shape_factor=2).generate_spiketrain()

        bst = BinnedSpikeTrain(spiketrain, bin_size=binsize)
        X[:, j] = bst.to_array()

    return X, C


def make_place_fields_with_stimulus():

    n_features = 14
    n_trials = 10

    n_confounds = 1
    n_samples_trial = 500

    min_rate = 0
    max_rate = 50
    binsize = 100 * pq.ms
    t_start = 0 * pq.s
    t_stop = (n_samples_trial * n_trials * binsize).rescale(pq.s)
    n_samples_total = n_samples_trial * n_trials

    average_rate = 5

    # generating the confound
    C = np.zeros(shape=[n_samples_trial, n_confounds])
    C[:, 0] = np.arange(0, n_samples_trial, 1)

    confound_rates = np.zeros(shape=[n_samples_trial, n_features])
    for i in range(n_features) :
        bump = norm.pdf(np.arange(-10, 10, 0.05))[150 :250]
        cr = np.zeros(n_samples_trial)
        cr[i * 30 :i * 30 + 100] = bump
        confound_rates[:, i] = cr

    # f, ax = plt.subplots(1, 1)
    # for i in range(n_features):
    #     ax.plot(confound_rates[:, i])
    C = np.vstack(n_trials * [C])

    # make the target
    T = np.hstack(
        [np.repeat(i, n_samples_trial) for i in [0, 1] * int(n_trials / 2)])

    confound_rates_all = []
    for trial in range(n_trials) :
        if math.fmod(trial, 2) == 0 :
            confound_rates_all.append(confound_rates)
        else :
            confound_rates_all.append(confound_rates * 3)

    confound_rates = np.vstack(confound_rates_all)

    # generating the neural data
    X = np.zeros([n_samples_trial * n_trials, n_features])

    for j in range(n_features) :
        rate = np.repeat(average_rate,
                         n_samples_trial * n_trials) + 80 * confound_rates[:, j]
        sampling_period = binsize
        rate_signal = neo.AnalogSignal(t_start=t_start, t_stop=t_stop,
                                       signal=rate * pq.Hz,
                                       sampling_period=sampling_period)
        spiketrain = NonStationaryPoissonProcess(
            rate_signal=rate_signal).generate_spiketrain()

        # spiketrain = NonStationaryGammaProcess(rate_signal=rate_signal,
        #                                        shape_factor=2).generate_spiketrain()

        bst = BinnedSpikeTrain(spiketrain, bin_size=binsize)
        X[:, j] = bst.to_array()

    return X, C, T